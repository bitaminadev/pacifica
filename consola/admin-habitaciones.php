<?php 
	@session_start(); 
	include_once("php/environment.php");
	if(!isset($_SESSION["usuario_consola"])){
		header("Location: login.php");
	}else{
		if(!validateModule(ID_MODULO_HABITACIONES))
			header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Consola administrativa | Habitaciones</title>
		<!--Metas-->
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--Archivos js y css-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/skin.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap-select.css" rel="stylesheet" type="text/css">
		<link href="css/productos.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src='js/habitaciones.js'></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.datatables.min.js"></script>
		<script src="js/datatables.min.js"></script>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script type="text/javascript" src="js/ajaxupload.3.5.js"></script>
		<script src="js/bootstrap-select.js"></script>
		<script src="js/skin.js"></script>
	</head>
	<body>
		<div id="wrapper">
		<?php require("commons/header.php"); ?>
		<div class="overlay" onclick='cerrar_menu();'></div>
		<?php require("commons/menu.php"); ?>
		<?php 
			include("../php/controllers/connect_sql.php");  
			include("../php/class/destinos.php");
			$obj_destinos = new Destinos(); 
			$consulta =  $obj_destinos->lista_destinos($bd,"");
			$destinos = "";
			while($var = mysqli_fetch_array($consulta)){
				$destinos.= '<option value="'.$var["id_destino"].'">'.utf8_encode($var["nombre_destino"]).'</option>';
			}
		?>
		<!-- Page Content -->
			<div id="page-content-wrapper">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
					<span class="hamb-top"></span>
					<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
				</button>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<fieldset>
								<legend>Administración de hoteles - Pacífica</legend>
								<div class="col-lg-7 auto">
									<p class="col-lg-4 boton cat" onclick='cargar_categorias();'>
										<i class="fa fa-bed" aria-hidden="true"></i>Categorías
									</p>
									<p class="col-lg-4 boton subcat" onclick='cargar_subcategorias();'>
										<i class="fa fa-bed" aria-hidden="true"></i>Tipo de habitación
									</p>
									<p class="col-lg-4 boton prod active" onclick="cargar_productos();">
										<i class="fa fa-bed" aria-hidden="true"></i>Habitaciones
									</p>
								</div>
								<div class="tabla1 tabla">
									<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_crear"]){ ?>
										<button type="button" class="btn btn-info btn-lg productos_modal" data-toggle="modal" data-target="#myModal" onclick="nuevo_producto()"><i class="fa fa-plus" aria-hidden="true"></i>Habitación</button>
									<?php } ?>
									<div class='tables'>
										<table class='table'>										
											<thead>
												<tr> <th>Destino</th> <th>Categoría</th> <th>Tipo</th> <th>Habitación</th><th>Foto</th> <th>Mensaje</th> <th>Slogan</th> <th>Precio general</th> <th>Precio miembros</th><th>Estatus</th> 
													<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_eliminar"]){ ?>
														<th>Eliminar</th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
								<div class="tabla2 tabla">
									<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_crear"]){ ?>
										<button type="button" class="btn btn-info btn-lg editar_catego" data-toggle="modal" data-target="#myModal3" onclick="nueva_categoria()"><i class="fa fa-plus" aria-hidden="true"></i>Categoría</button>
									<?php } ?>
									<div class='tables'>
										<table class='table'>
											<thead> 
												<tr><th>Destino</th><th>Categoría</th><th>Descripción</th><th>Imagen</th><th>Estatus</th>
													<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_eliminar"]){ ?>
														<th>Eliminar</th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
								<div class="tabla3 tabla">					 
									<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_crear"]){ ?>
										<button type="button" class="btn btn-info btn-lg editar_catego" data-toggle="modal" data-target="#myModal2" onclick="nuevo_tipo_habitacion()"><i class="fa fa-plus" aria-hidden="true"></i>Tipo habitación</button>									
									<?php } ?>
									<div class='tables'>
										<table class='table'>
											<thead>
												<tr><th>Destino</th><th>Categoría</th><th>Tipo</th><th>Mensaje</th><th>Slogan</th><th>Estatus</th>
													<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_eliminar"]){ ?>
														<th>Eliminar</th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
			<!-- /#page-content-wrapper -->
		</div>
		<!-- /#wrapper -->
		<div id="myModal" class="modal fade bs-example-modal-lg" role="dialog">
		  <div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-pencil" aria-hidden="true"></i>Editar habitación</h4>
			  </div>
			  <div class="modal-body">
			  		 <!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item general active">
						<a class="nav-link general active" data-toggle="tab" href="#general_habitacion" role="tab">GENERALES</a>
					</li>
					<li class="nav-item seo">
					<!-- <li class="nav-item seo disabled"> -->
						<a class="nav-link" href="#seo_habitacion" data-toggle="tab" role="tab">SEO</a>
					</li>
					<li class="nav-item gal">
					<!-- <li class="nav-item gal disabled"> -->
						<a class="nav-link" href="#galeria_habitacion" data-toggle="tab" role="tab">GALERIA</a>
					</li>
				</ul>
			  	<form id="subcategorias" onsubmit="return ingresar_habitacion()" enctype="multipart/form-data"> 
				  	<div class="tab-content">
						<div class="tab-pane active" style="padding: 20px 0 0px 0;" id="general_habitacion" role="tabpanel">
							<input type="hidden" id="cve_habitacion">
							<div class="row">
								<div class="form-group col-lg-6">
									<label>Status</label>
										<select class="form-control" id="status_habitacion" required="">
											<option value="1">Activo</option>
											<option value="0">Inactivo</option>
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Destino</label>
										<select class="form-control" id="destino_habitacion" onchange="cmb_categorias_habitacion();" required="">
											<?php echo $destinos; ?>
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Categoría</label>
									<select class="form-control" id="categoria_habitacion" onchange="cmb_tipo_habitacion();" required="">
						
									</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Tipo</label>
									<select class="form-control" id="tipo_habitacion" required="">
						
									</select>
								</div>
								<div class="col-lg-12">
									<div class="row">
										<div class="form-group col-lg-6">
											<label>Habitación</label>
											<input type="text" class="form-control" id="nombre_habitacion" value="">
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
									<div class="form-group">
										<label>Slogan</label>
										<input type="text" class="form-control" id="slogan_habitacion" value="">
									</div>
									<div class="form-group">
										<label>Mensaje general</label>
										<textarea class="form-control" id="mensaje_habitacion"></textarea>
									</div>
									<div class="form-group">
										<label>Descripción</label>
										<textarea class="form-control" id="descripcion_habitacion"></textarea>
									</div>
									<div class="form-group">
										<label>Detalles</label>
										<textarea class="form-control" id="detalle_habitacion"></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
									<div class="form-group">
										<label>Slogan</label>
										<input type="text" class="form-control" id="slogan_habitacion_en" value="">
									</div>
									<div class="form-group">
										<label>Mensaje general</label>
										<textarea class="form-control" id="mensaje_habitacion_en"></textarea>
									</div>
									<div class="form-group">
										<label>Descripción</label>
										<textarea class="form-control" id="descripcion_habitacion_en"></textarea>
									</div>
									<div class="form-group">
										<label>Detalles</label>
										<textarea class="form-control" id="detalle_habitacion_en"></textarea>
									</div>
								</div>

								<div class="form-group col-lg-6">
									<label>Precio general</label>
									<input type="text" class="form-control" id="precio_general" onkeypress="return EsNumero(event, this.id);" onblur="this.value = FormatoDecimales(this.value);" placeholder="00.00" value="">
								</div>
								<div class="form-group col-lg-6">
									<label>Precio miembros</label>
									<input type="text" class="form-control" id="precio_miembros" onkeypress="return EsNumero(event, this.id);" onblur="this.value = FormatoDecimales(this.value);" placeholder="00.00" value="">
								</div>
								<div class="form-group col-lg-12">
									<label>Subir plano habitación</label>
									<input type="text" name="plano" id="plano" class="form-control" style="width:70%;display:inline;" placeholder="Nombre del archivo" value="" disabled="">
									<button style="width:10%;height:35px;" id="btn-up-2" class="btn_anexo" onclick="subir_archivo_plano();"><i class="fa fa-upload" aria-hidden="true"></i></button>	
								</div>
							</div>
						</div>

						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="seo_habitacion"  role="tabpanel">
							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="form-group col-lg-6">
											<label>Url</label>
											<input type="text" class="form-control" id="url_habitacion" value="">
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
									<div class="form-group">
										<label>Meta título</label>
										<input type="text" class="form-control" id="meta_titulo_habitacion" value="">
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_habitacion"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_habitacion"></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
									<div class="form-group">
										<label>Meta título</label>
										<input type="text" class="form-control" id="meta_titulo_habitacion_en" value="">
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_habitacion_en"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_habitacion_en"></textarea>
									</div>
								</div>

							</div>
						</div>
						
						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="galeria_habitacion" role="tabpanel">
							<div class="row">
								<div class="col-lg-6">
									<div class="dv_conten_img">
										<input id="btn-up-4" style="width:100%;position:relative;" class="btn_charge_imgusr btn_charge_rel" type="button" value="Cargar Imágen" onclick="subir_imagen_galeria();">
										<p id="status-up-4"></p>
										<p class="tam_recusr" style="float: left;width: 100%;text-align: right;">Tamaño recomendado: 632 x 386px.</p>
										<input id="nombre_imagen_galeria" class="tituloimagen" type="text" name="nombre_imagen_galeria">
									</div>
								</div>
								<div class="col-lg-12" id="lista-imagenes">
									<div class="element-img" id="div_1">
								</div>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_actualizar"]){ ?>
							<input type="submit" name="enviar" class="enviar_habitacion" id="enviar" value="Agregar">
						<?php } ?>
						<p class="cerrar btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</p>
					</div>
				</form>
			  </div>
			</div>
			</div>
		  </div>
		</div>

		<div id="myModal3" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg" style="width: 96%;">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar Categoría</h4>
			  </div>
			  <div class="modal-body">
			  	 <!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item general active">
						<a class="nav-link general active" data-toggle="tab" href="#general_categoria" role="tab">GENERALES</a>
					</li>
					<li class="nav-item seo">
					<!-- <li class="nav-item seo disabled"> -->
						<a class="nav-link" href="#seo_categoria" data-toggle="tab" role="tab">SEO</a>
					</li>
					<li class="nav-item gal">
					<!-- <li class="nav-item gal disabled"> -->
						<a class="nav-link" href="#galeria_categoria" data-toggle="tab" role="tab">GALERIA</a>
					</li>
				</ul>
			  	<form id="categorias" onsubmit="return ingresar_categoria();" > 
				  	<div class="tab-content">
						<div class="tab-pane active" style="padding: 20px 0 0px 0;" id="general_categoria" role="tabpanel">
							<input type="hidden" id="cve_categoria">
							<div class="row">
								<div class="form-group col-lg-6">
									<label>Status</label>
										<select class="form-control" id="status_categoria" required="">
											<option value="1">Activo</option>
											<option value="0">Inactivo</option>
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Destino</label>
										<select class="form-control" id="destino_categoria" required="">
											<?php echo $destinos; ?>
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Nombre Categoría</label>
									<input type="text" class="form-control" id="nombre_categoria" required value="">
								</div>
								<!-- <div class="form-group col-lg-12">
									<label>Slogan destino</label>
									<textarea class="form-control" id="slogan_destino"></textarea>
								</div> -->
								<div class="col-lg-12">
									<div class="row">
										<div class="col-sm-6 col-md-6">
											<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
											<div class="form-group">
												<label>Descripción</label>
												<textarea class="form-control" id="descripcion_categoria"></textarea>
											</div>
										</div>
										<div class="col-sm-6 col-md-6">
											<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
											<div class="form-group">
												<label>Descripción</label>
												<textarea class="form-control" id="descripcion_categoria_en"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-6">
									<div class="dv_conten_img">
										<div class="dv_containt_img">
											<strong id="content_image_categoria"></strong>
											<input id="btn-up" class="btn_charge_imgusr" type="button" value="Cargar imagen principal" onclick="subir_imagen_categoria();">
											<p id="status-up"></p>
											<input id="nombre_imagen_categoria" class="tituloimagen" type="text" name="nombre_imagen_categoria" value="" >
										</div>
										<p class="tam_recusr">Tamaño recomendado: 1665px x 798.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="seo_categoria"  role="tabpanel">
							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="form-group col-lg-6">
											<label>Url</label>
											<input type="text" class="form-control" id="url_categoria" required value="">
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
									<div class="form-group">
										<label>Meta título</label>
										<input type="text" class="form-control" id="meta_titulo_categoria" value="">
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_categoria"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_categoria"></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
									<div class="form-group">
										<label>Meta título</label>
										<input type="text" class="form-control" id="meta_titulo_categoria_en" value="">
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_categoria_en"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_categoria_en"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="galeria_categoria" role="tabpanel">
							<div class="row">
								<div class="col-lg-6">
									<div class="dv_conten_img">
										<input id="btn-up-5" style="width:100%;position:relative;" class="btn_charge_imgusr btn_charge_rel" type="button" value="Cargar Imágen" onclick="subir_imagen_galeria_categoria();">
										<p id="status-up-5"></p>
										<p class="tam_recusr" style="float: left;width: 100%;text-align: right;">Tamaño recomendado: 632 x 386px.</p>
										<input id="nombre_imagen_galeria_categoria" class="tituloimagen" type="text" name="nombre_imagen_galeria_categoria">
									</div>
								</div>
								<div class="col-lg-12" id="lista-imagenes-categoria">
									<div class="element-img-cat" id="dcat_1">
								</div>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_actualizar"]){ ?>
							<input type="submit" name="enviar" class="enviar_categoria" id="enviar" value="Agregar">
						<?php } ?>
						<p class="cerrar btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</p>
					</div>
				</form>
			  </div>
			</div>
		  </div>
		</div>
		</div>

		<div id="myModal2" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg" style="width: 96%;">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar Tipo habitación</h4>
			  </div>
			  <div class="modal-body">
			  	 <!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item general active">
						<a class="nav-link general active" data-toggle="tab" href="#general_subcategoria" role="tab">GENERALES</a>
					</li>
					<li class="nav-item seo">
					<!-- <li class="nav-item seo disabled"> -->
						<a class="nav-link" href="#seo_subcategoria" data-toggle="tab" role="tab">SEO</a>
					</li>
				</ul>
			  	<form id="subcategorias" onsubmit="return ingresar_tipo_habitacion()" enctype="multipart/form-data"> 
				  	<div class="tab-content">
						<div class="tab-pane active" style="padding: 20px 0 0px 0;" id="general_subcategoria" role="tabpanel">
							<input type="hidden" id="cve_tipo_habitacion">
							<div class="row">
								<div class="form-group col-lg-6">
									<label>Status</label>
										<select class="form-control" id="status_tipo_habitacion" required="">
											<option value="1">Activo</option>
											<option value="0">Inactivo</option>
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Destino</label>
										<select class="form-control" id="destino_tipo_habitacion" onchange="cmb_categorias_tipo();" required="">
											<?php echo $destinos; ?>
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Categoría</label>
										<select class="form-control" id="categoria_tipo_habitacion" required="">
							
										</select>
								</div>
								<div class="form-group col-lg-6">
									<label>Tipo habitación</label>
									<input type="text" class="form-control" id="nombre_tipo_habitacion" required value="">
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
									<div class="form-group">
										<label>Mensaje general</label>
										<textarea class="form-control" id="mensaje_tipo_habitacion" required></textarea>
									</div>
									<div class="form-group">
										<label>Slogan tipo</label>
										<textarea class="form-control" id="slogan_tipo_habitacion" required></textarea>
									</div>
									<div class="form-group">
										<label>Descripción</label>
										<textarea class="form-control" id="descripcion_tipo_habitacion"></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
									<div class="form-group">
										<label>Mensaje general</label>
										<textarea class="form-control" id="mensaje_tipo_habitacion_en" required></textarea>
									</div>
									<div class="form-group">
										<label>Slogan tipo</label>
										<textarea class="form-control" id="slogan_tipo_habitacion_en" required></textarea>
									</div>
									<div class="form-group">
										<label>Descripción</label>
										<textarea class="form-control" id="descripcion_tipo_habitacion_en"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="seo_subcategoria"  role="tabpanel">
							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="form-group col-lg-6">
											<label>Url</label>
											<input type="text" class="form-control" id="url_tipo" required value="">
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
									<div class="form-group">
										<label>Meta título</label>
										<input type="text" class="form-control" id="meta_titulo_tipo" value="">
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_tipo"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_tipo"></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
									<div class="form-group">
										<label>Meta título</label>
										<input type="text" class="form-control" id="meta_titulo_tipo_en" value="">
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_tipo_en"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_tipo_en"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_actualizar"]){ ?>
							<input type="submit" name="enviar" class="enviar_tipo" id="enviar" value="Agregar">
						<?php } ?>
						<p class="cerrar btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</p>
					</div>
				</form>
			  </div>
			</div>
		  </div>
		</div>

	</body>
</html>