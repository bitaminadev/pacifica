<?php @session_start(); 
    if(isset($_GET["ev"])){
        switch($_GET["ev"]){
            case 1: guardar_seo_pagina();                   break;
            case 2: guardar_secciones_texto();              break;
            case 3: guardar_elemento_galeria();             break;
            case 4: eliminar_elemento_galeria();            break;
            case 5: guardar_seccion_imagen();               break;
            case 6: guardar_elemento_galeria_youtube();     break;
        }
    }
    function guardar_seo_pagina(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/paginas.php");
        $m_paginas = new Paginas();
        $id_pagina = $_POST["id_pagina"];
        $meta_titulo_es = utf8_decode($_POST["meta_titulo_es"]);
        $meta_titulo_en = utf8_decode($_POST["meta_titulo_en"]);
        $meta_descripcion_es = utf8_decode($_POST["meta_descripcion_es"]);
        $meta_descripcion_en = utf8_decode($_POST["meta_descripcion_en"]);
        $meta_keywords_es = utf8_decode($_POST["meta_keywords_es"]);
        $meta_keywords_en = utf8_decode($_POST["meta_keywords_en"]);

        $bandera = $m_paginas->update_pagina($bd, $id_pagina, $meta_titulo_es, $meta_titulo_en, $meta_descripcion_es, $meta_descripcion_en, $meta_keywords_es, $meta_keywords_en);
        echo $bandera;

        include("../../php/controllers/cerrar_php.php");
    }
    function guardar_secciones_texto(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/secciones.php");
        $m_secciones = new Secciones();
        $bandera = 1;
        foreach($_POST["secciones"] as $seccion){
            $seccion = array_map("utf8_decode", $seccion);
            $bandera = $m_secciones->update_seccion($bd, $seccion["id_seccion"], $seccion["contenido_es"], $seccion["contenido_en"]);
        }
        echo $bandera;

        include("../../php/controllers/cerrar_php.php");
    }
    function guardar_elemento_galeria(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/secciones.php");
        include("../../php/class/elementos-galeria.php");
        $m_secciones = new Secciones();
        $m_elementos = new ElementosGaleria();
        $bandera = -1;

        $id_seccion = $_POST["id_seccion"];
        $id_elemento = $_POST["id_elemento"];
        $tipo_elemento = utf8_decode($_POST["tipo_elemento"]);
        $titulo_elemento_es = utf8_decode($_POST["titulo_elemento_es"]);
        $titulo_elemento_en = utf8_decode($_POST["titulo_elemento_en"]);
        $contenido = "";
        $contenido_cubierta_video = "";
        $old_contenido = "";
        $old_contenido_cubierta_video = "";
        $old_tipo_elemento = "";

        $nombre_subcarpeta = "";
        if($id_elemento != 0){
            $result = $m_elementos->get_elemento($bd, $id_elemento);
            $elemento = mysqli_fetch_assoc($result);
            $nombre_subcarpeta = $elemento["nombre_subcarpeta"];
            $old_contenido = $elemento["contenido"];
            $old_contenido_cubierta_video = $elemento["contenido_cubierta_video"];
            $old_tipo_elemento = $elemento["tipo_elemento"];
        }else{
            $result = $m_secciones->get_seccion($bd, $id_seccion);
            $seccion = mysqli_fetch_assoc($result);
            $nombre_subcarpeta = $seccion["nombre_subcarpeta"];
        }

        if(isset($_FILES["contenido"])){
            $contenidoFile = $_FILES["contenido"];
        }else{
            $contenidoFile = ["error" => 4];
        }
        if($contenidoFile["error"] == 0){
            // subir archivo
            $fileName = $contenidoFile['name'];
            $temp = $contenidoFile['tmp_name'];
            $strBase = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
            $randomPrefix = substr(str_shuffle($strBase), 0, 6);
            $newFileName = $randomPrefix.'_'.reemplazar_caracteres($fileName);
            $destino = "../../".($tipo_elemento == "imagen" ? "images/" : "video/").$nombre_subcarpeta.$newFileName;
            if(move_uploaded_file($temp, $destino)){
                $contenido = $newFileName;
                // borrar anterior
                $old_name = ($old_contenido != "" ? $old_contenido : "_.noexiste");
                $old_file_path = "../../".($old_tipo_elemento == "imagen" ? "images/" : "video/").$nombre_subcarpeta.$old_name;
                if(file_exists($old_file_path)){
                    unlink($old_file_path);
                }
            }
        }else{
            $contenido = $old_contenido;
        }

        if(isset($_FILES["contenido_cubierta_video"])){
            $contenidoCubiertaFile = $_FILES["contenido_cubierta_video"];
        }else{
            $contenidoCubiertaFile = ["error" => 4];
        }
        if($contenidoCubiertaFile["error"] == 0 && $tipo_elemento == "video"){
            // subir archivo
            $fileName = $contenidoCubiertaFile['name'];
            $temp = $contenidoCubiertaFile['tmp_name'];
            $strBase = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
            $randomPrefix = substr(str_shuffle($strBase), 0, 6);
            $newFileName = $randomPrefix.'_'.reemplazar_caracteres($fileName);
            $destino = "../../images/".$nombre_subcarpeta.$newFileName;
            if(move_uploaded_file($temp, $destino)){
                $contenido_cubierta_video = $newFileName;
                // borrar anterior
                $old_name = ($old_contenido_cubierta_video != "" ? $old_contenido_cubierta_video : "_.noexiste");
                $old_file_path = "../../images/".$nombre_subcarpeta.$old_name;
                if(file_exists($old_file_path)){
                    unlink($old_file_path);
                }
            }
        }else{
            $contenido_cubierta_video = $old_contenido_cubierta_video;
        }

        if($id_elemento != 0){
            $bandera = $m_elementos->update_elemento($bd, $id_elemento, $contenido, $contenido_cubierta_video, $titulo_elemento_es, $titulo_elemento_en, $tipo_elemento);
        }else{
            $bandera = $m_elementos->insert_elemento($bd, $id_seccion, $contenido, $contenido_cubierta_video, $titulo_elemento_es, $titulo_elemento_en, $tipo_elemento);
            $result = $m_elementos->get_last_elemento($bd);
            $id_elemento = mysqli_fetch_assoc($result)["id_elemento"];
        }

        echo $bandera."<-->".$id_elemento."<-->".$nombre_subcarpeta."<-->".$contenido."<-->".$contenido_cubierta_video."<-->VideoError:_".$contenidoFile["error"]."_";
        include("../../php/controllers/cerrar_php.php");
    }
    function eliminar_elemento_galeria(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/elementos-galeria.php");
        $m_elementos = new ElementosGaleria();
        $bandera = -1;

        $id_elemento = $_POST["id_elemento"];
        $result = $m_elementos->get_elemento($bd, $id_elemento);
        $elemento = mysqli_fetch_assoc($result);
        $nombre_subcarpeta = $elemento["nombre_subcarpeta"];
        $contenido = $elemento["contenido"];
        $contenido_cubierta_video = $elemento["contenido_cubierta_video"];
        $tipo_elemento = $elemento["tipo_elemento"];

        $name = ($contenido != "" ? $contenido : "_.noexiste");
        $file_path = "../../".($tipo_elemento == "imagen" ? "images/" : "video/").$nombre_subcarpeta.$name;
        if(file_exists($file_path)){
            unlink($file_path);
        }
        $name = ($contenido_cubierta_video != "" ? $contenido_cubierta_video : "_.noexiste");
        $file_path = "../../images/".$nombre_subcarpeta.$name;
        if(file_exists($file_path)){
            unlink($file_path);
        }

        $bandera = $m_elementos->delete_elemento($bd, $id_elemento);

        echo $bandera;
        include("../../php/controllers/cerrar_php.php");
    }
    function guardar_seccion_imagen(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/secciones.php");
        $m_secciones = new Secciones();
        $bandera = -1;
        $dbg = print_r($_POST, TRUE)."\n\n\n".print_r($_FILES, TRUE)."\n\n\n";
        $id_seccion = $_POST["id_seccion"];
        if(isset($_POST["contenido_es_youtube"])){
            $contenido_es_youtube = $_POST["contenido_es_youtube"];
        }else{
            $contenido_es_youtube = "";
        }

        $nombre_subcarpeta = "";
        $contenido_es = "";
        $tipo_contenido = "";
        $result = $m_secciones->get_seccion($bd, $id_seccion);
        $seccion = mysqli_fetch_assoc($result);
        $nombre_subcarpeta = $seccion["nombre_subcarpeta"];
        $old_contenido_es = $seccion["contenido_es"];
        $old_tipo_contenido = $seccion["tipo_contenido"];

        if(isset($_FILES["contenido_es"])){
            $contenidoFile = $_FILES["contenido_es"];
        }else{
            $contenidoFile = ["error" => 4];
        }
        if($contenidoFile["error"] == 0){
            // subir archivo
            $fileName = $contenidoFile['name'];
            $temp = $contenidoFile['tmp_name'];
            $strBase = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
            $randomPrefix = substr(str_shuffle($strBase), 0, 6);
            $newFileName = $randomPrefix.'_'.reemplazar_caracteres($fileName);
            $destino = "../../images/".$nombre_subcarpeta.$newFileName;
            $dbg .= "|||".$destino;
            if(move_uploaded_file($temp, $destino)){
                $contenido_es = $newFileName;
                if($old_tipo_contenido == "imagen"){
                    $tipo_contenido = $old_tipo_contenido;
                }else{
                    $tipo_contenido = "iy_imagen";
                }
                // borrar anterior
                $old_name = ($old_contenido_es != "" ? $old_contenido_es : "_.noexiste");
                $old_file_path = "../../images/".$nombre_subcarpeta.$old_name;
                if(file_exists($old_file_path)){
                    unlink($old_file_path);
                }
            }
        }else{
            if($contenido_es_youtube != "" && $contenidoFile["error"] == 4){
                $contenido_es = $contenido_es_youtube;
                $tipo_contenido = "iy_youtube";
            }else{
                $contenido_es = $old_contenido_es;
                $tipo_contenido = $old_tipo_contenido;
            }
        }
        $contenido_en = $contenido_es;
        $bandera = $m_secciones->update_seccion($bd, $id_seccion, $contenido_es, $contenido_en, $tipo_contenido);

        echo $bandera."<-->".$nombre_subcarpeta."<-->".$contenido_es."<-->".$tipo_contenido."<-->".$dbg;
        include("../../php/controllers/cerrar_php.php");
    }
    function guardar_elemento_galeria_youtube(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/elementos-galeria.php");
        $m_elementos = new ElementosGaleria();

        $id_seccion = $_POST["id_seccion"];
        $tipo_elemento = "youtube";
        $titulo_elemento_es = "";
        $titulo_elemento_en = "";
        $contenido = utf8_decode($_POST["contenido"]);
        $contenido_cubierta_video = "";

        $bandera = $m_elementos->insert_elemento($bd, $id_seccion, $contenido, $contenido_cubierta_video, $titulo_elemento_es, $titulo_elemento_en, $tipo_elemento);
        $result = $m_elementos->get_last_elemento($bd);
        $id_elemento = mysqli_fetch_assoc($result)["id_elemento"];
        
        $parts = explode("embed/", $contenido);
        $videoId = $parts[1];
        $src = 'https://img.youtube.com/vi/'.$videoId.'/0.jpg';

        echo $bandera."<-->".$id_elemento."<-->".$contenido."<-->".$src;
        include("../../php/controllers/cerrar_php.php");
    }
    function reemplazar_caracteres($string){
        $string = str_replace("á", "a", $string);
        $string = str_replace("é", "e", $string);
        $string = str_replace("í", "i", $string);
        $string = str_replace("ó", "o", $string);
        $string = str_replace("ú", "u", $string);
        $string = str_replace("ü", "u", $string);
        $string = str_replace("ñ", "nh", $string);
        $string = str_replace("Á", "A", $string);
        $string = str_replace("É", "E", $string);
        $string = str_replace("Í", "I", $string);
        $string = str_replace("Ó", "O", $string);
        $string = str_replace("Ú", "U", $string);
        $string = str_replace("Ü", "U", $string);
        $string = str_replace("Ñ", "NH", $string);
        return $string;
    }
?>