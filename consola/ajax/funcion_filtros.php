<?php @session_start(); 
if(isset($_GET["ev"])){
	switch($_GET["ev"]){
        case 1: cmb_categorias(); break;
        case 2: cmb_tipo_habitaciones(); break;
    }
}
function cmb_categorias(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    $obj_cat = new HabitacionesCategoria();
    $id_destino = $_POST["Destino"];
    $salida = "";
    $consulta = $obj_cat->filtro_categorias_destinos($bd,$id_destino,"");
    while($data = mysqli_fetch_array($consulta)){
        $salida.= '<option value="'.$data["id_categoria"].'">'.utf8_encode($data["nombre_categoria"]).'</option>';
    }
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
function cmb_tipo_habitaciones(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_tipo.php");
    $obj_cat = new HabitacionesTipo();
    $id_categoria = $_POST["Categoria"];
    $salida = "";
    $consulta = $obj_cat->filtro_tipo_habitaciones($bd,$id_categoria,"");
    while($data = mysqli_fetch_array($consulta)){
        $salida.= '<option value="'.$data["id_tipo_habitacion"].'">'.utf8_encode($data["nombre_tipo"]).'</option>';
    }
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
?>