<?php @session_start(); 
include_once("../php/environment.php");
if(isset($_GET["ev"])){
	switch($_GET["ev"]){
        case 1: guardar_tipo(); break;
        case 2: eliminar_tipo(); break;
        case 3: mostrar_tipo(); break;
        case 4: tabla_tipo_habitacion(); break;
    }
}
function guardar_tipo(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    include("../../php/class/habitaciones_tipo.php");
    include("../../php/class/habitaciones.php");

    $obj_tipo = new HabitacionesTipo();
    $id = $_POST["id"];
    $categoria = $_POST["categoria"];
    $status = $_POST["status"];
    $nombre = utf8_decode($_POST["nombre"]);
    $mensaje = utf8_decode($_POST["mensaje"]);
    $slogan = utf8_decode($_POST["slogan"]);
    $descripcion = $_POST["descripcion"];
    $mensaje_en = utf8_decode($_POST["mensaje_en"]);
    $slogan_en = utf8_decode($_POST["slogan_en"]);
    $descripcion_en = $_POST["descripcion_en"];
    $url = limpiar_url($_POST["url"]);
    $meta_titulo = utf8_decode($_POST["meta_titulo"]);
    $meta_descripcion = utf8_decode($_POST["meta_descripcion"]);
    $meta_keywords = utf8_decode($_POST["meta_keywords"]);
    $meta_titulo_en = utf8_decode($_POST["meta_titulo_en"]);
    $meta_descripcion_en = utf8_decode($_POST["meta_descripcion_en"]);
    $meta_keywords_en = utf8_decode($_POST["meta_keywords_en"]);
    
    if($id == ""){
        $consulta = $obj_tipo -> registrar_tipo($bd,$categoria,$url,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status);
        $id = $obj_tipo ->  maximo_tipo($bd);
        $concat = $obj_tipo -> site_tipo_habitaciones($bd,$id);
        $datacat = mysqli_fetch_array($concat);
        $destinoUrl = $datacat["url_destino"];
        $tipoUrl = $datacat["url_tipo"];
        crearArchivo($destinoUrl, $tipoUrl, $id);

        echo $consulta;
    }else{
        $consulta = $obj_tipo -> actualizar_tipo($bd,$id,$categoria,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status);
        echo $consulta;
    }
    generarSitemap();
    include("../../php/controllers/cerrar_php.php");
}
function eliminar_tipo(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_tipo.php");
    $obj_tipo = new HabitacionesTipo();
    $id = $_POST["id"];
    $concat = $obj_tipo -> tabla_tipo_habitaciones($bd,$id);
    $datacat = mysqli_fetch_array($concat);
    $destinoUrl = $datacat["url_destino"];
    $tipoUrl = $datacat["url_tipo"];
    $pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$tipoUrl.".php";
    if (!unlink($filePath)){}
    echo $consulta = $obj_tipo -> eliminar_tipo($bd,$id);
    generarSitemap();
    include("../../php/controllers/cerrar_php.php");
}
function tabla_tipo_habitacion(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_tipo.php");
    $obj_cat = new HabitacionesTipo();
    
    $deletePermission = $_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_eliminar"];
    $salida = "";

    $consulta =  $obj_cat->tabla_tipo_habitaciones($bd,"");
	while($variables = mysqli_fetch_array($consulta)){
        $status = "color:#8acb87;";
        if($variables["status_tipo"] == 0){
            $status = "color:#ed1c22;";
        }
		$salida.= "<tr>";	
        $salida.= "<td>".utf8_encode($variables["nombre_destino"])."</td>";	
        $salida.= "<td>".utf8_encode($variables["nombre_categoria"])."</td>";	
		$salida.= "<td style='cursor:pointer' class='editars' onclick='mostrar_tipo_habitacion(".$variables["id_tipo_habitacion"].")' data-toggle='modal' data-target='#myModal2'>".'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'.utf8_encode($variables["nombre_tipo"])."</td>";
		$salida.= "<td>".utf8_encode($variables["mensaje_tipo"])."</td>";
        $salida.= "<td>".utf8_encode($variables["slogan_tipo"])."</td>";
        $salida.= "<td style='text-align:center;'>".'<i class="fa fa-circle" style="'.$status.'font-size:23px;"></i>'."</td>";
		$salida.= $deletePermission ? "<td class='anadir".$variables["id_tipo_habitacion"]."'><p onclick='eliminar_tipo_habitacion(".$variables["id_tipo_habitacion"].")' style='cursor:pointer;text-align:center;' class='eliminar'>".'<i class="fa fa-trash" aria-hidden="true"></i>'."</p></td>" : "";
		
		$salida.= "</tr>";
	}
    
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
function mostrar_tipo(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_tipo.php");
    $obj_tipo = new HabitacionesTipo();
    $id_tipo = $_POST["id"];
    $consulta = $obj_tipo ->tabla_tipo_habitaciones($bd,$id_tipo);
    $reg = array();
    while($var = mysqli_fetch_assoc($consulta)){
        $reg[0]['cve'] = $var['id_tipo_habitacion'];
        $reg[0]['id_destino'] = $var['id_destino'];
        $reg[0]['id_categoria'] = $var['id_categoria'];
        $reg[0]['url'] = $var['url_tipo'];
        $reg[0]['nombre'] = utf8_encode($var['nombre_tipo']);
        $reg[0]['mensaje'] = utf8_encode($var['mensaje_tipo']);
        $reg[0]['slogan'] = utf8_encode($var['slogan_tipo']);
        $reg[0]['descripcion'] = utf8_encode($var['descripcion_tipo']);
        $reg[0]['mensaje_en'] = utf8_encode($var['mensaje_tipo_en']);
        $reg[0]['slogan_en'] = utf8_encode($var['slogan_tipo_en']);
        $reg[0]['descripcion_en'] = utf8_encode($var['descripcion_tipo_en']);
        $reg[0]['status'] = $var['status_tipo'];
        $reg[0]['meta_titulo'] = utf8_encode($var['meta_titulo']);
        $reg[0]['meta_descripcion'] = utf8_encode($var['meta_descripcion']);
        $reg[0]['meta_keywords'] = utf8_encode($var['meta_keywords']);
        $reg[0]['meta_titulo_en'] = utf8_encode($var['meta_titulo_en']);
        $reg[0]['meta_descripcion_en'] = utf8_encode($var['meta_descripcion_en']);
        $reg[0]['meta_keywords_en'] = utf8_encode($var['meta_keywords_en']);
    }
    echo json_encode($reg[0]);
    include("../../php/controllers/cerrar_php.php");
}
function generarSitemap(){
    include("../../php/controllers/connect_sql.php");
    $obj_categorias = new HabitacionesCategoria();
    $obj_tipo = new HabitacionesTipo();
    $obj_habitaciones = new Habitaciones();
    $result = $obj_categorias->lista_categorias_habitaciones($bd,"");
    $result2 = $obj_tipo->site_tipo_habitaciones($bd,"");
    $result3 = $obj_habitaciones->site_habitaciones($bd,"");

    $sites_categorias = '';
    $sites_tipo = '';
    $sites_habitaciones = '';

    //Categoria
    while($row = mysqli_fetch_array($result)){
        $sites_categorias .= '<url>
                <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row["url_destino"].'/'.$row["url_categoria"].'</loc>
                <priority>0.70</priority>
            </url>
            <url>
                <loc>https://www.pacifica.com.mx/web/en/destination/'.$row["url_destino"].'/'.$row["url_categoria"].'</loc>
                <priority>0.70</priority>
            </url>';
    }
    //Tipo habitación
    while($row2 = mysqli_fetch_array($result2)){
        $sites_tipo .= '<url>
            <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row2["url_destino"].'/'.$row2["url_tipo"].'</loc>
            <priority>0.70</priority>
        </url>
        <url>
            <loc>https://www.pacifica.com.mx/web/en/destination/'.$row2["url_destino"].'/'.$row2["url_tipo"].'</loc>
            <priority>0.70</priority>
        </url>';
    }
    //Habitaciones
    while($row3 = mysqli_fetch_array($result3)){
        $sites_habitaciones .= '<url>
            <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row3["url_destino"].'/'.$row3["url_habitacion"].'</loc>
            <priority>0.70</priority>
        </url>
        <url>
            <loc>https://www.pacifica.com.mx/web/en/destination/'.$row3["url_destino"].'/'.$row3["url_habitacion"].'</loc>
            <priority>0.70</priority>
        </url>';
    }

    $sitemap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
            '.$sites_categorias.'
            '.$sites_tipo.'
            '.$sites_habitaciones.'
        </urlset>';
    $pathToRoot = '../../../';
    $sitemapFileName = 'sitemap_producto.xml';
    $fp = fopen($pathToRoot.$sitemapFileName, "w");
    fputs($fp, $sitemap);
    fclose($fp);
    include("../../php/controllers/cerrar_php.php");
}
function crearArchivo($destinoUrl, $tipoUrl, $tipoId){
	$pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$tipoUrl.".php";
    $fp = fopen($filePath, "w+");
    fclose($fp);
    chmod($filePath, 0777);
    $fp = fopen($filePath, "w+");
    $contenidoArchivo = '<?php $id_tipo = '.$tipoId.'; include("_habitaciones_tipo.php"); ?>';
    fwrite($fp, $contenidoArchivo);
    fclose($fp);

    // $targetDir = "en/destinos/$destinoUrl/";
    // $filePath = $pathToRoot.$targetDir.$tipoUrl.".php";
    // $fp = fopen($filePath, "w+");
    // fclose($fp);
    // chmod($filePath, 0777);
    // $fp = fopen($filePath, "w+");
    // fwrite($fp, $contenidoArchivo);
    // fclose($fp);
}
function limpiar_url($val){
	$val=strtolower($val);
	$val=str_replace(" ","-",$val);
	$val=str_replace("/","-",$val);
	$val=str_replace("|","-",$val);
	$val=str_replace("@","",$val);
	$val=str_replace("#","",$val);
	$val=str_replace("º","",$val);
	$val=str_replace("ª","",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace('&deg;',"",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace(".","",$val);
	$val=str_replace(",","",$val);
	$val=str_replace("\\","",$val);
	$val=str_replace("á","a",$val);
	$val=str_replace("é","e",$val);
	$val=str_replace("í","i",$val);
	$val=str_replace("ó","o",$val);
	$val=str_replace("ú","u",$val);
	$val=str_replace("ñ","n",$val);
	$val=str_replace("&aacute;","a",$val);
	$val=str_replace("&eacute;","e",$val);
	$val=str_replace("&iacute;","i",$val);
	$val=str_replace("&oacute;","o",$val);
	$val=str_replace("&uacute;","u",$val);
	$val=str_replace("&ntilde;","n",$val);
	$val=str_replace("&39;","",$val);
	$val=str_replace("&iquest;","",$val);
	$val=str_replace('&amp;"',"",$val);
	$val=str_replace("¿","",$val);
	$val=str_replace("&reg;","",$val);
	$val=str_replace("®","",$val);
	$val=str_replace("¡","",$val);
	$val=str_replace("?","",$val);
	$val=str_replace("!","",$val);
	$val=str_replace("'","",$val);
	$val=str_replace("€'","",$val);
	$val=str_replace("&","y",$val);
	$val=str_replace(":","",$val);
	$val=str_replace(";","",$val);
	$val=str_replace("--","-",$val);
    $val=str_replace("-.php",".php",$val);
    $val=str_replace(".php","",$val);
	return $val;
}
?>