<?php @session_start(); 
include_once("../php/environment.php");
if(isset($_GET["ev"])){
	switch($_GET["ev"]){
        case 1: guardar_habitacion(); break;
        case 2: eliminar_habitacion(); break;
        case 3: mostrar_habitacion(); break;
        case 4: tabla_habitaciones(); break;
        case 5: galeria_habitaciones(); break;
    }
}
function guardar_habitacion(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    include("../../php/class/habitaciones_tipo.php");
    include("../../php/class/habitaciones.php");
    include("../../php/class/habitaciones_galeria.php");
    $obj_habitaciones = new Habitaciones();
    $obj_galeria = new HabitacionesGaleria();
    $id = $_POST["id"];
    $tipo = $_POST["tipo"];
    $status = $_POST["status"];
    $nombre = utf8_decode($_POST["nombre"]);
    $mensaje = utf8_decode($_POST["mensaje"]);
    $slogan = utf8_decode($_POST["slogan"]);
    $detalle = $_POST["detalle"];
    $descripcion = $_POST["descripcion"];
    $mensaje_en = utf8_decode($_POST["mensaje_en"]);
    $slogan_en = utf8_decode($_POST["slogan_en"]);
    $detalle_en = $_POST["detalle_en"];
    $descripcion_en = $_POST["descripcion_en"];
    $plano = utf8_decode($_POST["plano"]);
    $url = limpiar_url($_POST["url"]);
    $meta_titulo = utf8_decode($_POST["meta_titulo"]);
    $meta_descripcion = utf8_decode($_POST["meta_descripcion"]);
    $meta_keywords = utf8_decode($_POST["meta_keywords"]);
    $meta_titulo_en = utf8_decode($_POST["meta_titulo_en"]);
    $meta_descripcion_en = utf8_decode($_POST["meta_descripcion_en"]);
    $meta_keywords_en = utf8_decode($_POST["meta_keywords_en"]);
    $galeria = $_POST["galeria"];

    $precio_general = 'NULL';
    $precio_miembros = 'NULL';
    if($_POST["precio_general"] != ""){
        $precio_general = "'".str_replace(",", "",$_POST["precio_general"])."'";
    }
    if($_POST["precio_miembros"] != ""){
        $precio_miembros = "'".str_replace(",", "",$_POST["precio_miembros"])."'";
    }

    if($id == ""){
        $consulta = $obj_habitaciones -> registrar_habitacion($bd,$tipo,$url,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$detalle,$detalle_en,$plano,$precio_general,$precio_miembros,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status);
        $id = $obj_habitaciones ->  maximo_habitacion($bd);
        $concat = $obj_habitaciones -> site_habitaciones($bd,$id);
        $datacat = mysqli_fetch_array($concat);
        $destinoUrl = $datacat["url_destino"];
        $habitacionUrl = $datacat["url_habitacion"];
        crearArchivo($destinoUrl, $habitacionUrl, $id);

        echo $consulta;
    }else{
        $consulta = $obj_habitaciones -> actualizar_habitacion($bd,$id,$tipo,$url,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$detalle,$detalle_en,$plano,$precio_general,$precio_miembros,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status);
        $obj_galeria -> eliminar_galeria($bd,$id);
        echo $consulta;
    } 
    for($i=0; $i < count($galeria); $i++){
        $obj_galeria -> registrar_galeria($bd,$id,$galeria[$i]);
    }
    generarSitemap();
    include("../../php/controllers/cerrar_php.php");
}
function eliminar_habitacion(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones.php");
    $obj_tipo = new Habitaciones();
    $id = $_POST["id"];
    $concat = $obj_tipo -> tabla_habitaciones($bd,$id,"");
    $datacat = mysqli_fetch_array($concat);
    $destinoUrl = $datacat["url_destino"];
    $habitacionUrl = $datacat["url_habitacion"];
    $pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$habitacionUrl.".php";
    if (!unlink($filePath)){}
	$targetDir = "en/destination/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$habitacionUrl.".php";
    if (!unlink($filePath)){}
    echo $consulta = $obj_tipo -> eliminar_habitacion($bd,$id);
    generarSitemap();
    include("../../php/controllers/cerrar_php.php");
}
function tabla_habitaciones(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones.php");
    $obj_cat = new Habitaciones();
    
    $deletePermission = $_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_eliminar"];
    $salida = "";

    $consulta =  $obj_cat->tabla_habitaciones($bd,"","");
	while($variables = mysqli_fetch_array($consulta)){
        $status = "color:#8acb87;";
        if($variables["status_habitacion"] == 0){
            $status = "color:#ed1c22;";
        }
        if($variables["imagen"] != ""){
            $variables["imagen"] = "<img src='../images/destinos/hoteles/habitaciones/galeria/".$variables["imagen"]."' /> "; 
        }
        if($variables["precio_general"] != ""){
            $variables["precio_general"] = "$".number_format($variables["precio_general"],2); 
        }
        if($variables["precio_miembros"] != ""){
            $variables["precio_miembros"] = "$".number_format($variables["precio_miembros"],2); 
        }
		$salida.= "<tr>";	
        $salida.= "<td>".utf8_encode($variables["nombre_destino"])."</td>";	
        $salida.= "<td>".utf8_encode($variables["nombre_categoria"])."</td>";
        $salida.= "<td>".utf8_encode($variables["nombre_tipo"])."</td>";	
		$salida.= "<td style='cursor:pointer' class='editars' onclick='mostrar_habitacion(".$variables["id_habitacion"].")' data-toggle='modal' data-target='#myModal'>".'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'.utf8_encode($variables["nombre_habitacion"])."</td>";
        $salida.= "<td>".$variables["imagen"]."</td>";
        $salida.= "<td>".utf8_encode($variables["mensaje_habitacion"])."</td>";
        $salida.= "<td>".utf8_encode($variables["slogan_habitacion"])."</td>";
        $salida.= "<td>".$variables["precio_general"]."</td>";
        $salida.= "<td>".$variables["precio_miembros"]."</td>";
        $salida.= "<td style='text-align:center;'>".'<i class="fa fa-circle" style="'.$status.'font-size:23px;"></i>'."</td>";
		$salida.= $deletePermission ? "<td class='anadir".$variables["id_habitacion"]."'><p onclick='eliminar_habitacion(".$variables["id_habitacion"].")' style='cursor:pointer;text-align:center;' class='eliminar'>".'<i class="fa fa-trash" aria-hidden="true"></i>'."</p></td>" : "";
		
		$salida.= "</tr>";
	}
    
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
function mostrar_habitacion(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones.php");
    $obj_habitacion = new Habitaciones();
    $id_habitacion = $_POST["id"];
    $consulta = $obj_habitacion ->tabla_habitaciones($bd,$id_habitacion,"");
    $reg = array();
    while($var = mysqli_fetch_assoc($consulta)){
        $reg[0]['cve'] = $var['id_habitacion'];
        $reg[0]['id_destino'] = $var['id_destino'];
        $reg[0]['id_categoria'] = $var['id_categoria'];
        $reg[0]['id_tipo'] = $var['id_tipo_habitacion'];
        $reg[0]['url'] = $var['url_habitacion'];
        $reg[0]['nombre'] = utf8_encode($var['nombre_habitacion']);
        $reg[0]['mensaje'] = utf8_encode($var['mensaje_habitacion']);
        $reg[0]['slogan'] = utf8_encode($var['slogan_habitacion']);
        $reg[0]['descripcion'] = utf8_encode($var['descripcion_habitacion']);
        $reg[0]['detalles'] = utf8_encode($var['detalles_habitacion']);
        $reg[0]['mensaje_en'] = utf8_encode($var['mensaje_habitacion_en']);
        $reg[0]['slogan_en'] = utf8_encode($var['slogan_habitacion_en']);
        $reg[0]['descripcion_en'] = utf8_encode($var['descripcion_habitacion_en']);
        $reg[0]['detalles_en'] = utf8_encode($var['detalles_habitacion_en']);
        $reg[0]['plano'] = $var['plano_habitacion'];
        $precio_general = "";
        $precio_miembros = "";
        if($var['precio_general'] != ""){
            $precio_general = number_format($var['precio_general'],2);
        }
        if($var['precio_miembros'] != ""){
            $precio_miembros = number_format($var['precio_miembros'],2);
        }
        $reg[0]['precio_general'] = $precio_general;
        $reg[0]['precio_miembros'] = $precio_miembros;
        $reg[0]['status'] = $var['status_habitacion'];
        $reg[0]['meta_titulo'] = utf8_encode($var['meta_titulo']);
        $reg[0]['meta_descripcion'] = utf8_encode($var['meta_descripcion']);
        $reg[0]['meta_keywords'] = utf8_encode($var['meta_keywords']);
        $reg[0]['meta_titulo_en'] = utf8_encode($var['meta_titulo_en']);
        $reg[0]['meta_descripcion_en'] = utf8_encode($var['meta_descripcion_en']);
        $reg[0]['meta_keywords_en'] = utf8_encode($var['meta_keywords_en']);
    }
    echo json_encode($reg[0]);
    include("../../php/controllers/cerrar_php.php");
}
function galeria_habitaciones(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_galeria.php");
    $obj_habitaciones = new HabitacionesGaleria();
    $id = $_POST["id"];
    $consulta = $obj_habitaciones ->lista_galeria($bd,$id);
    $lista = '<div class="element-img" id="div_1"></div>';
    $count = 1;
    while($data = mysqli_fetch_array($consulta)){
        $count++;
        $lista.= "<div class='element-img l-img-min' data-name-img='".$data["name"]."' id='div_".$count."'><img src='../images/destinos/hoteles/habitaciones/galeria/".$data["name"]."' id='txt_".$count."' />&nbsp;<span id='remove_".$count."' onclick='eliminar_galeria(this.id)' class='remove'><i class='fa fa-trash' aria-hidden='true'></i></span></div>";
    }
    echo $lista;
    include("../../php/controllers/cerrar_php.php");
}
function generarSitemap(){
    include("../../php/controllers/connect_sql.php");
    $obj_categorias = new HabitacionesCategoria();
    $obj_tipo = new HabitacionesTipo();
    $obj_habitaciones = new Habitaciones();
    $result = $obj_categorias->lista_categorias_habitaciones($bd,"");
    $result2 = $obj_tipo->site_tipo_habitaciones($bd,"");
    $result3 = $obj_habitaciones->site_habitaciones($bd,"");

    $sites_categorias = '';
    $sites_tipo = '';
    $sites_habitaciones = '';

    //Categoria
    while($row = mysqli_fetch_array($result)){
        $sites_categorias .= '<url>
                <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row["url_destino"].'/'.$row["url_categoria"].'</loc>
                <priority>0.70</priority>
            </url>
            <url>
                <loc>https://www.pacifica.com.mx/web/en/destination/'.$row["url_destino"].'/'.$row["url_categoria"].'</loc>
                <priority>0.70</priority>
            </url>';
    }
    //Tipo habitación
    while($row2 = mysqli_fetch_array($result2)){
        $sites_tipo .= '<url>
            <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row2["url_destino"].'/'.$row2["url_tipo"].'</loc>
            <priority>0.70</priority>
        </url>
        <url>
            <loc>https://www.pacifica.com.mx/web/en/destination/'.$row2["url_destino"].'/'.$row2["url_tipo"].'</loc>
            <priority>0.70</priority>
        </url>';
    }
    //Habitaciones
    while($row3 = mysqli_fetch_array($result3)){
        $sites_habitaciones .= '<url>
            <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row3["url_destino"].'/'.$row3["url_habitacion"].'</loc>
            <priority>0.70</priority>
        </url>
        <url>
            <loc>https://www.pacifica.com.mx/web/en/destination/'.$row3["url_destino"].'/'.$row3["url_habitacion"].'</loc>
            <priority>0.70</priority>
        </url>';
    }

    $sitemap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
            '.$sites_categorias.'
            '.$sites_tipo.'
            '.$sites_habitaciones.'
        </urlset>';
    $pathToRoot = '../../../';
    $sitemapFileName = 'sitemap_producto.xml';
    $fp = fopen($pathToRoot.$sitemapFileName, "w");
    fputs($fp, $sitemap);
    fclose($fp);
    include("../../php/controllers/cerrar_php.php");
}
function crearArchivo($destinoUrl, $habitacionUrl, $habitacionId){
	$pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$habitacionUrl.".php";
    $fp = fopen($filePath, "w+");
    fclose($fp);
    chmod($filePath, 0777);
    $fp = fopen($filePath, "w+");
    $contenidoArchivo = '<?php $id_habitacion = '.$habitacionId.'; include("_habitaciones_detalle.php"); ?>';
    fwrite($fp, $contenidoArchivo);
    fclose($fp);

    $targetDir = "en/destination/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$habitacionUrl.".php";
    $fp = fopen($filePath, "w+");
    fclose($fp);
    chmod($filePath, 0777);
    $fp = fopen($filePath, "w+");
    fwrite($fp, $contenidoArchivo);
    fclose($fp);
}
function limpiar_url($val){
	$val=strtolower($val);
    $val=str_replace("-.php",".php",$val);
    $val=str_replace(".php","",$val);
	$val=str_replace(" ","-",$val);
	$val=str_replace("/","-",$val);
	$val=str_replace("|","-",$val);
	$val=str_replace("@","",$val);
	$val=str_replace("#","",$val);
	$val=str_replace("º","",$val);
	$val=str_replace("ª","",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace('&deg;',"",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace(".","",$val);
	$val=str_replace(",","",$val);
	$val=str_replace("\\","",$val);
	$val=str_replace("á","a",$val);
	$val=str_replace("é","e",$val);
	$val=str_replace("í","i",$val);
	$val=str_replace("ó","o",$val);
	$val=str_replace("ú","u",$val);
	$val=str_replace("ñ","n",$val);
	$val=str_replace("&aacute;","a",$val);
	$val=str_replace("&eacute;","e",$val);
	$val=str_replace("&iacute;","i",$val);
	$val=str_replace("&oacute;","o",$val);
	$val=str_replace("&uacute;","u",$val);
	$val=str_replace("&ntilde;","n",$val);
	$val=str_replace("&39;","",$val);
	$val=str_replace("&iquest;","",$val);
	$val=str_replace('&amp;"',"",$val);
	$val=str_replace("¿","",$val);
	$val=str_replace("&reg;","",$val);
	$val=str_replace("®","",$val);
	$val=str_replace("¡","",$val);
	$val=str_replace("?","",$val);
	$val=str_replace("!","",$val);
	$val=str_replace("'","",$val);
	$val=str_replace("€'","",$val);
	$val=str_replace("&","y",$val);
	$val=str_replace(":","",$val);
	$val=str_replace(";","",$val);
	$val=str_replace("--","-",$val);
	return $val;
}
?>