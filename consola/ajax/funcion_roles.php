<?php 
    @session_start(); 
    if(isset($_GET["ev"])){
        switch($_GET["ev"]){
            case 1: cargar_lista_roles(); break;
            case 2: cargar_formulario_rol(); break;
            case 3: guardar_rol(); break;
        }
    }
    function cargar_lista_roles(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/roles.php");
        $m_roles = new Roles();
        $tableRows = '';
        $result = $m_roles->select_roles($bd);
        while($row = mysqli_fetch_assoc($result)){
            $row = array_map("utf8_encode", $row);
            $tableRows .= '
                <tr>
                    <td class="triggerEdit" onclick="editar_rol('.$row["id_rol"].');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.$row["nombre_rol"].'</td>
                    <td>'.$row["descripcion_rol"].'</td>
                </tr>
            ';
        }
        $response = '
            <table id="rolesTable" class="table">
                <thead><tr><th>Rol</th><th>Descripción</th></tr></thead>
                <tbody>'.$tableRows.'</tbody>
            </table>
        ';
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
    function cargar_formulario_rol(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/roles.php");
        include("../../php/class/permisos.php");
        include("../../php/class/modulos.php");
        $m_roles = new Roles();
        $m_permisos = new Permisos();
        $m_modulos = new Modulos();
        $id_rol = $_POST["id_rol"];
        $nombre_rol = "";
        $descripcion_rol = "";
        $permisosBlock = '';
        if($id_rol != ""){
            $result = $m_roles->select_roles($bd, $id_rol);
            $row = mysqli_fetch_assoc($result);
            $row = array_map("utf8_encode", $row);
            $nombre_rol = $row["nombre_rol"];
            $descripcion_rol = $row["descripcion_rol"];
            $isAdminDisabled = $row["es_admin"] == 1 ? "disabled" : "";
            $result = $m_permisos->select_permisos($bd, $id_rol);
            while($row = mysqli_fetch_assoc($result)){
                $row = array_map("utf8_encode", $row);
                $permisosBlock .= '
                    <tr data-id-modulo="'.$row["id_modulo"].'" data-id-permiso="'.$row["id_permiso"].'">
                        <td>'.$row["nombre_modulo"].'</td>
                        <td><input type="checkbox" name="permiso" '.($row["p_leer"] == 1 ? "checked" : "").' '.$isAdminDisabled.'></td>
                        <td><input type="checkbox" name="permiso" '.($row["p_crear"] == 1 ? "checked" : "").' '.$isAdminDisabled.'></td>
                        <td><input type="checkbox" name="permiso" '.($row["p_actualizar"] == 1 ? "checked" : "").' '.$isAdminDisabled.'></td>
                        <td><input type="checkbox" name="permiso" '.($row["p_eliminar"] == 1 ? "checked" : "").' '.$isAdminDisabled.'></td>
                    </tr>
                ';
            }
        }else{
            $result = $m_modulos->select_modulos($bd);
            while($row = mysqli_fetch_assoc($result)){
                $row = array_map("utf8_encode", $row);
                $permisosBlock .= '
                    <tr data-id-modulo="'.$row["id_modulo"].'" data-id-permiso="">
                        <td>'.$row["nombre_modulo"].'</td>
                        <td><input type="checkbox" name="permiso"></td>
                        <td><input type="checkbox" name="permiso"></td>
                        <td><input type="checkbox" name="permiso"></td>
                        <td><input type="checkbox" name="permiso"></td>
                    </tr>
                ';
            }
        }
        $response = '
            <form id="rolForm" noaction>
                <input type="hidden" name="id_rol" value="'.$id_rol.'">
                <div class="row">
                    <div class="col-sm-12 col-md-6 form-group">
                        <label>Nombre del Rol</label>
                        <input type="text" class="form-control" name="nombre_rol" value="'.$nombre_rol.'" required>
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        <label>Descripción del Rol</label>
                        <textarea type="text" class="form-control" name="descripcion_rol">'.$descripcion_rol.'</textarea>
                    </div>
                    <div class="col-sm-12 form-group">
                        <label>Permisos del Rol</label>
                        <table class="w-100 permissionTable">
                            <thead><tr><th>Módulo</th><th>Leer/Entrar</th><th>Crear</th><th>Actualizar</th><th>Eliminar</th></tr></thead>
                            <tbody>
                                '.$permisosBlock.'
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        ';
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
    function guardar_rol(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/roles.php");
        include("../../php/class/permisos.php");
        include("../../php/class/modulos.php");
        $m_roles = new Roles();
        $m_permisos = new Permisos();
        $m_modulos = new Modulos();
        $_POST["permisos"] = json_decode($_POST["permisos"], TRUE);
        $id_rol = $_POST["id_rol"];
        $nombre_rol = utf8_decode($_POST["nombre_rol"]);
        $descripcion_rol = utf8_decode($_POST["descripcion_rol"]);
        $response = 0;
        if($id_rol != ""){
            $response = $m_roles->update_rol($bd, $id_rol, $nombre_rol, $descripcion_rol) ? 1 : 0;
            if($response){
                foreach($_POST["permisos"] as $permiso){
                    $responsePermiso = $m_permisos->update_permiso($bd, $permiso["id_permiso"], $permiso["p_leer"], $permiso["p_crear"], $permiso["p_actualizar"], $permiso["p_eliminar"]);
                }
            }
        }else{
            $response = $m_roles->insert_rol($bd, $nombre_rol, $descripcion_rol) ? 1 : 0;
            $id_rol = mysqli_insert_id($bd);
            if($response){
                foreach($_POST["permisos"] as $permiso){
                    $responsePermiso = $m_permisos->insert_permiso($bd, $id_rol, $permiso["id_modulo"], $permiso["p_leer"], $permiso["p_crear"], $permiso["p_actualizar"], $permiso["p_eliminar"]);
                }
            }
        }
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
?>

