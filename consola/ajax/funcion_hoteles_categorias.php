<?php @session_start(); 
include_once("../php/environment.php");
if(isset($_GET["ev"])){
	switch($_GET["ev"]){
        case 1: guardar_categoria(); break;
        case 2: eliminar_categoria(); break;
        case 3: mostrar_categoria(); break;
        case 4: tabla_categorias(); break;
        case 5: galeria_categoria(); break;
    }
}
function guardar_categoria(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    include("../../php/class/habitaciones_tipo.php");
    include("../../php/class/habitaciones.php");
    include("../../php/class/habitaciones_categoria_galeria.php");
    $obj_habitaciones = new HabitacionesCategoria();
    $obj_galeria = new HabitacionesCategoriaGaleria();

    $id = $_POST["id"];
    $destino = $_POST["destino"];
    $status = $_POST["status"];
    $nombre = utf8_decode($_POST["nombre"]);
    $descripcion = $_POST["descripcion"];
    $descripcion_en = $_POST["descripcion_en"];
    $imagen = utf8_decode($_POST["imagen"]);
    $url = limpiar_url($_POST["url"]);
    $meta_titulo = utf8_decode($_POST["meta_titulo"]);
    $meta_descripcion = utf8_decode($_POST["meta_descripcion"]);
    $meta_keywords = utf8_decode($_POST["meta_keywords"]);
    $meta_titulo_en = utf8_decode($_POST["meta_titulo_en"]);
    $meta_descripcion_en = utf8_decode($_POST["meta_descripcion_en"]);
    $meta_keywords_en = utf8_decode($_POST["meta_keywords_en"]);
    
    if($id == ""){
        $consulta = $obj_habitaciones -> registrar_categoria($bd,$destino,$url,$nombre,$descripcion,$descripcion_en,$imagen,$meta_titulo,$meta_descripcion,$meta_keywords,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$status);
        $id = $obj_habitaciones ->  maximo_categoria($bd);
        $concat = $obj_habitaciones -> lista_categorias_habitaciones($bd,$id);
        $datacat = mysqli_fetch_array($concat);
        $destinoUrl = $datacat["url_destino"];
        $categoriaUrl = $datacat["url_categoria"];
        crearArchivo($destinoUrl, $categoriaUrl, $id);
        echo $consulta;
    }else{
        $consulta = $obj_habitaciones -> actualizar_categoria($bd,$id,$destino,$nombre,$descripcion,$descripcion_en,$imagen,$meta_titulo,$meta_descripcion,$meta_keywords,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$status);
        $obj_galeria -> eliminar_galeria($bd,$id);
        echo $consulta;
    }
    if(isset($_POST["galeria"])){
        $galeria = $_POST["galeria"];
        for($i=0; $i < count($galeria); $i++){
            $obj_galeria -> registrar_galeria($bd,$id,$galeria[$i]);
        }
    }
    generarSitemap();
    include("../../php/controllers/cerrar_php.php");
}
function eliminar_categoria(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    $obj_habitaciones = new HabitacionesCategoria();
    $id = $_POST["id"];
    $concat = $obj_habitaciones -> lista_categorias_habitaciones($bd,$id);
    $datacat = mysqli_fetch_array($concat);
    $destinoUrl = $datacat["url_destino"];
    $categoriaUrl = $datacat["url_categoria"];
    $pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$categoriaUrl.".php";
    if (!unlink($filePath)){}
    echo $consulta = $obj_habitaciones -> eliminar_categoria($bd,$id);
    generarSitemap();
    include("../../php/controllers/cerrar_php.php");
}
function tabla_categorias(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    $obj_cat = new HabitacionesCategoria();
    
    $deletePermission = $_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_HABITACIONES]["p_eliminar"];
    $salida = "";

    $consulta =  $obj_cat->lista_categorias_habitaciones($bd,"");
	while($variables = mysqli_fetch_array($consulta)){
        $status = "color:#8acb87;";
        if($variables["status_categoria"] == 0){
            $status = "color:#ed1c22;";
        }
		if($variables["imagen_categoria"] != ''){
			$variables["imagen_categoria"] = '<img src="../images/destinos/'.$variables["imagen_categoria"].'" alt="'.utf8_encode($variables["nombre_categoria"]).'"/>';
        }
		$salida.= "<tr>";	
		$salida.= "<td>".utf8_encode($variables["nombre_destino"])."</td>";	
		$salida.= "<td style='cursor:pointer' class='editars' onclick='mostrar_categoria(".$variables["id_categoria"].")' data-toggle='modal' data-target='#myModal3'>".'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'.utf8_encode($variables["nombre_categoria"])."</td>";
		$salida.= "<td>".utf8_encode($variables["descripcion_categoria"])."</td>";
        $salida.= "<td>".$variables["imagen_categoria"]."</td>";
        $salida.= "<td style='text-align:center;'>".'<i class="fa fa-circle" style="'.$status.'font-size:23px;"></i>'."</td>";
		$salida.= $deletePermission ? "<td class='anadir".$variables["id_categoria"]."'><p onclick='eliminar_categoria(".$variables["id_categoria"].")' style='cursor:pointer;text-align:center;' class='eliminar'>".'<i class="fa fa-trash" aria-hidden="true"></i>'."</p></td>" : "";
		
		$salida.= "</tr>";
	}
    
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
function mostrar_categoria(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria.php");
    $obj_categoria = new HabitacionesCategoria();
    $id_categoria = $_POST["id"];
    $consulta = $obj_categoria ->lista_categorias_habitaciones($bd,$id_categoria);
    $reg = array();
    while($var = mysqli_fetch_assoc($consulta)){
        $reg[0]['cve'] = $var['id_categoria'];
        $reg[0]['id_destino'] = $var['id_destino'];
        $reg[0]['url'] = $var['url_categoria'];
        $reg[0]['nombre'] = utf8_encode($var['nombre_categoria']);
        $reg[0]['descripcion'] = utf8_encode($var['descripcion_categoria']);
        $reg[0]['descripcion_en'] = utf8_encode($var['descripcion_categoria_en']);
        $reg[0]['imagen'] = $var['imagen_categoria'];
        $reg[0]['status'] = $var['status_categoria'];
        $reg[0]['meta_titulo'] = utf8_encode($var['meta_titulo']);
        $reg[0]['meta_descripcion'] = utf8_encode($var['meta_descripcion']);
        $reg[0]['meta_keywords'] = utf8_encode($var['meta_keywords']);
        $reg[0]['meta_titulo_en'] = utf8_encode($var['meta_titulo_en']);
        $reg[0]['meta_descripcion_en'] = utf8_encode($var['meta_descripcion_en']);
        $reg[0]['meta_keywords_en'] = utf8_encode($var['meta_keywords_en']);
    }
    echo json_encode($reg[0]);
    include("../../php/controllers/cerrar_php.php");
}
function galeria_categoria(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/habitaciones_categoria_galeria.php");
    $obj_cat = new HabitacionesCategoriaGaleria();
    $id = $_POST["id"];
    $consulta = $obj_cat ->lista_galeria($bd,$id);
    $lista = '<div class="element-img-cat" id="dcat_1"></div>';
    $count = 1;
    while($data = mysqli_fetch_array($consulta)){
        $count++;
        $lista.= "<div class='element-img-cat l-img-min-cat' data-name-img='".$data["name"]."' id='dcat_".$count."'><img src='../images/destinos/suites/galeria/".$data["name"]."' id='txt_cat_".$count."' />&nbsp;<span id='rcat_".$count."' onclick='eliminar_galeria_categoria(this.id)' class='remove'><i class='fa fa-trash' aria-hidden='true'></i></span></div>";
    }
    echo $lista;
    include("../../php/controllers/cerrar_php.php");
}
function generarSitemap(){
    include("../../php/controllers/connect_sql.php");
    $obj_categorias = new HabitacionesCategoria();
    $obj_tipo = new HabitacionesTipo();
    $obj_habitaciones = new Habitaciones();
    $result = $obj_categorias->lista_categorias_habitaciones($bd,"");
    $result2 = $obj_tipo->site_tipo_habitaciones($bd,"");
    $result3 = $obj_habitaciones->site_habitaciones($bd,"");

    $sites_categorias = '';
    $sites_tipo = '';
    $sites_habitaciones = '';

    //Categoria
    while($row = mysqli_fetch_array($result)){
        $sites_categorias .= '<url>
                <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row["url_destino"].'/'.$row["url_categoria"].'</loc>
                <priority>0.70</priority>
            </url>
            <url>
                <loc>https://www.pacifica.com.mx/web/en/destination/'.$row["url_destino"].'/'.$row["url_categoria"].'</loc>
                <priority>0.70</priority>
            </url>';
    }
    //Tipo habitación
    while($row2 = mysqli_fetch_array($result2)){
        $sites_tipo .= '<url>
            <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row2["url_destino"].'/'.$row2["url_tipo"].'</loc>
            <priority>0.70</priority>
        </url>
        <url>
            <loc>https://www.pacifica.com.mx/web/en/destination/'.$row2["url_destino"].'/'.$row2["url_tipo"].'</loc>
            <priority>0.70</priority>
        </url>';
    }
    //Habitaciones
    while($row3 = mysqli_fetch_array($result3)){
        $sites_habitaciones .= '<url>
            <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row3["url_destino"].'/'.$row3["url_habitacion"].'</loc>
            <priority>0.70</priority>
        </url>
        <url>
            <loc>https://www.pacifica.com.mx/web/en/destination/'.$row3["url_destino"].'/'.$row3["url_habitacion"].'</loc>
            <priority>0.70</priority>
        </url>';
    }

    $sitemap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
            '.$sites_categorias.'
            '.$sites_tipo.'
            '.$sites_habitaciones.'
        </urlset>';
    $pathToRoot = '../../../';
    $sitemapFileName = 'sitemap_producto.xml';
    $fp = fopen($pathToRoot.$sitemapFileName, "w");
    fputs($fp, $sitemap);
    fclose($fp);
    include("../../php/controllers/cerrar_php.php");
}
function crearArchivo($destinoUrl, $categoriaUrl, $categoriaId){
	$pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/";
    $filePath = $pathToRoot.$targetDir.$categoriaUrl.".php";
    $fp = fopen($filePath, "w+");
    fclose($fp);
    chmod($filePath, 0777);
    $fp = fopen($filePath, "w+");
    $contenidoArchivo = '<?php $id_categoria = '.$categoriaId.'; include("_habitaciones_categoria.php"); ?>';
    fwrite($fp, $contenidoArchivo);
    fclose($fp);

    // $targetDir = "en/destinos/$destinoUrl/";
    // $filePath = $pathToRoot.$targetDir.$categoriaUrl.".php";
    // $fp = fopen($filePath, "w+");
    // fclose($fp);
    // chmod($filePath, 0777);
    // $fp = fopen($filePath, "w+");
    // fwrite($fp, $contenidoArchivo);
    // fclose($fp);
}
function limpiar_url($val){
	$val=strtolower($val);
	$val=str_replace(" ","-",$val);
	$val=str_replace("/","-",$val);
	$val=str_replace("|","-",$val);
	$val=str_replace("@","",$val);
	$val=str_replace("#","",$val);
	$val=str_replace("º","",$val);
	$val=str_replace("ª","",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace('&deg;',"",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace(".","",$val);
	$val=str_replace(",","",$val);
	$val=str_replace("\\","",$val);
	$val=str_replace("á","a",$val);
	$val=str_replace("é","e",$val);
	$val=str_replace("í","i",$val);
	$val=str_replace("ó","o",$val);
	$val=str_replace("ú","u",$val);
	$val=str_replace("ñ","n",$val);
	$val=str_replace("&aacute;","a",$val);
	$val=str_replace("&eacute;","e",$val);
	$val=str_replace("&iacute;","i",$val);
	$val=str_replace("&oacute;","o",$val);
	$val=str_replace("&uacute;","u",$val);
	$val=str_replace("&ntilde;","n",$val);
	$val=str_replace("&39;","",$val);
	$val=str_replace("&iquest;","",$val);
	$val=str_replace('&amp;"',"",$val);
	$val=str_replace("¿","",$val);
	$val=str_replace("&reg;","",$val);
	$val=str_replace("®","",$val);
	$val=str_replace("¡","",$val);
	$val=str_replace("?","",$val);
	$val=str_replace("!","",$val);
	$val=str_replace("'","",$val);
	$val=str_replace("€'","",$val);
	$val=str_replace("&","y",$val);
	$val=str_replace(":","",$val);
	$val=str_replace(";","",$val);
	$val=str_replace("--","-",$val);
    $val=str_replace("-.php",".php",$val);
    $val=str_replace(".php","",$val);
	return $val;
}
?>