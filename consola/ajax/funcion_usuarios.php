<?php 
    @session_start(); 
	include_once("../php/environment.php");
    if(isset($_GET["ev"])){
        switch($_GET["ev"]){
            case 1: cargar_lista_usuarios(); break;
            case 2: cargar_formulario_usuario(); break;
            case 3: guardar_usuario(); break;
            case 4: cargar_tabla_permisos_por_rol(); break;
            case 5: eliminar_usuario(); break;
        }
    }
    function cargar_lista_usuarios(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/usuarios.php");
        $m_usuarios = new Usuarios();
        $deletePermission = $_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_USUARIOS]["p_eliminar"];
        $tableRows = '';
        $result = $m_usuarios->select_usuarios($bd);
        while($row = mysqli_fetch_assoc($result)){
            $row = array_map("utf8_encode", $row);
            $tableRows .= '
                <tr>
                    <td class="triggerEdit" onclick="editar_usuario('.$row["id_login"].');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.$row["nombre_login"].'</td>
                    <td>'.$row["correo_login"].'</td>
                    <td>'.$row["nombre_rol"].'</td>
                    '.($deletePermission ? '<td class="text-align-center"><i class="fa fa-trash deleteIcon" onclick="eliminar_usuario('.$row["id_login"].')"></i></td>' : '').'
                    </tr>
            ';
        }
        $response = '
            <table id="usuariosTable" class="table">
                <thead><tr><th>Usuario</th><th>Correo login</th><th>Rol</th>'.($deletePermission ? '<th class="text-align-center">Eliminar</th>' : '').'</tr></thead>
                <tbody>'.$tableRows.'</tbody>
            </table>
        ';
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
    function cargar_formulario_usuario(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/usuarios.php");
        include("../../php/class/roles.php");
        $m_usuarios = new Usuarios();
        $m_roles = new Roles();
        $id_login = $_POST["id_login"];
        $nombre_login = "";
        $correo_login = "";
        $password_login = "";
        $id_rol = "";
        $inputType = "text";
        $maskIcon = "fa-eye-slash";
        $permisosBlock = '';
        if($id_login != ""){
            $result = $m_usuarios->select_usuarios($bd, $id_login);
            $row = mysqli_fetch_assoc($result);
            $row = array_map("utf8_encode", $row);
            $nombre_login = $row["nombre_login"];
            $correo_login = $row["correo_login"];
            $password_login = $row["password_login"];
            $id_rol = $row["id_rol"];
            $inputType = "password";
            $maskIcon = "fa-eye";
        }
        $rolOptions = '<option value="">Selecciona un rol</option>';
        $result = $m_roles->select_roles($bd);
        while($row = mysqli_fetch_assoc($result)){
            $row = array_map("utf8_encode", $row);
            $rolOptions .= '<option value="'.$row["id_rol"].'" '.($row["id_rol"] == $id_rol ? 'selected' : '').'>'.$row["nombre_rol"].'</option>';
        }
        $response = '
            <form id="usuarioForm" noaction>
                <input type="hidden" name="id_login" value="'.$id_login.'">
                <div class="row">
                    <div class="col-sm-12 col-md-6 form-group">
                        <label>Nombre del Usuario</label>
                        <input type="text" class="form-control" name="nombre_login" value="'.$nombre_login.'" required>
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        <label>Rol</label>
                        <select class="form-control" name="id_rol" required>
                            '.$rolOptions.'
                        </select>
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        <label>Correo login</label>
                        <input type="email" class="form-control" name="correo_login" value="'.$correo_login.'" required>
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        <label>Contraseña login</label>
                        <div class="d-flex">
                            <input type="'.$inputType.'" class="form-control fillFlex" minlength="6" name="password_login" value="'.$password_login.'" autocomplete="off" required>
                            <span class="passwordBtnBlock flexCenterAll">
                                <i class="fa '.$maskIcon.'" id="toggleMaskPassword"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <label>Permisos del Rol</label>
                        <div id="permissionBlock"></div>
                    </div>
                </div>
            </form>
        ';
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
    function guardar_usuario(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/usuarios.php");
        $m_usuarios = new Usuarios();
        $id_login = $_POST["id_login"];
        $nombre_login = utf8_decode($_POST["nombre_login"]);
        $id_rol = $_POST["id_rol"];
        $correo_login = utf8_decode($_POST["correo_login"]);
        $password_login = utf8_decode($_POST["password_login"]);
        $response = 0;
        if($id_login != ""){
            $response = $m_usuarios->update_usuario($bd, $id_login, $nombre_login, $id_rol, $correo_login, $password_login) ? 1 : 0;
        }else{
            $response = $m_usuarios->insert_usuario($bd, $nombre_login, $id_rol, $correo_login, $password_login) ? 1 : 0;
            $id_login = mysqli_insert_id($bd);
        }
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
    function cargar_tabla_permisos_por_rol(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/permisos.php");
        $m_permisos = new Permisos();
        $id_rol = $_POST["id_rol"];
        $response = '';
        if($id_rol != ""){
            $permisosBlock = '';
            $result = $m_permisos->select_permisos($bd, $id_rol);
            while($row = mysqli_fetch_assoc($result)){
                $row = array_map("utf8_encode", $row);
                $permisosBlock .= '
                    <tr>
                        <td>'.$row["nombre_modulo"].'</td>
                        <td>'.($row["p_leer"] == 1 ? '<i class="fa fa-check"></i>' : "").'</td>
                        <td>'.($row["p_crear"] == 1 ? '<i class="fa fa-check"></i>' : "").'</td>
                        <td>'.($row["p_actualizar"] == 1 ? '<i class="fa fa-check"></i>' : "").'</td>
                        <td>'.($row["p_eliminar"] == 1 ? '<i class="fa fa-check"></i>' : "").'</td>
                    </tr>
                ';
            }
            $response = '
                <table class="w-100 permissionTable">
                    <thead><tr><th>Módulo</th><th>Leer/Entrar</th><th>Crear</th><th>Actualizar</th><th>Eliminar</th></tr></thead>
                    <tbody>
                        '.$permisosBlock.'
                    </tbody>
                </table>
            ';
        }
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
    function eliminar_usuario(){
        include("../../php/controllers/connect_sql.php");
        include("../../php/class/usuarios.php");
        $m_usuarios = new Usuarios();
        $id_login = $_POST["id_login"];
        $response = $m_usuarios->delete_usuario($bd, $id_login) ? 1 : 0;
        echo $response;
        include("../../php/controllers/cerrar_php.php");
    }
?>
