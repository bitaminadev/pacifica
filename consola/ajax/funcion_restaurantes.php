<?php @session_start(); 
include_once("../php/environment.php");
if(isset($_GET["ev"])){
	switch($_GET["ev"]){
        case 1: guardar_restaurante(); break;
        case 2: eliminar_restaurante(); break;
        case 3: mostrar_datos(); break;
        case 4: tabla_restaurantes(); break;
        case 5: lista_galeria(); break;
        case 6: consulta_orden_siguiente(); break;
    }
}
function guardar_restaurante(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/restaurantes.php");
    include("../../php/class/restaurantes_galeria.php");
    include("../../php/class/restaurantes_platillos.php");
    $obj_restaurantes = new Restaurantes();
    $obj_restaurantes_gal = new RestaurantesGaleria();
    $obj_restaurantes_pla = new RestaurantesPlatillos();

    $response = '';
    // Info general
    $id = $_POST["id"];
    $status = $_POST["status"];
    $nombre = utf8_decode($_POST["nombre"]);
    $destino = $_POST["destino"];
    $horario = utf8_decode($_POST["horario"]);
    $mensaje = utf8_decode($_POST["mensaje"]);
    $descripcion = $_POST["descripcion"];
    $imagen_principal = utf8_decode($_POST["imagen_principal"]);
    $url = limpiar_url($_POST["url"]);
    $meta_titulo = utf8_decode($_POST["meta_titulo"]);
    $meta_descripcion = utf8_decode($_POST["meta_descripcion"]);
    $meta_keywords = utf8_decode($_POST["meta_keywords"]);

    $horario_en = utf8_decode($_POST["horario_en"]);
    $mensaje_en = utf8_decode($_POST["mensaje_en"]);
    $descripcion_en = $_POST["descripcion_en"];
    $meta_titulo_en = utf8_decode($_POST["meta_titulo_en"]);
    $meta_descripcion_en = utf8_decode($_POST["meta_descripcion_en"]);
    $meta_keywords_en = utf8_decode($_POST["meta_keywords_en"]);
    $imagen_logo = utf8_decode($_POST["imagen_logo"]);

    // Info platillo
    $status_platillo =  $_POST["status_platillo"];
    $nombre_platillo = utf8_decode($_POST["nombre_platillo"]);
    $descripcion_platillo = utf8_decode($_POST["descripcion_platillo"]);
    $imagen_platillo = $_POST["imagen_platillo"];

    $nombre_platillo_en = utf8_decode($_POST["nombre_platillo_en"]);
    $descripcion_platillo_en = utf8_decode($_POST["descripcion_platillo_en"]);
    $destacado_principal = utf8_decode($_POST["destacado_principal"]);

    $orden_general = utf8_decode($_POST["orden_general"]);

    //Galeria
    $galeria = $_POST["galeria"];
    
    if($id == ""){
        $consulta = $obj_restaurantes->registrar_restaurante($bd,$destino,$url,$nombre,$mensaje,$descripcion,$imagen_principal,$horario,$status,$meta_titulo,$meta_descripcion,$meta_keywords,$mensaje_en,$descripcion_en,$horario_en,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$imagen_logo,$orden_general);
        $id = $obj_restaurantes->maximo_restaurante($bd);
        $result = $obj_restaurantes->mostrar_restaurante($bd,$id);
        $row = mysqli_fetch_assoc($result);
        $destinoUrl = $row["url_destino"];
        $restauranteUrl = str_replace(".php", "", $url);
        crearArchivo($destinoUrl, $restauranteUrl, $id);
        $response = $consulta;
    }else{
        $consulta = $obj_restaurantes -> actualizar_restaurante($bd,$id,$destino,$nombre,$mensaje,$descripcion,$imagen_principal,$horario,$status,$meta_titulo,$meta_descripcion,$meta_keywords,$mensaje_en,$descripcion_en,$horario_en,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$imagen_logo,$orden_general);
        $obj_restaurantes_gal -> eliminar_galeria($bd,$id);
        $response = $consulta;
    }
    //Registro o actualización de platillo
    $consultap = $obj_restaurantes_pla -> info_platillo($bd,$id);
    $id_platillo = "";
    if(($consultap->num_rows) > 0){
        $datap = mysqli_fetch_array($consultap);
        $obj_restaurantes_pla->actualizar_platillo($bd,$datap["id_platillo"],$id,$nombre_platillo,$descripcion_platillo,$imagen_platillo,1,$status_platillo,$nombre_platillo_en,$descripcion_platillo_en);
        $id_platillo = $datap["id_platillo"];
    }else{
        $obj_restaurantes_pla->registrar_platillo($bd,$id,$nombre_platillo,$descripcion_platillo,$imagen_platillo,1,$status_platillo,$nombre_platillo_en,$descripcion_platillo_en);
        $id_platillo = $obj_restaurantes_pla->maximo_platillo($bd);
    }
    if($destacado_principal == 1){
        $obj_restaurantes_pla->set_destacado_principal($bd, $id_platillo);
    }
    //Registro Galeria
    for($i=0; $i < count($galeria); $i++){
        $sufix = substr($galeria[$i], 0, 23);
        $tipo = "imagen";
        if($sufix == "https://www.youtube.com"){
            $tipo = "youtube";
        }
        $obj_restaurantes_gal -> registrar_galeria($bd,$id,$galeria[$i], $tipo);
    }
    generarSitemap();
    echo $response;
    include("../../php/controllers/cerrar_php.php");
}
function eliminar_restaurante(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/restaurantes.php");
    $obj_restaurante = new Restaurantes();
    $id = $_POST["id"];
    $result = $obj_restaurante->mostrar_restaurante($bd, $id);
    $row = mysqli_fetch_array($result);
    $destinoUrl = $row["url_destino"];
    $restauranteUrl = $row["url_restaurante"];
    $pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/gastronomia/";
    $filePath = $pathToRoot.$targetDir.$restauranteUrl.".php";
    $deleteFile = unlink($filePath);
    $targetDir = "en/destination/$destinoUrl/gastronomy/";
    $filePath = $pathToRoot.$targetDir.$restauranteUrl.".php";
    $deleteFile = unlink($filePath);
    $consulta = $obj_restaurante -> eliminar_restaurante($bd,$id);
    generarSitemap();
    echo $consulta;
    include("../../php/controllers/cerrar_php.php");
}
function lista_galeria(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/restaurantes_galeria.php");
    $obj_restaurante = new RestaurantesGaleria();
    $id = $_POST["id"];
    $consulta = $obj_restaurante ->galeria_restaurante($bd,$id);
    $lista = '<div class="element-img" id="div_1"></div>';
    $count = 1;
    while($data = mysqli_fetch_array($consulta)){
        $count++;
        if($data["tipo"] == "imagen"){
            $lista.= "
                <div class='element-img l-img-min' data-name-img='".$data["name"]."' id='div_".$count."'>
                    <img src='../images/destinos/gastronomia/galeria/".$data["name"]."' id='txt_".$count."' />&nbsp;
                    <span id='remove_".$count."' onclick='eliminar_galeria(this.id)' class='remove'>
                        <i class='fa fa-trash' aria-hidden='true'></i>
                    </span>
                </div>
            ";
        }else if($data["tipo"] == "youtube"){
            $parts = explode("embed/", $data['name']);
            $videoId = $parts[1];
            $src = 'https://img.youtube.com/vi/'.$videoId.'/0.jpg';
            $lista.= "
                <div class='element-img l-img-min' data-name-img='".$data["name"]."' id='div_".$count."'>
                    <img src='".$src."' id='txt_".$count."' />&nbsp;
                    <a href='".$data["name"]."' class='youtubeLinkGaleria' target='_blank'>
                        <i class='fa fa-youtube-play' aria-hidden='true'></i>
                    </a>
                    <span id='remove_".$count."' onclick='eliminar_galeria(this.id)' class='remove'>
                        <i class='fa fa-trash' aria-hidden='true'></i>
                    </span>
                </div>
            ";
        }
    }
    echo $lista;
    include("../../php/controllers/cerrar_php.php");
}
function mostrar_datos(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/restaurantes.php");
    include("../../php/class/restaurantes_platillos.php");
    
    $obj_restaurante = new Restaurantes();
    $obj_restaurantes_pla = new RestaurantesPlatillos();

    $id = $_POST["id"];
    $consulta = $obj_restaurante ->mostrar_restaurante($bd,$id);
    $reg = array();
    while($var = mysqli_fetch_assoc($consulta)){
        $consultap = $obj_restaurantes_pla->platillo_destacado_restaurante($bd,$id);
        $status_platillo = "";
        $nombre_platillo = "";
        $descripcion_platillo = "";
        $nombre_platillo_en = "";
        $descripcion_platillo_en = "";
        $destacado_principal = 0;
        $imagen_platillo = "";
        if(($consultap->num_rows) > 0){
            $datap = mysqli_fetch_array($consultap); 
            $status_platillo = $datap['status_platillo'];
            $nombre_platillo = utf8_encode($datap['nombre_platillo']);
            $descripcion_platillo = utf8_encode($datap['descripcion_platillo']);
            $nombre_platillo_en = utf8_encode($datap['nombre_platillo_en']);
            $descripcion_platillo_en = utf8_encode($datap['descripcion_platillo_en']);
            $destacado_principal = $datap['destacado_principal'];
            $imagen_platillo = utf8_encode($datap['foto_platillo']);
        }
        $reg[0]['cve'] = $var['id_restaurante'];
        $reg[0]['destino'] = $var['id_destino'];
        $reg[0]['url'] = $var['url_restaurante'];
        $reg[0]['nombre'] = utf8_encode($var['nombre_restaurante']);
        $reg[0]['mensaje'] = utf8_encode($var['mensaje_restaurante']);
        $reg[0]['mensaje_en'] = utf8_encode($var['mensaje_restaurante_en']);
        $reg[0]['descripcion'] = utf8_encode($var['descripcion_restaurante']);
        $reg[0]['descripcion_en'] = utf8_encode($var['descripcion_restaurante_en']);
        $reg[0]['imagen_general'] = $var['imagen_restaurante'];
        $reg[0]['imagen_logo'] = $var['imagen_logo'];
        $reg[0]['horario'] = utf8_encode($var['horario_restaurante']);
        $reg[0]['horario_en'] = utf8_encode($var['horario_restaurante_en']);
        $reg[0]['meta_titulo'] = utf8_encode($var['meta_titulo']);
        $reg[0]['meta_descripcion'] = utf8_encode($var['meta_descripcion']);
        $reg[0]['meta_keywords'] = utf8_encode($var['meta_keywords']);
        $reg[0]['meta_titulo_en'] = utf8_encode($var['meta_titulo_en']);
        $reg[0]['meta_descripcion_en'] = utf8_encode($var['meta_descripcion_en']);
        $reg[0]['meta_keywords_en'] = utf8_encode($var['meta_keywords_en']);
        $reg[0]['status'] = $var['status_restaurante'];
        $reg[0]['orden_general'] = $var['orden_general'];

        $reg[0]['status_platillo'] = $status_platillo;
        $reg[0]['nombre_platillo'] = $nombre_platillo;
        $reg[0]['descripcion_platillo'] = $descripcion_platillo;
        $reg[0]['nombre_platillo_en'] = $nombre_platillo_en;
        $reg[0]['descripcion_platillo_en'] = $descripcion_platillo_en;
        $reg[0]['destacado_principal'] = $destacado_principal;
        $reg[0]['imagen_platillo'] = $imagen_platillo;
    }
    echo json_encode($reg[0]);
    include("../../php/controllers/cerrar_php.php");
}
function tabla_restaurantes(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/restaurantes.php");
    $obj_restaurante = new Restaurantes();
    
    $deletePermission = $_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_RESTAURANTES]["p_eliminar"];
    $salida = "";

    $salida.= "<table class='table'>";
	$salida.= "<thead><tr>";	
	$salida.= "<th>Destino</th>";
    $salida.= "<th>Restaurante</th>";
    $salida.= "<th>Mensaje</th>";
	$salida.= "<th>Imagen principal</th>";
    $salida.= "<th>Estatus</th>";
	$salida.= $deletePermission ? "<th>Eliminar</th>" : "";	
    $salida.= "</tr></thead>";
    
    $salida.= "<tbody>";

    $consulta =  $obj_restaurante->lista_restaurantes($bd,"");
	while($variables = mysqli_fetch_array($consulta)){
        $status = "color:#8acb87;";
        if($variables["status_restaurante"] == 0){
            $status = "color:#ed1c22;";
        }
        if($variables["imagen_restaurante"] != ''){
			$variables["imagen_restaurante"] = '<img src="../images/destinos/gastronomia/general/'.$variables["imagen_restaurante"].'" alt="'.utf8_encode($variables["nombre_restaurante"]).'"/>';
        }
        $salida.= "<tr>";
        $salida.= "<td>".utf8_encode($variables["nombre_destino"])."</td>";		
		$salida.= "<td style='cursor:pointer' class='editars' onclick='traer(".$variables["id_restaurante"].")' data-toggle='modal' data-target='#myModal'>".'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'.utf8_encode($variables["nombre_restaurante"])."</td>";
        $salida.= "<td>".utf8_encode($variables["mensaje_restaurante"])."</td>";
		$salida.= "<td>".$variables["imagen_restaurante"]."</td>";
        $salida.= "<td style='text-align:center;'>".'<i class="fa fa-circle" style="'.$status.'font-size:23px;"></i>'."</td>";
		$salida.= $deletePermission ? "<td class='anadir".$variables["id_restaurante"]."'><p onclick='eliminar_restaurante(".$variables["id_restaurante"].")' style='cursor:pointer;text-align:center;' class='eliminar'>".'<i class="fa fa-trash" aria-hidden="true"></i>'."</p></td>" : "";
		
		$salida.= "</tr>";
	}
	$salida.= "</tbody>";
    $salida.= "</table>";
    
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
// Auxiliary functions
function generarSitemap(){
    include("../../php/controllers/connect_sql.php");
    $obj_restaurantes = new Restaurantes();
    $result = $obj_restaurantes->lista_restaurantes_activos($bd);
    $sites = '';
    while($row = mysqli_fetch_array($result)){
        $sites .= '
            <url>
                <loc>https://www.pacifica.com.mx/web/es/destinos/'.$row["url_destino"].'/gastronomia/'.$row["url_restaurante"].'</loc>
                <priority>0.70</priority>
            </url>
            <url>
                <loc>https://www.pacifica.com.mx/web/en/destination/'.$row["url_destino"].'/gastronomy/'.$row["url_restaurante"].'</loc>
                <priority>0.70</priority>
            </url>
        ';
    }
    $sitemap = 
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
            '.$sites.'
        </urlset>
    ';
    $pathToRoot = '../../../';
    $sitemapFileName = 'sitemap_restaurantes.xml';
    $fp = fopen($pathToRoot.$sitemapFileName, "w");
    fputs($fp, $sitemap);
    fclose($fp);
    include("../../php/controllers/cerrar_php.php");
}
function crearArchivo($destinoUrl, $restauranteUrl, $restauranteId){
	$pathToRoot = '../../';
	$targetDir = "es/destinos/$destinoUrl/gastronomia/";
    $filePath = $pathToRoot.$targetDir.$restauranteUrl.".php";
    $fp = fopen($filePath, "w+");
    fclose($fp);
    chmod($filePath, 0777);
    $fp = fopen($filePath, "w+");
    $contenidoArchivo = '<?php $id_restaurante = '.$restauranteId.'; include("_restaurante_detalle.php"); ?>';
    fwrite($fp, $contenidoArchivo);
    fclose($fp);
    $targetDir = "en/destination/$destinoUrl/gastronomy/";
    $filePath = $pathToRoot.$targetDir.$restauranteUrl.".php";
    $fp = fopen($filePath, "w+");
    fclose($fp);
    chmod($filePath, 0777);
    $fp = fopen($filePath, "w+");
    fwrite($fp, $contenidoArchivo);
    fclose($fp);
}
function consulta_orden_siguiente(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/restaurantes.php");
    $obj_restaurantes = new Restaurantes();
    $max_orden = $obj_restaurantes->obtener_orden_siguiente($bd);
    echo intval($max_orden) + 1;
}
function limpiar_url($val){
	$val=strtolower($val);
    $val=str_replace("-.php",".php",$val);
    $val=str_replace(".php","",$val);
	$val=str_replace(" ","-",$val);
	$val=str_replace("/","-",$val);
	$val=str_replace("|","-",$val);
	$val=str_replace("@","",$val);
	$val=str_replace("#","",$val);
	$val=str_replace("º","",$val);
	$val=str_replace("ª","",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace('&deg;',"",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace(".","",$val);
	$val=str_replace(",","",$val);
	$val=str_replace("\\","",$val);
	$val=str_replace("á","a",$val);
	$val=str_replace("é","e",$val);
	$val=str_replace("í","i",$val);
	$val=str_replace("ó","o",$val);
	$val=str_replace("ú","u",$val);
	$val=str_replace("ñ","n",$val);
	$val=str_replace("&aacute;","a",$val);
	$val=str_replace("&eacute;","e",$val);
	$val=str_replace("&iacute;","i",$val);
	$val=str_replace("&oacute;","o",$val);
	$val=str_replace("&uacute;","u",$val);
	$val=str_replace("&ntilde;","n",$val);
	$val=str_replace("&39;","",$val);
	$val=str_replace("&iquest;","",$val);
	$val=str_replace('&amp;"',"",$val);
	$val=str_replace("¿","",$val);
	$val=str_replace("&reg;","",$val);
	$val=str_replace("®","",$val);
	$val=str_replace("¡","",$val);
	$val=str_replace("?","",$val);
	$val=str_replace("!","",$val);
	$val=str_replace("'","",$val);
	$val=str_replace("€'","",$val);
	$val=str_replace("&","y",$val);
	$val=str_replace(":","",$val);
	$val=str_replace(";","",$val);
	$val=str_replace("--","-",$val);
	return $val;
}
?>