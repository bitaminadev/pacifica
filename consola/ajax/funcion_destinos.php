<?php @session_start(); 
include_once("../php/environment.php");
if(isset($_GET["ev"])){
	switch($_GET["ev"]){
        case 1: guardar_destino(); break;
        case 2: eliminar_destino(); break;
        case 3: mostrar_datos(); break;
        case 4: tabla_destinos(); break;
        case 5: lista_galeria(); break;
    }
}
function guardar_destino(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/destinos.php");
    include("../../php/class/destinos_galeria.php");
    $obj_destinos = new Destinos();
    $obj_galeria = new DestinosGaleria();
    $id = $_POST["id"];
    $status = $_POST["status"];
    $nombre = utf8_decode($_POST["nombre"]);
    $slogan = utf8_decode($_POST["slogan"]);
    $slogan_en = utf8_decode($_POST["slogan_en"]);
    $destacado_gastronomia = utf8_decode($_POST["destacado_gastronomia"]);
    $destacado_gastronomia_en = utf8_decode($_POST["destacado_gastronomia_en"]);
    $mensaje_mapa = utf8_decode($_POST["mensaje_mapa"]);
    $mensaje_mapa_en = utf8_decode($_POST["mensaje_mapa_en"]);
    $descripcion = $_POST["descripcion"];
    $descripcion_en = $_POST["descripcion_en"];
    $img_banner = utf8_decode($_POST["img_banner"]);
    $img_seccion = utf8_decode($_POST["img_seccion"]);
    $img_general = utf8_decode($_POST["img_general"]);
    $img_destacado_gastronomia = utf8_decode($_POST["img_destacado_gastronomia"]);
    $img_mapa = utf8_decode($_POST["img_mapa"]);
    $mapa = utf8_decode($_POST["mapa"]);
    $url = utf8_decode($_POST["url"]);
    $meta_titulo = utf8_decode($_POST["meta_titulo"]);
    $meta_descripcion = utf8_decode($_POST["meta_descripcion"]);
    $meta_keywords = utf8_decode($_POST["meta_keywords"]);
    $meta_titulo_en = utf8_decode($_POST["meta_titulo_en"]);
    $meta_descripcion_en = utf8_decode($_POST["meta_descripcion_en"]);
    $meta_keywords_en = utf8_decode($_POST["meta_keywords_en"]);
    $galeria = $_POST["galeria"];
    
    if($id == ""){
        $consulta = $obj_destinos -> registrar_destino($bd,$url,$nombre,$slogan,$slogan_en,$descripcion,$descripcion_en,$img_banner,$img_seccion,$img_general,$img_destacado_gastronomia,$img_mapa,$mapa,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status,$destacado_gastronomia,$destacado_gastronomia_en,$mensaje_mapa,$mensaje_mapa_en);
        $id = $obj_destinos ->  maximo_destino($bd);
        echo $consulta;
    }else{
        $consulta = $obj_destinos -> actualizar_destino($bd,$id,$nombre,$slogan,$slogan_en,$descripcion,$descripcion_en,$img_banner,$img_seccion,$img_general,$img_destacado_gastronomia,$img_mapa,$mapa,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status,$destacado_gastronomia,$destacado_gastronomia_en,$mensaje_mapa,$mensaje_mapa_en);
        $obj_galeria -> eliminar_galeria($bd,$id);
        echo $consulta;
    }
    for($i=0; $i < count($galeria); $i++){
        $obj_galeria -> registrar_galeria($bd,$id,$galeria[$i]);
    }
    include("../../php/controllers/cerrar_php.php");
}
function eliminar_destino(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/destinos.php");
    $obj_destinos = new Destinos();
	$id = $_POST["id"];
    echo $consulta = $obj_destinos -> eliminar_destino($bd,$id);
    include("../../php/controllers/cerrar_php.php");
}
function lista_galeria(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/destinos_galeria.php");
    $obj_destinos = new DestinosGaleria();
    $id = $_POST["id"];
    $consulta = $obj_destinos ->lista_galeria($bd,$id);
    $lista = '<div class="element-img" id="div_1"></div>';
    $count = 1;
    while($data = mysqli_fetch_array($consulta)){
        $count++;
        $lista.= "<div class='element-img l-img-min' data-name-img='".$data["name"]."' id='div_".$count."'><img src='../images/destinos/".$data["name"]."' id='txt_".$count."' />&nbsp;<span id='remove_".$count."' onclick='eliminar_galeria(this.id)' class='remove'><i class='fa fa-trash' aria-hidden='true'></i></span></div>";
    }
    echo $lista;
    include("../../php/controllers/cerrar_php.php");
}
function mostrar_datos(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/destinos.php");
    $obj_destinos = new Destinos();
    $id = $_POST["id"];
    $consulta = $obj_destinos ->lista_destinos($bd,$id);
    $reg = array();
    while($var = mysqli_fetch_assoc($consulta)){
        $reg[0]['cve'] = $var['id_destino'];
        $reg[0]['url'] = $var['url_destino'];
        $reg[0]['nombre'] = utf8_encode($var['nombre_destino']);
        $reg[0]['slogan'] = utf8_encode($var['slogan_destino']);
        $reg[0]['slogan_en'] = utf8_encode($var['slogan_destino_en']);
        $reg[0]['destacado_gastronomia'] = utf8_encode($var['destacado_gastronomia']);
        $reg[0]['destacado_gastronomia_en'] = utf8_encode($var['destacado_gastronomia_en']);
        $reg[0]['mensaje_mapa'] = utf8_encode($var['mensaje_mapa']);
        $reg[0]['mensaje_mapa_en'] = utf8_encode($var['mensaje_mapa_en']);
        $reg[0]['descripcion'] = utf8_encode($var['descripcion_destino']);
        $reg[0]['descripcion_en'] = utf8_encode($var['descripcion_destino_en']);
        $reg[0]['img_banner'] = $var['img_banner_general'];
        $reg[0]['img_interior'] = $var['img_banner_habitaciones'];
        $reg[0]['img_general'] = $var['img_general'];
        $reg[0]['img_destacado_gastronomia'] = $var['img_destacado_gastronomia'];
        $reg[0]['img_mapa'] = $var['img_mapa'];
        $reg[0]['mapa'] = $var['mapa'];
        $reg[0]['meta_titulo'] = utf8_encode($var['meta_titulo']);
        $reg[0]['meta_descripcion'] = utf8_encode($var['meta_descripcion']);
        $reg[0]['meta_keywords'] = utf8_encode($var['meta_keywords']);
        $reg[0]['meta_titulo_en'] = utf8_encode($var['meta_titulo_en']);
        $reg[0]['meta_descripcion_en'] = utf8_encode($var['meta_descripcion_en']);
        $reg[0]['meta_keywords_en'] = utf8_encode($var['meta_keywords_en']);
        $reg[0]['status'] = $var['status_destino'];
    }
    echo json_encode($reg[0]);
    include("../../php/controllers/cerrar_php.php");
}
function tabla_destinos(){
    include("../../php/controllers/connect_sql.php");
    include("../../php/class/destinos.php");
    $obj_destinos = new Destinos();
    
    $deletePermission = $_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_DESTINOS]["p_eliminar"];
    $salida = "";

    $salida.= "<table class='table'>";
	$salida.= "<thead><tr>";	
	$salida.= "<th>Destino</th>";
    $salida.= "<th>Slogan</th>";
    $salida.= "<th>Imagen principal</th>";
	$salida.= "<th>Banner</th>";
    $salida.= "<th>Imagen interiores</th>";
    $salida.= "<th>Estatus</th>";
	$salida.= $deletePermission ? "<th>Eliminar</th>" : "";	
    $salida.= "</tr></thead>";
    
    $salida.= "<tbody>";

    $consulta =  $obj_destinos->lista_destinos($bd,"");
	while($variables = mysqli_fetch_array($consulta)){
        $status = "color:#8acb87;";
        if($variables["status_destino"] == 0){
            $status = "color:#ed1c22;";
        }
        if($variables["img_general"] != ''){
			$variables["img_general"] = '<img src="../images/destinos/general/'.$variables["img_general"].'" alt="'.utf8_encode($variables["nombre_destino"]).'"/>';
        }
		if($variables["img_banner_general"] != ''){
			$variables["img_banner_general"] = '<img src="../images/destinos/'.$variables["img_banner_general"].'" alt="'.utf8_encode($variables["nombre_destino"]).'"/>';
        }
        if($variables["img_banner_habitaciones"] != ''){
			$variables["img_banner_habitaciones"] = '<img src="../images/destinos/hoteles/'.$variables["img_banner_habitaciones"].'" alt="'.utf8_encode($variables["nombre_destino"]).'"/>';
		}
		$salida.= "<tr>";		
		$salida.= "<td style='cursor:pointer' class='editars' onclick='traer(".$variables["id_destino"].")' data-toggle='modal' data-target='#myModal'>".'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'.utf8_encode($variables["nombre_destino"])."</td>";
        $salida.= "<td>".utf8_encode($variables["slogan_destino"])."</td>";
        $salida.= "<td>".$variables["img_general"]."</td>";
		$salida.= "<td>".$variables["img_banner_general"]."</td>";
        $salida.= "<td>".$variables["img_banner_habitaciones"]."</td>";
        $salida.= "<td style='text-align:center;'>".'<i class="fa fa-circle" style="'.$status.'font-size:23px;"></i>'."</td>";
        if($deletePermission)
            $salida.= "<td class='anadir".$variables["id_destino"]."'><p onclick='eliminar_destino(".$variables["id_destino"].")' style='cursor:pointer;text-align:center;' class='eliminar'>".'<i class="fa fa-trash" aria-hidden="true"></i>'."</p></td>";
		
		$salida.= "</tr>";
	}
	$salida.= "</tbody>";
    $salida.= "</table>";
    
    echo $salida;
    include("../../php/controllers/cerrar_php.php");
}
?>