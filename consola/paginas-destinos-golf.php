<?php @session_start(); 
	include_once("php/environment.php");
	if(!isset($_SESSION["usuario_consola"])){
		header("Location: login.php");
		die();
	}else{
		if(!validateModule(ID_MODULO_CONTENIDO))
			header("Location: index.php");
	}
	include("../php/controllers/connect_sql.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Consola administrativa | Páginas - Destinos - Golf</title>
		<!--Metas-->
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--Archivos js y css-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap-select.css" rel="stylesheet" type="text/css">
		<link href="css/skin.css" rel="stylesheet" type="text/css">
		<link href="css/productos.css" rel="stylesheet" type="text/css">
		<link href="css/paginas.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.datatables.min.js"></script>
		<script src="js/datatables.min.js"></script>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script type="text/javascript" src="js/ajaxupload.3.5.js"></script>
		<script src="js/bootstrap-select.js"></script>
		<script src="js/skin.js"></script>
		<script src='js/pages.js'></script>
	</head>
	<body>
		<div id="wrapper">
		<?php require("commons/header.php"); ?>
		<div class="overlay" onclick='cerrar_menu();'></div>
			<?php require("commons/menu.php"); ?>
			<?php 
				include("../php/class/paginas.php");
				include("../php/class/secciones.php");
				include("../php/class/elementos-galeria.php");
				$m_paginas = new Paginas();
				$m_secciones = new Secciones();
				$m_elementos = new ElementosGaleria();
			?>
			<div id="page-content-wrapper">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
					<span class="hamb-top"></span>
					<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
				</button>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<fieldset>
								<legend>Administración de páginas - Destinos - Golf</legend>

                                <div class="tabla" style="display: none;">
                                    <div class="form-group">
                                        <label>Destino</label>
                                        <select class="form-control" style="width: unset;min-width: 240px;" id="select_page">
                                            <option value="ixtapa_page">Ixtapa</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="ixtapa_page" class="destino_option" style="display: none;" class="col-sm-12">
									<p class="col-xs-4 col-sm-3 boton-header" style="display: none;" data-display-page="ixtapa-home">
										<i class="fa fa-bed" aria-hidden="true"></i>Home
									</p>
								</div>

								<?php 
									$id_pagina = 16;
									$result = $m_paginas->get_pagina($bd, $id_pagina);
									$pagina = mysqli_fetch_assoc($result);
									$pagina = array_map("utf8_encode", $pagina);
									$result = $m_secciones->get_secciones_por_pagina($bd, $id_pagina);
									$secciones = [];
									while($seccion = mysqli_fetch_assoc($result)){
										array_push($secciones, array_map("utf8_encode", $seccion));
									}
								?>
                                <div id="ixtapa-home" style="display:none;" class="tabla page_option">
									<h4 class="form-group">Ixtapa - Golf</h4>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item active">
											<a class="nav-link general active" data-toggle="tab" href="#pagina_generales-ixtapa-home" role="tab">GENERALES</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#pagina_seo-ixtapa-home" data-toggle="tab" role="tab">SEO</a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="pagina_generales-ixtapa-home" role="tabpanel">
											<?php 
												// $groupTextSections = TRUE;
												require("views/contenido_pagina.php");
											?>
										</div>
										<div class="tab-pane" id="pagina_seo-ixtapa-home" role="tabpanel">
											<?php require("views/seo_pagina.php"); ?>
										</div>
									</div>
								</div>

							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php include("../php/controllers/cerrar_php.php"); ?>
