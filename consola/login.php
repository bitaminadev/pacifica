<!DOCTYPE html>
<html lang="ES-es">
	<head>
		<meta charset="UTF-8">
		<title>Pacífica resort | Consola administrativa</title>
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="images/favicon/manifest.json">
		<link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link href="css/login.css" rel="stylesheet" type="text/css" />
        <link href="../css/webfonts/all.min.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet"> 
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	</head>
	<body>
		<?php if(isset($_GET['error'])&&$_GET['error'] == 1)
			echo '
				<div class="alert alert-danger" role="alert">
					<strong>Alerta!</strong> Usuario o contraseña no validos
				</div>
			';
		?>
		<div class="container">
			<form class="form-signin" method="post" name="login" action="php/session/validar-user.php">
				<h2 class="form-signin-heading"><img src="images/logo_pacifica.svg" alt="Consola pacífica" /></h2>
                <div class="containt_pac">
                    <h3 class="subt_lgk"><span>Ingresa tus datos para acceder a la consola</span></h3>
                    <label for="inputEmail" class="sr-only">Usuario</label>
                    <input name="correo" type="email" id="inputEmail" class="form-control frmc-login" placeholder="Correo electrónico" required autofocus>
                    <label for="inputPassword" class="sr-only">Contraseña</label>
                    <input name="password" type="password" id="inputPassword" class="form-control frmc-login" placeholder="Contraseña" required>
                    <div class="dv_accion">
                        <button class="btn_black_dest" type="submit">Iniciar sesión</button>
                    </div>
                </div>
			</form>
		</div>
	</body>
</html>