<?php include_once("php/environment.php"); ?>
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand"><a href="#">Menú</a></li>
                <!-- <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Dashboard principal</a></li> -->
                <?php if(validateModule(ID_MODULO_DESTINOS)){ ?>
                    <li><a href="admin-destinos.php"><i class="fa fa-plane" aria-hidden="true"></i>Destinos</a></li>
                <?php } ?>
                <?php if(validateModule(ID_MODULO_HABITACIONES)){ ?>
                    <li><a href="admin-habitaciones.php"><i class="fa fa-hotel" aria-hidden="true"></i>Habitaciones</a></li>
                <?php } ?>
                <?php if(validateModule(ID_MODULO_RESTAURANTES)){ ?>
    		        <li><a href="admin-restaurantes.php"><i class="fa fa-cutlery" aria-hidden="true"></i>Restaurantes</a></li>
                <?php } ?>
                <?php if(validateModule(ID_MODULO_CONTENIDO)){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-newspaper-o" aria-hidden="true"></i>Contenido de Páginas<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="paginas-home.php"><!--&boxvr;-->&minus;&nbsp;<i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                            <li><a href="paginas-bodas.php"><!--&boxvr;-->&minus;&nbsp;<i class="fa fa-venus-mars" aria-hidden="true"></i>Bodas</a></li>
                            <li><a href="paginas-gastronomia.php"><!--&boxvr;-->&minus;&nbsp;<i class="fa fa-cutlery" aria-hidden="true"></i>Gastronomía</a></li>
                            <li><a href="paginas-entretenimiento.php"><!--&boxvr;-->&minus;&nbsp;<i class="fa fa-ticket" aria-hidden="true"></i>Entretenimiento</a></li>
                            <li><a href="paginas-se-socio.php"><!--&boxvr;-->&minus;&nbsp;<i class="fa fa-vcard-o" aria-hidden="true"></i>Sé Socio</a></li>
                            <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><!--&boxvr;-->&minus;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>Acerca de</a>
                                <ul class="dropdown-menu">
                                    <li><a href="paginas-acerca-de-reconocimientos.php">&nbsp;&nbsp;&nbsp;<!--&boxvr;-->&minus;&nbsp;<i class="fa fa-trophy" aria-hidden="true"></i>Reconocimientos</a></li>
                                    <li><a href="paginas-acerca-de-compromiso.php">&nbsp;&nbsp;&nbsp;<!--&boxur;-->&minus;&nbsp;<i class="fa fa-child" aria-hidden="true"></i>Compromiso</a></li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><!--&boxur;-->&minus;&nbsp;<i class="fa fa-plane" aria-hidden="true"></i>Destinos</a>
                                <ul class="dropdown-menu">
                                    <li><a href="paginas-destinos-gastronomia.php">&nbsp;&nbsp;&nbsp;<!--&boxvr;-->&minus;&nbsp;<i class="fa fa-cutlery" aria-hidden="true"></i>Gastronomía</a></li>
                                    <li><a href="paginas-destinos-que-hacer.php">&nbsp;&nbsp;&nbsp;<!--&boxvr;-->&minus;&nbsp;<i class="fa fa-bicycle" aria-hidden="true"></i>Qué hacer</a></li>
                                    <li><a href="paginas-destinos-bodas.php">&nbsp;&nbsp;&nbsp;<!--&boxvr;-->&minus;&nbsp;<i class="fa fa-venus-mars" aria-hidden="true"></i>Bodas</a></li>
                                    <li><a href="paginas-destinos-golf.php">&nbsp;&nbsp;&nbsp;<!--&boxvr;-->&minus;&nbsp;<i class="fa fa-futbol-o" aria-hidden="true"></i>Golf</a></li>
                                    <li><a href="paginas-destinos-spa.php">&nbsp;&nbsp;&nbsp;<!--&boxvr;-->&minus;&nbsp;<i class="fa fa-heartbeat" aria-hidden="true"></i>Spa</a></li>
                                    <li><a href="paginas-destinos-entretenimiento.php">&nbsp;&nbsp;&nbsp;<!--&boxur;-->&minus;&nbsp;<i class="fa fa-ticket" aria-hidden="true"></i>Entretenimiento</a></li>
                                </ul>
                            </li>
                            <li class="divider"></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if(validateModule(ID_MODULO_CONTACTOS_CONTACTO) || validateModule(ID_MODULO_CONTACTOS_SOCIO) || validateModule(ID_MODULO_CONTACTOS_BODAS)){ ?>
                    <li><a href="admin-contactos.php"><i class="fa fa-phone" aria-hidden="true"></i>Contactos</a></li>
                <?php } ?>
                <?php if(validateModule(ID_MODULO_USUARIOS) || validateModule(ID_MODULO_ROLES)){ ?>
                    <li class="divider"></li>
                    <li class="dropdown open">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i>Configuración<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php if(validateModule(ID_MODULO_USUARIOS)){ ?>
                                <li><a href="config-usuarios.php">−&nbsp;<i aria-hidden="true" class="fa fa-user-circle"></i>Usuarios</a></li>
                            <?php } ?>
                            <?php if(validateModule(ID_MODULO_ROLES)){ ?>
                                <li><a href="config-permisos.php">−&nbsp;<i class="fa fa-braille" aria-hidden="true"></i>Roles/Permisos</a></li>
                            <?php } ?>
                            <li class="divider"></li>
                        </ul>
                    </li>
                <?php } ?>
                <li><a href="#" onclick='logout();'><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
            </ul>
        </nav>
        <style>
            .dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
            .dropdown-submenu:hover>a:after{border-left-color:#555;}
        </style>
        <script type="text/javascript">
            // $(document).ready(function(){
                $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                    event.preventDefault(); 
                    event.stopPropagation(); 
                    $(this).parent().siblings().removeClass('open');
                    $(this).parent().toggleClass('open');
                });
            // });
        </script>
        <!-- /#sidebar-wrapper -->