var table;
$(document).ready(function(){
	cargar();
	/* $('#myModal').on('hidden.bs.modal', function (e) {
		$(".modal-body").html('');
		$(".modal-title").html("");
	}); */
	tinymce.init({
		selector: "#descripcion",  // change this value according to your HTML
		plugin: "a_tinymce_plugin",
		a_plugin_option: true,
		menubar: false,
		a_configuration_option: 400
	});
	tinymce.init({
		selector: "#descripcion_en",  // change this value according to your HTML
		plugin: "a_tinymce_plugin",
		a_plugin_option: true,
		menubar: false,
		a_configuration_option: 400
	});
	subir_imagen_restaurante();
	subir_imagen_galeria();
	subir_imagen_platillo();
});
function cargar(){
	$.ajax({
		url: "ajax/funcion_restaurantes.php?ev=4",
		beforeSend: function(){ },
		success: function(datos){
			if (typeof table != "undefined"){
				table.destroy();
			}
			$(".tabla").html(datos);
			recargar_tabla();
		}
	});
}
function recargar_tabla(){
	cont = 0;
	table = $(".table").DataTable({
		"bStateSave": true,
		"language": {
			"sSearch":"Buscar:",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		"sPaginationType" : "full_numbers","order":[[1,"desc"]],
        "lengthMenu": [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "All"]
        ],
    });
}
function nuevo_registro(){
	$.ajax({
		url: "ajax/funcion_restaurantes.php?ev=6",
		type: "POST",
		success: function(datos){
			console.log(datos);
			clear_data();
			$('#orden_general').val(datos.trim());
			$('#myModal h4.modal-title').html('Nuevo Restaurante');
			$(".enviar_categoria").val("Guardar");		
		}
	});
}
function clear_data(){
	$('#orden_general').val("");
	$("input[name=add_youtube]").val("").trigger("input");
	$("#cve_restaurante").val("");
	$("#nombre_restaurante").val("");
    $("#horario").val("");
    $("#horario_en").val("");
    $("#mensaje").val("");
    $("#mensaje_en").val("");
	tinymce.get("descripcion").setContent("");
	tinymce.get("descripcion_en").setContent("");
	$("#nombre_imagen_principal").val("");
    $("#content_image_principal").html('');
	$("#nombre_imagen_logo").val("");
    $("#content_image_logo").html('');
    $("#nombre_platillo").val("");
    $("#nombre_platillo_en").val("");
    $("#descripcion_platillo").val("");
    $("#descripcion_platillo_en").val("");
	$("#destacado_principal").prop("checked", false);
    $("#nombre_imagen_platillo").val("");
    $("#content_image_platillo").html('');
	$("#url_restaurante").val("");
	document.getElementById("url_restaurante").disabled = false;
	$("#meta_titulo").val("");
	$("#meta_descripcion").val("");
    $("#meta_keywords").val("");
	$("#meta_titulo_en").val("");
	$("#meta_descripcion_en").val("");
    $("#meta_keywords_en").val("");
    $("#lista-imagenes").html(`<div class="element-img" id="div_1"></div>`);
}

$(document).on("input", "input[name=add_youtube]", function(){
    if(this.value == ""){
        $(this).removeClass("wrong").removeClass("correct");
    }else{
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = this.value.match(regExp);
        if(match && match[2].length == 11){
            $(this).val('https://www.youtube.com/embed/' + match[2]);
            $(this).removeClass("wrong");
            $(this).addClass("correct");
        }else{
            $(this).removeClass("correct");
            $(this).addClass("wrong");
        }
    }
});
$(document).on("click", ".addYoutube", function(){
	if($("input[name=add_youtube]", $("#myModal")).hasClass("correct")){
		contenido_galeria($("input[name=add_youtube]", $("#myModal")).val(), "youtube");
		$("input[name=add_youtube]", $("#myModal")).val("").trigger("input");
	}else{
		alert("Ingresa una URL de Youtube válida.")
	}
});


function traer(id){
	if(id > 0){
		$(".modal-title").html("Editar Platillo");
		$("#enviar").val("Actualizar");
	}else{
		$(".modal-title").html("Nuevo Platillo");
		$("#enviar").val("Guardar");
	}
	clear_data();
	$.ajax({
		url: "ajax/funcion_restaurantes.php?ev=3",
		type: "POST",
		data: { id:id},
		beforeSend: function(){ },
		success: function(datos){
			data = JSON.parse(datos);
			console.log(data);
			$("#cve_restaurante").val(data.cve);
            $("#status").val(data.status);
            $("#nombre_restaurante").val(data.nombre);
            $("#destino_restaurante").val(data.destino);
            $("#horario").val(data.horario);
            $("#horario_en").val(data.horario_en);
            $("#mensaje").val(data.mensaje);
            $("#mensaje_en").val(data.mensaje_en);
			tinymce.get("descripcion").setContent(data.descripcion);
			tinymce.get("descripcion_en").setContent(data.descripcion_en);
			$("#nombre_imagen_principal").val(data.imagen_general);
			if(data.imagen_general != ""){
				$("#content_image_principal").html('<img class="img-responsive" src="../images/destinos/gastronomia/general/'+data.imagen_general+'" />');
			}
			$("#nombre_imagen_logo").val(data.imagen_logo);
			if(data.imagen_logo != ""){
				$("#content_image_logo").html('<img class="img-responsive" src="../images/destinos/gastronomia/general/'+data.imagen_logo+'" />');
			}
			$("#url_restaurante").val(data.url);
			document.getElementById("url_restaurante").disabled = true;
			$("#meta_titulo").val(data.meta_titulo);
			$("#meta_descripcion").val(data.meta_descripcion);
            $("#meta_keywords").val(data.meta_keywords);
			$("#meta_titulo_en").val(data.meta_titulo_en);
			$("#meta_descripcion_en").val(data.meta_descripcion_en);
            $("#meta_keywords_en").val(data.meta_keywords_en);
            
            $("#nombre_platillo").val(data.nombre_platillo);
            $("#nombre_platillo_en").val(data.nombre_platillo_en);
            $("#nombre_imagen_platillo").val(data.imagen_platillo);
            if(data.imagen_platillo != ""){
				$("#content_image_platillo").html('<img class="img-responsive" src="../images/destinos/gastronomia/platillos/'+data.imagen_platillo+'" />');
			}
            $("#descripcion_platillo").val(data.descripcion_platillo);
			$("#descripcion_platillo_en").val(data.descripcion_platillo_en);
			$("#destacado_principal").prop("checked", (data.destacado_principal == 1 ? true : false));
            $("#status_platillo").val(data.status_platillo);
			$('#orden_general').val(data.orden_general);
			galeria_db(data.cve);
		}
	});
}
function eliminar_restaurante(restauranteId){
	if(confirm("Estás seguro de eliminar este restaurante?")){
		$.ajax({
			url: "ajax/funcion_restaurantes.php?ev=2",
			type: "POST",
			data: { id:restauranteId, },
			beforeSend: function(){ },
			success: function(datos){
				if(datos == "1"){
					alert("Datos guardados correctamente");
					cargar();
					$('#myModal').modal('hide');
				}else{
					alert("Ocurrió un problema");
				}
			}
		});
	}
}
function ingresar_restaurante(){
	let restaurante = document.getElementById("cve_restaurante").value.trim();
	let status = document.getElementById("status").value.trim();
    let nombre = document.getElementById("nombre_restaurante").value.trim();
    let destino = document.getElementById("destino_restaurante").value.trim();
    let horario = document.getElementById("horario").value.trim();
    let mensaje = document.getElementById("mensaje").value.trim();
    let descripcion = tinyMCE.get("descripcion").getContent();
	let imagen_principal = document.getElementById("nombre_imagen_principal").value.trim(); 
	let url = document.getElementById("url_restaurante").value.trim(); 
	let meta_titulo = document.getElementById("meta_titulo").value.trim(); 
	let meta_descripcion = document.getElementById("meta_descripcion").value.trim(); 
    let meta_keywords = document.getElementById("meta_keywords").value.trim();

    let descripcion_en = tinyMCE.get("descripcion_en").getContent();
	let horario_en = document.getElementById("horario_en").value.trim();
	let mensaje_en = document.getElementById("mensaje_en").value.trim();
	let meta_titulo_en = document.getElementById("meta_titulo_en").value.trim(); 
	let meta_descripcion_en = document.getElementById("meta_descripcion_en").value.trim(); 
	let meta_keywords_en = document.getElementById("meta_keywords_en").value.trim();
	let imagen_logo = document.getElementById("nombre_imagen_logo").value.trim(); 

	let status_platillo = document.getElementById("status_platillo").value.trim();
    let nombre_platillo = document.getElementById("nombre_platillo").value.trim();
    let descripcion_platillo = document.getElementById("descripcion_platillo").value.trim();
    let imagen_platillo = document.getElementById("nombre_imagen_platillo").value.trim();

    let nombre_platillo_en = document.getElementById("nombre_platillo_en").value.trim();
	let descripcion_platillo_en = document.getElementById("descripcion_platillo_en").value.trim();
	let destacado_principal = ($("#destacado_principal").prop("checked") ? 1 : 0);
    let orden_general = document.getElementById("orden_general").value.trim();

	let galeria = new Array();
	$(".l-img-min").each(function(){
		galeria.push($(this).attr('data-name-img'));
    });
	if(nombre == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_restaurante").focus();
	}else if(descripcion == ''){
		alert("Campo obligatorio");
		document.getElementById("descripcion").focus();
	}else if(descripcion_en == ''){
		alert("Campo obligatorio");
		document.getElementById("descripcion_en").focus();
	}else if(imagen_principal == ''){
		alert("Campo obligatorio");
		document.getElementById("btn-up-1").focus();
	}else if(imagen_logo == ''){
		alert("Campo obligatorio");
		document.getElementById("btn-up-3").focus();
	}else {
		$.ajax({
			url: "ajax/funcion_restaurantes.php?ev=1",
			type: "POST",
			data: { id:restaurante, status:status, nombre:nombre, destino:destino, horario:horario, mensaje:mensaje, descripcion:descripcion, imagen_principal:imagen_principal, url:url, meta_titulo:meta_titulo, meta_descripcion:meta_descripcion, meta_keywords:meta_keywords, status_platillo:status_platillo, nombre_platillo:nombre_platillo, descripcion_platillo:descripcion_platillo, imagen_platillo:imagen_platillo, galeria:galeria, descripcion_en:descripcion_en, horario_en:horario_en, mensaje_en:mensaje_en, meta_titulo_en:meta_titulo_en, meta_descripcion_en:meta_descripcion_en, meta_keywords_en:meta_keywords_en, nombre_platillo_en:nombre_platillo_en, descripcion_platillo_en:descripcion_platillo_en, imagen_logo:imagen_logo, orden_general:orden_general, destacado_principal:destacado_principal },
			beforeSend: function(){ },
			success: function(datos){
				if(datos == "1"){
					alert("Datos guardados correctamente");
					cargar();
					$('#myModal').modal('hide');
				}else{
					alert("Ocurrió un problema");
				}
			}
		});
	}
	return false;
}
function subir_imagen_restaurante(){
	var btnUpload=$('#btn-up-1');
	var mestatus=$('#status-up-1');
	new AjaxUpload(btnUpload, {
		action: 'php/upload-images.php?ev=1',
		name: 'uploadfile[]',
		type: 'post',
		data: {dir:"../../images/destinos/gastronomia/general/"},
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
				alert('Solo JPG, JPEG, PNG o GIF');
				return false;
			}
			mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
		},
		onComplete: function(file, response){
			//On completion clear the status
			mestatus.text('');
			if(response!="error"){
				response=response.split("||");
				response[0];
				$("#content_image_principal").html('<img src="../images/destinos/gastronomia/general/'+response[1]+'" height="auto" class="img-responsive" >');
				$("#nombre_imagen_principal").val(response[1]);
			}else{
				alert("Error al subir "+file);
			}
		}
	});
}
function subir_logo_restaurante(){
	var btnUpload=$('#btn-up-3');
	var mestatus=$('#status-up-3');
	new AjaxUpload(btnUpload, {
		action: 'php/upload-images.php?ev=1',
		name: 'uploadfile[]',
		type: 'post',
		data: {dir:"../../images/destinos/gastronomia/general/"},
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
				alert('Solo JPG, JPEG, PNG o GIF');
				return false;
			}
			mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
		},
		onComplete: function(file, response){
			//On completion clear the status
			mestatus.text('');
			if(response!="error"){
				response=response.split("||");
				response[0];
				$("#content_image_logo").html('<img src="../images/destinos/gastronomia/general/'+response[1]+'" height="auto" class="img-responsive" >');
				$("#nombre_imagen_logo").val(response[1]);
			}else{
				alert("Error al subir "+file);
			}
		}
	});
}
function subir_imagen_platillo(){
	var btnUpload=$('#btn-up-2');
		var mestatus=$('#status-up-2');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/gastronomia/platillos/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				if(response!="error"){
					response=response.split("||");
					response[0];
					$("#content_image_platillo").html('<img src="../images/destinos/gastronomia/platillos/'+response[1]+'" height="auto" class="img-responsive" >');
					$("#nombre_imagen_platillo").val(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function subir_imagen_galeria(){
	var btnUpload=$('#btn-up-4');
		var mestatus=$('#status-up-4');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/gastronomia/galeria/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				if(response!="error"){
					response=response.split("||");
					response[0];
					contenido_galeria(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function contenido_galeria(imagen, tipo = "imagen"){
	var total_element = $(".element-img").length;
	var lastid = $(".element-img:last").attr("id");
	var split_id = lastid.split("_");
	var nextindex = Number(split_id[1]) + 1;

	// Adding new div container after last occurance of element class
	$(".element-img:last").after("<div class='element-img l-img-min' data-name-img='"+imagen+"' id='div_"+ nextindex +"'></div>");

	var element = ``;
	if(tipo == "imagen"){
		element = `
			<img src='../images/destinos/gastronomia/galeria/` + imagen + `' id='txt_`+ nextindex +`' />&nbsp;
			<span id='remove_` + nextindex + `' onclick='eliminar_galeria(this.id)' class='remove'>
				<i class='fa fa-trash' aria-hidden='true'></i>
			</span>
		`;
	}else{
		var parts = imagen.split("embed/");
		var videoId = parts[1];
		var src = 'https://img.youtube.com/vi/' + videoId + '/0.jpg';
		element = `
			<img src='` + src + `' id='txt_` + nextindex + `' />&nbsp;
			<a href='` + imagen + `' class='youtubeLinkGaleria' target='_blank'>
				<i class='fa fa-youtube-play' aria-hidden='true'></i>
			</a>
			<span id='remove_` + nextindex + `' onclick='eliminar_galeria(this.id)' class='remove'>
				<i class='fa fa-trash' aria-hidden='true'></i>
			</span>
		`;
	}

	$("#div_" + nextindex).append(element);
}
function eliminar_galeria(id){
	//var id = this.id;
	var split_id = id.split("_");
	var deleteindex = split_id[1];
  
	// Remove <div> with id
	$("#div_" + deleteindex).remove();
	//https://makitweb.com/dynamically-add-and-remove-element-with-jquery/
}
function galeria_db(id){
	$.ajax({
		url: "ajax/funcion_restaurantes.php?ev=5",
		data: { id:id },
		type: "POST",
		success: function(datos){
			$("#lista-imagenes").html(datos);
		}
	});
}