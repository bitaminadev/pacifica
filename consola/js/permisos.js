var table;
$(document).ready(function(){
	cargar_roles();
});
function cargar_roles(){
	$.ajax({
		url: "ajax/funcion_roles.php?ev=1",
		type: "POST",
		data: {},
		success: function(responseText){
			$("#tableContainer").html(responseText);
			recargar_tabla();
		}
	});
}
function recargar_tabla(){
	table = $("#rolesTable").DataTable({
		order:[[1,"asc"]],
		bStateSave: true,
        language: {
            sSearch:        "Buscar&nbsp;&nbsp;",
            sLengthMenu:    "Mostrar&nbsp;&nbsp;_MENU_",
            sZeroRecords:   "No se encontraron resultados",
            sEmptyTable:    "Ningún dato disponible en esta tabla",
            sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:  "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:   "",
            sUrl:           "",
            sInfoThousands: ",",
            sLoadingRecords:"Cargando...",
            oPaginate: {
                sFirst:     "Primero",
                sLast:      "Último",
                sNext:      "Siguiente",
                sPrevious:  "Anterior"
            },
            oAria: {
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
		sPaginationType : "full_numbers",
	});
}
function editar_rol(id_rol){
	$("#modalRol .modal-body").html("");
    $("#saveProducto").prop("disabled", false).find(".waitingIcon").remove();
	$.ajax({
        type: "POST",
        dataType: "text",
        url: "ajax/funcion_roles.php?ev=2",
        data: {id_rol: id_rol},
        success: function(responseText){
            if(id_rol == "") $("#modalRol .modal-title").html("Agregar Rol");
            else $("#modalRol .modal-title").html("Editar Rol");
            $("#modalRol .modal-body").html(responseText);
            $("#modalRol").modal("show");
        }
    });
}
function guardar_rol(){
	var form = $("#rolForm")[0];
    $("#saveRol").prop("disabled", true).append(` <i class="fa fa-spinner fa-spin waitingIcon"></i>`);
    var id_rol = $("[name=id_rol]", form).val();
    var nombre_rol = $("[name=nombre_rol]", form).val().trim();
	var descripcion_rol = $("[name=descripcion_rol]", form).val().trim();
	var permisosCount = $("tr[data-id-modulo] input[type=checkbox]:checked").length;
    var invalidsMessage = '';
	if(nombre_rol == "") invalidsMessage += '\n - Nombre rol';
	if(permisosCount == 0) invalidsMessage += '\n - Al menos un permiso';
    if(invalidsMessage != ""){
        invalidsMessage = 'Es obligatorio:' + invalidsMessage;
        $("#saveRol").prop("disabled", false).find(".waitingIcon").remove();
        alert(invalidsMessage);
    }else{
        var formData = new FormData();
        formData.append("id_rol", id_rol);
		formData.append("nombre_rol", nombre_rol);
		formData.append("descripcion_rol", descripcion_rol);
		var permisos = [];
		$("tr[data-id-modulo]").each((i, item) => {
			permisos.push({
				id_permiso: $(item).data("id-permiso"),
				id_modulo: $(item).data("id-modulo"),
				p_leer: ($("input[type=checkbox]", $("td", item)[1]).prop("checked") ? 1 : 0),
				p_crear: ($("input[type=checkbox]", $("td", item)[2]).prop("checked") ? 1 : 0),
				p_actualizar: ($("input[type=checkbox]", $("td", item)[3]).prop("checked") ? 1 : 0),
				p_eliminar: ($("input[type=checkbox]", $("td", item)[4]).prop("checked") ? 1 : 0),
			});
		})
		formData.append("permisos", JSON.stringify(permisos));
        $.ajax({
            type: 'POST',
            dataType: 'text',
            url: "ajax/funcion_roles.php?ev=3",
            data: formData,
            contentType: false,
            processData: false,
            success: function(responseText){
                console.log(responseText);
                if(responseText.trim() == "1"){
                    alert("Se guardó la información correctamente.");
                    cargar_roles();
                    $("#modalRol").modal("hide");
                }else{
                    alert("Ocurrió un error al guardar la información.");
                }
				$("#saveRol").prop("disabled", false).find(".waitingIcon").remove();
            },
        });
    }
}

