$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
function cerrar_menu(){
	$(".hamburger").click();
}
function logout(){
	$.ajax({
		url: "logout.php",
		success: function(datos){
			window.location.href = "login.php";
		}
	});
}
function EsNumero(e, id) {
	var tecla = (document.all) ? e.keyCode : e.which;
	if (tecla == 13 || tecla == 32 || tecla == 8 || tecla == 0) return true;
	if (tecla == 46) {
		var name = document.getElementById(id).value;
		
		var charExists = (name.indexOf('.') >= 0) ? true : false;
		if (charExists)
		return false;
			
		else
		return true;
	}
	patron = /\d/; // Solo números
	te = String.fromCharCode(tecla); 
	return patron.test(te);

} 
function FormatoDecimales(num, prefix) {
	prefix = prefix || "";
	num = num.replace(/,/g, '');
	var splitStr = num.split(".");
	var splitLeft = splitStr[0];
	var splitRight = splitStr[1];
	if (splitRight != undefined)
		splitRight = "." + splitRight.substring(0, 2);
	else
		splitRight = ".00";

	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)) {
		splitLeft = splitLeft.replace(regx, "$1" + "," + "$2");
	}
	if (splitLeft == "")
		splitLeft = "0";
	if(splitLeft == "0" && splitRight == ".00"){
		return prefix + "";
	}else{
		return prefix + splitLeft + splitRight;
	}
	
}