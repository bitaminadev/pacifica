const IMAGE_MAX_SIZE = 2097152; // 2 MB
const VIDEO_MAX_SIZE = 12582912; // 12 MB
$(document).ready(function(){
    $("#select_page").trigger("change");
    var mgeAux = document.createElement('div');
    mgeAux.innerHTML = modalEliminarElementoGaleria;
    document.body.appendChild(mgeAux.querySelector("div"));
    
    var meAux = document.createElement('div');
    meAux.innerHTML = modalElementoGaleriaExt;
    document.body.appendChild(meAux.querySelector("div"));

    var megyAux = document.createElement('div');
    megyAux.innerHTML = modalElementoGaleriaYoutube;
    document.body.appendChild(megyAux.querySelector("div"));

    $.each($("table.tableEditContent"), function(i, v){
        recargar_tabla($(v).data("id-seccion"));
    });
});
$(document).on("change", "#select_page", function(){
    dests = document.querySelectorAll(".destino_option");
    [].forEach.call(dests, function(dest){
        dest.style.display = "none";
    });
    document.querySelector("#" + this.value).style.display = "block";
    pgs = document.querySelector("#" + this.value).querySelectorAll(".boton-header");
    $(pgs[pgs.length - 1]).trigger("click");
});
$(document).on("click", ".boton-header", function(){
    dests = document.querySelectorAll(".page_option");
    [].forEach.call(dests, function(dest){
        dest.style.display = "none";
    });
    document.querySelector("#" + this.dataset.displayPage).style.display = "block";
    dests = document.querySelectorAll(".boton-header");
    [].forEach.call(dests, function(dest){
        dest.classList.remove("active");
    });
    this.classList.add("active");
});

function recargar_tabla(id_seccion){
	$("table.tableEditContent[data-id-seccion=" + id_seccion + "]").DataTable({
		"order":[[0,"asc"]],
        "dom": 't<"bottom"pi><"clear">',
		"bStateSave": false,
        "lengthChange": false,
        "searching": false,
		"language": {
			// "sSearch":"Buscar:",
			// "sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		"sPaginationType" : "full_numbers",
	});
}
function validate_seo(form){
    button = form.querySelector(".submitButton");
    blockButton(button);
    id_pagina = form.querySelector("[name='id_pagina']").value;
    meta_titulo_es = form.querySelector("[name='meta_titulo_es']").value;
    meta_titulo_en = form.querySelector("[name='meta_titulo_en']").value;
    meta_descripcion_es = form.querySelector("[name='meta_descripcion_es']").value.replace(/\n/g, "");
    meta_descripcion_en = form.querySelector("[name='meta_descripcion_en']").value.replace(/\n/g, "");
    meta_keywords_es = form.querySelector("[name='meta_keywords_es']").value;
    meta_keywords_en = form.querySelector("[name='meta_keywords_en']").value;
    if(form.reportValidity()){
        $.ajax({
			url: "ajax/funcion_paginas.php?ev=1",
			type: "POST",
			data: {id_pagina: id_pagina, meta_titulo_es: meta_titulo_es, meta_titulo_en: meta_titulo_en, meta_descripcion_es: meta_descripcion_es, meta_descripcion_en: meta_descripcion_en, meta_keywords_es: meta_keywords_es, meta_keywords_en: meta_keywords_en},
			success: function(responseText){
                if(responseText.trim() == 1){
                    alert("La información se guardó correctamente.");
                }else{
                    alert("Ocurrió un problema al guardar la información.");
                }
                unblockButton(button);
            }
        });
    }else{
        alert("Completa toda la información.");
        unblockButton(button);
    }
    return false;
}
function blockButton(buttonElement){
    $(buttonElement).prop("disabled", true);
}
function unblockButton(buttonElement){
    $(buttonElement).prop("disabled", false);
}
$(document).on('keypress', 'textarea.noEnter', function(e){
    if((e.keyCode || e.which) == 13){
        return false;
    }
});
function editar_secciones_texto(form){
    button = form.querySelector(".submitButton");
    blockButton(button);
    if(form.reportValidity()){
        id_secciones = [];
        $("[data-id-seccion]", form).filter(function(){ if(!id_secciones.includes($(this).data("id-seccion"))){ id_secciones.push($(this).data("id-seccion")); } });
        secciones = [];
        id_secciones.forEach(function(v){
            secciones.push({
                id_seccion: v,
                contenido_es: $("[data-field=contenido_es][data-id-seccion=" + v + "]", form).val().replace(/\n/g, "<br>"),
                contenido_en: $("[data-field=contenido_en][data-id-seccion=" + v + "]", form).val().replace(/\n/g, "<br>"),
            });
        });
        $.ajax({
			url: "ajax/funcion_paginas.php?ev=2",
			type: "POST",
			data: {secciones: secciones},
			success: function(responseText){
                if(responseText.trim() == 1){
                    alert("La información se guardó correctamente.");
                }else{
                    alert("Ocurrió un problema al guardar la información.");
                }
                unblockButton(button);
            }
        });
    }else{
        alert("Completa toda la información.");
        unblockButton(button);
    }
    return false;
}
function mostrar_bloque_segun_tipo_elemento(tipo_elemento){
    if(tipo_elemento == "imagen"){
        $("#preview_contenido_img").css("display", "block");
        $("#preview_contenido_vid").css("display", "none");
        $("#contenido_cubierta_video_block").css("display", "none");
    }else if(tipo_elemento == "video"){
        $("#preview_contenido_img").css("display", "none");
        $("#preview_contenido_vid").css("display", "block");
        $("#contenido_cubierta_video_block").css("display", "block");
    }
}


function prepare_editar_elemento_galeria_ext(trElement, id_seccion, id_elemento = 0){
    modal = document.querySelector("#editar_elemento_galeria_ext");
    modal.querySelector("input[name=id_seccion]").value = id_seccion;
    modal.querySelector("input[name=id_elemento]").value = id_elemento;
    var tipo_elemento = "imagen";
    var contenido_src_img = "../images/no-image.png", contenido_src_vid = "../images/no-image.png", contenido_cubierta_video_src = "../images/no-image.png";
    var titulo_elemento_es = "", titulo_elemento_en = "";
    var contenido_required = true;
    if(trElement !== null){
        contenido_required = false;
        tipo_elemento = trElement.querySelectorAll("td")[1].textContent;
        if(tipo_elemento == "imagen"){
            contenido_src_img = trElement.querySelectorAll("td")[2].getElementsByTagName("img")[0].src;
        }else if(tipo_elemento == "video"){
            contenido_src_vid = trElement.querySelectorAll("td")[2].getElementsByTagName("source")[0].src;
            contenido_cubierta_video_src = trElement.querySelectorAll("td")[3].getElementsByTagName("img")[0].src;
        }
        titulo_elemento_es = trElement.querySelectorAll("td")[4].textContent;
        titulo_elemento_en = trElement.querySelectorAll("td")[5].textContent;
    }
    mostrar_bloque_segun_tipo_elemento(tipo_elemento);
    modal.querySelector("#preview_contenido_img").src = contenido_src_img;
    modal.querySelector("#preview_contenido_img").dataset.initialSrc = contenido_src_img;
    modal.querySelector("#preview_contenido_vid").querySelector("source").src = contenido_src_vid;
    modal.querySelector("#preview_contenido_vid").querySelector("source").dataset.initialSrc = contenido_src_vid;
    modal.querySelector("#preview_contenido_cubierta_video").src = contenido_cubierta_video_src;
    modal.querySelector("#preview_contenido_cubierta_video").dataset.initialSrc = contenido_cubierta_video_src;
    modal.querySelector("input[name=tipo_elemento]").value = tipo_elemento;
    modal.querySelector("input[name=initial_tipo_elemento]").value = tipo_elemento;
    modal.querySelector("textarea[name=titulo_elemento_es]").value = titulo_elemento_es;
    modal.querySelector("textarea[name=titulo_elemento_en]").value = titulo_elemento_en;
    modal.querySelector("#preview_contenido_vid").load();
    modal.querySelector("input[name=contenido]").required = contenido_required;
    modal.querySelector("input[name=contenido]").value = "";
    modal.querySelector("input[name=contenido_cubierta_video]").value = "";
}
function editar_elemento_galeria_ext(form){
    button = form.querySelector(".submitButton");
    blockButton(button);
    if(form.reportValidity()){
        formData = new FormData(form);
        $.ajax({
            url: "ajax/funcion_paginas.php?ev=3",
            dataType: 'text',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(responseText){
                console.log(responseText);
                data = responseText.split("<-->");
                console.log(data);
                if(data[0].trim() == 1){
                    alert("La información se guardó correctamente.");
                    if(form.querySelector("input[name=id_elemento]").value == 0){
                        form.querySelector("input[name=id_elemento]").value = data[1].trim();
                    }
                    id_elemento = form.querySelector("input[name=id_elemento]").value;
                    trElement = document.querySelector("tr[data-id-elemento='" + id_elemento + "']");
                    contenido = data[3].trim();
                    contenido_cubierta_video = data[4].trim();
                    tipo_elemento = form.querySelector("input[name=tipo_elemento]").value;
                    titulo_elemento_es = form.querySelector("textarea[name=titulo_elemento_es]").value;
                    titulo_elemento_en = form.querySelector("textarea[name=titulo_elemento_en]").value;
                    nombre_subcarpeta = data[2].trim();
                    classs = ``;
                    tag = ``;
                    tag_cubierta = ``;
                    if(tipo_elemento == "imagen"){
                        classs = `na`;
                        ruta_archivo = `../images/` + nombre_subcarpeta + contenido;
                        tag = `<img src="` + ruta_archivo + `" alt="">`;
                        tag_cubierta = `<i class="fa fa-times"></i>&nbsp;No aplica`;
                    }else if(tipo_elemento == "video"){
                        prefix_ruta_video = "../video/" + nombre_subcarpeta;
                        tag = `
                            <video autoplay="" muted="" loop="">
                                <source src="` + prefix_ruta_video + contenido + `" type="video/mp4">
                            </video>
                        `;
                        tag_cubierta = `<img src="../images/` + (contenido_cubierta_video != "" ? nombre_subcarpeta + contenido_cubierta_video : `no-image.png`) + `" alt="">`;
                    }
                    if(trElement === null){ // Agregar nuevo renglón
                        id_seccion = form.querySelector("input[name=id_seccion]").value;
                        trElement = $(`
                            <tr data-id-elemento="` + id_elemento + `">
                                <td style="display: none;">` + id_elemento + `</td>
                                <td>` + tipo_elemento + `</td>
                                <td>` + tag + `</td>
                                <td class="` + classs + `">` + tag_cubierta + `</td>
                                <td>` + titulo_elemento_es + `</td>
                                <td>` + titulo_elemento_en + `</td>
                                <td class="actionIcon" onclick="prepare_editar_elemento_galeria_ext(this.closest('tr'), ` + id_seccion + `, ` + id_elemento + `);" data-toggle="modal" data-target="#modal-elemento-galeria-ext"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>
                                <td class="actionIcon" onclick="prepare_eliminar_elemento_galeria(` + id_seccion + `, ` + id_elemento + `, 'galeria_extended');" data-toggle="modal" data-target="#modal-eliminar-elemento-galeria"><i class="fa fa-trash" aria-hidden="true"></i></td>
                            </tr>
                        `).get(0);
                        tableElement = document.querySelector("table[data-id-seccion='" + id_seccion + "']");
                        $(tableElement).DataTable().destroy();
                        tableElement.appendChild(trElement);
                        recargar_tabla(id_seccion);
                    }else{ // Editar renglón con nueva información
                        trElement.querySelectorAll("td")[1].textContent = tipo_elemento;
                        trElement.querySelectorAll("td")[2].innerHTML = tag;
                        trElement.querySelectorAll("td")[3].classList.value = classs;
                        trElement.querySelectorAll("td")[3].innerHTML = tag_cubierta;
                        trElement.querySelectorAll("td")[4].textContent = titulo_elemento_es;
                        trElement.querySelectorAll("td")[5].textContent = titulo_elemento_en;
                    }
                }else{
                    alert("Ocurrió un problema al guardar la información.");
                    window.location.reload();
                }
                unblockButton(button);
                $("#modal-elemento-galeria-ext").modal("hide");
            }
        });
    }else{
        alert("Completa toda la información.");
        unblockButton(button);
    }
    return false;
}
$(document).on("change", "#editar_elemento_galeria_ext input[type=file]", function(){
    allowedExtensions = this.accept.split(",");
    allowedExtensions = allowedExtensions.map(function(ext){ return ext.trim(); });
    ext = "";
    if(this.value != ""){
        ext = this.value.split(".")[1].toLowerCase();
        if(!(allowedExtensions.includes("." + ext))){
            alert("Tipo de archivo no válido.");
            this.value = "";
        }else{
            fileSize = this.files[0].size;
            if((ext == "mp4" && fileSize > VIDEO_MAX_SIZE) || (ext != "mp4" && fileSize > IMAGE_MAX_SIZE)){
                alert("El tamaño del archivo excede el tamaño permitido.");
                this.value = "";
            }else{
                if(this.closest(".rowTable").querySelector("input[name=initial_tipo_elemento]") !== null){
                    if(ext == "mp4"){
                        document.querySelector("#editar_elemento_galeria_ext").querySelector("input[name=tipo_elemento]").value = "video";
                        previewImage_ext(this, ext);
                    }else{
                        document.querySelector("#editar_elemento_galeria_ext").querySelector("input[name=tipo_elemento]").value = "imagen";
                        previewImage_ext(this, ext);
                    }
                }else{
                    previewImage_ext(this, ext);
                }
            }
        }
    }
    if(this.value == ""){
        prev = this.closest(".rowTable").querySelector("input[name=initial_tipo_elemento]");
        if(prev !== null){
            mostrar_bloque_segun_tipo_elemento(prev.value);
        }
        targets = this.closest(".rowTable").querySelectorAll("[name=previewField]");
        [].forEach.call(targets, function(target){
            if(target.tagName == "IMG"){
                target.src = target.dataset.initialSrc;
            }else{
                target.querySelector("source").src = target.querySelector("source").dataset.initialSrc;
                target.load();
            }
        });
    }
});
function previewImage_ext(inputElement, ext){
    mostrar_bloque_segun_tipo_elemento(document.querySelector("#editar_elemento_galeria_ext").querySelector("input[name=tipo_elemento]").value);
    if(inputElement.files && inputElement.files[0]){
        if(ext == "mp4"){
            target = inputElement.closest(".rowTable").querySelector("video[name=previewField]");
            target.querySelector("source").src = URL.createObjectURL(inputElement.files[0]);
            target.load();
        }else{
            target = inputElement.closest(".rowTable").querySelector("img[name=previewField]");
            var reader = new FileReader();
            reader.onload = function(e){
                target.src = e.target.result;
            }
            reader.readAsDataURL(inputElement.files[0]);
        }
    }
}

function agregar_elemento_galeria(buttonElement){
    id_seccion = buttonElement.dataset.idSeccion;
    document.querySelector("input.add-to-gallery[data-id-seccion='" + id_seccion + "']").click();
}
function agregar_elemento_galeria_youtube(form){
    id_seccion = form.querySelector("input[name=id_seccion]").value;
    button = form.querySelector(".submitButton");
    blockButton(button);
    var flag = false;
    if($("input[type=text]", form).val() != ""){
        if($($("input[type=text]", form)).hasClass("correct")){
            flag = true;
        }
    }
    if(flag){
        formData = new FormData(form);
        $.ajax({
            url: "ajax/funcion_paginas.php?ev=6",
            dataType: 'text',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(responseText){
                data = responseText.split("<-->");
                if(data[0].trim() == 1){
                    alert("La información se guardó correctamente.");
                    id_elemento = data[1].trim();
                    contenido = data[2].trim();
                    src = data[3].trim();
                    galeriaElement = $(`
                        <div class="elemento-galeria" data-id-elemento="` + id_elemento + `">
                            <span class="delete-elemento" onclick="prepare_eliminar_elemento_galeria(` + id_seccion + `, ` + id_elemento + `, 'galeria');" data-toggle="modal" data-target="#modal-eliminar-elemento-galeria"><i class="fa fa-trash" aria-hidden="true"></i></span>
                            <a href="` + contenido + `" class="youtubeLink" target="_blank">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                            <img src="` + src + `" alt="Galería">
                        </div>
                    `).get(0);
                    document.querySelector(".galeria-grid[data-id-seccion='" + id_seccion + "']").appendChild(galeriaElement);
                }else{
                    alert("Ocurrió un problema al guardar la información.");
                }
                unblockButton(button);
                $("#modal-elemento-galeria-youtube").modal("hide");
            }
        });
    }else{
        alert("Ingrese una URL de youtube válida.");
        unblockButton(button);
    }
    return false;
}

$(document).on("change", "input.add-to-gallery[type=file]", function(){
    allowedExtensions = this.accept.split(",");
    allowedExtensions = allowedExtensions.map(function(ext){ return ext.trim(); });
    ext = "";
    id_seccion = this.dataset.idSeccion;
    button = document.querySelector(".add-to-gallery[data-id-seccion='" + id_seccion + "']");
    if(this.value != ""){
        ext = this.value.split(".")[1].toLowerCase();
        if(!(allowedExtensions.includes("." + ext))){
            alert("Tipo de archivo no válido.");
            this.value = "";
        }else{
            fileSize = this.files[0].size;
            if(fileSize > IMAGE_MAX_SIZE){
                alert("El tamaño del archivo excede el tamaño permitido.");
                this.value = "";
            }else{
                // Subir imagen
                formData = new FormData();
                formData.append("id_seccion", id_seccion);
                formData.append("id_elemento", 0);
                formData.append("tipo_elemento", "imagen");
                formData.append("titulo_elemento_es", "");
                formData.append("titulo_elemento_en", "");
                formData.append("contenido", this.files[0]);
                $.ajax({
                    url: "ajax/funcion_paginas.php?ev=3",
                    dataType: 'text',
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function(responseText){
                        data = responseText.split("<-->");
                        if(data[0].trim() == 1){
                            // alert("La información se guardó correctamente.");
                            id_elemento = data[1].trim();
                            contenido = data[3].trim();
                            nombre_subcarpeta = data[2].trim();
                            galeriaElement = $(`
                                <div class="elemento-galeria" data-id-elemento="` + id_elemento + `">
                                    <span class="delete-elemento" onclick="prepare_eliminar_elemento_galeria(` + id_seccion + `, ` + id_elemento + `, 'galeria');" data-toggle="modal" data-target="#modal-eliminar-elemento-galeria"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                    <img src="../images/` + nombre_subcarpeta + contenido + `" alt="Galería">
                                </div>
                            `).get(0);
                            document.querySelector(".galeria-grid[data-id-seccion='" + id_seccion + "']").appendChild(galeriaElement);
                        }else{
                            alert("Ocurrió un problema al guardar el archivo.");
                        }
                        // unblockButton(button);
                    }
                });
            }
        }
    }
    // unblockButton(button);
});
function prepare_elemento_galeria_youtube(buttonElement){
    id_seccion = buttonElement.dataset.idSeccion;
    modal = document.querySelector("#modal-elemento-galeria-youtube");
    modal.querySelector("input[name=id_seccion]").value = id_seccion;
    modal.querySelector("input[name=tipo_elemento]").value = "youtube";
    modal.querySelector("input[name=contenido]").value = "";
    $(modal.querySelector("input[name=contenido]")).trigger("input");
}

function prepare_eliminar_elemento_galeria(id_seccion, id_elemento, tipo_contenido){
    modal = document.querySelector("#modal-eliminar-elemento-galeria");
    modal.querySelector("input[name=id_seccion]").value = id_seccion;
    modal.querySelector("input[name=id_elemento]").value = id_elemento;
    modal.querySelector("input[name=tipo_contenido]").value = tipo_contenido;
}
function eliminar_elemento_galeria(form){
    button = form.querySelector(".submitButtonNoStyles");
    blockButton(button);
    id_seccion = form.querySelector("input[name=id_seccion]").value;
    id_elemento = form.querySelector("input[name=id_elemento]").value;
    tipo_contenido = form.querySelector("input[name=tipo_contenido]").value;
    $.ajax({
        url: "ajax/funcion_paginas.php?ev=4",
        type: "POST",
        data: {id_elemento: id_elemento},
        success: function(responseText){
            if(responseText.trim() == 1){
                alert("Se eliminó el elemento de la galería.");
                if(tipo_contenido == "galeria_extended"){
                    tableElement = document.querySelector("table[data-id-seccion='" + id_seccion + "']");
                    $(tableElement).DataTable().destroy();
                    trElement = document.querySelector("tr[data-id-elemento='" + id_elemento + "']");
                    trElement.parentNode.removeChild(trElement);
                    recargar_tabla(id_seccion);
                }else if(tipo_contenido == "galeria"){
                    galeriaElement = document.querySelector(".elemento-galeria[data-id-elemento='" + id_elemento + "']");
                    galeriaElement.parentNode.removeChild(galeriaElement);
                }
            }else{
                alert("Ocurrió un problema al procesar la información.");
            }
            unblockButton(button);
            $("#modal-eliminar-elemento-galeria").modal("hide");
        }
    });
    return false;
}


function editar_seccion_imagen(form){
    button = form.querySelector(".submitButton");
    blockButton(button);
    var flag = false;
    if($("input[type=file]", form).val() != ""){
        flag = true;
    }else{
        if($("input[type=text]", form).val() != ""){
            if($($("input[type=text]", form)).hasClass("correct")){
                flag = true;
            }
        }
    }
    if(flag){
        formData = new FormData(form);
        $.ajax({
            url: "ajax/funcion_paginas.php?ev=5",
            dataType: 'text',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(responseText){
                console.log(responseText);
                data = responseText.split("<-->");
                if(data[0].trim() == 1){
                    var nombre_subcarpeta = data[1].trim();
                    var contenido_es = data[2].trim();
                    var src = "";
                    if(data[3].trim() == "imagen" || data[3].trim() == "iy_imagen"){
                        src = '../images/' + nombre_subcarpeta + contenido_es;
                    }else if(data[3].trim() == "iy_youtube"){
                        var parts = contenido_es.split("embed/");
                        var videoId = parts[1];
                        src = 'https://img.youtube.com/vi/' + videoId + '/0.jpg';
                    }
                    alert("La información se guardó correctamente.");
                    var imgElement = form.querySelector(".imagePreview img");
                    imgElement.src = src;
                    imgElement.dataset.initialSrc = imgElement.src;
                    $("input[type=file]", form).val("");
                    $("input[type=text]", form).val("").removeClass("wrong").removeClass("correct");
                }else{
                    alert("Ocurrió un problema al guardar el archivo.");
                }
                unblockButton(button);
            }
        });
    }else{
        alert("Selecciona un archivo o ingrese una URL de youtube válida.");
        unblockButton(button);
    }
    return false;
}
$(document).on("change", ".editar-seccion-imagen input[type=file]", function(){
    allowedExtensions = this.accept.split(",");
    allowedExtensions = allowedExtensions.map(function(ext){ return ext.trim(); });
    ext = "";
    if(this.value != ""){
        $(this).siblings("input[name=contenido_es_youtube]").val("").removeClass("wrong").removeClass("correct");
        ext = this.value.split(".")[1].toLowerCase();
        if(!(allowedExtensions.includes("." + ext))){
            alert("Tipo de archivo no válido.");
            this.value = "";
        }else{
            fileSize = this.files[0].size;
            if(fileSize > IMAGE_MAX_SIZE){
                alert("El tamaño del archivo excede el tamaño permitido.");
                this.value = "";
            }else{
                previewImage(this);
            }
        }
    }
    if(this.value == ""){
        target = this.closest("form.editar-seccion-imagen").querySelector(".imagePreview img");
        target.src = target.dataset.initialSrc;
    }
});
function previewImage(inputElement){
    if(inputElement.files && inputElement.files[0]){
        target = inputElement.closest("form.editar-seccion-imagen").querySelector(".imagePreview img");
        var reader = new FileReader();
        reader.onload = function(e){
            target.src = e.target.result;
        }
        reader.readAsDataURL(inputElement.files[0]);
    }
}
function previewImageYoutube(inputElement, previewUrl = ""){
    target = inputElement.closest("form").querySelector(".imagePreview img");
    if(previewUrl == ""){
        previewUrl = target.dataset.initialSrc;
    }
    target.src = previewUrl;
}
$(document).on("input", ".youtubeURL", function(){
    $(this).siblings("input[name=contenido_es]").val("").trigger("change");
    if(this.value == ""){
        $(this).removeClass("wrong").removeClass("correct");
        previewImageYoutube(this);
    }else{
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = this.value.match(regExp);
        if(match && match[2].length == 11){
            $(this).val('https://www.youtube.com/embed/' + match[2]);
            $(this).removeClass("wrong");
            $(this).addClass("correct");
            previewImageYoutube(this, 'https://img.youtube.com/vi/' + match[2] + '/0.jpg');
        }else{
            $(this).removeClass("correct");
            $(this).addClass("wrong");
            previewImageYoutube(this);
        }
    }
});

const modalEliminarElementoGaleria = `
    <div id="modal-eliminar-elemento-galeria" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: crimson; color: white;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">¿Eliminar elemento?</h4>
                </div>
                <div class="modal-body" style="text-align: center; font-size: 1.2em;">
                    Nota: Los archivos se eliminarán de forma permanente del sitio.
                </div>
                <div class="modal-footer">
                    <form onsubmit="return eliminar_elemento_galeria(this);">
                        <input type="hidden" name="id_seccion" value="">
                        <input type="hidden" name="id_elemento" value="">
                        <input type="hidden" name="tipo_contenido" value="">
                        <button class="btn btn-success submitButtonNoStyles">Confirmar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
`;
const modalElementoGaleriaExt = `
    <div id="modal-elemento-galeria-ext" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-pencil" aria-hidden="true"></i>Editar elelemnto de Galería</h4>
                </div>
                    <!-- Nav tabs -->
                <form id="editar_elemento_galeria_ext" onsubmit="return editar_elemento_galeria_ext(this);">
                    <div class="modal-body">
                        <input type="hidden" name="id_seccion" value="">
                        <input type="hidden" name="id_elemento" value="">
                        <input type="hidden" name="tipo_elemento" value="">
                        <div class="form-group col-sm-12">
                            <label>Imagen / Video</label>
                            <table class="rowTable">
                                <tbody>
                                    <tr>
                                        <td class="imagePreview">
                                            <input type="hidden" name="initial_tipo_elemento" value="">
                                            <img name="previewField" id="preview_contenido_img" src="../images/no-image.png" data-initial-src="../images/no-image.png" alt="Imagen contenido">
                                            <video name="previewField" id="preview_contenido_vid" autoplay="" muted="" loop="">
                                                <source src="../images/no-image.png" data-initial-src="../images/no-image.png" type="video/mp4">
                                            </video>
                                        </td>
                                        <td>
                                            <input type="file" class="form-control" name="contenido" accept=".png, .jpeg, .jpg, .gif, .mp4">
                                            <label class="specs_info">*Imagen: Peso máximo: 2 MB</label>
                                            <label class="specs_info">*Video: Peso máximo: 12 MB</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="contenido_cubierta_video_block" class="form-group col-sm-12">
                            <label>Imagen de Cubierta/Preview de video</label>
                            <table class="rowTable">
                                <tbody>
                                    <tr>
                                        <td class="imagePreview">
                                            <img name="previewField" id="preview_contenido_cubierta_video" src="../images/no-image.png" data-initial-src="../images/no-image.png" alt="Imagen contenido">
                                        </td>
                                        <td>
                                            <input type="file" class="form-control" name="contenido_cubierta_video" accept=".png, .jpeg, .jpg, .gif">
                                            <label class="specs_info">*Imagen: Peso máximo: 2 MB</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label><i class="lang-icon lang-es-normal"></i>Título</label>
                            <textarea class="form-control noEnter" rows="3" name="titulo_elemento_es"></textarea>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label><i class="lang-icon lang-en-normal"></i>Título</label>
                            <textarea class="form-control noEnter" rows="3" name="titulo_elemento_en"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="submitButton">Actualizar</button>
                        <p class="cerrar btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
`;
const modalElementoGaleriaYoutube = `
    <div id="modal-elemento-galeria-youtube" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"><i class="fa fa-youtube-play" aria-hidden="true"></i>Agregar video desde Youtube</h4>
                </div>
                <form id="editar_elemento_galeria_youtube" onsubmit="return agregar_elemento_galeria_youtube(this);">
                    <div class="modal-body">
                        <input type="hidden" name="id_seccion" value="">
                        <input type="hidden" name="tipo_elemento" value="">
                        <div id="contenido_cubierta_video_block" class="form-group col-sm-12">
                            <table class="rowTable">
                                <tbody>
                                    <tr>
                                        <td class="imagePreview">
                                            <img name="previewField" src="../images/no-image.png" data-initial-src="../images/no-image.png" alt="Imagen contenido">
                                        </td>
                                        <td>
                                            <input type="text" placeholder="URL de YouTube" class="form-control youtubeURL" name="contenido" required>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="submitButton">Actualizar</button>
                        <p class="cerrar btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
`;