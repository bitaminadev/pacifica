$("document").ready(function(){
	cargar_productos();
	cmb_categorias_tipo();
	cmb_categorias_habitacion();
	subir_imagen_categoria();
	subir_archivo_plano();
	subir_imagen_galeria();
	subir_imagen_galeria_categoria();
	$(function () {
		$('#myTab a:last').tab('show');
	});

	tinymce.init({
		selector: "#descripcion_categoria, #descripcion_categoria_en, #descripcion_tipo_habitacion, #descripcion_tipo_habitacion_en, #descripcion_habitacion, #descripcion_habitacion_en, #detalle_habitacion, #detalle_habitacion_en",  // change this value according to your HTML
		plugin: "a_tinymce_plugin",
		a_plugin_option: true,
		menubar: false,
		a_configuration_option: 400
	});
});
var table;
var table2;
var table3;
function recargar_tabla(nombre){
	cont = 0;
	if(nombre == 'tabla1'){
		table = $("."+nombre+" table").DataTable({
			"order":[[0,"asc"]],
			"bStateSave": true,
			"language": {
				"sSearch":"Buscar:",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"sPaginationType" : "full_numbers",
		});
	}else if(nombre == 'tabla2'){
		table2 = $("."+nombre+" table").DataTable({
			"order":[[0,"asc"]],
			"bStateSave": true,
			"language": {
				"sSearch":"Buscar:",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"sPaginationType" : "full_numbers",
		});
	}else if(nombre == 'tabla3'){
		table3 = $("."+nombre+" table").DataTable({
			"order":[[0,"asc"]],
			"bStateSave": true,
			"language": {
				"sSearch":"Buscar:",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"sPaginationType" : "full_numbers",
		});
	}
}
function cmb_categorias_tipo(){
	let destino = $("#destino_tipo_habitacion").val();
	$.ajax({
		url: "ajax/funcion_filtros.php?ev=1",
		type: "POST",
		async:false,
		data: {Destino:destino},
		beforeSend: function(){ },
		success: function(datos){
			$("#categoria_tipo_habitacion").html(datos);
		}
	});
}
function cmb_categorias_habitacion(){
	let destino = $("#destino_habitacion").val();
	$.ajax({
		url: "ajax/funcion_filtros.php?ev=1",
		type: "POST",
		async: false,
		data: {Destino:destino},
		beforeSend: function(){ },
		success: function(datos){
			$("#categoria_habitacion").html(datos);
			cmb_tipo_habitacion();
		}
	});
}
function cmb_tipo_habitacion(){
	let categoria = $("#categoria_habitacion").val();
	$.ajax({
		url: "ajax/funcion_filtros.php?ev=2",
		type: "POST",
		async: false,
		data: {Categoria:categoria},
		beforeSend: function(){ },
		success: function(datos){
			$("#tipo_habitacion").html(datos);
		}
	});
}
/* CATEGORIAS HABITACIÓN */
function clear_category_product(){
	$("#cve_categoria").val("");
	$("#nombre_categoria").val("");
	tinymce.get("descripcion_categoria").setContent("");
	tinymce.get("descripcion_categoria_en").setContent("");
	$("#nombre_imagen_categoria").val("");
	$("#url_categoria").val("");
	$("#content_image_categoria").html('');
	document.getElementById("url_categoria").disabled = false;
	$("#meta_titulo_categoria").val("");
	$("#meta_descripcion_categoria").val("");
	$("#meta_keywords_categoria").val("");
	$("#meta_titulo_categoria_en").val("");
	$("#meta_descripcion_categoria_en").val("");
	$("#meta_keywords_categoria_en").val("");
	$("#lista-imagenes-categoria").html("");
}
function cargar_categorias(){
	$(".tabla").css("display","none");
	$(".tabla2").css("display","block");
	$.ajax({
		url: "ajax/funcion_hoteles_categorias.php?ev=4",
		beforeSend: function(){ $(".tabla2 tbody").html("<tr><td colspan='5' style='text-align:  center;'><img src='images/iconos/reload.gif' alt='cargando' class='cargandos' /></td></tr>"); },
		success: function(datos){
			$(".boton").removeClass("active");
			$(".cat").addClass("active");
			$("body").removeClass("modal-open");
            if (typeof table2 != "undefined") {
                table2.destroy();
            }
            table2 = '';
			$(".tabla2 table tbody").html(datos);
			recargar_tabla("tabla2");
		}
	});
	return false;
}

function nueva_categoria(){
	$('#myModal3 h4.modal-title').html('Nueva Categoría');
	$(".enviar_categoria").val("Guardar");
	clear_category_product();
}
function mostrar_categoria(cve){
	if(cve > 0){
		$(".modal-title").html("Editar categoría");
		$(".enviar_categoria").val("Actualizar");
	}else{
		$(".modal-title").html("Nueva categoría");
		$(".enviar_categoria").val("Guardar");
	}
	clear_category_product();
	$.ajax({
		url: "ajax/funcion_hoteles_categorias.php?ev=3",
		type: "POST",
		data: { id:cve},
		beforeSend: function(){ },
		success: function(datos){
			data = JSON.parse(datos);
			$("#cve_categoria").val(data.cve);
			$("#status_categoria").val(data.status);
			$("#destino_categoria").val(data.id_destino);
			$("#nombre_categoria").val(data.nombre);
			tinymce.get("descripcion_categoria").setContent(data.descripcion);
			tinymce.get("descripcion_categoria_en").setContent(data.descripcion_en);
			$("#nombre_imagen_categoria").val(data.imagen);
			if(data.imagen != ""){
				$("#content_image_categoria").html('<img class="img-responsive" src="../images/destinos/'+data.imagen+'" />');
			}
			$("#url_categoria").val(data.url);
			document.getElementById("url_categoria").disabled = true;
			$("#meta_titulo_categoria").val(data.meta_titulo);
			$("#meta_descripcion_categoria").val(data.meta_descripcion);
			$("#meta_keywords_categoria").val(data.meta_keywords);
			$("#meta_titulo_categoria_en").val(data.meta_titulo_en);
			$("#meta_descripcion_categoria_en").val(data.meta_descripcion_en);
			$("#meta_keywords_categoria_en").val(data.meta_keywords_en);
			galeria_categoria_db(data.cve);
		}
	});
}
function subir_imagen_categoria(){
	var btnUpload=$('#btn-up');
	var mestatus=$('#status-up');
	new AjaxUpload(btnUpload, {
		action: 'php/upload-images.php?ev=1',
		name: 'uploadfile[]',
		type: 'post',
		data: {dir:"../../images/destinos/"},
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
				alert('Solo JPG, JPEG, PNG o GIF');
				return false;
			}
			mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
		},
		onComplete: function(file, response){
			//On completion clear the status
			mestatus.text('');
			//On completion clear the status
			//files.html('');
			//Add uploaded file to list
			if(response!="error"){
				response=response.split("||");
				response[0];
				$("#content_image_categoria").html('<img src="../images/destinos/'+response[1]+'" height="auto" class="img-responsive" >');
				$("#nombre_imagen_categoria").val(response[1]);
			}else{
				alert("Error al subir "+file);
			}
		}
	});
}
function ingresar_categoria(){
	let categoria = document.getElementById("cve_categoria").value.trim();
	let destino = document.getElementById("destino_categoria").value.trim();
	let status = document.getElementById("status_categoria").value.trim();
	let nombre = document.getElementById("nombre_categoria").value.trim();
	let descripcion = tinyMCE.get("descripcion_categoria").getContent();
	let descripcion_en = tinyMCE.get("descripcion_categoria_en").getContent();
	let imagen = document.getElementById("nombre_imagen_categoria").value.trim();
	let url = document.getElementById("url_categoria").value.trim(); 
	let meta_titulo = document.getElementById("meta_titulo_categoria").value.trim(); 
	let meta_descripcion = document.getElementById("meta_descripcion_categoria").value.trim(); 
	let meta_keywords = document.getElementById("meta_keywords_categoria").value.trim();
	let meta_titulo_en = document.getElementById("meta_titulo_categoria_en").value.trim(); 
	let meta_descripcion_en = document.getElementById("meta_descripcion_categoria_en").value.trim(); 
	let meta_keywords_en = document.getElementById("meta_keywords_categoria_en").value.trim();
	let galeria = new Array();
	$(".l-img-min-cat").each(function(){
		galeria.push($(this).attr('data-name-img'));
	});
	 
	if(nombre == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_categoria").focus();
	}else if(descripcion == ''){		
		alert("Campo obligatorio");
		document.getElementById("descripcion_categoria").focus();
	}else if(imagen == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_imagen_categoria").focus();
	}else {
		$.ajax({
			url: "ajax/funcion_hoteles_categorias.php?ev=1",
			type: "POST",
			data: { id:categoria,destino:destino, status:status, nombre:nombre,descripcion:descripcion,descripcion_en:descripcion_en,imagen:imagen,url:url,meta_titulo:meta_titulo,meta_descripcion:meta_descripcion,meta_keywords:meta_keywords,meta_titulo_en:meta_titulo_en,meta_descripcion_en:meta_descripcion_en,meta_keywords_en:meta_keywords_en,galeria:galeria},
			beforeSend: function(){ },
			success: function(datos){
				if(datos == "1"){
					alert("Datos guardados correctamente");
					cargar_categorias();
					$('#myModal3').modal('hide');
				}else{
					alert("Ocurrió un problema");
				}
			}
		});
	}
	return false;
}
function eliminar_categoria(id){
	if(confirm("¿Deseas eliminar está categoría?")){
		$.ajax({
			url: "ajax/funcion_hoteles_categorias.php?ev=2",
			data: { id:id },
			type: "POST",
			success: function(datos){
				if(datos == 1){
					cargar_categorias();
				}
			}
		});
	}
}
function subir_imagen_galeria_categoria(){
	var btnUpload=$('#btn-up-5');
		var mestatus=$('#status-up-5');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/suites/galeria/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					contenido_galeria_categoria(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function contenido_galeria_categoria(imagen){
	var total_element = $(".element-img-cat").length;
	var lastid = $(".element-img-cat:last").attr("id");
	var split_id = lastid.split("_");
	var nextindex = Number(split_id[1]) + 1;

	// Adding new div container after last occurance of element class
	$(".element-img-cat:last").after("<div class='element-img-cat l-img-min-cat' data-name-img='"+imagen+"' id='dcat_"+ nextindex +"'></div>");

	$("#dcat_" + nextindex).append("<img src='../images/destinos/suites/galeria/"+imagen+"' id='txt_cat_"+ nextindex +"' />&nbsp;<span id='rcat_" + nextindex + "' onclick='eliminar_galeria_categoria(this.id)' class='remove'><i class='fa fa-trash' aria-hidden='true'></i></span>");
}
function eliminar_galeria_categoria(id){
	//var id = this.id;
	var split_id = id.split("_");
	var deleteindex = split_id[1];
  
	// Remove <div> with id
	$("#dcat_" + deleteindex).remove();
}
function galeria_categoria_db(id){
	$.ajax({
		url: "ajax/funcion_hoteles_categorias.php?ev=5",
		data: { id:id },
		type: "POST",
		success: function(datos){
			$("#lista-imagenes-categoria").html(datos);
		}
	});
}
/* ------------- */

/* TIPO DE HABITACIONES */
function clear_type_product(){
	$("#cve_tipo_habitacion").val("");
	$("#nombre_tipo_habitacion").val("");
	$("#mensaje_tipo_habitacion").val("");
	$("#mensaje_tipo_habitacion_en").val("");
	$("#slogan_tipo_habitacion").val("");
	$("#slogan_tipo_habitacion_en").val("");
	tinymce.get("descripcion_tipo_habitacion").setContent("");
	tinymce.get("descripcion_tipo_habitacion_en").setContent("");
	$("#url_tipo").val("");
	document.getElementById("url_tipo").disabled = false;
	$("#meta_titulo_tipo").val("");
	$("#meta_descripcion_tipo").val("");
	$("#meta_keywords_tipo").val("");
	$("#meta_titulo_tipo_en").val("");
	$("#meta_descripcion_tipo_en").val("");
	$("#meta_keywords_tipo_en").val("");
}
function nuevo_tipo_habitacion(){
	$('#myModal2 h4.modal-title').html('Nuevo Tipo habitación');
	$(".enviar_tipo").val("Guardar");
	clear_type_product();
}
function cargar_subcategorias(){
	$(".tabla").css("display","none");
	$(".tabla3").css("display","block");
	$.ajax({
		url: "ajax/funcion_habitaciones_tipo.php?ev=4",
		beforeSend: function(){ $(".tabla3 tbody").html("<tr><td colspan='6' style='text-align:  center;'><img src='images/iconos/reload.gif' alt='cargando' class='cargandos' /></td></tr>"); },
		success: function(datos){
			$(".boton").removeClass("active");
			$(".subcat").addClass("active");
			$("body").removeClass("modal-open");
            if (typeof table3 != "undefined") {
                table3.destroy();
            }
            table3 = '';
			$(".tabla3 table tbody").html(datos);
			recargar_tabla("tabla3");
		}
	});
	return false;
}
function mostrar_tipo_habitacion(cve){
	if(cve > 0){
		$(".modal-title").html("Editar Tipo habitación");
		$(".enviar_tipo").val("Actualizar");
	}else{
		$(".modal-title").html("Nuevo Tipo habitación");
		$(".enviar_tipo").val("Guardar");
	}
	clear_type_product();
	$.ajax({
		url: "ajax/funcion_habitaciones_tipo.php?ev=3",
		type: "POST",
		data: { id:cve},
		beforeSend: function(){ },
		success: function(datos){
			data = JSON.parse(datos);
			$("#cve_tipo_habitacion").val(data.cve);
			$("#status_tipo_habitacion").val(data.status);
			$("#destino_tipo_habitacion").val(data.id_destino);
			cmb_categorias_tipo();
			$("#categoria_tipo_habitacion").val(data.id_categoria);
			$("#nombre_tipo_habitacion").val(data.nombre);
			$("#mensaje_tipo_habitacion").val(data.mensaje);
			$("#slogan_tipo_habitacion").val(data.slogan);
			tinymce.get("descripcion_tipo_habitacion").setContent(data.descripcion);
			$("#mensaje_tipo_habitacion_en").val(data.mensaje_en);
			$("#slogan_tipo_habitacion_en").val(data.slogan_en);
			tinymce.get("descripcion_tipo_habitacion_en").setContent(data.descripcion_en);
			$("#url_tipo").val(data.url);
			document.getElementById("url_tipo").disabled = true;
			$("#meta_titulo_tipo").val(data.meta_titulo);
			$("#meta_descripcion_tipo").val(data.meta_descripcion);
			$("#meta_keywords_tipo").val(data.meta_keywords);
			$("#meta_titulo_tipo_en").val(data.meta_titulo_en);
			$("#meta_descripcion_tipo_en").val(data.meta_descripcion_en);
			$("#meta_keywords_tipo_en").val(data.meta_keywords_en);
		}
	});
}
function ingresar_tipo_habitacion(){
	let tipo = document.getElementById("cve_tipo_habitacion").value.trim();
	let categoria = document.getElementById("categoria_tipo_habitacion").value.trim();
	let status = document.getElementById("status_tipo_habitacion").value.trim();
	let nombre = document.getElementById("nombre_tipo_habitacion").value.trim();
	let mensaje = document.getElementById("mensaje_tipo_habitacion").value.trim();
	let slogan = document.getElementById("slogan_tipo_habitacion").value.trim();
	let descripcion = tinyMCE.get("descripcion_tipo_habitacion").getContent();
	let mensaje_en = document.getElementById("mensaje_tipo_habitacion_en").value.trim();
	let slogan_en = document.getElementById("slogan_tipo_habitacion_en").value.trim();
	let descripcion_en = tinyMCE.get("descripcion_tipo_habitacion_en").getContent();
	let url = document.getElementById("url_tipo").value.trim(); 
	let meta_titulo = document.getElementById("meta_titulo_tipo").value.trim(); 
	let meta_descripcion = document.getElementById("meta_descripcion_tipo").value.trim(); 
	let meta_keywords = document.getElementById("meta_keywords_tipo").value.trim(); 
	let meta_titulo_en = document.getElementById("meta_titulo_tipo_en").value.trim(); 
	let meta_descripcion_en = document.getElementById("meta_descripcion_tipo_en").value.trim(); 
	let meta_keywords_en = document.getElementById("meta_keywords_tipo_en").value.trim(); 
	if(nombre == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_categoria").focus();
	}else if(descripcion == ''){		
		alert("Campo obligatorio");
		document.getElementById("descripcion_categoria").focus();
	}else if(slogan == ''){		
		alert("Campo obligatorio");
		document.getElementById("slogan_tipo_habitacion").focus();
	}else if(mensaje == ''){		
		alert("Campo obligatorio");
		document.getElementById("mensaje_tipo_habitacion").focus();
	}else {
		$.ajax({
			url: "ajax/funcion_habitaciones_tipo.php?ev=1",
			type: "POST",
			data: { id:tipo,categoria:categoria, status:status, nombre:nombre,mensaje:mensaje,mensaje_en:mensaje_en,slogan:slogan,slogan_en:slogan_en,descripcion:descripcion,descripcion_en:descripcion_en,url:url,meta_titulo:meta_titulo,meta_titulo_en:meta_titulo_en,meta_descripcion:meta_descripcion,meta_descripcion_en:meta_descripcion_en,meta_keywords:meta_keywords,meta_keywords_en:meta_keywords_en},
			beforeSend: function(){ },
			success: function(datos){
				if(datos == "1"){
					alert("Datos guardados correctamente");
					cargar_subcategorias();
					$('#myModal2').modal('hide');
				}else{
					alert("Ocurrió un problema");
				}
			}
		});
	}
	return false;
}
function eliminar_tipo_habitacion(id){
	if(confirm("¿Deseas eliminar esté tipo de habitación?")){
		$.ajax({
			url: "ajax/funcion_habitaciones_tipo.php?ev=2",
			data: { id:id },
			type: "POST",
			success: function(datos){
				if(datos == 1){
					cargar_subcategorias();
				}
			}
		});
	}
}
/* ------------------ */

/* HABITACIONES */
function clear_product(){
	$("#cve_habitacion").val("");
	$("#nombre_habitacion").val("");
	$("#slogan_habitacion").val("");
	$("#mensaje_habitacion").val("");
	tinymce.get("descripcion_habitacion").setContent("");
	tinymce.get("detalle_habitacion").setContent("");
	$("#slogan_habitacion_en").val("");
	$("#mensaje_habitacion_en").val("");
	tinymce.get("descripcion_habitacion_en").setContent("");
	tinymce.get("detalle_habitacion_en").setContent("");
	$("#precio_general").val("");
	$("#precio_miembros").val("");
	$("#plano").val("");
	$("#url_habitacion").val("");
	document.getElementById("url_habitacion").disabled = false;
	$("#meta_titulo_habitacion").val("");
	$("#meta_descripcion_habitacion").val("");
	$("#meta_keywords_habitacion").val("");
	$("#meta_titulo_habitacion_en").val("");
	$("#meta_descripcion_habitacion_en").val("");
	$("#meta_keywords_habitacion_en").val("");
	$("#lista-imagenes").html("");
}
function nuevo_producto(){
	$('#myModal h4.modal-title').html('Nueva habitación');
	$(".enviar_habitacion").val("Guardar");
	clear_product();
}
function cargar_productos(){
    $(".tabla").css("display","none");
	$(".tabla1").css("display","block");
	$.ajax({
		url: "ajax/funcion_habitaciones.php?ev=4",
		beforeSend: function(){ $(".tabla1 tbody").html("<tr><td colspan='11' style='text-align:  center;'><img src='images/iconos/reload.gif' alt='cargando' class='cargandos' /></td></tr>"); },
		success: function(datos){
			$(".boton").removeClass("active");
			$(".prod").addClass("active");
			$(".modal-backdrop.fade.in").remove();
			$("body").removeClass("modal-open");/**/
            if (typeof table != "undefined") {
                table.destroy();
            }
            table = '';
			$(".tabla1 table tbody").html(datos);
			recargar_tabla("tabla1");
		}
	});
	return false;
}
function mostrar_habitacion(cve){
	if(cve > 0){
		$(".modal-title").html("Editar Habitación");
		$(".enviar_habitacion").val("Actualizar");
	}else{
		$(".modal-title").html("Nueva Habitación");
		$(".enviar_habitacion").val("Guardar");
	}
	clear_product();
	$.ajax({
		url: "ajax/funcion_habitaciones.php?ev=3",
		type: "POST",
		data: { id:cve},
		beforeSend: function(){ },
		success: function(datos){
			data = JSON.parse(datos);
			$("#cve_habitacion").val(data.cve);
			$("#status_habitacion").val(data.status);
			$("#destino_habitacion").val(data.id_destino);
			cmb_categorias_habitacion();
			$("#categoria_habitacion").val(data.id_categoria);
			cmb_tipo_habitacion();
			$("#tipo_habitacion").val(data.id_tipo);
			$("#nombre_habitacion").val(data.nombre);
			$("#slogan_habitacion").val(data.slogan);
			$("#mensaje_habitacion").val(data.mensaje);
			tinymce.get("descripcion_habitacion").setContent(data.descripcion);
			tinymce.get("detalle_habitacion").setContent(data.detalles);
			$("#slogan_habitacion_en").val(data.slogan_en);
			$("#mensaje_habitacion_en").val(data.mensaje_en);
			tinymce.get("descripcion_habitacion_en").setContent(data.descripcion_en);
			tinymce.get("detalle_habitacion_en").setContent(data.detalles_en);
			$("#precio_general").val(data.precio_general);
			$("#precio_miembros").val(data.precio_miembros);
			$("#plano").val(data.plano);
			$("#url_habitacion").val(data.url);
			document.getElementById("url_habitacion").disabled = true;
			$("#meta_titulo_habitacion").val(data.meta_titulo);
			$("#meta_descripcion_habitacion").val(data.meta_descripcion);
			$("#meta_keywords_habitacion").val(data.meta_keywords);
			$("#meta_titulo_habitacion_en").val(data.meta_titulo_en);
			$("#meta_descripcion_habitacion_en").val(data.meta_descripcion_en);
			$("#meta_keywords_habitacion_en").val(data.meta_keywords_en);
			galeria_habitaciones_db(data.cve);
		}
	});
}
function subir_archivo_plano(){
	var btnUpload=$('#btn-up-2');
		var mestatus=$('#status-up-2');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/hoteles/habitaciones/plano/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif|pdf|doc|docx|xls)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					$("#plano").val(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function subir_imagen_galeria(){
	var btnUpload=$('#btn-up-4');
		var mestatus=$('#status-up-4');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/hoteles/habitaciones/galeria/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				console.log(response);
				if(response!="error"){
					response=response.split("||");
					response[0];
					contenido_galeria(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function contenido_galeria(imagen){
	var total_element = $(".element-img").length;
	var lastid = $(".element-img:last").attr("id");
	var split_id = lastid.split("_");
	var nextindex = Number(split_id[1]) + 1;

	// Adding new div container after last occurance of element class
	$(".element-img:last").after("<div class='element-img l-img-min' data-name-img='"+imagen+"' id='div_"+ nextindex +"'></div>");

	$("#div_" + nextindex).append("<img src='../images/destinos/hoteles/habitaciones/galeria/"+imagen+"' id='txt_"+ nextindex +"' />&nbsp;<span id='remove_" + nextindex + "' onclick='eliminar_galeria(this.id)' class='remove'><i class='fa fa-trash' aria-hidden='true'></i></span>");
}
function eliminar_galeria(id){
	//var id = this.id;
	var split_id = id.split("_");
	var deleteindex = split_id[1];
  
	// Remove <div> with id
	$("#div_" + deleteindex).remove();
}
function galeria_habitaciones_db(id){
	$.ajax({
		url: "ajax/funcion_habitaciones.php?ev=5",
		data: { id:id },
		type: "POST",
		success: function(datos){
			$("#lista-imagenes").html(datos);
		}
	});
}
function ingresar_habitacion(){
	let habitacion = document.getElementById("cve_habitacion").value.trim();
	let tipo = document.getElementById("tipo_habitacion").value.trim();
	let status = document.getElementById("status_habitacion").value.trim();
	let nombre = document.getElementById("nombre_habitacion").value.trim();
	let mensaje = document.getElementById("mensaje_habitacion").value.trim();
	let slogan = document.getElementById("slogan_habitacion").value.trim();
	let descripcion = tinyMCE.get("descripcion_habitacion").getContent();
	let detalle = tinyMCE.get("detalle_habitacion").getContent();
	let mensaje_en = document.getElementById("mensaje_habitacion_en").value.trim();
	let slogan_en = document.getElementById("slogan_habitacion_en").value.trim();
	let descripcion_en = tinyMCE.get("descripcion_habitacion_en").getContent();
	let detalle_en = tinyMCE.get("detalle_habitacion_en").getContent();
	let precio_general = document.getElementById("precio_general").value.trim();
	let precio_miembros = document.getElementById("precio_miembros").value.trim();
	let plano = document.getElementById("plano").value.trim();
	let url = document.getElementById("url_habitacion").value.trim(); 
	let meta_titulo = document.getElementById("meta_titulo_habitacion").value.trim(); 
	let meta_descripcion = document.getElementById("meta_descripcion_habitacion").value.trim(); 
	let meta_keywords = document.getElementById("meta_keywords_habitacion").value.trim();
	let meta_titulo_en = document.getElementById("meta_titulo_habitacion_en").value.trim(); 
	let meta_descripcion_en = document.getElementById("meta_descripcion_habitacion_en").value.trim(); 
	let meta_keywords_en = document.getElementById("meta_keywords_habitacion_en").value.trim();
	let galeria = new Array();
	$(".l-img-min").each(function(){
		galeria.push($(this).attr('data-name-img'));
    });
	if(nombre == ''){				
		alert("Campo obligatorio: Habitación");
		document.getElementById("nombre_habitacion").focus();
	}else if(descripcion == ''){		
		alert("Campo obligatorio: Descripción");
		document.getElementById("descripcion_habitacion").focus();
	}else if(slogan == ''){		
		alert("Campo obligatorio: Slogan");
		document.getElementById("slogan_habitacion").focus();
	}else if(mensaje == ''){		
		alert("Campo obligatorio: Mensaje general");
		document.getElementById("mensaje_habitacion").focus();
	}else {
		$.ajax({
			url: "ajax/funcion_habitaciones.php?ev=1",
			type: "POST",
			data: { id:habitacion,tipo:tipo, status:status, nombre:nombre,mensaje:mensaje,mensaje_en:mensaje_en,slogan:slogan,slogan_en:slogan_en,detalle:detalle,detalle_en:detalle_en,descripcion:descripcion,descripcion_en:descripcion_en,precio_general:precio_general,precio_miembros:precio_miembros,plano:plano,url:url,meta_titulo:meta_titulo,meta_titulo_en:meta_titulo_en,meta_descripcion:meta_descripcion,meta_descripcion_en:meta_descripcion_en,meta_keywords:meta_keywords,meta_keywords_en:meta_keywords_en,galeria:galeria},
			beforeSend: function(){ },
			success: function(datos){
				if(datos == "1"){
					alert("Datos guardados correctamente");
					cargar_productos();
					$('#myModal').modal('hide');
				}else{
					alert("Ocurrió un problema");
				}
			}
		});
	}
	return false;
}
function eliminar_habitacion(id){
	if(confirm("¿Deseas eliminar está habitación?")){
		$.ajax({
			url: "ajax/funcion_habitaciones.php?ev=2",
			data: { id:id },
			type: "POST",
			success: function(datos){
				if(datos == 1){
					cargar_productos();
				}
			}
		});
	}
}
/* ------------- */