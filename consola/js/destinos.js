var table;
$(document).ready(function(){
	cargar();
	/* $('#myModal').on('hidden.bs.modal', function (e) {
		$(".modal-body").html('');
		$(".modal-title").html("");
	}); */
	tinymce.init({
		selector: "#descripcion, #descripcion_en",  // change this value according to your HTML
		plugin: "a_tinymce_plugin",
		a_plugin_option: true,
		menubar: false,
		a_configuration_option: 400
	});
	subir_imagen_banner();
	subir_imagen_seccion();
	subir_imagen_galeria();
	subir_archivo_mapa();
	subir_imagen_general();
	subir_imagen_destacado_gastronomia();
	subir_imagen_mapa();
});
function cargar(){
	$.ajax({
		url: "ajax/funcion_destinos.php?ev=4",
		beforeSend: function(){ },
		success: function(datos){
			if (typeof table != "undefined"){
				table.destroy();
			}
			$(".tabla").html(datos);
			recargar_tabla();
		}
	});
}
function recargar_tabla(){
	cont = 0;
	table = $(".table").DataTable({
		"bStateSave": true,
		"language": {
			"sSearch":"Buscar:",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		"sPaginationType" : "full_numbers","order":[[1,"desc"]],
        "lengthMenu": [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "All"]
        ],
    });
}
function nuevo_registro(){
	$('#myModal h4.modal-title').html('Nuevo Destino');
	$(".enviar_categoria").val("Guardar");
}
function clear_destiny(){
	$("#cve_destino").val("");
	$("#nombre_destino").val("");
	$("#slogan_destino").val("");
	$("#slogan_destino_en").val("");
	$("#destacado_gastronomia").val("");
	$("#destacado_gastronomia_en").val("");
	$("#mensaje_mapa").val("");
	$("#mensaje_mapa_en").val("");
	tinymce.get("descripcion").setContent("");
	tinymce.get("descripcion_en").setContent("");
	$("#nombre_imagen_banner").val("");
	$("#content_image_banner").html('');
	$("#nombre_imagen_seccion").val("");
	$("#content_image_seccion").html('');
	$("#nombre_imagen_general").val("");
	$("#content_image_general").html('');
	$("#nombre_imagen_destacado_gastronomia").val("");
	$("#content_image_destacado_gastronomia").html('');
	$("#nombre_imagen_mapa").val("");
	$("#content_image_mapa").html('');
	$("#mapa").val("");
	$("#url_destino").val("");
	document.getElementById("url_destino").disabled = false;
	$("#meta_titulo").val("");
	$("#meta_descripcion").val("");
	$("#meta_keywords").val("");
	$("#meta_titulo_en").val("");
	$("#meta_descripcion_en").val("");
	$("#meta_keywords_en").val("");
}
function traer(id){
	if(id > 0){
		$(".modal-title").html("Editar Destino");
		$("#enviar").val("Actualizar");
	}else{
		$(".modal-title").html("Nuevo Destino");
		$("#enviar").val("Guardar");
	}
	clear_destiny();
	$.ajax({
		url: "ajax/funcion_destinos.php?ev=3",
		type: "POST",
		data: { id:id},
		beforeSend: function(){ },
		success: function(datos){
			data = JSON.parse(datos);
			$("#cve_destino").val(data.cve);
			$("#status").val(data.status);
			$("#nombre_destino").val(data.nombre);
			$("#slogan_destino").val(data.slogan);
			$("#slogan_destino_en").val(data.slogan_en);
			$("#destacado_gastronomia").val(data.destacado_gastronomia);
			$("#destacado_gastronomia_en").val(data.destacado_gastronomia_en);
			$("#mensaje_mapa").val(data.mensaje_mapa);
			$("#mensaje_mapa_en").val(data.mensaje_mapa_en);
			tinymce.get("descripcion").setContent(data.descripcion);
			tinymce.get("descripcion_en").setContent(data.descripcion_en);
			$("#nombre_imagen_banner").val(data.img_banner);
			if(data.img_banner != ""){
				$("#content_image_banner").html('<img class="img-responsive" src="../images/destinos/'+data.img_banner+'" />');
			}
			$("#nombre_imagen_seccion").val(data.img_interior);
			if(data.img_interior != ""){
				$("#content_image_seccion").html('<img class="img-responsive" src="../images/destinos/hoteles/'+data.img_interior+'" />');
			}
			$("#nombre_imagen_general").val(data.img_general);
			if(data.img_general != ""){
				$("#content_image_general").html('<img class="img-responsive" src="../images/destinos/general/'+data.img_general+'" />');
			}
			$("#nombre_imagen_destacado_gastronomia").val(data.img_destacado_gastronomia);
			if(data.img_destacado_gastronomia != ""){
				$("#content_image_destacado_gastronomia").html('<img class="img-responsive" src="../images/destinos/general/'+data.img_destacado_gastronomia+'" />');
			}
			$("#nombre_imagen_mapa").val(data.img_mapa);
			if(data.img_mapa != ""){
				$("#content_image_mapa").html('<img class="img-responsive" src="../images/destinos/explora/'+data.img_mapa+'" />');
			}
			$("#mapa").val(data.mapa);
			$("#url_destino").val(data.url);
			document.getElementById("url_destino").disabled = true;
			$("#meta_titulo").val(data.meta_titulo);
			$("#meta_descripcion").val(data.meta_descripcion);
			$("#meta_keywords").val(data.meta_keywords);
			$("#meta_titulo_en").val(data.meta_titulo_en);
			$("#meta_descripcion_en").val(data.meta_descripcion_en);
			$("#meta_keywords_en").val(data.meta_keywords_en);
			galeria_db(data.cve);
		}
	});
}
function ingresar_destino(){
	let destino = document.getElementById("cve_destino").value.trim();
	let status = document.getElementById("status").value.trim();
	let nombre = document.getElementById("nombre_destino").value.trim();
	let slogan = document.getElementById("slogan_destino").value.trim();
	let slogan_en = document.getElementById("slogan_destino_en").value.trim();
	let destacado_gastronomia = document.getElementById("destacado_gastronomia").value.trim();
	let destacado_gastronomia_en = document.getElementById("destacado_gastronomia_en").value.trim();
	let mensaje_mapa = document.getElementById("mensaje_mapa").value.trim();
	let mensaje_mapa_en = document.getElementById("mensaje_mapa_en").value.trim();
	let descripcion = tinyMCE.get("descripcion").getContent();
	let descripcion_en = tinyMCE.get("descripcion_en").getContent();
	let img_banner = document.getElementById("nombre_imagen_banner").value.trim();
	let img_seccion = document.getElementById("nombre_imagen_seccion").value.trim();
	let img_general = document.getElementById("nombre_imagen_general").value.trim();
	let img_destacado_gastronomia = document.getElementById("nombre_imagen_destacado_gastronomia").value.trim();
	let img_mapa = document.getElementById("nombre_imagen_mapa").value.trim();
	let mapa = document.getElementById("mapa").value.trim(); 
	let url = document.getElementById("url_destino").value.trim(); 
	let meta_titulo = document.getElementById("meta_titulo").value.trim(); 
	let meta_descripcion = document.getElementById("meta_descripcion").value.trim(); 
	let meta_keywords = document.getElementById("meta_keywords").value.trim(); 
	let meta_titulo_en = document.getElementById("meta_titulo_en").value.trim(); 
	let meta_descripcion_en = document.getElementById("meta_descripcion_en").value.trim(); 
	let meta_keywords_en = document.getElementById("meta_keywords_en").value.trim(); 
	let galeria = new Array();
	$(".l-img-min").each(function(){
		galeria.push($(this).attr('data-name-img'));
    });
	if(nombre == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_destino").focus();
	}else if(slogan == ''){		
		alert("Campo obligatorio");
		document.getElementById("slogan_destino").focus();
	}else if(slogan_en == ''){		
		alert("Campo obligatorio");
		document.getElementById("slogan_destino_en").focus();
	}else if(destacado_gastronomia == ''){		
		alert("Campo obligatorio");
		document.getElementById("destacado_gastronomia").focus();
	}else if(destacado_gastronomia_en == ''){		
		alert("Campo obligatorio");
		document.getElementById("destacado_gastronomia_en").focus();
	}else if(mensaje_mapa == ''){		
		alert("Campo obligatorio");
		document.getElementById("mensaje_mapa").focus();
	}else if(mensaje_mapa_en == ''){		
		alert("Campo obligatorio");
		document.getElementById("mensaje_mapa_en").focus();
	}else if(descripcion == ''){		
		alert("Campo obligatorio");
		document.getElementById("descripcion").focus();
	}else if(img_banner == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_imagen_banner").focus();
	}else if(img_seccion == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_imagen_seccion").focus();
	}else if(img_general == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_imagen_general").focus();
	}else if(img_destacado_gastronomia == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_imagen_destacado_gastronomia").focus();
	}else if(img_mapa == ''){		
		alert("Campo obligatorio");
		document.getElementById("nombre_imagen_mapa").focus();
	}else {
		$.ajax({
			url: "ajax/funcion_destinos.php?ev=1",
			type: "POST",
			data: { id:destino, status:status, nombre:nombre,slogan:slogan,slogan_en:slogan_en,destacado_gastronomia:destacado_gastronomia,destacado_gastronomia_en:destacado_gastronomia_en,mensaje_mapa:mensaje_mapa,mensaje_mapa_en:mensaje_mapa_en,descripcion:descripcion,descripcion_en:descripcion_en,img_banner:img_banner,img_seccion:img_seccion,mapa:mapa,url:url,meta_titulo:meta_titulo,meta_titulo_en:meta_titulo_en,meta_descripcion:meta_descripcion,meta_descripcion_en:meta_descripcion_en,meta_keywords:meta_keywords,meta_keywords_en:meta_keywords_en,galeria:galeria,img_general:img_general,img_destacado_gastronomia:img_destacado_gastronomia,img_mapa:img_mapa},
			beforeSend: function(){ },
			success: function(datos){
				if(datos == "1"){
					alert("Datos guardados correctamente");
					cargar();
					$('#myModal').modal('hide');
				}else{
					alert("Ocurrió un problema");
				}
			}
		});
	}
	return false;
}
function eliminar_destino(id){
	if(confirm("¿Deseas eliminar esté destino?")){
		$.ajax({
			url: "ajax/funcion_destinos.php?ev=2",
			data: { id:id },
			type: "POST",
			success: function(datos){
				if(datos == 1){
					cargar();
				}
			}
		});
	}
}
function subir_imagen_banner(){
	var btnUpload=$('#btn-up-1');
		var mestatus=$('#status-up-1');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					$("#content_image_banner").html('<img src="../images/destinos/'+response[1]+'" height="auto" class="img-responsive" >');
					$("#nombre_imagen_banner").val(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function subir_imagen_seccion(){
	var btnUpload=$('#btn-up-2');
		var mestatus=$('#status-up-2');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/hoteles/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					$("#content_image_seccion").html('<img src="../images/destinos/hoteles/'+response[1]+'" height="auto" class="img-responsive" >');
					$("#nombre_imagen_seccion").val(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function subir_imagen_general(){
	var btnUpload=$('#btn-up-4');
		var mestatus=$('#status-up-4');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/general/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					$("#content_image_general").html('<img src="../images/destinos/general/'+response[1]+'" height="auto" class="img-responsive" >');
					$("#nombre_imagen_general").val(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function subir_imagen_destacado_gastronomia(){
	var btnUpload=$('#btn-up-destacado-g');
	var mestatus=$('#status-up-destacado-g');
	new AjaxUpload(btnUpload, {
		action: 'php/upload-images.php?ev=1',
		name: 'uploadfile[]',
		type: 'post',
		data: {dir:"../../images/destinos/general/"},
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
				alert('Solo JPG, JPEG, PNG o GIF');
				return false;
			}
			mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
		},
		onComplete: function(file, response){
			//On completion clear the status
			mestatus.text('');
			//On completion clear the status
			//files.html('');
			//Add uploaded file to list
			if(response!="error"){
				response=response.split("||");
				response[0];
				$("#content_image_destacado_gastronomia").html('<img src="../images/destinos/general/'+response[1]+'" height="auto" class="img-responsive" >');
				$("#nombre_imagen_destacado_gastronomia").val(response[1]);
			}else{
				alert("Error al subir "+file);
			}
		}
	});
}
function subir_imagen_mapa(){
	var btnUpload=$('#btn-up-5');
	var mestatus=$('#status-up-5');
	new AjaxUpload(btnUpload, {
		action: 'php/upload-images.php?ev=1',
		name: 'uploadfile[]',
		type: 'post',
		data: {dir:"../../images/destinos/explora/"},
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
				alert('Solo JPG, JPEG, PNG o GIF');
				return false;
			}
			mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
		},
		onComplete: function(file, response){
			//On completion clear the status
			mestatus.text('');
			//On completion clear the status
			//files.html('');
			//Add uploaded file to list
			if(response!="error"){
				response=response.split("||");
				response[0];
				$("#content_image_mapa").html('<img src="../images/destinos/explora/'+response[1]+'" height="auto" class="img-responsive" >');
				$("#nombre_imagen_mapa").val(response[1]);
			}else{
				alert("Error al subir "+file);
			}
		}
	});
}
function subir_archivo_mapa(){
	var btnUpload=$('#btn-up-3');
		var mestatus=$('#status-up-3');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../pdf/mapa/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif|pdf|doc|docx|xls)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					$("#mapa").val(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function subir_imagen_galeria(){
	var btnUpload=$('#btn-up-6');
		var mestatus=$('#status-up-6');
		new AjaxUpload(btnUpload, {
			action: 'php/upload-images.php?ev=1',
			name: 'uploadfile[]',
			type: 'post',
			data: {dir:"../../images/destinos/"},
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|jpeg|png|gif)$/.test(ext))){
					alert('Solo JPG, JPEG, PNG o GIF');
					return false;
				}
				mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				mestatus.text('');
				//On completion clear the status
				//files.html('');
				//Add uploaded file to list
				if(response!="error"){
					response=response.split("||");
					response[0];
					//$("#content-image_deal").html('<img src="https://app.kaptacrm.net/images/cesba/registros/'+response[1]+'" height="auto" width="112px" style="margin: 8px 0 0 0;" >');
					//$("#nombre_imagen_usuario").val(response[1]);
					contenido_galeria(response[1]);
				}else{
					alert("Error al subir "+file);
				}
			}
		});
}
function contenido_galeria(imagen){
	var total_element = $(".element-img").length;
	var lastid = $(".element-img:last").attr("id");
	var split_id = lastid.split("_");
	var nextindex = Number(split_id[1]) + 1;

	// Adding new div container after last occurance of element class
	$(".element-img:last").after("<div class='element-img l-img-min' data-name-img='"+imagen+"' id='div_"+ nextindex +"'></div>");

	$("#div_" + nextindex).append("<img src='../images/destinos/"+imagen+"' id='txt_"+ nextindex +"' />&nbsp;<span id='remove_" + nextindex + "' onclick='eliminar_galeria(this.id)' class='remove'><i class='fa fa-trash' aria-hidden='true'></i></span>");
}
function eliminar_galeria(id){
	//var id = this.id;
	var split_id = id.split("_");
	var deleteindex = split_id[1];
  
	// Remove <div> with id
	$("#div_" + deleteindex).remove();
	//https://makitweb.com/dynamically-add-and-remove-element-with-jquery/
}
function galeria_db(id){
	$.ajax({
		url: "ajax/funcion_destinos.php?ev=5",
		data: { id:id },
		type: "POST",
		success: function(datos){
			$("#lista-imagenes").html(datos);
		}
	});
}