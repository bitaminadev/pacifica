$(document).ready(function(){
    $.each($("table.tableEditContent"), function(i, v){
        recargar_tabla(v);
    });
});
function recargar_tabla(tableElement){
	$(tableElement).DataTable({
		"order":[[10,"desc"]],
        "dom": '<"top"lf>t<"bottom"pi><"clear">',
		"bStateSave": false,
        "lengthChange": false,
        "searching": false,
		"language": {
			"sSearch":"Buscar:",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		"sPaginationType" : "full_numbers",
	});
}
