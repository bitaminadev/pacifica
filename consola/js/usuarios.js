var table;
$(document).ready(function(){
	cargar_usuarios();
});
function cargar_usuarios(){
	$.ajax({
		url: "ajax/funcion_usuarios.php?ev=1",
		type: "POST",
		data: {},
		success: function(responseText){
			$("#tableContainer").html(responseText);
			recargar_tabla();
		}
	});
}
function recargar_tabla(){
	table = $("#usuariosTable").DataTable({
		order:[[1,"asc"]],
		bStateSave: true,
        language: {
            sSearch:        "Buscar&nbsp;&nbsp;",
            sLengthMenu:    "Mostrar&nbsp;&nbsp;_MENU_",
            sZeroRecords:   "No se encontraron resultados",
            sEmptyTable:    "Ningún dato disponible en esta tabla",
            sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered:  "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix:   "",
            sUrl:           "",
            sInfoThousands: ",",
            sLoadingRecords:"Cargando...",
            oPaginate: {
                sFirst:     "Primero",
                sLast:      "Último",
                sNext:      "Siguiente",
                sPrevious:  "Anterior"
            },
            oAria: {
                sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
		sPaginationType : "full_numbers",
	});
}
function editar_usuario(id_login){
	$("#modalUsuario .modal-body").html("");
    $("#saveProducto").prop("disabled", false).find(".waitingIcon").remove();
	$.ajax({
        type: "POST",
        dataType: "text",
        url: "ajax/funcion_usuarios.php?ev=2",
        data: {id_login: id_login},
        success: function(responseText){
            if(id_login == "") $("#modalUsuario .modal-title").html("Agregar Usuario");
            else $("#modalUsuario .modal-title").html("Editar Usuario");
            $("#modalUsuario .modal-body").html(responseText);
            $("#usuarioForm [name=id_rol]").trigger("change");
            $("#modalUsuario").modal("show");
        }
    });
}
$(document).on("click", "#toggleMaskPassword", function(){
    $this = $(this);
    $password = $this.parent().prev("input");
    if($password.attr("type") == "password"){
        $this.addClass("fa-eye-slash").removeClass("fa-eye");
        $password.attr("type", "text");
    }else{
        $this.addClass("fa-eye").removeClass("fa-eye-slash");
        $password.attr("type", "password");
    }
});
$(document).on("change", "#usuarioForm [name=id_rol]", function(){
	$.ajax({
        type: "POST",
        dataType: "text",
        url: "ajax/funcion_usuarios.php?ev=4",
        data: {id_rol: $(this).val()},
        success: function(responseText){
            $("#permissionBlock").html(responseText);
        }
    });
});
function guardar_usuario(){
	var form = $("#usuarioForm")[0];
    $("#saveUsuario").prop("disabled", true).append(` <i class="fa fa-spinner fa-spin waitingIcon"></i>`);
    var id_login = $("[name=id_login]", form).val();
    var nombre_login = $("[name=nombre_login]", form).val().trim();
    var id_rol = $("[name=id_rol]", form).val();
	var correo_login = $("[name=correo_login]", form).val().trim();
	var password_login = $("[name=password_login]", form).val().trim();
    var invalidsMessage = '';
	if(nombre_login == "") invalidsMessage += '\n - Nombre usuario';
	if(id_rol == "") invalidsMessage += '\n - Rol';
    if(correo_login == "") invalidsMessage += '\n - Correo para login';
    else if(!$("[name=correo_login]", form)[0].reportValidity()) invalidsMessage += '\n - Un correo con formato correcto';
	if(password_login == "") invalidsMessage += '\n - Contraseña para login';
	else if(password_login.length < 6) invalidsMessage += '\n - Una contraseña con al menos 6 caracteres';
    if(invalidsMessage != ""){
        invalidsMessage = 'Es obligatorio:' + invalidsMessage;
        $("#saveUsuario").prop("disabled", false).find(".waitingIcon").remove();
        alert(invalidsMessage);
    }else{
        var formData = new FormData();
        formData.append("id_login", id_login);
		formData.append("nombre_login", nombre_login);
		formData.append("id_rol", id_rol);
		formData.append("correo_login", correo_login);
		formData.append("password_login", password_login);
        $.ajax({
            type: 'POST',
            dataType: 'text',
            url: "ajax/funcion_usuarios.php?ev=3",
            data: formData,
            contentType: false,
            processData: false,
            success: function(responseText){
                if(responseText.trim() == "1"){
                    alert("Se guardó la información correctamente.");
                    cargar_usuarios();
                    $("#modalUsuario").modal("hide");
                }else{
                    alert("Ocurrió un error al guardar la información.");
                }
				$("#saveUsuario").prop("disabled", false).find(".waitingIcon").remove();
            },
        });
    }
}
function eliminar_usuario(id_login){
    if(confirm("¿Realmente deseas eliminar este usuario?")){
        $.ajax({
            type: "POST",
            dataType: "text",
            url: "ajax/funcion_usuarios.php?ev=5",
            data: {id_login: id_login},
            success: function(responseText){
                console.log(responseText);
                if(responseText.trim() == "1"){
                    alert("Se eliminó el usuario.");
                    cargar_usuarios();
                }else{
                    alert("Ocurrió un error al procesar la información.");
                }
            }
        });
    }
}
