<?php
    $gts = FALSE; // Group sections of type "text"
    if(isset($groupTextSections)){ $gts = $groupTextSections; }
    $secciones_texto_to_group = [];
    $secciones_rest_of = [];
    if($gts){
        $secciones_texto_to_group = array_filter($secciones, function($s){ return $s["tipo_contenido"] == "texto"; });
        $secciones_rest_of = array_filter($secciones, function($s){ return $s["tipo_contenido"] != "texto"; });
    }else{
        $secciones_rest_of = $secciones;
    }
?>

<?php
    if(count($secciones_texto_to_group) > 0){ ?>
        <form class="contentSection" onsubmit="return editar_secciones_texto(this);">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
                    <?php
                        foreach($secciones_texto_to_group as $seccion){ 
                            $seccion["contenido_es"] = str_replace("<br>", "\n", $seccion["contenido_es"]);
                    ?>
                        <div class="form-group">
                            <label><?php echo $seccion["nombre_seccion"]; ?></label>
                            <textarea class="form-control" rows="6" id="contenido_es_<?php echo $seccion["id_seccion"]; ?>" name="contenido_es_<?php echo $seccion["id_seccion"]; ?>" data-field="contenido_es" data-id-seccion="<?php echo $seccion["id_seccion"]; ?>"><?php echo $seccion["contenido_es"]; ?></textarea>
                        </div>
                    <?php } ?>
                    <hr>
                </div>
                <div class="col-sm-12 col-md-6">
                    <h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
                    <?php
                        foreach($secciones_texto_to_group as $seccion){ 
                            $seccion["contenido_en"] = str_replace("<br>", "\n", $seccion["contenido_en"]);
                    ?>
                        <div class="form-group">
                            <label><?php echo $seccion["nombre_seccion"]; ?></label>
                            <textarea class="form-control" rows="6" id="contenido_en_<?php echo $seccion["id_seccion"]; ?>" name="contenido_en_<?php echo $seccion["id_seccion"]; ?>" data-field="contenido_en" data-id-seccion="<?php echo $seccion["id_seccion"]; ?>"><?php echo $seccion["contenido_en"]; ?></textarea>
                        </div>
                    <?php } ?>
                    <hr>
                </div>
                <div class="col-sm-12">
                    <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                        <button class="submitButton">Actualizar</button>
                    <?php } ?>
                </div>
            </div>
        </form>
<?php } ?>
<?php 
    $contImg = 0;
    foreach($secciones_rest_of as $seccion){ 
        if($seccion["tipo_contenido"] == "imagen" || $seccion["tipo_contenido"] == "iy_imagen" || $seccion["tipo_contenido"] == "iy_youtube"){ 
            $contImg ++;
            $classPadding = ($contImg % 2 == 1 ? "pr-like-no-row" : "pl-like-no-row"); ?>
            <div class="col-sm-12 col-md-6 <?php echo $classPadding; ?>">
                <div class="row">
                    <form class="editar-seccion-imagen" onsubmit="return editar_seccion_imagen(this);">
                        <input type="hidden" name="id_seccion" value="<?php echo $seccion['id_seccion']; ?>">
                        <div class="form-group">
                            <label><?php echo $seccion["nombre_seccion"]; ?></label>
                            <table class="rowTable">
                                <tbody>
                                    <tr>
                                        <td class="imagePreview">
                                            <?php
                                                if($seccion["tipo_contenido"] == "imagen" || $seccion["tipo_contenido"] == "iy_imagen"){
                                                    $src = '../images/'.$seccion['nombre_subcarpeta'].$seccion['contenido_es'];
                                                }else if($seccion["tipo_contenido"] == "iy_youtube"){
                                                    $parts = explode("embed/", $seccion['contenido_es']);
                                                    $videoId = $parts[1];
                                                    $src = 'https://img.youtube.com/vi/'.$videoId.'/0.jpg';
                                                }
                                            ?>
                                            <img src="<?php echo $src; ?>" data-initial-src="<?php echo $src; ?>" alt="">
                                        </td>
                                        <td>
                                            <input type="file" class="form-control" name="contenido_es" data-id-seccion="<?php echo $seccion['id_seccion']; ?>" accept=".png, .jpeg, .jpg, .gif">
                                            <?php if($seccion["tipo_contenido"] == "iy_imagen" || $seccion["tipo_contenido"] == "iy_youtube"){ ?>
                                                <input type="text" placeholder="URL de YouTube" class="form-control youtubeURL" name="contenido_es_youtube" data-id-seccion="<?php echo $seccion['id_seccion']; ?>">
                                            <?php } ?>
                                            <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                                                <button class="submitButton">Actualizar</button>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        <?php } else if($seccion["tipo_contenido"] == "texto"){ $contImg = 0; ?>
            <form class="contentSection" onsubmit="return editar_secciones_texto(this);">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <?php
                            $seccion["contenido_es"] = str_replace("<br>", "\n", $seccion["contenido_es"]);
                        ?>
                        <div class="form-group">
                            <label><i class="lang-icon lang-es-normal"></i><?php echo $seccion["nombre_seccion"]; ?></label>
                            <textarea class="form-control" rows="6" id="contenido_es_<?php echo $seccion["id_seccion"]; ?>" name="contenido_es_<?php echo $seccion["id_seccion"]; ?>" data-field="contenido_es" data-id-seccion="<?php echo $seccion["id_seccion"]; ?>"><?php echo $seccion["contenido_es"]; ?></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <?php
                            $seccion["contenido_en"] = str_replace("<br>", "\n", $seccion["contenido_en"]);
                        ?>
                        <div class="form-group">
                            <label><i class="lang-icon lang-en-normal"></i><?php echo $seccion["nombre_seccion"]; ?></label>
                            <textarea class="form-control" rows="6" id="contenido_en_<?php echo $seccion["id_seccion"]; ?>" name="contenido_en_<?php echo $seccion["id_seccion"]; ?>" data-field="contenido_en" data-id-seccion="<?php echo $seccion["id_seccion"]; ?>"><?php echo $seccion["contenido_en"]; ?></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                            <button class="submitButton">Actualizar</button>
                        <?php } ?>
                    </div>
                </div>
            </form>
        <?php } else if($seccion["tipo_contenido"] == "galeria_extended"){ $contImg = 0; ?>
            <div class="contentSectionTable">
                <label><?php echo $seccion["nombre_seccion"]; ?></label>
                <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                    <button type="button" class="btn btn-info btn-lg agregar-elemento-galeria" data-toggle="modal" data-target="#modal-elemento-galeria-ext" onclick="prepare_editar_elemento_galeria_ext(null, <?php echo $seccion['id_seccion']; ?>);"><i class="fa fa-plus" aria-hidden="true"></i>Imagen / Video</button>
                <?php } ?>
                <table class='table tableEditContent' data-id-seccion="<?php echo $seccion["id_seccion"]; ?>">
                    <thead>
                        <tr>
                            <th style="display: none;">id</th>
                            <th>Tipo</th>
                            <th>Elemento</th>
                            <th>Cubierta de video</th>
                            <th><i class="lang-icon lang-es-normal"></i>Título español</th>
                            <th><i class="lang-icon lang-en-normal"></i>Título inglés</th>
                            <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            <?php } ?>
                        </tr>
                    </thead>        
                    <tbody>
                        <?php
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, $seccion["id_seccion"]);
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $elemento = array_map("utf8_encode", $elemento); ?>
                                <tr data-id-elemento="<?php echo $elemento["id_elemento"]; ?>">
                                    <td style="display: none;"><?php echo $elemento["id_elemento"]; ?></td>
                                    <td><?php echo $elemento["tipo_elemento"]; ?></td>
                                    <?php
                                        $class = '';
                                        $tag = '';
                                        $tag_cubierta = '';
                                        if($elemento["tipo_elemento"] == "imagen"){
                                            $class = 'na';
                                            $ruta_archivo = "../images/".$elemento["nombre_subcarpeta"].$elemento["contenido"];
                                            $tag = '<img src="'.$ruta_archivo.'" alt="">';
                                            $tag_cubierta = '<i class="fa fa-times"></i>&nbsp;No aplica';
                                        }else if($elemento["tipo_elemento"] == "video"){
                                            $prefix_ruta_video = "../video/".$elemento["nombre_subcarpeta"];
                                            $tag = '
                                                <video autoplay="" muted="" loop="">
                                                    <source src="'.$prefix_ruta_video.$elemento["contenido"].'" type="video/mp4">
                                                </video>
                                            ';
                                            $tag_cubierta = '<img src="../images/'.($elemento["contenido_cubierta_video"] != "" ? $elemento["nombre_subcarpeta"].$elemento["contenido_cubierta_video"] : "no-image.png").'" alt="">';
                                        }
                                    ?>
                                    <td><?php echo $tag; ?></td>
                                    <td class="<?php echo $class; ?>"><?php echo $tag_cubierta; ?></td>
                                    <td><?php echo $elemento["titulo_elemento_es"]; ?></td>
                                    <td><?php echo $elemento["titulo_elemento_en"]; ?></td>
                                    <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                                        <td class="actionIcon" onclick="prepare_editar_elemento_galeria_ext(this.closest('tr'), <?php echo $seccion['id_seccion']; ?>, <?php echo $elemento['id_elemento']; ?>);" data-toggle="modal" data-target="#modal-elemento-galeria-ext"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>
                                        <td class="actionIcon" onclick="prepare_eliminar_elemento_galeria(<?php echo $seccion['id_seccion']; ?>, <?php echo $elemento['id_elemento']; ?>, 'galeria_extended');" data-toggle="modal" data-target="#modal-eliminar-elemento-galeria"><i class="fa fa-trash" aria-hidden="true"></i></td>
                                    <?php } ?>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } else if($seccion["tipo_contenido"] == "galeria"){ $contImg = 0; ?>
            <div class="contentSection">
                <label><?php echo $seccion["nombre_seccion"]; ?></label>
                <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                    <button type="button" class="btn btn-info btn-lg add-to-gallery" onclick="agregar_elemento_galeria(this);" data-id-seccion="<?php echo $seccion['id_seccion']; ?>"><i class="fa fa-plus" aria-hidden="true"></i>Imagen</button>
                    <button type="button" class="btn btn-info btn-lg add-to-gallery-youtube" onclick="prepare_elemento_galeria_youtube(this);" data-id-seccion="<?php echo $seccion['id_seccion']; ?>" data-toggle="modal" data-target="#modal-elemento-galeria-youtube"><i class="fa fa-plus" aria-hidden="true"></i>Video Youtube</button>
                <?php } ?>
                <input type="file" class="form-control add-to-gallery" data-id-seccion="<?php echo $seccion['id_seccion']; ?>" style="display: none;" name="contenido-<?php echo $seccion['id_seccion']; ?>" accept=".png, .jpeg, .jpg, .gif">
                <div class="galeria-grid" data-id-seccion="<?php echo $seccion['id_seccion']; ?>">
                    <?php
                        $result_1 = $m_elementos->get_elementos_por_seccion($bd, $seccion["id_seccion"]);
                        while($elemento = mysqli_fetch_assoc($result_1)){
                            $elemento = array_map("utf8_encode", $elemento); ?>
                            <div class="elemento-galeria" data-id-elemento="<?php echo $elemento['id_elemento']; ?>">
                                <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                                    <span class="delete-elemento" onclick="prepare_eliminar_elemento_galeria(<?php echo $seccion['id_seccion']; ?>, <?php echo $elemento['id_elemento']; ?>, 'galeria');" data-toggle="modal" data-target="#modal-eliminar-elemento-galeria"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                <?php } ?>
                                <?php 
                                    if($elemento["tipo_elemento"] == "imagen"){
                                        $src = "../images/".$elemento['nombre_subcarpeta'].$elemento['contenido'];
                                    }else if($elemento["tipo_elemento"] == "youtube"){
                                        $parts = explode("embed/", $elemento['contenido']);
                                        $videoId = $parts[1];
                                        $src = 'https://img.youtube.com/vi/'.$videoId.'/0.jpg';
                                        echo '
                                            <a href="'.$elemento['contenido'].'" class="youtubeLink" target="_blank">
                                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                            </a>
                                        ';
                                    }
                                ?>
                                <img src="<?php echo $src; ?>" alt="Galería">
                            </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
<?php } ?>
