<form class="contentSection" onsubmit="return validate_seo(this);">
    <input type="hidden" name="id_pagina" value="<?php echo $pagina["id_pagina"]; ?>">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
            <div class="form-group">
                <label>Título</label>
                <input class="form-control" type="text" name="meta_titulo_es" maxlength="70" value="<?php echo $pagina["meta_titulo_es"]; ?>">
            </div>
            <div class="form-group">
                <label>Descripción</label>
                <textarea class="form-control noEnter" rows="6" name="meta_descripcion_es" maxlength="155"><?php echo $pagina["meta_descripcion_es"]; ?></textarea>
            </div>
            <div class="form-group">
                <label>Keywords</label>
                <input class="form-control" type="text" name="meta_keywords_es" maxlength="180" value="<?php echo $pagina["meta_keywords_es"]; ?>">
            </div>
            <hr>
        </div>
        <div class="col-sm-12 col-md-6">
            <h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
            <div class="form-group">
                <label>Título</label>
                <input class="form-control" type="text" name="meta_titulo_en" maxlength="70" value="<?php echo $pagina["meta_titulo_en"]; ?>">
            </div>
            <div class="form-group">
                <label>Descripción</label>
                <textarea class="form-control noEnter" rows="6" name="meta_descripcion_en" maxlength="155"><?php echo $pagina["meta_descripcion_en"]; ?></textarea>
            </div>
            <div class="form-group">
                <label>Keywords</label>
                <input class="form-control" type="text" name="meta_keywords_en" maxlength="180" value="<?php echo $pagina["meta_keywords_en"]; ?>">
            </div>
            <hr>
        </div>
        <div class="col-sm-12">
            <?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_CONTENIDO]["p_actualizar"]){ ?>
                <button class="submitButton">Actualizar</button>
            <?php } ?>
        </div>
    </div>
</form>