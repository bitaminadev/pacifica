<?php @session_start(); 
	include_once("php/environment.php");
	if(!isset($_SESSION["usuario_consola"])){
		header("Location: login.php");
	}else{
		if(!validateModule(ID_MODULO_CONTACTOS_CONTACTO) && !validateModule(ID_MODULO_CONTACTOS_SOCIO) && !validateModule(ID_MODULO_CONTACTOS_BODAS))
			header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html>	
	<head>
		<title>Consola administrativa | Contactos</title>
		<!--Metas-->
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />		
		<!--Archivos js y css-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/skin.css" rel="stylesheet" type="text/css">
		<link href="css/productos.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.datatables.min.js"></script>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script type="text/javascript" src="js/ajaxupload.3.5.js"></script>
		<script src="js/datatables.min.js"></script>
		<script src="js/skin.js"></script>
		<script src="js/contactos.js"></script>
	</head>	
	<body>
		<div id="wrapper">
		<?php require("commons/header.php"); ?>
		<div class="overlay" onclick='cerrar_menu();'></div>
		<?php require("commons/menu.php"); ?>
			<div id="page-content-wrapper">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
					<span class="hamb-top"></span>
					<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
				</button>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<fieldset>
								<legend>Destinos - Contactos</legend>
								<div class="tabla" style='border-top:none;'>
                                    <table class='table tableEditContent'>
                                        <thead>
                                            <tr>
                                                <th style="display: none;">id</th>
                                                <th>Formulario</th>
                                                <th>Nombre</th>
                                                <th>¿Es socio?</th>
                                                <th>Correo</th>
                                                <th>Teléfono</th>
                                                <th>Asunto</th>
                                                <th>Fecha Evento</th>
                                                <th>Fecha Respaldo</th>
                                                <th>Invitados</th>
                                                <th>Fecha Registro</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php
												date_default_timezone_set("America/Mexico_City");
												require("../php/controllers/connect_sql.php");
												require("../php/class/contactos.php");
												$m_contactos = new Contactos();
												$result = $m_contactos->get_all_contactos($bd);
												while($contacto = mysqli_fetch_assoc($result)){ 
													if(
														$_SESSION["usuario_consola"]["su"] || 
														($contacto["tipo_contacto"] == "contacto" && $_SESSION["permisos"][ID_MODULO_CONTACTOS_CONTACTO]["p_leer"] == 1) || 
														($contacto["tipo_contacto"] == "socio" && $_SESSION["permisos"][ID_MODULO_CONTACTOS_SOCIO]["p_leer"] == 1) || 
														($contacto["tipo_contacto"] == "bodas" && $_SESSION["permisos"][ID_MODULO_CONTACTOS_BODAS]["p_leer"] == 1) 
													){ 
											?>
												<tr>
													<td style="display: none;"><?php echo $contacto["id_contacto"]; ?></td>
													<td><?php echo ucfirst($contacto["tipo_contacto"]); ?></td>
													<td><?php echo utf8_decode($contacto["nombre"]); ?></td>
													<td><?php echo ($contacto["es_socio"] == 1 ? '<i style="color: green;" class="fa fa-check"></i>' : ($contacto["tipo_contacto"] == "contacto" ? '<i style="color: crimson;" class="fa fa-times"></i>' : '---')); ?></td>
													<td><?php echo $contacto["correo"]; ?></td>
													<td><?php echo $contacto["telefono"]; ?></td>
													<td><?php echo utf8_decode($contacto["mensaje"]); ?></td>
													<td><?php echo ($contacto["fecha_evento"] != "" ? format_date($contacto["fecha_evento"]) : '---'); ?></td>
													<td><?php echo ($contacto["fecha_respaldo"] != "" ? format_date($contacto["fecha_respaldo"]) : '---'); ?></td>
													<td><?php echo ($contacto["tipo_contacto"] == "bodas" ? $contacto["cantidad_invitados"] : '---'); ?></td>
													<td data-sort="<?php echo $contacto["fecha_contacto"]; ?>"><?php echo format_date($contacto["fecha_contacto"]); ?></td>
												</tr>
											<?php 
													}
												} 
												require("../php/controllers/cerrar_php.php");
											?>
                                        </tbody>
                                    </table>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>
<?php
	function format_date($datetime){
		$dateParts = explode(" ", $datetime);
		$date = explode("-", $dateParts[0]);
		$dateString = implode("-", [$date[2], spanishMonthString($date[1]), $date[0]]);
		$timeString = count($dateParts) > 1 ? " ".$dateParts[1] : "";
		return $dateString.$timeString;
	}
	function spanishMonthString($month){
		$monthString = "";
		switch(intval($month)){
			case 1: $monthString = "enero"; break;
			case 2: $monthString = "febrero"; break;
			case 3: $monthString = "marzo"; break;
			case 4: $monthString = "abril"; break;
			case 5: $monthString = "mayo"; break;
			case 6: $monthString = "junio"; break;
			case 7: $monthString = "julio"; break;
			case 8: $monthString = "aagosto"; break;
			case 9: $monthString = "septiembre"; break;
			case 10: $monthString = "octubre"; break;
			case 11: $monthString = "noviembre"; break;
			case 12: $monthString = "diciembre"; break;
		}
		return $monthString;
	}
?>