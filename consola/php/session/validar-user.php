<?php 
	@session_start(); 
	$bandera = true;
	if(!isset($_POST["correo"]) || trim($_POST["correo"] == "")){
		$bandera == false;
	}
	if(!isset($_POST["password"]) || trim($_POST["password"] == "")){
		$bandera == false;
	}
	if($bandera == true){
		validar();
	}

	function validar(){
		include("../../../php/controllers/connect_sql.php");
		include("../../../php/class/functions.php");
		include("../../../php/class/acceso.php");
		include("../../../php/class/permisos.php");
		$obj_general = new Funciones_Generales();
		$obj_acceso = new Acceso();
		$m_permisos = new Permisos();

		$correo = $obj_general -> limpiarCadena($_POST["correo"]);
		$password = $obj_general ->limpiarCadena($_POST["password"]); 

		$usuario = $obj_acceso -> mostrar_acceso($bd,$correo,$password);
		if(($usuario->num_rows) > 0){
			$datos_cliente = mysqli_fetch_array($usuario);
			$_SESSION["usuario_consola"] = [
				"nombre_login" => $datos_cliente["nombre_login"],
				"correo_login" => $datos_cliente["correo_login"],
				"id_rol" => $datos_cliente["id_rol"],
				"id_login" => $datos_cliente["id_login"],
				"su" => $datos_cliente["es_superusuario"],
			];
			$result = $m_permisos->select_permisos($bd, $datos_cliente["id_rol"]);
			$permisos = [];
            while($row = mysqli_fetch_assoc($result)){
				$row = array_map("utf8_encode", $row);
				$permisos[$row["id_modulo"]] = [
					"p_leer" => $row["p_leer"],
					"p_crear" => $row["p_crear"],
					"p_actualizar" => $row["p_actualizar"],
					"p_eliminar" => $row["p_eliminar"],
				];
            }
			$_SESSION["permisos"] = $permisos;
			echo "<script language='javascript'>window.location='../../index.php'</script>";
		}else{
			echo "<script language='javascript'>window.location='../../login.php?error=1'</script>";
		}
		include("../../../php/controllers/cerrar_php.php");
	}
?>
