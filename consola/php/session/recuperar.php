<?php
    @session_start();
    require("../db/conectar_mysqli.php");
	require("../encryption_functions.php");
	$password_raw1 = $_POST["password"];
    $password_raw2 = $_POST["passwordRepeat"];
    if($password_raw1 == $password_raw2){
        $newPassword = mysqli_real_escape_string($bd, limpiarCadena(encriptar($password_raw1)));
        require("../models/m_parents_session.php");
        $parentsSession = new ParentsSession($bd);
        $consulta = $parentsSession->change_password($_SESSION["parentId"], $newPassword, $_SESSION["parent"]);
        if($consulta == 1){
            $_SESSION['recuperar'] = 0;
            header("Location: https://padres.kaptacrm.net/index.php", true, 303);
            die();
        }else{
            header("Location: https://padres.kaptacrm.net/logout.php", true, 303);
            die();
        }
    }else{
        header("Location: https://padres.kaptacrm.net/logout.php", true, 303);
        die();
    }
	function limpiarCadena($valor){
		$valor = str_ireplace("SELECT","",$valor);
		$valor = str_ireplace("COPY","",$valor);
		$valor = str_ireplace("DELETE","",$valor);
		$valor = str_ireplace("DROP","",$valor);
		$valor = str_ireplace("DUMP","",$valor);
		$valor = str_ireplace(" OR ","",$valor);
		$valor = str_ireplace("%","",$valor);
		$valor = str_ireplace("LIKE","",$valor);
		$valor = str_ireplace("--","",$valor); 
		$valor = str_ireplace("^","",$valor);
		$valor = str_ireplace("[","",$valor);
		$valor = str_ireplace("]","",$valor);
		$valor = str_ireplace("!","",$valor);
		$valor = str_ireplace("¡","",$valor);
		$valor = str_ireplace("?","",$valor);
		$valor = str_ireplace("=","",$valor);
		return $valor;
	}
	require("../db/cerrar_mysqli.php");
?>