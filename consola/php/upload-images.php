<?php 
	if(isset($_GET["ev"])){
		switch($_GET["ev"]){
			case 1: subir_imagenes_galeria();
					break;
		}
    }
    function subir_imagenes_galeria(){
		$dir=$_POST["dir"];
		if(is_dir($dir)){
			chmod($dir, 0777);
		}
		if(isset($_FILES['uploadfile'])){
			$errors= array();
			foreach($_FILES['uploadfile']['tmp_name'] as $key => $tmp_name ){
				$uploaddir = $dir; 

				$imagen=basename($_FILES['uploadfile']['name'][$key]); 

				$imagenFullName = explode(".", basename($_FILES['uploadfile']['name'][$key]));
				$extension = array_pop($imagenFullName);
				$imagenName = implode(".", $imagenFullName); // Se vuelven a agregar puntos (".") En caso que el nombre original los tenga además de el último que indica la extensión
				$imagen_n = validFSName($imagenName);
				$randomPrefix = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'), 0, 6);
				$newFileName = $randomPrefix.'_'.$imagen_n.".".$extension;

				// $file = $uploaddir ."".$imagen; 
				$file = $uploaddir."".$newFileName; 
				// $file_name= "".$_FILES['uploadfile']['name'][$key];		
				$file_name = $newFileName;
				if(move_uploaded_file($_FILES['uploadfile']['tmp_name'][$key], $file)){ 
					echo $dir.$file_name."||".$file_name;
				}else{
					echo "error";}  
			}
		}
    }
    function validFSName($string){
        $validName = "";
        $latinChars = ["Á", "É", "Í", "Ó", "Ú", "Ü", "Ñ", "á", "é", "í", "ó", "ú", "ü", "ñ"];
        $htmlEntities = ["A", "E", "I", "O", "U", "U", "NH", "a", "e", "i", "o", "u", "u", "nh"];
        $validName = str_replace($latinChars, $htmlEntities, $string);
        $validName = preg_replace( '/[^a-zA-Z0-9]+/', '_', $validName);
        return $validName;
    }
?>