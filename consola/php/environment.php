<?php
    define("ID_MODULO_DESTINOS", 1);
    define("ID_MODULO_HABITACIONES", 2);
    define("ID_MODULO_RESTAURANTES", 3);
    define("ID_MODULO_CONTENIDO", 4);
    define("ID_MODULO_CONTACTOS_CONTACTO", 5);
    define("ID_MODULO_CONTACTOS_SOCIO", 6);
    define("ID_MODULO_CONTACTOS_BODAS", 7);
    define("ID_MODULO_USUARIOS", 8);
    define("ID_MODULO_ROLES", 9);

    function validateModule($id_modulo){
        return  
            $_SESSION["usuario_consola"]["su"] || 
            $_SESSION["permisos"][$id_modulo]["p_leer"] || 
            $_SESSION["permisos"][$id_modulo]["p_crear"] || 
            $_SESSION["permisos"][$id_modulo]["p_actualizar"] || 
            $_SESSION["permisos"][$id_modulo]["p_eliminar"]
        ;
    }
?>
