<?php 
	@session_start(); 
	include_once("php/environment.php");
	if(!isset($_SESSION["usuario_consola"])){
		header("Location: login.php");
		die();
	}else{
		if(!validateModule(ID_MODULO_USUARIOS))
			header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Consola administrativa | Usuarios</title>
		<!--Metas-->
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--Archivos js y css-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/skin.css" rel="stylesheet" type="text/css">
		<link href="css/productos.css" rel="stylesheet" type="text/css">
		<link href="css/commonStyles.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.datatables.min.js"></script>
		<script src="js/datatables.min.js"></script>
		<script src="js/skin.js"></script>
		<script src="js/usuarios.js"></script>
	</head>
	<body>
		<div id="wrapper">
		<?php require("commons/header.php"); ?>
		<div class="overlay" onclick='cerrar_menu();'></div>
			<?php require("commons/menu.php"); ?>
			<div id="page-content-wrapper">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
					<span class="hamb-top"></span>
					<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
				</button>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<fieldset>
								<legend>Administración de usuarios</legend>
								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-12 form-group mt-3">
											<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_USUARIOS]["p_crear"]){ ?>
												<button type="button" class="btn btn-primary float-right" onclick="editar_usuario(``)"><i class="fa fa-plus"></i> Agregar Usuario</button>
											<?php } ?>
										</div>
		                                <div id="tableContainer" class="col-sm-12 form-group"></div>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="modalUsuario" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header darkBlock">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Editar Usuario</h4>
					</div>
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_USUARIOS]["p_actualizar"]){ ?>
							<button id="saveUsuario" onclick="guardar_usuario();" type="button" class="btn btn-success mr-2">Guardar</button>
						<?php } ?>
						<button type="button" class="btn" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Cancelar</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
