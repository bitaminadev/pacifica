<?php @session_start(); 
	if(!isset($_SESSION["usuario_consola"])){
		header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html>	
	<head>
		<title>Pacífica resort | Consola administrativa</title>
		<!--Metas-->
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />		
		<!--Archivos js y css-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/skin.css" rel="stylesheet" type="text/css">
		<link href="css/index.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/skin.js"></script>
		<!-- <script src='js/consola/index.js'></script> -->
	</head>	
	<body>
		<div id="wrapper">
		<?php require("commons/header.php"); ?>
		<div class="overlay" onclick='cerrar_menu();'></div>
		<?php require("commons/menu.php"); ?>		
		<!-- Page Content -->
			<div id="page-content-wrapper">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
					<span class="hamb-top"></span>
					<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
				</button>
				<!-- <div class="container">
					<div class="row">
						<div class="col-lg-12">
							<fieldset>
								<div class="col-lg-6 auto">
									<a href="cotizaciones.php" class="col-lg-3 col_dhs boton"><i class="fa fa-money" aria-hidden="true"></i>Cotizaciones</a>
									<a href="contactos.php" class="col-lg-3 col_dhs boton"><i class="fa fa-user" aria-hidden="true"></i>Contactos</a>
									<a href="proyectos.php" class="col-lg-3 col_dhs boton"><i class="fa fa-suitcase" aria-hidden="true"></i>Proyectos</a>
									<a href="productos.php" class="col-lg-3 col_dhs boton"><i class="fa fa-product-hunt" aria-hidden="true"></i>Productos</a>
								</div>
								<div class="datos_rapidos">
									<h1>DATOS RÁPIDOS</h1>
									<div class="filtros col-lg-4">
										<select id="anios" onchange="recargar();"></select>
										<select id="meses" onchange="recargar();">
											<?php 
												/* include("../php/class/functions.php");
												$obj_general = new Funciones_Generales();
												echo "<option value=''>Mes...</option>";
												for($i = 1; $i <= 12; $i++){
													$mes = $obj_general -> traer_meses($i);
													echo "<option value='".$i."'>".$mes."</option>";
												} */
											?>
										</select>
									</div>
									<div class='cotizaciones_mes'>
										<div class='cccc sin_atender'>
											<label>Sin atender<strong></strong></label>
										</div>
										<div class='cccc atendidas'>
											<label>En curso<strong></strong></label>
										</div>
										<div class='cccc declinadas'>
											<label>Canceladas<strong></strong></label>
										</div>
										<div class='cccc vendidas'>
											<label>Vendidas<strong></strong></label>
										</div>
										<div class="grafica">
											<div id="todas"></div>
										</div>
										<div class="datos">
											<div class="ccc total_cot"><label>Total cotizaciones<strong></strong></label></div>
										</div>
										<div class="maximos">
											<div class="mas_vendidos">
											</div>
										</div>
									</div>
									
								</div>
							</fieldset>
						</div>
					</div>
				</div> -->
			</div>
			<!-- /#page-content-wrapper -->
		</div>
		<!-- /#wrapper -->
	</body>

</html>