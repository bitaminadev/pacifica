<?php 
	@session_start(); 
	include_once("php/environment.php");
	if(!isset($_SESSION["usuario_consola"])){
		header("Location: login.php");
	}else{
		if(!validateModule(ID_MODULO_DESTINOS))
			header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html>	
	<head>
		<title>Consola administrativa | Destinos</title>
		<!--Metas-->
		<meta name="robots" content="noindex,nofollow">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />		
		<!--Archivos js y css-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/skin.css" rel="stylesheet" type="text/css">
		<link href="css/productos.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			.cat_img img {
			    height: auto;
			    max-width: 80%;
			    width: auto;
			}
		</style>
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.datatables.min.js"></script>
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script type="text/javascript" src="js/ajaxupload.3.5.js"></script>
		<script src="js/datatables.min.js"></script>
		<script src="js/skin.js"></script>
		<script src="js/destinos.js"></script>
	</head>	
	<body>
		<div id="wrapper">
		<?php require("commons/header.php"); ?>
		<div class="overlay" onclick='cerrar_menu();'></div>
		<?php require("commons/menu.php"); ?>
		
		<!-- Page Content -->
			<div id="page-content-wrapper">
				<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
					<span class="hamb-top"></span>
					<span class="hamb-middle"></span>
					<span class="hamb-bottom"></span>
				</button>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<fieldset>
								<legend>Destinos - Pacífica</legend>
								<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_DESTINOS]["p_crear"]){ ?>
									<button type="button" class="btn btn-info btn-lg productos_modal" data-toggle="modal" data-target="#myModal" onclick="nuevo_registro();"><i class="fa fa-plus" aria-hidden="true"></i>Destinos</button>
								<?php } ?>
								<div class="tabla" style='border-top:none;'>
							
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
			<!-- /#page-content-wrapper -->
		</div>
		<!-- /#wrapper -->
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  	<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
				  	<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Nuevo destino</h4>
				  	</div>
				  	<div class="modal-body">

                    <!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item general active">
							<a class="nav-link general active" data-toggle="tab" href="#producto" role="tab">GENERALES</a>
						</li>
						<li class="nav-item seo">
						<!-- <li class="nav-item seo disabled"> -->
							<a class="nav-link" href="#seo" data-toggle="tab" role="tab">SEO</a>
						</li>
						<li class="nav-item gal">
						<!-- <li class="nav-item gal disabled"> -->
							<a class="nav-link" href="#galeria" data-toggle="tab" role="tab">GALERIA</a>
						</li>
					</ul>
					<form id="categorias" onsubmit="return ingresar_destino()" enctype="multipart/form-data" > 
					<div class="tab-content">
						<div class="tab-pane active" style="padding: 20px 0 0px 0;" id="producto" role="tabpanel">
							       <input type="hidden" id="cve_destino">
									<div class="row">
										<div class="form-group col-lg-6">
											<label>Status</label>
												<select class="form-control" id="status" required >
													<option value="1">Activo</option>
													<option value="0">Inactivo</option></select>
										</div>
										<div class="form-group col-lg-6">
											<label>Nombre destino</label>
											<input type="text" class="form-control" id="nombre_destino" required >
										</div>
										<div class="form-group col-sm-12 col-md-6">
											<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
											<div class="form-group">
												<label>Slogan destino</label>
												<textarea class="form-control" id="slogan_destino" required ></textarea>
											</div>
											<div class="form-group">
												<label>Descripción</label>
												<textarea class="form-control" id="descripcion" ></textarea>
											</div>
											<div class="form-group">
												<label>Destacados: Gastronomía</label>
												<textarea class="form-control" id="destacado_gastronomia"></textarea>
											</div>
											<div class="form-group">
												<label>Mensaje mapa</label>
												<textarea class="form-control" id="mensaje_mapa" required ></textarea>
											</div>
										</div>
										<div class="form-group col-sm-12 col-md-6">
											<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
											<div class="form-group">
												<label>Slogan destino</label>
												<textarea class="form-control" id="slogan_destino_en" required ></textarea>
											</div>
											<div class="form-group">
												<label>Descripción</label>
												<textarea class="form-control" id="descripcion_en" ></textarea>
											</div>
											<div class="form-group">
												<label>Destacados: Gastronomía</label>
												<textarea class="form-control" id="destacado_gastronomia_en"></textarea>
											</div>
											<div class="form-group">
												<label>Mensaje mapa</label>
												<textarea class="form-control" id="mensaje_mapa_en" required ></textarea>
											</div>
										</div>
										<div class="form-group col-lg-12">
											<label>Subir mapa</label>
											<input type="text" name="mapa" id="mapa" class="form-control" style="width:79%;display:inline;" placeholder="Nombre del archivo" value="" disabled >
											<p id="status-up-3" style="display: initial;"></p>
											<button style="width:10%;height:35px;" id="btn-up-3" class="btn_anexo" onclick="subir_archivo_mapa();"><i class="fa fa-upload" aria-hidden="true"></i></button>	
										</div>
										<div class="form-group col-lg-6">
											<div class="dv_conten_img">
												<div class="dv_containt_img">
													<strong id="content_image_banner"></strong>
													<input id="btn-up-1" class="btn_charge_imgusr" type="button" value="Cargar imagen banner" onclick="subir_imagen_banner();">
													<p id="status-up-1"></p>
													<input id="nombre_imagen_banner" class="tituloimagen" type="text" name="nombre_imagen_banner" value="" >
												</div>
												<p class="tam_recusr">Tamaño recomendado: 1665px x 798.</p>
											</div>
										</div>
										<div class="form-group col-lg-6">
											<div class="dv_conten_img">
												<div class="dv_containt_img">
													<strong id="content_image_seccion"></strong>
													<input id="btn-up-2" class="btn_charge_imgusr" type="button" value="Cargar imagen sección" onclick="subir_imagen_seccion();">
													<p id="status-up-2"></p>
													<input id="nombre_imagen_seccion" class="tituloimagen" type="text" name="nombre_imagen_seccion" value="" >
												</div>
												<p class="tam_recusr">Tamaño recomendado: 1665px x 385.</p>
											</div>
										</div>
										<div class="form-group col-lg-6">
											<div class="dv_conten_img">
												<div class="dv_containt_img">
													<strong id="content_image_general"></strong>
													<input id="btn-up-4" class="btn_charge_imgusr" type="button" value="Cargar imagen general" onclick="subir_imagen_general();">
													<p id="status-up-4"></p>
													<input id="nombre_imagen_general" class="tituloimagen" type="text" name="nombre_imagen_general" value="" >
												</div>
												<p class="tam_recusr">Tamaño recomendado: 770px x 520.</p>
											</div>
										</div>
										<div class="form-group col-lg-6">
											<div class="dv_conten_img">
												<div class="dv_containt_img">
													<strong id="content_image_destacado_gastronomia"></strong>
													<input id="btn-up-destacado-g" class="btn_charge_imgusr" type="button" value="Cargar imagen de destacado: Gastronomía" onclick="subir_imagen_destacado_gastronomia();">
													<p id="status-up-destacado-g"></p>
													<input id="nombre_imagen_destacado_gastronomia" class="tituloimagen" type="text" name="nombre_imagen_destacado_gastronomia" value="" >
												</div>
												<p class="tam_recusr">Tamaño recomendado: 770px x 520.</p>
											</div>
										</div>
										<div class="form-group col-lg-6">
											<div class="dv_conten_img">
												<div class="dv_containt_img">
													<strong id="content_image_mapa"></strong>
													<input id="btn-up-5" class="btn_charge_imgusr" type="button" value="Cargar imagen mapa" onclick="subir_imagen_mapa();">
													<p id="status-up-5"></p>
													<input id="nombre_imagen_mapa" class="tituloimagen" type="text" name="nombre_imagen_mapa" value="" >
												</div>
												<p class="tam_recusr">Tamaño recomendado: 770px x 520.</p>
											</div>
										</div>
									</div>
									
							
						</div>
						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="seo"  role="tabpanel">
							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="form-group col-lg-6">
											<label>Url</label>
											<input type="text" class="form-control" id="url_destino" required >
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-es-normal"></i>Español</h4>
									<div class="form-group">
										<label>Meta título</label>
										<textarea class="form-control" id="meta_titulo"></textarea>
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords"></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<h4 class="langIndicator"><i class="lang-icon lang-en-normal"></i>Inglés</h4>
									<div class="form-group">
										<label>Meta título</label>
										<textarea class="form-control" id="meta_titulo_en"></textarea>
									</div>
									<div class="form-group">
										<label>Meta descripción</label>
										<textarea class="form-control" id="meta_descripcion_en"></textarea>
									</div>
									<div class="form-group">
										<label>Meta keywords</label>
										<textarea class="form-control" id="meta_keywords_en"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" style="padding: 20px 0 0px 0;" id="galeria" role="tabpanel">
							<div class="row">
								<div class="col-lg-6">
									<div class="dv_conten_img">
										<input id="btn-up-6" style="width:100%;position:relative;" class="btn_charge_imgusr btn_charge_rel" type="button" value="Cargar Imágen" onclick="subir_imagen_galeria();">
										<p id="status-up-6"></p>
										<p class="tam_recusr" style="float: left;width: 100%;text-align: right;">Tamaño recomendado: 730 x 480px.</p>
										<input id="nombre_imagen_galeria" class="tituloimagen" type="text" name="nombre_imagen_galeria">
									</div>
								</div>
								<div class="col-lg-12" id="lista-imagenes">
									<div class="element-img" id="div_1">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
							<?php if($_SESSION["usuario_consola"]["su"] || $_SESSION["permisos"][ID_MODULO_DESTINOS]["p_actualizar"]){ ?>
								<input type="submit" name="enviar" id="enviar" value="Agregar">
							<?php } ?>
							<p class="cerrar btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</p>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>