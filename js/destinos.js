$(document).ready(function(){
    vSlider();
    vSliderCardOut();
    setTimeout(function(){
        // document.querySelector("a.v-slider__arrow_next").click();
        try{
          document.querySelector("a.v-slider__arrow_next").click();
        }catch{
          document.querySelector(".slick-list.draggable").style.height = "unset";
        }
    }, 1500);
});
function vSlider() {
    $('.slide-destinations').slick({
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: 'linear',
      prevArrow: "<a href='#' class='v-slider__arrow_prev v-button i-arrow-light'></a>",
      nextArrow: "<a href='#' class='v-slider__arrow_next v-button i-arrow-light'></a>",
      speed: 700,
      adaptiveHeight: true,
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 576,
          settings: {
          variableWidth: true,
          speed: 700,
          centerMode: true,
          }
        }
      ]
    });
    $('.slide-destinations').slick('slickSetOption', 'speed', 100, true);
  }
  function vSliderCardOut() {
    $('.v-slider_card-out').slick({
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: 'linear',
      prevArrow: "<a href='#' class='v-slider__arrow_prev v-button i-arrow-light'></a>",
      nextArrow: "<a href='#' class='v-slider__arrow_next v-button i-arrow-light'></a>",
      speed: 500
    });
  }
 