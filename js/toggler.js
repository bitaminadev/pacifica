/* Archivo toggler.js */
Element.prototype.toggler = function (options = {})
{
    // This function makes the toggler action toggle
    Element.prototype.toggleItem = function()
    {
    // Goes to the over all toggler parent and selects all the togglers that it finds
    let togglers = this.parentElement.parentElement.querySelectorAll(options.toggler);

    // Toggles this active Class
    this.parentElement.classList.toggle(options.active);

    // It closes all the active classes that it finds, except this one
    for (let i = 0; i < togglers.length; i ++)
    {
        if (togglers[i] != this)
        {
            // Removes the active class for elements that where not selected
            togglers[i].parentElement.classList.remove(options.active);

            // If the selected element is active, adds the not active to the others
            if(this.parentElement.classList.contains(options.active))
            {
                if(!togglers[i].classList.contains(options.notActive))
                    togglers[i].classList.add(options.notActive);
                }
                else
                {
                    togglers[i].classList.remove(options.notActive);
                }
            }
        }
        this.classList.remove(options.notActive);
    }

    // Toggler constructor to set default values if not given
    const constructor = (options) => {
        options.item = options.item || '.togglerItem';
        options.toggler = options.toggler || '.togglerToggler';
        options.active = options.active || 'togglerActive';
        options.notActive = options.notActive || 'togglerNotActive';
        options.callback = options.callback || false;
        return options;
    }; options = constructor(options);

    // Takes all the togglers to set their behavior
    let togglers = this.querySelectorAll(options.toggler);

    for (let i = 0; i < togglers.length; i ++)
    {
        togglers[i].addEventListener('mouseover', function()
        {
            this.toggleItem();
            if (options.callback) options.callback(this);
        });
    }
}
NodeList.prototype.toggler = HTMLCollection.prototype.toggler = function(options) {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i]) {
            this[i].toggler(options);
        }
    }
}


