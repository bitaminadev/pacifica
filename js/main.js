$(document).ready(function(){
	setBodyPadding();
	document.querySelector('.nav').submenu({nested:true});
	setSubmenusTop();
	setBackgrounds();
	document.querySelector('.navContent').toggler(
	{
		toggler: '[data-togglesection]',
		active: 'active',
		callback: showSection
	});
	$(".mapLocation").tooltip({
		placement: "top"
	});
	$(window).scroll(function(){ 
		var headerH = $("header").outerHeight(true);   
		var scrollTopVal = $(this).scrollTop();
		if ( scrollTopVal > 100 ) {
			$(".navLogoSmall").addClass("displayBlock");
			$(".navLogoSmall").removeClass("displayNone");
			$(".navLogoBig").addClass("displayNone");
			$(".navLogoBig").removeClass("displayBlock");
		} else {
			$(".navLogoBig").addClass("displayBlock");
			$(".navLogoBig").removeClass("displayNone");
			$(".navLogoSmall").addClass("displayNone");
			$(".navLogoSmall").removeClass("displayBlock");
		}
	});
	// START Autoplaying when displayed and pausing when hiding Youtube videos on galleries (habing the class "youtubeVideo")
	/* NOTA: Actualmente no funciona en el slider, ya que el slider crea clones de los slides para realizar sus operaciones, por lo que los elementos "Observados" nunca se muestran realimente */ 
	observer = new IntersectionObserver(function(entries){
		if(entries[0].isIntersecting === true){
			entries[0].target.querySelector(".youtubeVideo").contentWindow.postMessage('{"event":"command","func":"playVideo","args":[]}', '*');
		}else{
			entries[0].target.querySelector(".youtubeVideo").contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":[]}', '*');
		}
	}, { threshold: [1] });
	$(".youtubeContainer:has(.youtubeVideo)").each((index, item) =>
	{
		observer.observe(item);
	});
	// Trigger playing any video visible since the starting
	setTimeout(() => {
		$(".youtubeContainer:has(.youtubeVideo)").each((index, item) =>
		{
			observer.unobserve(item);
			observer.observe(item);
		});
	}, 1000);
	// END Autoplaying and pausing Youtube videos on galleries
	checkDisplayNavaside();
	window.onresize = function(){
		console.log("resized window");
		setBodyPadding();
		setSubmenusTop();
		checkDisplayNavaside();
	};
});
function checkDisplayNavaside(){
	if($(window).outerWidth(true) >= 992){
		$('.nav-main-destination').toggle(true);
	}else{
		$('.nav-main-destination').toggle(false);
	}
}
function setBodyPadding()
{
	document.querySelector('body').style.paddingTop = $('.nav').height() + 'px';
}
Element.prototype.submenu = function(options = {})
{
	const constructor = (options) => 
	{
		options.submenu = options.submenu || '.submenu';
		options.displayer = options.displayer || '.submenuDisplayer';
		options.nested = options.nested || false;

		return options;
	}; options = constructor(options);

	let displayers = this.querySelectorAll(options.displayer);
	let submenus = this.querySelectorAll('.nav .submenu');

	for (let i = 0; i < displayers.length; i ++) displayers[i].addEventListener('click', function(e)
	{
		let currentSubmenu = this.parentElement.querySelector('.submenu');

		if (options.nested) 
		{
			submenus = this.parentElement.dataset.layer;
			submenus = document.querySelectorAll('[data-layer="'+submenus+'"] > .submenu');
		}

		if(e.target == this)
		{
			for(let i = 0; i < submenus.length; i ++) if (submenus[i] != currentSubmenu) submenus[i].classList.remove('active');
			currentSubmenu.classList.toggle('active');
		}
	});

}

function toggleMobileMenu()
{
	let mobileMenu = document.querySelector('.nav .menu');
	mobileMenu.classList.toggle('active');
}
function linkTo(link, blank = false)
{
	if (!blank) window.location = link;
	else window.open(link, '_blank');
}
function setBackgrounds()
{
	let backgrounds = document.querySelectorAll('[data-background]');
	backgrounds.forEach(item =>
	{
		item.style.backgroundImage = 'url('+item.dataset.background+')';
	});
}
const setSubmenusTop = () => 
{
	let destinationsSubmenus = document.querySelectorAll('.destinations .submenu .submenu');
	let hotelsSubmenus = document.querySelectorAll('.hotels .submenu .submenu');
	let itemHeight = document.querySelector('.nav li li').offsetHeight;

	let i = 0;

	for (; i < destinationsSubmenus.length; i++) 
	{
		destinationsSubmenus[i].style.setProperty("top", (-i*itemHeight) + 'px', "important");
	}

	i = 0;

	for (; i < hotelsSubmenus.length; i++) 
	{
		hotelsSubmenus[i].style.setProperty("top", (-i*itemHeight) + 'px', "important");
	}
}

/* toggler */
function showSection(object)
{
	sections = object.parentElement.closest(".togglerContainer").querySelector('.navSectionContent').children;
    for (let i = 0; i < sections.length; i ++)
    {
		if(!sections[i].classList.contains("notHidden")){
			if(!sections[i].classList.contains("hidden"))
				sections[i].classList.add("hidden");
			if(sections[i].classList.contains(object.dataset.togglesection))
				sections[i].classList.remove("hidden");
		}
    }
}

let currentGalleryItem = 0;

function showGalleryItem(item)
{
	let imagesItems = document.querySelector('.gallery').querySelectorAll('.gallery__item')
	let textItems = document.querySelector('.gallery__text__container').querySelectorAll('.gallery__text');

	for (let i = 0 ; i < imagesItems.length; i ++)
	{
		imagesItems[i].classList.remove('active');
		textItems[i].classList.remove('active');
	}
	imagesItems[currentGalleryItem].classList.add('active');
	textItems[currentGalleryItem].classList.add('active');
}

function setGalleryItem(item)
{
	currentGalleryItem = item;
	showGalleryItem(currentGalleryItem);
}