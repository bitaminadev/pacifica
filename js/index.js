var mySwiper;
$(document).ready(function(){
	init_swiper();
	$(".li-hotel").click(function(){
		let cve = $(this).attr('data-item-h');
		$(".li-hotel").removeClass("active-li");
		$(this).addClass("active-li");
		$(".ds_tab_home, .block_text_dest").removeClass("d-none, d-block");
		$(".ds_tab_home, .block_text_dest").addClass("d-none");
		$("#dest-num-"+cve+", #dest-text-"+cve).removeClass("d-none");
		$("#dest-num-"+cve+", #dest-text-"+cve).addClass("d-block");
	});
});
function init_swiper(){
	console.log("Initializing swiper");
	mySwiper = new Swiper ('.swiper-container', {
		direction: 'horizontal',
		slidesPerView: 1,
		spaceBetween: 1,
		slidesPerGroup: 1,
		// loop: true,
		loop: false,
		observer: true,
		observeParents: true,
		autoplay: false,
		/* autoplay: {
			delay: 5000,
		}, */
		breakpoints: {
			900:{
				slidesPerView: 1,
				spaceBetween: 0
			}
		},
		pagination: {
			el: '.pag1',
			clickable: true,
			type: 'bullets',
		},
		navigation: {
			nextEl: '.next-img-gallery',
			prevEl: '.prev-img-gallery'
		}
	});
}
function reproducir(id_elemento){
	var vid2 = document.getElementById("pretzel-video-" + id_elemento); 
	vid2.play();
	// $(".icon-play-h, .item-banner-abs").hide();
	$(".fixedBannerLogo-" + id_elemento).addClass("d-none");
	$("#icon-play-h-" + id_elemento).parent()[0].querySelector(".messageForGalleryElement").classList.add("hidden");
	$("#icon-play-h-" + id_elemento).hide();
	$("#close-video-" + id_elemento).removeClass("d-none");
	$("#close-video-" + id_elemento).addClass("d-block");
	$("#cover-image-" + id_elemento).removeClass("d-block").addClass("d-none");
}
function pause(id_elemento){
	var vid = document.getElementById("pretzel-video-" + id_elemento);
	vid.pause(); 
	// $(".icon-play-h, .item-banner-abs").show();
	$(".fixedBannerLogo-" + id_elemento).removeClass("d-none");
	$("#icon-play-h-" + id_elemento).parent()[0].querySelector(".messageForGalleryElement").classList.remove("hidden");
	$("#icon-play-h-" + id_elemento).show();
	$("#close-video-" + id_elemento).removeClass("d-block");
	$("#close-video-" + id_elemento).addClass("d-none");
	$("#cover-image-" + id_elemento).removeClass("d-none").addClass("d-block");
}