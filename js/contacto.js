function sendEmailContact(form){
    form.querySelector("button[type='submit'").disabled = true;
    if(form.checkValidity()){
        data = {
            tipo_contacto: form.querySelector("[name='tipo_contacto']").value,
            nombre: form.querySelector("[name='nombre']").value,
            es_socio: form.querySelector("[name='es_socio']:checked").value,
            correo: form.querySelector("[name='correo']").value,
            telefono: form.querySelector("[name='telefono']").value,
            mensaje: form.querySelector("[name='mensaje']").value,
            fecha_evento: form.querySelector("[name='fecha_evento']").value,
            fecha_respaldo: form.querySelector("[name='fecha_respaldo']").value,
            cantidad_invitados: form.querySelector("[name='cantidad_invitados']").value,
        };
        $.ajax({
            type: "POST",
            url: "../php/ajax/registro_contacto.php?ev=1",
            data: data,
            success: function(responseText){
                console.log(responseText);
                var alertMessage = '';
                var windowLocation = '';
                if(window.location.pathname.match(/\/es\//)){
                    alertMessage = 'Ocurrió un error al enviar. Por favor intenta nuevamente.';
                    windowLocation = 'correo-enviado';
                }else{
                    alertMessage = 'An error occurred while sending. Please try again.';
                    windowLocation = 'email-sent';
                }
                if(responseText.trim() == "1"){
                    window.location = windowLocation;
                }else{
                    alert(alertMessage);
                    form.querySelector("button[type='submit'").disabled = false;
                }
            }
        });
    }
    return false;
}