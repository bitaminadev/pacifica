<?php 
	class Roles{
        function select_roles($bd, $id_rol = ""){
            $id_rol = mysqli_real_escape_string($bd, $id_rol);
            $where = $id_rol != "" ? " AND r.id_rol = $id_rol" : "";
            $sql = 
                "SELECT r.*
                    FROM c_roles r
                    WHERE 1 $where";
            return mysqli_query($bd, $sql);
        }
        function insert_rol($bd, $nombre_rol, $descripcion_rol){
            $nombre_rol = mysqli_real_escape_string($bd, $nombre_rol);
            $descripcion_rol = mysqli_real_escape_string($bd, $descripcion_rol);
            $sql = 
                "INSERT INTO c_roles (nombre_rol, descripcion_rol)
                    VALUES ('$nombre_rol', '$descripcion_rol')";
            return mysqli_query($bd, $sql);
        }
        function update_rol($bd, $id_rol, $nombre_rol, $descripcion_rol){
            $id_rol = mysqli_real_escape_string($bd, $id_rol);
            $nombre_rol = mysqli_real_escape_string($bd, $nombre_rol);
            $descripcion_rol = mysqli_real_escape_string($bd, $descripcion_rol);
            $sql = 
                "UPDATE c_roles
                    SET
                        nombre_rol = '$nombre_rol',
                        descripcion_rol = '$descripcion_rol'
                    WHERE id_rol = $id_rol";
            return mysqli_query($bd, $sql);
        }
    }
?>