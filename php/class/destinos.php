<?php 
	class Destinos{
        function registrar_destino($bd,$url_destino,$nombre_destino,$slogan_destino,$slogan_destino_en,$descripcion_destino,$descripcion_destino_en,$imagen_banner,$imagen_interior,$imagen_general,$img_destacado_gastronomia,$img_mapa,$mapa,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status,$destacado_gastronomia,$destacado_gastronomia_en,$mensaje_mapa,$mensaje_mapa_en){
			$url_destino = mysqli_real_escape_string($bd, $url_destino);
			$nombre_destino = mysqli_real_escape_string($bd, $nombre_destino);
			$slogan_destino = mysqli_real_escape_string($bd, $slogan_destino);
			$slogan_destino_en = mysqli_real_escape_string($bd, $slogan_destino_en);
			$descripcion_destino = mysqli_real_escape_string($bd, $descripcion_destino);
			$descripcion_destino_en = mysqli_real_escape_string($bd, $descripcion_destino_en);
			$imagen_banner = mysqli_real_escape_string($bd, $imagen_banner);
			$imagen_interior = mysqli_real_escape_string($bd, $imagen_interior);
			$imagen_general = mysqli_real_escape_string($bd, $imagen_general);
			$img_destacado_gastronomia = mysqli_real_escape_string($bd, $img_destacado_gastronomia);
			$img_mapa = mysqli_real_escape_string($bd, $img_mapa);
			$mapa = mysqli_real_escape_string($bd, $mapa);
			$meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
			$meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
			$meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
			$meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
			$meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
			$meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
			$status = mysqli_real_escape_string($bd, $status);
			$destacado_gastronomia = mysqli_real_escape_string($bd, $destacado_gastronomia);
			$destacado_gastronomia_en = mysqli_real_escape_string($bd, $destacado_gastronomia_en);
			$mensaje_mapa = mysqli_real_escape_string($bd, $mensaje_mapa);
			$mensaje_mapa_en = mysqli_real_escape_string($bd, $mensaje_mapa_en);
			$sql = "INSERT INTO w_destinos (url_destino, nombre_destino, slogan_destino, slogan_destino_en, descripcion_destino, descripcion_destino_en, img_banner_general, img_banner_habitaciones,img_general,img_destacado_gastronomia,img_mapa,mapa,meta_titulo,meta_titulo_en,meta_descripcion,meta_descripcion_en,meta_keywords,meta_keywords_en,status_destino,destacado_gastronomia,destacado_gastronomia_enmensaje_mapa,mensaje_mapa_en) VALUES ('$url_destino','$nombre_destino','$slogan_destino','$slogan_destino_en','$descripcion_destino','$descripcion_destino_en','$imagen_banner','$imagen_interior','$imagen_general','$img_destacado_gastronomia','$img_mapa','$mapa','$meta_titulo','$meta_titulo_en','$meta_descripcion','$meta_descripcion_en','$meta_keywords','$meta_keywords_en','$status','$destacado_gastronomia','$destacado_gastronomia_en','$mensaje_mapa','$mensaje_mapa_en')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function actualizar_destino($bd,$id_destino,$nombre_destino,$slogan_destino,$slogan_destino_en,$descripcion_destino,$descripcion_destino_en,$imagen_banner,$imagen_interior,$imagen_general,$img_destacado_gastronomia,$img_mapa,$mapa,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status,$destacado_gastronomia,$destacado_gastronomia_en,$mensaje_mapa,$mensaje_mapa_en){
			$id_destino = mysqli_real_escape_string($bd, $id_destino);
			$nombre_destino = mysqli_real_escape_string($bd, $nombre_destino);
			$slogan_destino = mysqli_real_escape_string($bd, $slogan_destino);
			$slogan_destino_en = mysqli_real_escape_string($bd, $slogan_destino_en);
			$descripcion_destino = mysqli_real_escape_string($bd, $descripcion_destino);
			$descripcion_destino_en = mysqli_real_escape_string($bd, $descripcion_destino_en);
			$imagen_banner = mysqli_real_escape_string($bd, $imagen_banner);
			$imagen_interior = mysqli_real_escape_string($bd, $imagen_interior);
			$imagen_general = mysqli_real_escape_string($bd, $imagen_general);
			$img_destacado_gastronomia = mysqli_real_escape_string($bd, $img_destacado_gastronomia);
			$img_mapa = mysqli_real_escape_string($bd, $img_mapa);
			$mapa = mysqli_real_escape_string($bd, $mapa);
			$meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
			$meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
			$meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
			$meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
			$meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
			$meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
			$status = mysqli_real_escape_string($bd, $status);
			$destacado_gastronomia = mysqli_real_escape_string($bd, $destacado_gastronomia);
			$destacado_gastronomia_en = mysqli_real_escape_string($bd, $destacado_gastronomia_en);
			$mensaje_mapa = mysqli_real_escape_string($bd, $mensaje_mapa);
			$mensaje_mapa_en = mysqli_real_escape_string($bd, $mensaje_mapa_en);
			$sql = "UPDATE w_destinos SET nombre_destino = '".$nombre_destino."', slogan_destino = '".$slogan_destino."', slogan_destino_en = '".$slogan_destino_en."', descripcion_destino = '".$descripcion_destino."', descripcion_destino_en = '".$descripcion_destino_en."', img_banner_general ='".$imagen_banner."', img_banner_habitaciones ='".$imagen_interior."', img_general ='".$imagen_general."', img_destacado_gastronomia = '$img_destacado_gastronomia', img_mapa = '$img_mapa', mapa = '".$mapa."', meta_titulo = '".$meta_titulo."', meta_titulo_en = '".$meta_titulo_en."', meta_descripcion = '".$meta_descripcion."', meta_descripcion_en = '".$meta_descripcion_en."', meta_keywords = '".$meta_keywords."', meta_keywords_en = '".$meta_keywords_en."', status_destino = '".$status."', destacado_gastronomia = '$destacado_gastronomia', destacado_gastronomia_en = '$destacado_gastronomia_en', mensaje_mapa = '$mensaje_mapa', mensaje_mapa_en = '$mensaje_mapa_en' WHERE id_destino = '".$id_destino."'";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
		}
		function maximo_destino($bd){
			$consultap = mysqli_query($bd,"SELECT max(id_destino) as maximo FROM w_destinos");
			$parent = mysqli_fetch_array($consultap);
			return $parent["maximo"];
        }
        function eliminar_destino($bd,$id){
			$id = mysqli_real_escape_string($bd, $id);
			$bandera = 0;
			$sql = "DELETE FROM w_destinos where id_destino = '".$id."' ";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function lista_destinos($bd,$id_destino){
			$id_destino = mysqli_real_escape_string($bd, $id_destino);
            $where = "";
            if($id_destino != ""){
                $where = 'and id_destino = '.$id_destino.' ';
            }
			$sql = "select * from w_destinos where nombre_destino <> ''   ".$where."  order by nombre_destino ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
		}        
    }
?>