<?php 
	class HabitacionesCategoria{
        function registrar_categoria($bd,$id_destino,$url_categoria,$nombre_categoria,$descripcion_categoria,$descripcion_categoria_en,$imagen,$meta_titulo,$meta_descripcion,$meta_keywords,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$status){
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $url_categoria = mysqli_real_escape_string($bd, $url_categoria);
            $nombre_categoria = mysqli_real_escape_string($bd, $nombre_categoria);
            $descripcion_categoria = mysqli_real_escape_string($bd, $descripcion_categoria);
            $descripcion_categoria_en = mysqli_real_escape_string($bd, $descripcion_categoria_en);
            $imagen = mysqli_real_escape_string($bd, $imagen);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $status = mysqli_real_escape_string($bd, $status);
			$sql = "INSERT INTO w_habitaciones_categoria (url_categoria, id_destino, nombre_categoria, descripcion_categoria, descripcion_categoria_en, imagen_categoria,meta_titulo,meta_titulo_en,meta_descripcion,meta_descripcion_en,meta_keywords,meta_keywords_en,status_categoria) VALUES ('$url_categoria','$id_destino','$nombre_categoria','$descripcion_categoria','$descripcion_categoria_en','$imagen','$meta_titulo','$meta_titulo_en','$meta_descripcion','$meta_descripcion_en','$meta_keywords','$meta_keywords_en','$status')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}	
            return $bandera;
        }
        function actualizar_categoria($bd,$id_categoria,$id_destino,$nombre,$descripcion_categoria,$descripcion_categoria_en,$imagen,$meta_titulo,$meta_descripcion,$meta_keywords,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$status){
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $descripcion_categoria = mysqli_real_escape_string($bd, $descripcion_categoria);
            $descripcion_categoria_en = mysqli_real_escape_string($bd, $descripcion_categoria_en);
            $imagen = mysqli_real_escape_string($bd, $imagen);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $status = mysqli_real_escape_string($bd, $status);
			$sql = "UPDATE w_habitaciones_categoria SET id_destino = '".$id_destino."', nombre_categoria = '".$nombre."', descripcion_categoria = '".$descripcion_categoria."', descripcion_categoria_en = '".$descripcion_categoria_en."', imagen_categoria ='".$imagen."', meta_titulo = '".$meta_titulo."', meta_titulo_en = '".$meta_titulo_en."', meta_descripcion = '".$meta_descripcion."', meta_descripcion_en = '".$meta_descripcion_en."', meta_keywords = '".$meta_keywords."', meta_keywords_en = '".$meta_keywords_en."', status_categoria = '".$status."' WHERE id_categoria = '".$id_categoria."'";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
		}
		function maximo_categoria($bd){
			$consultap = mysqli_query($bd,"SELECT max(id_categoria) as maximo FROM w_habitaciones_categoria");
			$parent = mysqli_fetch_array($consultap);
			return $parent["maximo"];
        }
        function eliminar_categoria($bd,$id){
            $id = mysqli_real_escape_string($bd, $id);
			$bandera = 0;
			$sql = "DELETE FROM w_habitaciones_categoria where id_categoria = '".$id."' ";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function lista_categorias_destino($bd,$id_destino){
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $where = "";
            if($id_destino != ""){
                $where = 'and d.id_destino = '.$id_destino.' ';
            }
			$sql = "SELECT c.*, d.nombre_destino FROM w_habitaciones_categoria as c 
            inner join w_destinos as d on d.id_destino = c.id_destino
            where c.nombre_categoria <> '' ".$where." ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function lista_categorias_habitaciones($bd,$id_categoria,$status=""){
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $status = mysqli_real_escape_string($bd, $status);
            $where = "";
            if($id_categoria != ""){
                $where .= 'and c.id_categoria = '.$id_categoria.' ';
            }
            if($status != ""){
                $where .= 'and c.status_categoria = '.$status.' ';
            }
            /* if($id_tipo_habitacion != ""){
                $where = 'and id_tipo_habitacion = '.$id_tipo_habitacion.' ';
            } */
			$sql = "SELECT c.*, d.nombre_destino, d.url_destino FROM w_habitaciones_categoria as c 
            inner join w_destinos as d on d.id_destino = c.id_destino
            where c.nombre_categoria <> '' ".$where." ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function filtro_categorias_destinos($bd,$id_destino,$id_categoria){
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $where = "";
            if($id_categoria != ""){
                $where = 'and c.id_categoria = '.$id_categoria.' ';
            }
            if($id_destino != ""){
                $where = 'and c.id_destino = '.$id_destino.' ';
            }
			$sql = "SELECT c.*, d.nombre_destino FROM w_habitaciones_categoria as c 
            inner join w_destinos as d on d.id_destino = c.id_destino
            where c.nombre_categoria <> '' ".$where." order by c.nombre_categoria ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
    }
?>