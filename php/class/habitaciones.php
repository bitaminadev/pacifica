<?php 
	class Habitaciones{
        function registrar_habitacion($bd,$id_tipo,$url,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$detalles,$detalles_en,$plano,$precio_general,$precio_miembros,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status){            
            $id_tipo = mysqli_real_escape_string($bd, $id_tipo);
            $url = mysqli_real_escape_string($bd, $url);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $mensaje_en = mysqli_real_escape_string($bd, $mensaje_en);
            $slogan = mysqli_real_escape_string($bd, $slogan);
            $slogan_en = mysqli_real_escape_string($bd, $slogan_en);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $descripcion_en = mysqli_real_escape_string($bd, $descripcion_en);
            $detalles = mysqli_real_escape_string($bd, $detalles);
            $detalles_en = mysqli_real_escape_string($bd, $detalles_en);
            $plano = mysqli_real_escape_string($bd, $plano);
            // $precio_general = mysqli_real_escape_string($bd, $precio_general);
            // $precio_miembros = mysqli_real_escape_string($bd, $precio_miembros);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $status = mysqli_real_escape_string($bd, $status);
            $sql = "INSERT INTO w_habitaciones (url_habitacion, id_tipo_habitacion, nombre_habitacion, mensaje_habitacion, mensaje_habitacion_en, slogan_habitacion, slogan_habitacion_en, descripcion_habitacion, descripcion_habitacion_en, detalles_habitacion, detalles_habitacion_en, plano_habitacion, precio_general, precio_miembros, meta_titulo, meta_titulo_en, meta_descripcion, meta_descripcion_en, meta_keywords, meta_keywords_en, status_habitacion) VALUES ('$url','$id_tipo','$nombre','$mensaje','$mensaje_en','$slogan','$slogan_en','$descripcion','$descripcion_en','$detalles','$detalles_en','$plano',$precio_general,$precio_miembros,'$meta_titulo','$meta_titulo_en','$meta_descripcion','$meta_descripcion_en','$meta_keywords','$meta_keywords_en','$status')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}	
            return $bandera;
        }
        function actualizar_habitacion($bd,$id,$id_tipo,$url,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$detalles,$detalles_en,$plano,$precio_general,$precio_miembros,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status){
            $id = mysqli_real_escape_string($bd, $id);
            $id_tipo = mysqli_real_escape_string($bd, $id_tipo);
            $url = mysqli_real_escape_string($bd, $url);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $mensaje_en = mysqli_real_escape_string($bd, $mensaje_en);
            $slogan = mysqli_real_escape_string($bd, $slogan);
            $slogan_en = mysqli_real_escape_string($bd, $slogan_en);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $descripcion_en = mysqli_real_escape_string($bd, $descripcion_en);
            $detalles = mysqli_real_escape_string($bd, $detalles);
            $detalles_en = mysqli_real_escape_string($bd, $detalles_en);
            $plano = mysqli_real_escape_string($bd, $plano);
            // $precio_general = mysqli_real_escape_string($bd, $precio_general);
            // $precio_miembros = mysqli_real_escape_string($bd, $precio_miembros);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $status = mysqli_real_escape_string($bd, $status);
			$sql = "UPDATE w_habitaciones SET id_tipo_habitacion = '".$id_tipo."', nombre_habitacion = '".$nombre."', mensaje_habitacion = '".$mensaje."', mensaje_habitacion_en = '".$mensaje_en."', slogan_habitacion = '".$slogan."', slogan_habitacion_en = '".$slogan_en."', descripcion_habitacion = '".$descripcion."', descripcion_habitacion_en = '".$descripcion_en."', detalles_habitacion = '".$detalles."', detalles_habitacion_en = '".$detalles_en."', plano_habitacion = '".$plano."', precio_general = ".$precio_general.", precio_miembros = ".$precio_miembros.", meta_titulo = '".$meta_titulo."', meta_titulo_en = '".$meta_titulo_en."', meta_descripcion = '".$meta_descripcion."', meta_descripcion_en = '".$meta_descripcion_en."', meta_keywords = '".$meta_keywords."', meta_keywords_en = '".$meta_keywords_en."', status_habitacion = '".$status."' WHERE id_habitacion = '".$id."'";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function maximo_habitacion($bd){
			$consultap = mysqli_query($bd,"SELECT max(id_habitacion) as maximo FROM w_habitaciones");
			$parent = mysqli_fetch_array($consultap);
			return $parent["maximo"];
        }
        function eliminar_habitacion($bd,$id){
            $id = mysqli_real_escape_string($bd, $id);
			$bandera = 0;
			$sql = "DELETE FROM w_habitaciones where id_habitacion = '".$id."' ";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function lista_habitaciones($bd,$id_habitacion,$id_tipo_habitacion){
            $id_habitacion = mysqli_real_escape_string($bd, $id_habitacion);
            $id_tipo_habitacion = mysqli_real_escape_string($bd, $id_tipo_habitacion);
            $where = "";
            if($id_habitacion != ""){
                $where = 'and h.id_habitacion = '.$id_habitacion.' ';
            }
            if($id_tipo_habitacion != ""){
                $where = 'and h.id_tipo_habitacion = '.$id_tipo_habitacion.' ';
            }
            $sql = "select h.*, t.id_tipo_habitacion, t.nombre_tipo, c.id_categoria, c.nombre_categoria, d.id_destino, d.nombre_destino 
            from w_habitaciones as h 
            inner join w_habitaciones_tipo as t on t.id_tipo_habitacion = h.id_tipo_habitacion 
            inner join w_habitaciones_categoria as c on c.id_categoria = t.id_categoria 
            inner join w_destinos as d on d.id_destino = c.id_destino  
            where h.status_habitacion = 1 AND h.nombre_habitacion <> ''   ".$where."  order by h.id_habitacion ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function site_habitaciones($bd,$id_habitacion){
            $id_habitacion = mysqli_real_escape_string($bd, $id_habitacion);
            $where = "";
            if($id_habitacion != ""){
                $where = 'and h.id_habitacion = '.$id_habitacion.' ';
            }
            $sql = "select h.*, d.id_destino, d.nombre_destino, d.url_destino  
            from w_habitaciones as h 
            inner join w_habitaciones_tipo as t on t.id_tipo_habitacion = h.id_tipo_habitacion 
            inner join w_habitaciones_categoria as c on c.id_categoria = t.id_categoria 
            inner join w_destinos as d on d.id_destino = c.id_destino  
            where h.status_habitacion = 1 AND h.nombre_habitacion <> ''   ".$where." and t.status_tipo = 1 and c.status_categoria = 1  order by h.id_habitacion ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function tabla_habitaciones($bd,$id_habitacion,$id_tipo_habitacion){
            $id_habitacion = mysqli_real_escape_string($bd, $id_habitacion);
            $id_tipo_habitacion = mysqli_real_escape_string($bd, $id_tipo_habitacion);
            $where = "";
            if($id_habitacion != ""){
                $where = 'and h.id_habitacion = '.$id_habitacion.' ';
            }
            if($id_tipo_habitacion != ""){
                $where = 'and h.id_tipo_habitacion = '.$id_tipo_habitacion.' ';
            }
            $sql = "select h.*, t.id_tipo_habitacion, t.nombre_tipo, c.id_categoria, c.nombre_categoria, d.id_destino, d.nombre_destino, 
            (select name from w_habitaciones_galeria as g where h.id_habitacion = g.id_habitacion and g.name <> '' limit 1 ) as imagen 
            from w_habitaciones as h 
            inner join w_habitaciones_tipo as t on t.id_tipo_habitacion = h.id_tipo_habitacion 
            inner join w_habitaciones_categoria as c on c.id_categoria = t.id_categoria 
            inner join w_destinos as d on d.id_destino = c.id_destino  
            where h.nombre_habitacion <> ''   ".$where."  order by h.id_habitacion ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
		}
    }
?>