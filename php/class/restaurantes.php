<?php 
	class Restaurantes{
        function registrar_restaurante($bd,$destino,$url,$nombre,$mensaje,$descripcion,$imagen,$horario,$status,$meta_titulo,$meta_descripcion,$meta_keywords,$mensaje_en,$descripcion_en,$horario_en,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$imagen_logo,$orden_general){            
            $destino = mysqli_real_escape_string($bd, $destino);
            $url = mysqli_real_escape_string($bd, $url);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $imagen = mysqli_real_escape_string($bd, $imagen);
            $horario = mysqli_real_escape_string($bd, $horario);
            $status = mysqli_real_escape_string($bd, $status);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $mensaje_en = mysqli_real_escape_string($bd, $mensaje_en);
            $descripcion_en = mysqli_real_escape_string($bd, $descripcion_en);
            $horario_en = mysqli_real_escape_string($bd, $horario_en);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $imagen_logo = mysqli_real_escape_string($bd, $imagen_logo);
            $orden_general = mysqli_real_escape_string($bd, $orden_general);
			$sql = "INSERT INTO w_restaurantes (id_destino,url_restaurante, nombre_restaurante, mensaje_restaurante, descripcion_restaurante, imagen_restaurante, horario_restaurante, status_restaurante, meta_titulo, meta_descripcion, meta_keywords, mensaje_restaurante_en, descripcion_restaurante_en, horario_restaurante_en, meta_titulo_en, meta_descripcion_en, meta_keywords_en, imagen_logo, orden_general) VALUES ('$destino','$url','$nombre','$mensaje','$descripcion','$imagen','$horario','$status','$meta_titulo','$meta_descripcion','$meta_keywords','$mensaje_en','$descripcion_en','$horario_en','$meta_titulo_en','$meta_descripcion_en','$meta_keywords_en','$imagen_logo', '$orden_general')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}	
			return $bandera;
        }
        function actualizar_restaurante($bd,$id,$destino,$nombre,$mensaje,$descripcion,$imagen,$horario,$status,$meta_titulo,$meta_descripcion,$meta_keywords,$mensaje_en,$descripcion_en,$horario_en,$meta_titulo_en,$meta_descripcion_en,$meta_keywords_en,$imagen_logo,$orden_general){
            $id = mysqli_real_escape_string($bd, $id);
            $destino = mysqli_real_escape_string($bd, $destino);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $imagen = mysqli_real_escape_string($bd, $imagen);
            $horario = mysqli_real_escape_string($bd, $horario);
            $status = mysqli_real_escape_string($bd, $status);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $mensaje_en = mysqli_real_escape_string($bd, $mensaje_en);
            $descripcion_en = mysqli_real_escape_string($bd, $descripcion_en);
            $horario_en = mysqli_real_escape_string($bd, $horario_en);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $imagen_logo = mysqli_real_escape_string($bd, $imagen_logo);
            $orden_general = mysqli_real_escape_string($bd, $orden_general);
			$sql = "UPDATE w_restaurantes SET id_destino = '$destino', nombre_restaurante = '$nombre', mensaje_restaurante = '$mensaje', descripcion_restaurante = '$descripcion', imagen_restaurante ='$imagen', horario_restaurante ='$horario', status_restaurante ='$status', meta_titulo = '$meta_titulo', meta_descripcion = '$meta_descripcion', meta_keywords = '$meta_keywords', mensaje_restaurante_en = '$mensaje_en', descripcion_restaurante_en = '$descripcion_en', horario_restaurante_en = '$horario_en', meta_titulo_en = '$meta_titulo_en', meta_descripcion_en = '$meta_descripcion_en', meta_keywords_en = '$meta_keywords_en', imagen_logo = '$imagen_logo', orden_general = '$orden_general' WHERE id_restaurante = '$id'";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
		}
		function maximo_restaurante($bd){
			$consultap = mysqli_query($bd,"SELECT max(id_restaurante) as maximo FROM w_restaurantes");
			$parent = mysqli_fetch_array($consultap);
			return $parent["maximo"];
        }
        function lista_restaurantes($bd,$id_destino,$status=""){
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $status = mysqli_real_escape_string($bd, $status);
            $where = "";
            if($id_destino != ""){
                $where .= 'and r.id_destino = '.$id_destino.' ';
            }
            if($status != ""){
                $where .= 'and r.status_restaurante = '.$status.' ';
            }
            $sql = "select r.*, d.nombre_destino, d.url_destino
            from w_restaurantes as r
			left join w_destinos d on r.id_destino = d.id_destino
            where r.nombre_restaurante <> ''   ".$where."  order by r.orden_general ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function lista_random_restaurantes($bd,$id_destino){
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $where = "";
            if($id_destino != ""){
                $where = 'and r.id_destino = '.$id_destino.' ';
            }
            $sql = "select r.*, d.nombre_destino, d.url_destino
            from w_restaurantes as r
			left join w_destinos d on r.id_destino = d.id_destino
            where r.nombre_restaurante <> ''   ".$where."  ORDER BY RAND() ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function detalle_restaurante($bd,$id_restaurante){
            $id_restaurante = mysqli_real_escape_string($bd, $id_restaurante);
            $where = "";
            if($id_restaurante != ""){
                $where = 'and r.id_restaurante = '.$id_restaurante.' ';
            }
            $sql = "select r.*
            from w_restaurantes as r
            where r.nombre_restaurante <> ''   ".$where." ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function mostrar_restaurante($bd,$id_restaurante){
            $id_restaurante = mysqli_real_escape_string($bd, $id_restaurante);
            $where = "";
            if($id_restaurante != ""){
                $where = 'and r.id_restaurante = '.$id_restaurante.' ';
            }
            $sql = "select r.*, d.nombre_destino, d.url_destino
            from w_restaurantes as r
			left join w_destinos d on r.id_destino = d.id_destino
            where r.nombre_restaurante <> ''   ".$where."  order by r.id_restaurante ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function lista_restaurantes_activos($bd){
            $sql = 
                "SELECT r.*, d.nombre_destino, d.url_destino
                    FROM w_restaurantes r
                    LEFT JOIN w_destinos d ON r.id_destino = d.id_destino
                    WHERE r.status_restaurante = 1
                    ORDER BY r.orden_general";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function eliminar_restaurante($bd,$restauranteId){
            $restauranteId = mysqli_real_escape_string($bd, $restauranteId);
            $sql = "DELETE FROM w_restaurantes WHERE id_restaurante = $restauranteId";
			return (mysqli_query($bd, $sql) ? 1 : 0);
        }
        function obtener_orden_siguiente($bd){
            $sql = "SELECT COALESCE(MAX(orden_general), 0) orden_general FROM w_restaurantes";
			$consulta = mysqli_query($bd, $sql);
			$row = mysqli_fetch_assoc($consulta);
			return $row["orden_general"];
        }
    }
?>