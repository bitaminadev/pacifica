<?php 
	class HabitacionesTipo{
        function registrar_tipo($bd,$id_categoria,$url,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status){
            $id = mysqli_real_escape_string($bd, $id);
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $url = mysqli_real_escape_string($bd, $url);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $mensaje_en = mysqli_real_escape_string($bd, $mensaje_en);
            $slogan = mysqli_real_escape_string($bd, $slogan);
            $slogan_en = mysqli_real_escape_string($bd, $slogan_en);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $descripcion_en = mysqli_real_escape_string($bd, $descripcion_en);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $status = mysqli_real_escape_string($bd, $status);
			$sql = "INSERT INTO w_habitaciones_tipo (url_tipo, id_categoria, nombre_tipo, mensaje_tipo, mensaje_tipo_en, slogan_tipo, slogan_tipo_en, descripcion_tipo, descripcion_tipo_en, meta_titulo, meta_titulo_en, meta_descripcion, meta_descripcion_en, meta_keywords, meta_keywords_en, status_tipo) VALUES ('$url','$id_categoria','$nombre','$mensaje','$mensaje_en','$slogan','$slogan_en','$descripcion','$descripcion_en','$meta_titulo','$meta_titulo_en','$meta_descripcion','$meta_descripcion_en','$meta_keywords','$meta_keywords_en','$status')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}	
            return $bandera;
        }
        function actualizar_tipo($bd,$id,$id_categoria,$nombre,$mensaje,$mensaje_en,$slogan,$slogan_en,$descripcion,$descripcion_en,$meta_titulo,$meta_titulo_en,$meta_descripcion,$meta_descripcion_en,$meta_keywords,$meta_keywords_en,$status){
            $id = mysqli_real_escape_string($bd, $id);
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $mensaje_en = mysqli_real_escape_string($bd, $mensaje_en);
            $slogan = mysqli_real_escape_string($bd, $slogan);
            $slogan_en = mysqli_real_escape_string($bd, $slogan_en);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $descripcion_en = mysqli_real_escape_string($bd, $descripcion_en);
            $meta_titulo = mysqli_real_escape_string($bd, $meta_titulo);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion = mysqli_real_escape_string($bd, $meta_descripcion);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords = mysqli_real_escape_string($bd, $meta_keywords);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $status = mysqli_real_escape_string($bd, $status);
			$sql = "UPDATE w_habitaciones_tipo SET id_categoria = '".$id_categoria."', nombre_tipo = '".$nombre."', mensaje_tipo = '".$mensaje."', mensaje_tipo_en = '".$mensaje_en."', slogan_tipo = '".$slogan."', slogan_tipo_en = '".$slogan_en."', descripcion_tipo = '".$descripcion."', descripcion_tipo_en = '".$descripcion_en."', meta_titulo = '".$meta_titulo."', meta_titulo_en = '".$meta_titulo_en."', meta_descripcion = '".$meta_descripcion."', meta_descripcion_en = '".$meta_descripcion_en."', meta_keywords = '".$meta_keywords."', meta_keywords_en = '".$meta_keywords_en."', status_tipo = '".$status."' WHERE id_tipo_habitacion = '".$id."'";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
		}
		function maximo_tipo($bd){
			$consultap = mysqli_query($bd,"SELECT max(id_tipo_habitacion) as maximo FROM w_habitaciones_tipo");
			$parent = mysqli_fetch_array($consultap);
			return $parent["maximo"];
        }
        function eliminar_tipo($bd,$id){
            $id = mysqli_real_escape_string($bd, $id);
			$bandera = 0;
			$sql = "DELETE FROM w_habitaciones_tipo where id_tipo_habitacion = '".$id."' ";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function lista_tipo_habitaciones($bd,$id_categoria,$status=""){
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $status = mysqli_real_escape_string($bd, $status);
            $where = "";
            if($id_categoria != ""){
                $where = 'and t.id_categoria = '.$id_categoria.' ';
            }
            if($status != ""){
                $where .= 'and t.status_tipo = '.$status.' ';
            }
            /* if($id_tipo_habitacion != ""){
                $where = 'and id_tipo_habitacion = '.$id_tipo_habitacion.' ';
            } */
			$sql = "SELECT t.*, c.id_categoria, c.nombre_categoria FROM w_habitaciones_tipo as t 
            inner join w_habitaciones_categoria as c on t.id_categoria = c.id_categoria
            where t.nombre_tipo <> '' ".$where." ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function info_tipo_habitaciones($bd,$id_tipo_habitacion){
            $id_tipo_habitacion = mysqli_real_escape_string($bd, $id_tipo_habitacion);
            $where = "";
            if($id_tipo_habitacion != ""){
                $where = 'and id_tipo_habitacion = '.$id_tipo_habitacion.' ';
            }
			$sql = "SELECT t.*, c.id_categoria, c.nombre_categoria FROM w_habitaciones_tipo as t 
            inner join w_habitaciones_categoria as c on t.id_categoria = c.id_categoria
            where t.nombre_tipo <> '' ".$where." ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function tabla_tipo_habitaciones($bd,$id_tipo){
            $id_tipo = mysqli_real_escape_string($bd, $id_tipo);
            $where = "";
            if($id_tipo != ""){
                $where = 'and t.id_tipo_habitacion = '.$id_tipo.' ';
            }
            /* if($id_tipo_habitacion != ""){
                $where = 'and id_tipo_habitacion = '.$id_tipo_habitacion.' ';
            } */
			$sql = "SELECT t.*, c.id_categoria, c.nombre_categoria, d.id_destino, d.nombre_destino, d.url_destino   
            FROM w_habitaciones_tipo as t 
            inner join w_habitaciones_categoria as c on t.id_categoria = c.id_categoria 
            inner join w_destinos as d on d.id_destino = c.id_destino
            where t.nombre_tipo <> '' ".$where." ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function site_tipo_habitaciones($bd,$id_tipo){
            $id_tipo = mysqli_real_escape_string($bd, $id_tipo);
            $where = "";
            if($id_tipo != ""){
                $where = 'and t.id_tipo_habitacion = '.$id_tipo.' ';
            }
			$sql = "SELECT t.*, c.id_categoria, c.nombre_categoria, d.id_destino, d.nombre_destino, d.url_destino   
            FROM w_habitaciones_tipo as t 
            inner join w_habitaciones_categoria as c on t.id_categoria = c.id_categoria 
            inner join w_destinos as d on d.id_destino = c.id_destino
            where t.nombre_tipo <> '' ".$where." and t.status_tipo = 1 and c.status_categoria = 1 ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function filtro_tipo_habitaciones($bd,$id_categoria,$id_tipo){
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $id_tipo = mysqli_real_escape_string($bd, $id_tipo);
            $where = "";
            if($id_tipo != ""){
                $where = 'and t.id_tipo_habitacion = '.$id_tipo.' ';
            }
            if($id_categoria != ""){
                $where = 'and t.id_categoria = '.$id_categoria.' ';
            }
			$sql = "SELECT t.*  
            FROM w_habitaciones_tipo as t 
            inner join w_habitaciones_categoria as c on t.id_categoria = c.id_categoria 
            where t.nombre_tipo <> '' ".$where." order by nombre_tipo ";
			$consulta = mysqli_query($bd,$sql);
            return $consulta;
		}
    }
?>