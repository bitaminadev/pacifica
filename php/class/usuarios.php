<?php 
	class Usuarios{
        function select_usuarios($bd, $id_login = ""){
            $id_login = mysqli_real_escape_string($bd, $id_login);
            $where = $id_login != "" ? " AND u.id_login = $id_login" : "";
            $sql = 
                "SELECT u.*, r.nombre_rol
                    FROM w_login u
                    LEFT JOIN c_roles r ON u.id_rol = r.id_rol
                    WHERE es_superusuario = 0 $where";
            return mysqli_query($bd, $sql);
        }
        function insert_usuario($bd, $nombre_login, $id_rol, $correo_login, $password_login){
            $nombre_login = mysqli_real_escape_string($bd, $nombre_login);
            $id_rol = mysqli_real_escape_string($bd, $id_rol);
            $correo_login = mysqli_real_escape_string($bd, $correo_login);
            $password_login = mysqli_real_escape_string($bd, $password_login);
            $sql = 
                "INSERT INTO w_login (nombre_login, id_rol, correo_login, password_login)
                    VALUES ('$nombre_login', $id_rol, '$correo_login', '$password_login')";
            return mysqli_query($bd, $sql);
        }
        function update_usuario($bd, $id_login, $nombre_login, $id_rol, $correo_login, $password_login){
            $id_login = mysqli_real_escape_string($bd, $id_login);
            $nombre_login = mysqli_real_escape_string($bd, $nombre_login);
            $id_rol = mysqli_real_escape_string($bd, $id_rol);
            $correo_login = mysqli_real_escape_string($bd, $correo_login);
            $password_login = mysqli_real_escape_string($bd, $password_login);
            $sql = 
                "UPDATE w_login
                    SET
                        nombre_login = '$nombre_login',
                        id_rol = $id_rol,
                        correo_login = '$correo_login',
                        password_login = '$password_login'
                    WHERE id_login = $id_login";
            return mysqli_query($bd, $sql);
        }
        function delete_usuario($bd, $id_login){
            $id_login = mysqli_real_escape_string($bd, $id_login);
            $sql = "DELETE FROM w_login WHERE id_login = $id_login";
            return mysqli_query($bd, $sql);
        }
    }
?>