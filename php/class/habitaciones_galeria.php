<?php
    class HabitacionesGaleria{
        function registrar_galeria($bd,$id,$imagen){
            $id = mysqli_real_escape_string($bd, $id);
            $imagen = mysqli_real_escape_string($bd, $imagen);
			$sql = "INSERT INTO w_habitaciones_galeria (id_habitacion,name) VALUES ('$id','$imagen')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}	
            return $bandera;
        }
        function eliminar_galeria($bd,$id_habitacion){
            $id_habitacion = mysqli_real_escape_string($bd, $id_habitacion);
			$bandera = 0;
			$sql = "DELETE FROM w_habitaciones_galeria where id_habitacion = '".$id_habitacion."' ";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
        }
        function lista_galeria($bd,$id_habitacion){
            $id_habitacion = mysqli_real_escape_string($bd, $id_habitacion);
            $where = "";
            if($id_habitacion != ""){
                $where = 'and id_habitacion = '.$id_habitacion.' ';
            }
			$sql = "select * from w_habitaciones_galeria where name <> ''   ".$where."  order by id_galeria ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function lista_galeria_categoria($bd,$id_categoria){
            $id_categoria = mysqli_real_escape_string($bd, $id_categoria);
            $where = "";
            if($id_categoria != ""){
                $where = 'and t.id_categoria = '.$id_categoria.' ';
            }
			$sql = "SELECT f.* 
            FROM w_habitaciones_galeria as f 
            inner join w_habitaciones as h on f.id_habitacion = h.id_habitacion 
            inner join w_habitaciones_tipo as t on t.id_tipo_habitacion = h.id_tipo_habitacion 
            WHERE f.name <> '' ".$where."
            GROUP BY t.id_tipo_habitacion ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function imagen_random_tipo($bd,$id_tipo){
            $id_tipo = mysqli_real_escape_string($bd, $id_tipo);
            $where = "";
            if($id_tipo != ""){
                $where = 'and t.id_tipo_habitacion = '.$id_tipo.' ';
            }
			$sql = "SELECT f.* FROM w_habitaciones_galeria as f 
            inner join w_habitaciones as h on f.id_habitacion = h.id_habitacion 
            inner join w_habitaciones_tipo as t on t.id_tipo_habitacion = h.id_tipo_habitacion 
            WHERE f.name <> '' ".$where." 
            GROUP BY t.id_tipo_habitacion ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function imagen_random_producto($bd,$id_habitacion){
            $id_habitacion = mysqli_real_escape_string($bd, $id_habitacion);
            $where = "";
            if($id_habitacion != ""){
                $where = 'and f.id_habitacion = '.$id_habitacion.' ';
            }
			$sql = "SELECT f.* FROM w_habitaciones_galeria as f 
            inner join w_habitaciones as h on f.id_habitacion = h.id_habitacion 
            WHERE f.name <> '' ".$where." 
            Limit 1 ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
    }
?>