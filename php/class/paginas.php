<?php 
	class Paginas{
        function get_pagina($bd, $id_pagina){
            $id_pagina = mysqli_real_escape_string($bd, $id_pagina);
            $sql = 
                "SELECT p.*
                    FROM w_paginas p 
                    WHERE p.id_pagina = $id_pagina";
            return mysqli_query($bd, $sql);
        }
        function update_pagina($bd, $id_pagina, $meta_titulo_es, $meta_titulo_en, $meta_descripcion_es, $meta_descripcion_en, $meta_keywords_es, $meta_keywords_en){
            $meta_titulo_es = mysqli_real_escape_string($bd, $meta_titulo_es);
            $meta_titulo_en = mysqli_real_escape_string($bd, $meta_titulo_en);
            $meta_descripcion_es = mysqli_real_escape_string($bd, $meta_descripcion_es);
            $meta_descripcion_en = mysqli_real_escape_string($bd, $meta_descripcion_en);
            $meta_keywords_es = mysqli_real_escape_string($bd, $meta_keywords_es);
            $meta_keywords_en = mysqli_real_escape_string($bd, $meta_keywords_en);
            $sql = 
                "UPDATE w_paginas
                    SET
                        meta_titulo_es = '$meta_titulo_es',
                        meta_titulo_en = '$meta_titulo_en',
                        meta_descripcion_es = '$meta_descripcion_es',
                        meta_descripcion_en = '$meta_descripcion_en',
                        meta_keywords_es = '$meta_keywords_es',
                        meta_keywords_en = '$meta_keywords_en'
                    WHERE id_pagina = $id_pagina";
            $bandera = 0;
            if(mysqli_query($bd, $sql))
                $bandera = 1;
            return $bandera;
        }
    }
?>