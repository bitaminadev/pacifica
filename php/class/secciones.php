<?php
    class Secciones{
        function get_secciones_por_pagina($bd, $id_pagina){
            $id_pagina = mysqli_real_escape_string($bd, $id_pagina);
            $sql = 
                "SELECT s.*, p.nombre_subcarpeta 
                    FROM w_secciones s
                    LEFT JOIN w_paginas p ON s.id_pagina = p.id_pagina
                    WHERE s.id_pagina = $id_pagina";
                    // ORDER BY (CASE WHEN s.tipo_contenido = 'texto' THEN 0 ELSE 1 END) ASC";
            return mysqli_query($bd, $sql);
        }
        function get_seccion($bd, $id_seccion){
            $id_seccion = mysqli_real_escape_string($bd, $id_seccion);
            $sql = 
                "SELECT s.*, p.nombre_subcarpeta 
                    FROM w_secciones s
                    LEFT JOIN w_paginas p ON s.id_pagina = p.id_pagina
                    WHERE s.id_seccion = $id_seccion";
            return mysqli_query($bd, $sql);
        }
        function update_seccion($bd, $id_seccion, $contenido_es, $contenido_en, $tipo_contenido = ""){
            $id_seccion = mysqli_real_escape_string($bd, $id_seccion);
            $contenido_es = mysqli_real_escape_string($bd, $contenido_es);
            $contenido_en = mysqli_real_escape_string($bd, $contenido_en);
            $tipo_contenido = mysqli_real_escape_string($bd, $tipo_contenido);
            $update = "";
            if($tipo_contenido != ""){
                $update = ", tipo_contenido = '$tipo_contenido'";
            }
            $sql = 
                "UPDATE w_secciones
                    SET 
                        contenido_es = '$contenido_es', 
                        contenido_en = '$contenido_en'
                        $update 
                    WHERE id_seccion = $id_seccion";
            $bandera = 0;
            if(mysqli_query($bd, $sql))
                $bandera = 1;
            return $bandera;
        }
    }
?>