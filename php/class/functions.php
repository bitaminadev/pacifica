<?php 
class Funciones_Generales{
    function limpiarCadena($valor){
        $valor = str_ireplace("SELECT","",$valor);
        $valor = str_ireplace("COPY","",$valor);
        $valor = str_ireplace("DELETE","",$valor);
        $valor = str_ireplace("DROP","",$valor);
        $valor = str_ireplace("DUMP","",$valor);
        $valor = str_ireplace(" OR ","",$valor);
        $valor = str_ireplace("%","",$valor);
        $valor = str_ireplace("LIKE","",$valor);
        $valor = str_ireplace("--","",$valor);
        $valor = str_ireplace("^","",$valor);
        $valor = str_ireplace("[","",$valor);
        $valor = str_ireplace("]","",$valor);
        $valor = str_ireplace("!","",$valor);
        $valor = str_ireplace("¡","",$valor);
        $valor = str_ireplace("?","",$valor);
        $valor = str_ireplace("=","",$valor);
        $valor = str_ireplace("&","",$valor);
        return $valor;
    }
    function traer_meses($mes){
		$mes_retorno = "";
		switch($mes){
			case 1:				$mes_retorno = "Enero";				break;
			case 2:				$mes_retorno = "Febrero";			break;
			case 3:				$mes_retorno = "Marzo";				break;
			case 4:				$mes_retorno = "Abril";				break;
			case 5:				$mes_retorno = "Mayo";				break;
			case 6:				$mes_retorno = "Junio";				break;
			case 7:				$mes_retorno = "Julio";				break;
			case 8:				$mes_retorno = "Agosto";			break;
			case 9:				$mes_retorno = "Septiembre";		break;
			case 10:			$mes_retorno = "Octubre";			break;
			case 11:			$mes_retorno = "Noviembre";			break;
			case 12:			$mes_retorno = "Diciembre";			break;
		}
		return $mes_retorno;
	}
}
?>