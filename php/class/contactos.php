<?php
    class Contactos{
        function get_all_contactos($bd){
            $sql = "SELECT * FROM w_contactos ORDER BY fecha_contacto DESC";
            return mysqli_query($bd, $sql);
        }
        function get_contacto($bd, $id_contacto){
            $id_contacto = mysqli_real_escape_string($bd, $id_contacto);
            $sql = "SELECT * FROM w_contactos WHERE id_contacto = $id_contacto";
            return mysqli_query($bd, $sql);
        }
        function get_last_contacto_id($bd){
            $sql = "SELECT MAX(id_contacto) AS id_contacto FROM w_contactos";
            return mysqli_query($bd, $sql);
        }
        function insert_contacto($bd, $nombre, $es_socio, $correo, $telefono, $mensaje, $fecha_evento, $fecha_respaldo, $cantidad_invitados, $fecha_contacto, $status, $tipo_contacto){
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $es_socio = mysqli_real_escape_string($bd, $es_socio);
            $correo = mysqli_real_escape_string($bd, $correo);
            $telefono = mysqli_real_escape_string($bd, $telefono);
            $mensaje = mysqli_real_escape_string($bd, $mensaje);
            $fecha_evento = mysqli_real_escape_string($bd, $fecha_evento);
            $fecha_evento = ($fecha_evento == "" ? "NULL" : "'$fecha_evento'");
            $fecha_respaldo = mysqli_real_escape_string($bd, $fecha_respaldo);
            $fecha_respaldo = ($fecha_respaldo == "" ? "NULL" : "'$fecha_respaldo'");
            $cantidad_invitados = mysqli_real_escape_string($bd, $cantidad_invitados);
            $fecha_contacto = mysqli_real_escape_string($bd, $fecha_contacto);
            $status = mysqli_real_escape_string($bd, $status);
            $tipo_contacto = mysqli_real_escape_string($bd, $tipo_contacto);
            $sql = 
                "INSERT INTO w_contactos (nombre, es_socio, correo, telefono, mensaje, fecha_evento, fecha_respaldo, cantidad_invitados, fecha_contacto, status, tipo_contacto)
                    VALUES ('$nombre', $es_socio, '$correo', '$telefono', '$mensaje', $fecha_evento, $fecha_respaldo, $cantidad_invitados, '$fecha_contacto', $status, '$tipo_contacto')";
            $bandera = 0;
            if(mysqli_query($bd, $sql))
                $bandera = 1;
            return $bandera;
        }
        function notificar_nuevo_contacto($bd, $mailer, $id_contacto){
            $id_contacto = mysqli_real_escape_string($bd, $id_contacto);
            $result = $this->get_contacto($bd, $id_contacto);
            $contacto = mysqli_fetch_assoc($result);
            $bodasBlock = '';
            if($contacto["tipo_contacto"] == "bodas"){
                $dateParts = explode("-", $contacto["fecha_evento"]);
                $dateParts[1] = $this->replaceMonth($dateParts[1]);
                $fecha_evento = implode("-", [$dateParts[2], $dateParts[1], $dateParts[0]]);
                $dateParts = explode("-", $contacto["fecha_respaldo"]);
                $dateParts[1] = $this->replaceMonth($dateParts[1]);
                $fecha_respaldo = $dateParts[2]." de ".$dateParts[1]." del ".$dateParts[0];
                $bodasBlock = '
                    <tr><td>Fecha de evento: </td><td>'.$fecha_evento.'</td></tr>
                    <tr><td>Fecha de respaldo: </td><td>'.$fecha_respaldo.'</td></tr>
                    <tr><td>Cantidad de invitados: </td><td>'.$contacto["cantidad_invitados"].'</td></tr>
                ';
            }
            $contenido = '
                <table width="600" align="center" valign="center" cellpadding="10">
                    <tbody>
                        <tr><td colspan="2" style="text-align:center; background-color: rgba(2,44,75,1);"><img style="width: 25%;" src="https://demos.posicionart.com/pacifica/images/pacifica_small.png"></td></tr>
                        <tr><td colspan="2">Correo enviado desde: Formulario de '.ucfirst($contacto["tipo_contacto"]).'</td></tr>
                        '.($contacto["tipo_contacto"] == "contacto" ? '<tr><td>¿Es socio?: </td><td>'.($contacto["es_socio"] == 0 ? 'No' : 'S&iacute;').'</td></tr>' : '').'
                        <tr><td style="min-width: 140px;">Nombre: </td><td>'.utf8_decode($contacto["nombre"]).'</td></tr>
                        <tr><td>Correo: </td><td><a href="mailto:'.$contacto["correo"].'" target="_blank">'.$contacto["correo"].'</a></td></tr>
                        <tr><td>Tel&eacute;fono: </td><td>'.$contacto["telefono"].'</td></tr>
                        <tr><td>Mensaje: </td><td>'.utf8_decode($contacto["mensaje"]).'</td></tr>
                        '.($contacto["tipo_contacto"] == "bodas" ? $bodasBlock : '').'
                        </tbody>
                </table>
            ';
            // $mailer->IsSMTP();
            // $mailer->SMTPAuth = true;
            // $mailer->Host = ""; 
            // $mailer->Username = "";
            // $mailer->Password = ""; 
            // $mailer->Port = 587; 
        	$subject = "PACÍFICA - Nuevo contacto";
            $mailer->From = "servicio.ventas@pacifica.com.mx";
            $mailer->FromName = "Pacífica Resort";
            $mailer->AddAddress("webmaster@pacifica.com.mx");
            if($contacto["tipo_contacto"] == "contacto"){
                $mailer->AddBCC("it.notificaciones@pacifica.com.mx");
            }
            if($contacto["tipo_contacto"] == "bodas"){
                $mailer->AddBCC("tuboda@pacifica.com.mx");
            }
            $mailer->AddBCC("testposicionart@gmail.com");
            $mailer->Subject = $subject;
            $mailer->Body = $contenido;
            $mailer->MsgHTML($contenido);
            $mailer->CharSet = 'UTF-8';
            return $mailer->Send();
        }
        private function replaceMonth($month){
            $str = "";
            switch(intval($month)){
                case 1: $str = "enero"; break;
                case 2: $str = "febrero"; break;
                case 3: $str = "marzo"; break;
                case 4: $str = "abril"; break;
                case 5: $str = "mayo"; break;
                case 6: $str = "junio"; break;
                case 7: $str = "julio"; break;
                case 8: $str = "agosto"; break;
                case 9: $str = "septiembre"; break;
                case 10: $str = "octubre"; break;
                case 11: $str = "noviembre"; break;
                case 12: $str = "diciembre"; break;
            }
            return $str;
        }
    }
?>