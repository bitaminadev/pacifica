<?php 
	class Permisos{
        function select_permisos($bd, $id_rol){
            $id_rol = mysqli_real_escape_string($bd, $id_rol);
            $sql = 
                "SELECT p.*, m.nombre_modulo
                    FROM c_permisos p
                    LEFT JOIN c_roles r ON p.id_rol = r.id_rol
                    LEFT JOIN c_modulos m ON p.id_modulo = m.id_modulo
                    WHERE p.id_rol = $id_rol
                    ORDER BY p.id_modulo";
            return mysqli_query($bd, $sql);
        }
        function insert_permiso($bd, $id_rol, $id_modulo, $p_leer, $p_crear, $p_actualizar, $p_eliminar){
            $id_rol = mysqli_real_escape_string($bd, $id_rol);
            $id_modulo = mysqli_real_escape_string($bd, $id_modulo);
            $p_leer = mysqli_real_escape_string($bd, $p_leer);
            $p_crear = mysqli_real_escape_string($bd, $p_crear);
            $p_actualizar = mysqli_real_escape_string($bd, $p_actualizar);
            $p_eliminar = mysqli_real_escape_string($bd, $p_eliminar);
            $sql = 
                "INSERT INTO c_permisos (id_rol, id_modulo, p_leer, p_crear, p_actualizar, p_eliminar)
                    VALUES ($id_rol, $id_modulo, $p_leer, $p_crear, $p_actualizar, $p_eliminar)";
            return mysqli_query($bd, $sql);
        }
        function update_permiso($bd, $id_permiso, $p_leer, $p_crear, $p_actualizar, $p_eliminar){
            $id_permiso = mysqli_real_escape_string($bd, $id_permiso);
            $p_leer = mysqli_real_escape_string($bd, $p_leer);
            $p_crear = mysqli_real_escape_string($bd, $p_crear);
            $p_actualizar = mysqli_real_escape_string($bd, $p_actualizar);
            $p_eliminar = mysqli_real_escape_string($bd, $p_eliminar);
            $sql = 
                "UPDATE c_permisos
                    SET
                        p_leer = $p_leer,
                        p_crear = $p_crear,
                        p_actualizar = $p_actualizar,
                        p_eliminar = $p_eliminar
                    WHERE id_permiso = $id_permiso";
            return mysqli_query($bd, $sql);
        }
    }
?>