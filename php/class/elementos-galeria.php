<?php
    class ElementosGaleria{
        function get_elementos_por_seccion($bd, $id_seccion){
            $id_seccion = mysqli_real_escape_string($bd, $id_seccion);
            $sql = 
                "SELECT g.*, p.nombre_subcarpeta 
                    FROM w_galeria_elementos g
                    LEFT JOIN w_secciones s ON g.id_seccion = s.id_seccion
                    LEFT JOIN w_paginas p ON s.id_pagina = p.id_pagina
                    WHERE g.id_seccion = $id_seccion";
            return mysqli_query($bd, $sql);
        }
        function get_elemento($bd, $id_elemento){
            $id_elemento = mysqli_real_escape_string($bd, $id_elemento);
            $sql = 
                "SELECT g.*, p.nombre_subcarpeta 
                    FROM w_galeria_elementos g
                    LEFT JOIN w_secciones s ON g.id_seccion = s.id_seccion
                    LEFT JOIN w_paginas p ON s.id_pagina = p.id_pagina
                    WHERE g.id_elemento = $id_elemento";
            return mysqli_query($bd, $sql);
        }
        function insert_elemento($bd, $id_seccion, $contenido, $contenido_cubierta_video, $titulo_elemento_es, $titulo_elemento_en, $tipo_elemento){
            $id_seccion = mysqli_real_escape_string($bd, $id_seccion);
            $contenido = mysqli_real_escape_string($bd, $contenido);
            $contenido_cubierta_video = mysqli_real_escape_string($bd, $contenido_cubierta_video);
            $titulo_elemento_es = mysqli_real_escape_string($bd, $titulo_elemento_es);
            $titulo_elemento_en = mysqli_real_escape_string($bd, $titulo_elemento_en);
            $tipo_elemento = mysqli_real_escape_string($bd, $tipo_elemento);
            $sql = 
                "INSERT INTO w_galeria_elementos (id_seccion, contenido, contenido_cubierta_video, titulo_elemento_es, titulo_elemento_en, tipo_elemento)
                    VALUES ($id_seccion, '$contenido', '$contenido_cubierta_video', '$titulo_elemento_es', '$titulo_elemento_en', '$tipo_elemento')";
            $bandera = 0;
            if(mysqli_query($bd, $sql))
                $bandera = 1;
            return $bandera;
        }
        function get_last_elemento($bd){
            $sql = "SELECT * FROM w_galeria_elementos ORDER BY id_elemento DESC LIMIT 1";
            return mysqli_query($bd, $sql);
        }
        function update_elemento($bd, $id_elemento, $contenido, $contenido_cubierta_video, $titulo_elemento_es, $titulo_elemento_en, $tipo_elemento){
            $id_elemento = mysqli_real_escape_string($bd, $id_elemento);
            $contenido = mysqli_real_escape_string($bd, $contenido);
            $contenido_cubierta_video = mysqli_real_escape_string($bd, $contenido_cubierta_video);
            $titulo_elemento_es = mysqli_real_escape_string($bd, $titulo_elemento_es);
            $titulo_elemento_en = mysqli_real_escape_string($bd, $titulo_elemento_en);
            $tipo_elemento = mysqli_real_escape_string($bd, $tipo_elemento);
            $sql = 
                "UPDATE w_galeria_elementos 
                    SET 
                        contenido = '$contenido',
                        contenido_cubierta_video = '$contenido_cubierta_video',
                        titulo_elemento_es = '$titulo_elemento_es',
                        titulo_elemento_en = '$titulo_elemento_en',
                        tipo_elemento = '$tipo_elemento'
                    WHERE id_elemento = $id_elemento";
            $bandera = 0;
            if(mysqli_query($bd, $sql))
                $bandera = 1;
            return $bandera;
        }
        function delete_elemento($bd, $id_elemento){
            $id_elemento = mysqli_real_escape_string($bd, $id_elemento);
            $sql = "DELETE FROM w_galeria_elementos WHERE id_elemento = $id_elemento";
            $bandera = 0;
            if(mysqli_query($bd, $sql))
                $bandera = 1;
            return $bandera;
        }
    }
?>