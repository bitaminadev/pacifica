<?php 
	class Modulos{
        function select_modulos($bd, $id_modulo = ""){
            $id_modulo = mysqli_real_escape_string($bd, $id_modulo);
            $where = $id_modulo != "" ? " AND m.id_modulo = $id_modulo" : "";
            $sql = 
                "SELECT m.*
                    FROM c_modulos m
                    WHERE 1 $where
                    ORDER BY m.id_modulo";
            return mysqli_query($bd, $sql);
        }
    }
?>