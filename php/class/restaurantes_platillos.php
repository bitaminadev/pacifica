<?php 
	class RestaurantesPlatillos{
        function registrar_platillo($bd,$id_restaurante,$nombre,$descripcion,$foto,$destacado,$status,$nombre_platillo_en,$descripcion_platillo_en){
            $id_restaurante = mysqli_real_escape_string($bd, $id_restaurante);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $foto = mysqli_real_escape_string($bd, $foto);
            $destacado = mysqli_real_escape_string($bd, $destacado);
            $status = mysqli_real_escape_string($bd, $status);
            $nombre_platillo_en = mysqli_real_escape_string($bd, $nombre_platillo_en);
            $descripcion_platillo_en = mysqli_real_escape_string($bd, $descripcion_platillo_en);
			$sql = "INSERT INTO w_restaurantes_platillos (id_restaurante,nombre_platillo,descripcion_platillo,foto_platillo,destacado,status_platillo,nombre_platillo_en,descripcion_platillo_en) VALUES ('$id_restaurante','$nombre','$descripcion','$foto','$destacado','$status','$nombre_platillo_en','$descripcion_platillo_en')";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}	
            return $bandera;
        }
        function actualizar_platillo($bd,$id,$id_restaurante,$nombre,$descripcion,$foto,$destacado,$status,$nombre_platillo_en,$descripcion_platillo_en){
            $id = mysqli_real_escape_string($bd, $id);
            $nombre = mysqli_real_escape_string($bd, $nombre);
            $descripcion = mysqli_real_escape_string($bd, $descripcion);
            $foto = mysqli_real_escape_string($bd, $foto);
            $destacado = mysqli_real_escape_string($bd, $destacado);
            $status = mysqli_real_escape_string($bd, $status);
            $nombre_platillo_en = mysqli_real_escape_string($bd, $nombre_platillo_en);
            $descripcion_platillo_en = mysqli_real_escape_string($bd, $descripcion_platillo_en);
			$sql = "UPDATE w_restaurantes_platillos SET id_restaurante = '$id_restaurante', nombre_platillo = '$nombre', descripcion_platillo = '$descripcion', foto_platillo = '$foto', destacado ='$destacado', status_platillo ='$status', nombre_platillo_en = '$nombre_platillo_en', descripcion_platillo_en = '$descripcion_platillo_en' WHERE id_platillo = '$id'";
			if(mysqli_query($bd,$sql)){
				$bandera = 1;
			}else{
				$bandera = 0;
			}
			return $bandera;
		}
        function info_platillo($bd,$id_restaurante){
            $id_restaurante = mysqli_real_escape_string($bd, $id_restaurante);
            $sql = "SELECT id_platillo  
            FROM w_restaurantes_platillos 
            where id_restaurante = '".$id_restaurante."' ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function platillo_destacado_general($bd,$id_destino){
            $id_destino = mysqli_real_escape_string($bd, $id_destino);
            $where = "";
            if($id_destino != ""){
                $where = 'and r.id_destino = '.$id_destino.' ';
            }
            $sql = "SELECT p.*, r.nombre_restaurante, r.id_restaurante, r.url_restaurante 
            FROM w_restaurantes_platillos as p 
            inner join w_restaurantes as r on p.id_restaurante = r.id_restaurante 
            where p.nombre_platillo <> '' and p.destacado_principal = 1  ".$where."  order by p.id_platillo ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
        }
        function platillo_destacado_restaurante($bd,$id_restaurante){
            $id_restaurante = mysqli_real_escape_string($bd, $id_restaurante);
            $where = "";
            if($id_restaurante != ""){
                $where = 'and r.id_restaurante = '.$id_restaurante.' ';
            }
            $sql = "SELECT p.*, r.nombre_restaurante, r.id_restaurante, r.url_restaurante 
            FROM w_restaurantes_platillos as p 
            inner join w_restaurantes as r on p.id_restaurante = r.id_restaurante 
            where p.nombre_platillo <> '' and p.destacado = 1  ".$where."  order by p.id_platillo ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
		}
		function maximo_platillo($bd){
			$consultap = mysqli_query($bd,"SELECT max(id_platillo) as maximo FROM w_restaurantes_platillos");
			$parent = mysqli_fetch_array($consultap);
			return $parent["maximo"];
        }
        function set_destacado_principal($bd, $id_platillo){
            $id_platillo = mysqli_real_escape_string($bd, $id_platillo);
            $bandera = 0;
            if(mysqli_query($bd,"UPDATE w_restaurantes_platillos SET destacado_principal = 0")){
                if(mysqli_query($bd,"UPDATE w_restaurantes_platillos SET destacado_principal = 1 WHERE id_platillo = $id_platillo")){
                    $bandera = 1;
                }	
            }
            return $bandera;
        }
    }
?>