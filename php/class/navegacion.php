<?php 
	class Menu{
        function nav_items_category($bd,$id_destino){
            $where = "";
            if($id_destino != ""){
                $where = 'and d.id_destino = '.$id_destino.' ';
            }
            $sql = "SELECT c.id_categoria, c.nombre_categoria, c.url_categoria
            FROM w_habitaciones_categoria as c 
            inner join w_destinos as d on d.id_destino = c.id_destino
            where c.nombre_categoria <> '' and c.status_categoria = 1 ".$where." ";
            $consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function nav_items_type($bd,$id_categoria){
            $where = "";
            if($id_categoria != ""){
                $where = 'and t.id_categoria = '.$id_categoria.' ';
            }
            $sql = "SELECT t.id_tipo_habitacion, t.nombre_tipo, t.url_tipo
            FROM w_habitaciones_tipo as t 
            where t.nombre_tipo <> '' and t.status_tipo = 1 ".$where." ";
            $consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function nav_items_product($bd,$id_tipo){
            $where = "";
            if($id_tipo != ""){
                $where = 'and h.id_tipo_habitacion = '.$id_tipo.' ';
            }
            $sql = "SELECT h.id_habitacion, h.nombre_habitacion, h.url_habitacion
            FROM w_habitaciones as h 
            where h.nombre_habitacion <> '' and h.status_habitacion = 1 ".$where." ";
            $consulta = mysqli_query($bd,$sql);
            return $consulta;
        }
        function nav_items_restaurants($bd,$id_destino){
            $where = "";
            if($id_destino != ""){
                $where = 'and r.id_destino = '.$id_destino.' ';
            }
            $sql = "SELECT r.*, d.url_destino
            from w_restaurantes as r
			left join w_destinos d on r.id_destino = d.id_destino
            where r.nombre_restaurante <> ''  and r.status_restaurante = 1  ".$where."  order by r.orden_general ";
			$consulta = mysqli_query($bd,$sql);
			return $consulta;
		}
    }
?>