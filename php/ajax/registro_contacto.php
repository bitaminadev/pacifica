<?php @session_start(); 
	if(isset($_GET["ev"])){
        switch($_GET["ev"]){
            case 1: registrar_contacto();               break;
        }
    }
    function registrar_contacto(){
        date_default_timezone_set("America/Mexico_City");
        setlocale(LC_TIME, 'es_ES.UTF-8');
        require("../controllers/connect_sql.php");
        require("../class/contactos.php");
        $m_contactos = new Contactos();

        $nombre = utf8_encode($_POST["nombre"]);
        $es_socio = $_POST["es_socio"];
        $correo = utf8_encode($_POST["correo"]);
        $telefono = $_POST["telefono"];
        $mensaje = utf8_encode($_POST["mensaje"]);
        $fecha_evento = $_POST["fecha_evento"];
        $fecha_respaldo = $_POST["fecha_respaldo"];
        $cantidad_invitados = $_POST["cantidad_invitados"];
        $fecha_contacto = date("Y-m-d H:i:s");
        $status = 0;
        $tipo_contacto = $_POST["tipo_contacto"];

        $bandera = $m_contactos->insert_contacto($bd, $nombre, $es_socio, $correo, $telefono, $mensaje, $fecha_evento, $fecha_respaldo, $cantidad_invitados, $fecha_contacto, $status, $tipo_contacto);
        if($bandera == 1){
            require("../controllers/class.phpmailer.php");
            require("../controllers/class.smtp.php");
            $mailer = new PHPMailer();
            $result = $m_contactos->get_last_contacto_id($bd);
            $contacto = mysqli_fetch_assoc($result);
            $id_contacto = $contacto["id_contacto"];
            $correo = $m_contactos->notificar_nuevo_contacto($bd, $mailer, $id_contacto);
        }

        echo $bandera;
        require("../controllers/cerrar_php.php");
    }
?>
