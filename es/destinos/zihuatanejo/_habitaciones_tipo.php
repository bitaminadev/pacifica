<!DOCTYPE html>
<?php 
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/habitaciones_tipo.php");
    require("../../../php/class/habitaciones.php");
    require("../../../php/class/habitaciones_galeria.php");
    require("../../../php/class/navegacion.php");
    $_nb2 = "active_nav_sect";
    $_ds1 = "show";
    $id_destino = 2;
    $url_destino = "zihuatanejo";

    //Datos generales del destino
    $destinos = new Destinos();
    $destino = $destinos->lista_destinos($bd, $id_destino);
    $destino = mysqli_fetch_assoc($destino);
    
    //Datos generales del tipo de habitación
    $myConsulta = new HabitacionesTipo();
    $consulta = $myConsulta->info_tipo_habitaciones($bd,$id_tipo);
    $var = mysqli_fetch_array($consulta);
    $id_categoria = $var["id_categoria"];

    $title = utf8_encode($var["meta_titulo"]);
	$description = utf8_encode($var["meta_descripcion"]);
	$keywords = utf8_encode($var["meta_keywords"]);
    
    //Habitaciones
    $consulta_hab = new Habitaciones();
    $habitaciones = $consulta_hab->lista_habitaciones($bd,"",$id_tipo);

    $consulta_gal = new HabitacionesGaleria();


    $page = 'home';
    include('../../commons/_headOpen.php');

    if($var["status_tipo"] == 0){
        header("Location: ".$httpProtocol.$host.$url.$ln);
        die();
    }
    
    $js .= '';
    $css .= '
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/destinos/producto.html');
    include('../../views/footer.html');
?>
</body>
</html>