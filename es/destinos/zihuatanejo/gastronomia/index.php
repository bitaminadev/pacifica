<!DOCTYPE html>
<?php 
    $page = 'home';
    require("../../../../php/controllers/connect_sql.php");
    require("../../../../php/class/destinos.php");
    require("../../../../php/class/restaurantes.php");
    require("../../../../php/class/restaurantes_platillos.php");
    require("../../../../php/class/navegacion.php");
    $_nb4 = "active_nav_sect"; 
    $_ds2 = "show";
    $id_destino = 2;
    $url_destino = "zihuatanejo";

    $myConsulta = new Destinos();
    $restaurantes = new Restaurantes();
    $platillos = new RestaurantesPlatillos();
    $consulta = $myConsulta->lista_destinos($bd,$id_destino);
    $var = mysqli_fetch_array($consulta);
    
    $con_platillo = $platillos ->platillo_destacado_general($bd,1);
    $vpl = mysqli_fetch_array($con_platillo);

    $cn_rst = $restaurantes ->lista_restaurantes($bd,"",1);

    $id_pagina = 18;
    include("../../../../php/class/paginas.php");
    include("../../../../php/class/secciones.php");
    include("../../../../php/class/elementos-galeria.php");
    $m_paginas = new Paginas();
    $m_secciones = new Secciones();
    $m_elementos = new ElementosGaleria();
    $result = $m_paginas->get_pagina($bd, $id_pagina);
    $pagina = mysqli_fetch_assoc($result);
    $pagina = array_map("utf8_encode", $pagina);
    $result = $m_secciones->get_secciones_por_pagina($bd, $id_pagina);
    $secciones = [];
    while($seccion = mysqli_fetch_assoc($result)){
        array_push($secciones, array_map("utf8_encode", $seccion));
    }
    $title = $pagina["meta_titulo_es"];
	$description = $pagina["meta_descripcion_es"];
	$keywords = $pagina["meta_keywords_es"];

    include('../../../commons/_headOpen.php');
    $js .= '';
    $css .= '
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../../commons/_headClose.php');
    include('../../../commons/_nav.php');
    // include('../../../views/gastronomia/index.html');
    include('../../../views/destinos/zihuatanejo/gastronomia/index.php');
    include('../../../views/footer.html');
?>
</body>
</html>
