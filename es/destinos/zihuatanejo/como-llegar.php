<!DOCTYPE html>
<?php 
    $title = 'Rutas para llegar a Pacífica Grand Zihuatanejo';
	$description = 'Descubre cómo llegar a Pacífica Grand Zihuatanejo en auto desde diferentes ciudades de México, con un aproximado de horas.';
	$keywords = 'destinos zihuatanejo, mapas zihuatanejo, rutas zihuatanejo, como llegara  Pacifica Grand';
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/navegacion.php");
    $_nb3 = "active_nav_sect"; 
    $id_destino = 2;
    $url_destino = "zihuatanejo";

    $myConsulta = new Destinos();
    $consulta = $myConsulta->lista_destinos($bd,$id_destino);
    $var = mysqli_fetch_array($consulta);
    // $title = 'Pacífica | '.utf8_encode($var["nombre_destino"]);
    // $description = '';
    
    $a = 17.621641;
    $b = -101.547965;

    $page = 'home';
    include('../../commons/_headOpen.php');
    $js .= '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBq1TmYzh04WOLyFlZQdMWAkMQXYXNvA60"></script>
    <script src="'.$httpProtocol.$host.$url.'js/como-llegar.js"></script>
    <script>var a='.$a.',b='.$b.';</script>
    ';
    $css .= '
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/como-llegar.html');
    include('../../views/footer.html');
?>
</body>
</html>
