<!DOCTYPE html>
<?php 
    $title = 'Rutas y Mapas para Pacífica Resort Ixtapa';
	$description = 'Mapas y rutas a Pacifica Resort Ixtapa desde diferentes ciudades de México, con un aproximado de horas en automóvil.';
	$keywords = 'mapas ixtapa, rutas ixtapa, como llegar a Pacifica Resort, carretera a ixtapa';
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/navegacion.php");
    $_nb3 = "active_nav_sect"; 
    $id_destino = 1;
    $url_destino = "ixtapa";

    $myConsulta = new Destinos();
    $consulta = $myConsulta->lista_destinos($bd,1);
    $var = mysqli_fetch_array($consulta);
    // $title = 'Pacífica | '.utf8_encode($var["nombre_destino"]);
	// $description = '';

    $a = 17.651639;
    $b = -101.598864;

    $page = 'home';
    include('../../commons/_headOpen.php');
    $js .= '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBq1TmYzh04WOLyFlZQdMWAkMQXYXNvA60"></script>
    <script src="'.$httpProtocol.$host.$url.'js/como-llegar.js"></script>
    <script>var a='.$a.',b='.$b.';</script>
    ';
    $css .= '
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/como-llegar.html');
    include('../../views/footer.html');
?>
</body>
</html>