<!DOCTYPE html>
<?php 
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/habitaciones_categoria.php");
    require("../../../php/class/habitaciones_tipo.php");
    require("../../../php/class/habitaciones_galeria.php");
    require("../../../php/class/habitaciones_categoria_galeria.php");
    require("../../../php/class/navegacion.php");
    $_nb2 = "active_nav_sect";
    $_ds1 = "show";
    $id_destino = 1;
    $url_destino = "ixtapa";

    //Datos generales del destino
    $destinos = new Destinos();
    $destino = $destinos->lista_destinos($bd, $id_destino);
    $destino = mysqli_fetch_assoc($destino);

    $categorias = new HabitacionesCategoria();
    $myConsulta = new HabitacionesTipo();
    $consulta_gal = new HabitacionesCategoriaGaleria();
    $consulta_gal_hab = new HabitacionesGaleria();

    //Información categoría
    $consulta_cat = $categorias->lista_categorias_habitaciones($bd,$id_categoria,1);
    $var = mysqli_fetch_array($consulta_cat);

    $title = utf8_encode($var["meta_titulo"]);
	$description = utf8_encode($var["meta_descripcion"]);
	$keywords = utf8_encode($var["meta_keywords"]);

    //Consultar los tipos de habitación
    $consulta = $myConsulta->lista_tipo_habitaciones($bd,$id_categoria,1);
    //Galería random de habitaciones de acuerdo a categoría
    $habitaciones_gal = $consulta_gal->lista_galeria($bd,$id_categoria);
    

    $page = 'home';
    include('../../commons/_headOpen.php');

    if($var["status_categoria"] == 0){
        header("Location: ".$httpProtocol.$host.$url.$ln);
        die();
    }

    $js .= '<script src="'.$httpProtocol.$host.$url.'js/slick.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/destinos.js"></script>';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/slick.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/destinos/habitaciones.html');
    include('../../views/footer.html');
?>
</body>
</html>