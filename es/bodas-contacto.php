<!DOCTYPE html>
<?php 
    $title = 'Pacífica | Contacto para bodas';
	$description = 'Planeemos juntos la boda de tus sueños en Pacífica Resort. Ponte en contacto con nosotros aquí.';
	$keywords = 'contacto pacífica, bodas pacífica, teléfono bodas pacífica, email pacífica resort';
    $page = 'home';
    include('commons/_headOpen.php');
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");
    
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/contacto.js"></script>';
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesContact.css">';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/bodas-contacto.html');
    include('views/footer.html');
?>
</body>
</html>