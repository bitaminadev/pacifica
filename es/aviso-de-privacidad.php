<!DOCTYPE html>
<?php 
    $title = 'Pacífica | Resort Aviso de privacidad';
	$description = 'Te invitamos a conocer nuestro aviso de privacidad';
	$keywords = 'aviso privacidad, declaración de privacidad, datos personales';
    $page = 'home';
    include('commons/_headOpen.php');
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");
    
    $js .= '';
    $css .= '';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/aviso-de-privacidad.html');
    include('views/footer.html');
?>
</body>
</html>