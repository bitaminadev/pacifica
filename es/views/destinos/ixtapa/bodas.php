<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/pacifica_ixtapa_luxury_main_banner.jpg" alt="Ixtapa">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0">Ixtapa</h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">BODAS</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[103]; ?></h2>
                    <p class="mb-4 text-line-bottom text-center">Servicios nupciales.</p>
                    <div class="my-4 text-normal">
                        <p><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[104]; ?></p>
                    </div>
                </div>
                <div class="w-100 p-0 mt-5">
                    <div class="slide-destinations">
                        <?php 
                            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[105];
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 105);
                            $cont = 999;
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $cont ++;
                                $elemento = array_map("utf8_encode", $elemento);
                                if($elemento["tipo_elemento"] == "imagen"){ ?>
                                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                                    <figure class="v-slide" ><div class="youtubeContainer">
                                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                    </div></figure>
                                <?php
                                } 
                            }
                        ?>
                    </div>
                </div>
                <div class="w-100 mb-5">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 pr-0 card-rel m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url.''.'images/destinos/bodas/ixtapa_bodas_destacado.jpg';  ?>" alt="01">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down bodas px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Contáctanos</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom">¿Empezamos a planear tu boda ideal?</h4>
                                <div class="text-normal my-4 text-justify text-mini">
                                    <p>Déjanos saber qué planes tienes y te haremos llegar una cotización.<br>
                                        <a target="_blank" href="maito:tuboda@pacifica.com.mx">tuboda@pacifica.com.mx</a><br>
                                        <strong>MX </strong>55 4160 9065 <strong>&</strong> 800 711 1934 ext. 9065<br>
                                        <strong>USA </strong>877 387 2370 ext. 9065<br>
                                        <strong>Canadá </strong>866 924 8888 ext. 9065
                                    </p>
                                </div>
                                <a class="btn_black_dest text-uppercase text-left" href="<?php echo $httpProtocol.$host.$url.$ln; ?>bodas-contacto<?php echo $ext; ?>">Cotiza ya</a>
                            </div>
                        </section>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>