<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/<?php echo utf8_encode($var["img_banner_habitaciones"]); ?>" alt="<?php echo utf8_encode($var["nombre_destino"]); ?>">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo utf8_encode($var["nombre_destino"]); ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">¿Qué hacer?</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 pt-4 pb-0 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[51]; ?></h2>
                    <p class="mb-4 text-line-bottom text-center">Actividades en Pacífica Resort Ixtapa</p>
                    <div class="my-4 text-normal">
                        <p><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[52]; ?></p>
                    </div>
                </div>
                <div class="w-100 mb-5 mt-5">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 pr-0 card-rel m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[55]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[55]; ?>" alt="01">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Compras</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[53]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[54]; ?></p>
                                <a class="btn_black_dest text-uppercase text-left" href="<?php echo $httpProtocol.$host.$url.$ln.''.'destinos/ixtapa/que-hacer/compras'.$ext.'';  ?>">Más información</a>
                            </div>
                        </section>
                    </div>
                    <div class="row">
                        <section class="col-md-12 col-lg-6 order-2 order-lg-1 card-white">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Dónde nadar</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[56]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[57]; ?></p>
                                <a class="btn_black_dest text-uppercase text-left" href="<?php echo $httpProtocol.$host.$url.$ln.''.'destinos/ixtapa/que-hacer/albercas'.$ext.'';  ?>">Más información</a>
                            </div>
                        </section>
                        <figure class="col-md-12 col-lg-6 card-rel pl-0 order-1 order-lg-2 m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[58]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[58]; ?>" alt="01">
                        </figure>
                    </div>
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 card-rel pr-0 m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[61]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[61]; ?>" alt="01">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">¿Qué hacer?</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[59]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[60]; ?></p>
                                <a class="btn_black_dest text-uppercase text-left" href="<?php echo $httpProtocol.$host.$url.$ln.''.'destinos/ixtapa/que-hacer/actividades'.$ext.'';  ?>">Más información</a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>     
        </div>
    </div>
</div>