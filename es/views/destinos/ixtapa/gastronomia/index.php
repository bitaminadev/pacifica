<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[112]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[112]; ?>" alt="<?php echo utf8_encode($var["nombre_destino"]); ?>">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo utf8_encode($var["nombre_destino"]); ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">Gastronomía</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 pt-4 pb-0 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[113]; ?></h2>
                </div>
                <div class="w-100 mb-5 mt-5">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 pr-0 card-rel m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[115]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[115]; ?>" alt="Pacifica luxury suites aqua">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[114]; ?></p>
                            </div>
                        </section>
                    </div>
                    <?php
                    if($vpl["nombre_platillo"] != ""){
                        echo '<div class="row">
                            <section class="col-md-12 col-lg-6 card-white order-2 order-lg-1">
                                <div class="map-down px-4 text-left">
                                    <p class="m-0 p-0 text-uppercase text-mini">Platillo recomendado</p>
                                    <h4 class="m-0 p-0 text-uppercase subtitle-card-bottom">'.utf8_encode($vpl["nombre_platillo"]).'</h4>
                                    <p class="text-normal my-4 text-justify text-mini">'.utf8_encode($vpl["descripcion_platillo"]).'</p>
                                    <a class="btn_black_dest w-75" href="'.$httpProtocol.$host.$url.$ln.'destinos/ixtapa/gastronomia/'.$vpl["url_restaurante"].'">Conoce más de '.utf8_encode($vpl["nombre_restaurante"]).'</a>
                                </div>
                            </section>
                            <figure class="col-md-12 col-lg-6 card-rel m-0 pl-0 order-1 order-lg-2">
                                <img class="img-fluid" src="'.$httpProtocol.$host.$url.'images/destinos/gastronomia/platillos/'.$vpl["foto_platillo"].'" alt="Pacifica luxury suites">
                            </figure>
                        </div>';
                    }
                    ?>
                </div>

                <div class="row">
                    <?php 
                        while($vrst = mysqli_fetch_array($cn_rst)){
                            echo '<figure class="col-md-12 col-lg-6 card-rel pr-0 border m-0 p-0">
                                    <img class="img-fluid fit-image" src="'.$httpProtocol.$host.$url.'images/destinos/gastronomia/general/'.$vrst["imagen_restaurante"].'" alt="'.utf8_encode($vrst["nombre_restaurante"]).'">
                                    <figcaption class="w-100 h-100 card-link card-link-tr">
                                        <a class="text-white text-center text-uppercase pt-5 w-100 d-block link-service" href="'.$httpProtocol.$host.$url.$ln.'destinos/'.$vrst["url_destino"].'/gastronomia/'.$vrst["url_restaurante"].'">'.utf8_encode($vrst["nombre_restaurante"]).'
                                            '.($vrst["imagen_logo"] != "" ? '<img src="'.$httpProtocol.$host.$url.'images/destinos/gastronomia/general/'.$vrst["imagen_logo"].'" alt="Logo '.utf8_encode($vrst["nombre_restaurante"]).'" class="previewRestaurantLogo">' : '').'
                                        </a>  
                                    </figcaption>                            
                                </figure>';
                        }
                    ?>
                </div>
            </div>     
        </div>
    </div>
</div>