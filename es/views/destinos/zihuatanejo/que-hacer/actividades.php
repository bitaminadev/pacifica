<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/<?php echo utf8_encode($var["img_banner_habitaciones"]); ?>" alt="<?php echo utf8_encode($var["nombre_destino"]); ?>">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo utf8_encode($var["nombre_destino"]); ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">Actividades</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[96]; ?></h2>
                </div>
                <div class="w-100 mb-5 mt-5">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 pr-0 card-rel m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[98]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[98]; ?>" alt="01">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="text-normal my-5 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[97]; ?></p>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[99]; ?></h2>
                </div>
                <div class="w-100 p-0 mt-5">
                    <div class="v-slider_card-out">
                        <article class="">
                            <div class="row">
                                <figure class="wcol-md-12 col-lg-6m-0 fig-card pr-0" ><img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[102]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[102]; ?>" alt="00"></figure>
                                <div class="col-md-12 col-lg-6 h-100 card-dest-gray">
                                    <div class="description-card p-3">
                                        <h4><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[100]; ?></h4>
                                        <p class="my-4 text-normal text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[101]; ?></p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>     
        </div>
    </div>
</div>