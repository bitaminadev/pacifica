<div class="w-100 banner_fluid">
        <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[10]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[10]; ?>" alt="Ixtapa">
        <div class="message-banner" >
            <!-- <h1 class="text-uppercase text-white text-center w-100 m-0">Gastronomía en Pacìfica</h1> -->
            <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[11]; ?></h1>
            <hr class="line-text-message my-2" />
        </div>        
    </div>
    <div class="w-100">
        <div class="">
            <div class="row my-0 mx-0 p-0">
                <div class="w-75 mt-5 mx-auto p-0 pt-4">
                    <div class="w-100 text-mini">
                        <div class="row">
                            <section class="col-md-12 col-lg-6 text-justify px-4">
                                <!-- Sabemos que la comida es una parte muy importante de tu estancia con nosotros, por lo que nuestros restaurantes no solo ofrecen platillos que satisfacen hasta a los paladares más exigentes sino que se encuentran en locaciones con vistas impresionantes de la Bahía de Ixtapa y que han sido cuidadas hasta el más mínimo detalle para que tu estancia con nosotros sea memorable. -->
                                <?php echo array_column($secciones, 'contenido_es', 'id_seccion')[12]; ?>
                            </section>
                            <section class="col-md-12 col-lg-6 font-weight-bold px-4">
                                <label class="mb-4">Conoce más sobre la gastronomía en cada uno de nuestros destinos.</label><br>
                                <div class="col-sm-12 col-md-5 py-2 font-italic float-left border-bottom" style="cursor:pointer; font-size:0.9em;" onclick="linkTo('<?php echo $httpProtocol.$host.$url.$ln; ?>destinos/ixtapa/gastronomia/')">Ixtapa<i class="fas fa-chevron-right float-right"></i></div>
                                <div class="col-sm-12 col-md-5 py-2 font-italic float-right border-bottom" style="cursor:pointer; font-size:0.9em;" onclick="linkTo('<?php echo $httpProtocol.$host.$url.$ln; ?>destinos/zihuatanejo/gastronomia/')">Zihuatanejo<i class="fas fa-chevron-right float-right"></i></div>
                                <div class="col-sm-12 col-md-5 py-2 font-italic float-left border-bottom" style="cursor:pointer; font-size:0.9em;" onclick="linkTo('<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/gastronomia/el-faro<?php echo $ext; ?>')">El mejor restaurante de Ixtapa<i class="fas fa-chevron-right float-right"></i></div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-5 pt-4">
                    <div class="col-sm-12">
                        <div class="row">
                            <section class="col-md-12 col-lg-6 card-dest-gray order-2 order-lg-1 px-5">
                                <div class="row">
                                    <div class="map-down p-5 text-left">
                                        <p class="text-uppercase text-mini mb-0">Oferta gastronómica</p>
                                        <!-- <h4 class="text-uppercase subtitle-card-bottom">¿Dónde comer y beber?</h4> -->
                                        <h4 class="text-uppercase subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[13]; ?></h4>
                                        <!-- <p class="text-normal my-4 text-justify text-mini">Con 8 maravillosos restaurantes para elegir, entre los que se encuentra el mejor de Ixtapa, Pacífica Resort te garantiza una experiencia culinaria de primer nivel durante tu estadía. Ya sea que busques comida tradicional mexicana o una selección de comida internacional, tenemos opciones para todos los paladares.</p> -->
                                        <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[14]; ?></p>
                                    </div>
                               
                                </div>
                        </section>
                        <figure class="col-md-12 col-lg-6 pl-0 card-rel order-1 order-lg-2 m-0">
                            <div class="row">
                                <!-- <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/gastronomy/pacifica_gastronomia_destacado.jpg" alt="Pacifica luxury suites aqua"> -->
                                <img class="img-fluid fit-image" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[15]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[15]; ?>" alt="Pacifica luxury suites aqua">
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="w-75 my-5 mx-auto px-0 py-4">
                    <?php
                        if($vpl["nombre_platillo"] != ""){
                            echo '<div class="row">
                                <figure class="col-md-12 col-lg-6 card-rel m-0">
                                    <img class="img-fluid fit-image" src="'.$httpProtocol.$host.$url.'images/destinos/gastronomia/platillos/'.$vpl["foto_platillo"].'" alt="Pacifica luxury suites">
                                </figure>
                                <section class="col-md-12 col-lg-6 card-white">
                                    <div class="map-down px-4 text-left">
                                        <p class="m-0 p-0 text-uppercase text-mini">Platillo recomendado</p>
                                        <h4 class="m-0 p-0 text-uppercase subtitle-card-bottom">'.utf8_encode($vpl["nombre_platillo"]).'</h4>
                                        <p class="text-normal my-4 text-justify text-mini">'.utf8_encode($vpl["descripcion_platillo"]).'</p>
                                        <a class="btn_black_dest w-100" href="'.$httpProtocol.$host.$url.$ln.'destinos/ixtapa/gastronomia/'.$vpl["url_restaurante"].'">Conoce más de '.utf8_encode($vpl["nombre_restaurante"]).'</a>
                                    </div>
                                </section>
                            </div>';
                        }
                    ?>
                    <div class="row mt-5 pt-4">
                        <?php 
                            while($vrst = mysqli_fetch_array($cn_rst)){
                                echo '<figure class="col-md-12 col-lg-6 pr-0 card-rel border m-0 p-0">
                                        <img class="img-fluid fit-image" src="'.$httpProtocol.$host.$url.'images/destinos/gastronomia/general/'.$vrst["imagen_restaurante"].'" alt="'.utf8_encode($vrst["nombre_restaurante"]).'">
                                        <figcaption class="w-100 h-100 card-link card-link-tr">
                                            <a class="text-white text-center text-uppercase pt-5 w-100 d-block link-service" href="'.$httpProtocol.$host.$url.$ln.'destinos/'.$vrst["url_destino"].'/gastronomia/'.$vrst["url_restaurante"].'">'.utf8_encode($vrst["nombre_restaurante"]).'
                                                '.($vrst["imagen_logo"] != "" ? '<img src="'.$httpProtocol.$host.$url.'images/destinos/gastronomia/general/'.$vrst["imagen_logo"].'" alt="Logo '.utf8_encode($vrst["nombre_restaurante"]).'" class="previewRestaurantLogo">' : '').'
                                            </a>  
                                        </figcaption>                            
                                    </figure>';
                            }
                        ?>
                    </div>
                </div>     
            </div>
        </div>
    </div>