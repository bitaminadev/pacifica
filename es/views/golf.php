<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/pacifica_ixtapa_luxury_main_banner.jpg" alt="Ixtapa">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0">Ixtapa</h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">GOLF</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[106]; ?></h2>
                    <p class="mb-4 text-line-bottom text-center">Para aquellos que aman y disfrutan del golf.</p>
                    <div class="my-4 text-normal">
                        <p><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[107]; ?></p>
                    </div>
                </div>
                <div class="w-100 p-0 mt-5">
                    <div class="slide-destinations">
                        <?php 
                            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[108];
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 108);
                            $cont = 0;
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $cont ++;
                                $elemento = array_map("utf8_encode", $elemento);
                                if($elemento["tipo_elemento"] == "imagen"){ ?>
                                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                                    <figure class="v-slide" ><div class="youtubeContainer">
                                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                    </div></figure>
                                <?php
                                } 
                            }
                        ?>
                    </div>
                </div>
                <div class="w-100 mb-5">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 card-rel pr-0 m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[111]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[111]; ?>" alt="01">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Servicios</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[109]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[110]; ?></p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>