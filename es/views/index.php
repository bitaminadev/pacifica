<div id="banner-home" class="w-100">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php 
                $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[1];
                $result_1 = $m_elementos->get_elementos_por_seccion($bd, 1);
                while($elemento = mysqli_fetch_assoc($result_1)){
                    $elemento = array_map("utf8_encode", $elemento); ?>
                    <div class="swiper-slide">
                        <div class="item-banner-home">
                            <img src="<?php echo $httpProtocol.$host.$url ?>images/brand/logo_pacifica.svg" class="fixedBannerLogo fixedBannerLogo-<?php echo $elemento['id_elemento']; ?>" alt="Pacifica Resort">
                            <?php if($elemento["titulo_elemento_es"] != ""){ ?>
                                <div class="messageForGalleryElement" >
                                    <h1 class="text-uppercase text-white text-center w-100"><?php echo $elemento["titulo_elemento_es"]; ?></h1>
                                    <hr class="line-text-title" />
                                </div>
                            <?php } ?>
                            <?php if($elemento["tipo_elemento"] == "video"){ ?>
                                <button type="button" onclick="pause(<?php echo $elemento['id_elemento']; ?>);" class="close close-home d-none" id="close-video-<?php echo $elemento['id_elemento']; ?>" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?php if($elemento["titulo_elemento_es"] != ""){ ?>
                                    <img id="cover-image-<?php echo $elemento['id_elemento']; ?>" class="img-fluid item-banner-abs item-banner-home w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido_cubierta_video"]; ?>" alt="Pacifica"/>
                                <?php } ?>
                                <img id="icon-play-h-<?php echo $elemento['id_elemento']; ?>" class="icon-play-h" onclick="reproducir(<?php echo $elemento['id_elemento']; ?>);" src="<?php echo $httpProtocol.$host.$url ?>images/home/video_home_play.png" alt="play">
                                <video controlslist="nodownload" id="pretzel-video-<?php echo $elemento['id_elemento']; ?>" class="w-100" muted loop="loop" style="overflow: hidden;">
                                    <source src="<?php echo $httpProtocol.$host.$url ?>video/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" type="video/mp4" >
                                </video>
                            <?php }else{ ?>
                                <img class="img-fluid item-banner-home w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="Pacifica"/>
                            <?php } ?>
                        </div>
                    </div>
            <?php } ?>
        </div>
        <div class="swiper-button-prev swiper-button-white prev-img-gallery"></div>
        <div class="swiper-button-next swiper-button-white next-img-gallery"></div>        
    </div>
</div>
<div class="w-100 row-section-two pt-5">
    <div class="container">
        <div class="row my-0 mx-2 mx-lg-5 px-3 p-0 mb-5">
            <div class="col-lg-5 px-0 py-3">
                <h2 class="p-0 px-lg-5 h-subtitle text-center"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[3]; ?></h2>
            </div>
            <div class="col-lg-7 px-0">
                <div class="h-100 w-100 border-left border-gray">
                    <p class="p-0 pl-lg-5 py-lg-3 text-normal"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[4]; ?></p>
                </div>
            </div>
        </div>
        <div class="row my-0 mx-2 mx-lg-5 px-2 p-0">
            <div class="col-lg-4 px-0 bgcolor-white">
                <?php 
                    //Datos destinos
                    $query_destinations = $obj_destinations->lista_destinos($bd,"");
                    $image_ixtapa = "";
                    $image_zihuatanejo = "";
                    while($data_destination = mysqli_fetch_array($query_destinations)){
                        switch($data_destination["id_destino"]){
                            case 1: $image_ixtapa = $data_destination["img_general"];  break;
                            case 2: $image_zihuatanejo = $data_destination["img_general"]; break;
                        }
                    }
                ?>
                <div class="w-100 px-4 d-block block_text_dest" id="dest-text-1">
                    <h2 class="my-3 subtitle-seccion">Ixtapa Zihuatanejo | Guerrero</h2>
                    <h3 class="my-3 mt-4 mb-4 h-subtitle">Un paraíso<br> en el Pacífico</h3>
                    <p class="my-3 mt-3 text-normal">Disfruta de la belleza y las atracciones que han hecho de esta playa uno de los destinos turísticos favoritos de mexicanos y extranjeros por igual.</p>
                </div>
                <div class="w-100 px-4 d-none block_text_dest" id="dest-text-2">
                    <h2 class="my-3 subtitle-seccion">Zihuatanejo</h2>
                    <h3 class="my-3 mt-4 mb-4 h-subtitle">Un paraíso<br> en el Pacífico</h3>
                    <p class="my-3 mt-3 text-normal">Disfruta de la belleza y las atracciones que han hecho de esta playa uno de los destinos turísticos favoritos de mexicanos y extranjeros por igual.</p>
                </div>
                <div class="w-100 mt-4">
                    <ul class="list-unstyled">
                        <li class="px-4 py-2 border border-bottom-0 border-right-0 border-gray li-hotel active-li" data-item-h="1">IXTAPA ZIHUATANEJO</li>
                        <li class="px-4 py-2 border border-right-0 border-gray  li-hotel" data-item-h="2" >ZIHUATANEJO</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 px-0">
                <div class="ds_tab_home w-100 d-block" id="dest-num-1">
                    <a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/"><img class="img-fluid w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/general/<?php echo $image_ixtapa; ?>" alt="Ixtapa"></a>
                </div>
                <div class="ds_tab_home w-100 d-none" id="dest-num-2">
                    <a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/"><img class="img-fluid w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/general/<?php echo $image_zihuatanejo; ?>" alt="Zihuatanejo"></a>
                </div>
            </div>
        </div>
    </div>
</div> 
<div class="w-100 mb-5">
    <div class="container">
        <div class="row m-0 p-0">
            <div class="col-lg-12 text-center">
                <div class="w-100 col-gray pt-5">
                    <div class="w-100 mt-3 mb-5"><h3 class="d-md-block d-lg-inline h3-text">Hoteles Resort</h3> <span class="d-inline pl-4 pr-2 span-line">|</span> <p class="d-inline p-text-home">Selecciona un resort para conocer más.</p></div>
                    <ul class="list-unstyled text-center w-100 pb-5 mb-0 d-flex justify-content-center">
                        <li class="pr-1"><a class="link_hotel" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/luxury-suites<?php echo $ext; ?>"><img src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_resort_luxury_home.png" alt="Pacifica luxury suites" class="img-fluid"><span class="text-uppercase d-block mt-2 name-hotel">Luxury suites</span></a></li>
                        <li class="pr-1"><a class="link_hotel" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/premium-suites<?php echo $ext; ?>"><img src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_resort_premium_home.png" alt="Pacifica premium suites" class="img-fluid"><span class="text-uppercase d-block mt-2 name-hotel">Premium suites</span></a></li>
                        <li class="pr-1"><a class="link_hotel" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/pacifica-grand<?php echo $ext; ?>"><img src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_grand_home.png" alt="Pacifica grand zihuatanejo" class="img-fluid"><span class="text-uppercase d-block mt-2 name-hotel">Pacífica grand</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row m-0 p-0">
            <div class="col-lg-8">
                <div class="d-flex flex-wrap">
                    <div class="w-50 card-rel"><img class="img-fluid w-100" src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_home_servicios.jpg" alt="Pacifica luxury suites"><a class="text-white text-center card-link pt-5 w-100 h-100 link-service" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/que-hacer/">Servicios</a>  </div>
                    <div class="w-50 card-rel"><img class="img-fluid w-100" src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_home_gastronomia.jpg" alt="Pacifica luxury suites"><a class="text-white text-center card-link pt-5 w-100 h-100 link-service" href="<?php echo $httpProtocol.$host.$url.$ln ?>gastronomia<?php echo $ext; ?>">Gastronomía</a></div>
                    <div class="w-50 card-rel"><img class="img-fluid w-100" src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_home_entretenimiento.jpg" alt="Pacifica luxury suites"><a class="text-white text-center card-link pt-5 w-100 h-100 link-service" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/entretenimiento<?php echo $ext; ?>">Entretenimiento</a></div>
                    <div class="w-50 card-rel"><img class="img-fluid w-100" src="<?php echo $httpProtocol.$host.$url ?>images/home/pacifica_home_actividades.jpg" alt="Pacifica luxury suites"><a class="text-white text-center card-link pt-5 w-100 h-100 link-service" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/que-hacer/actividades<?php echo $ext; ?>">Actividades</a></div>
                </div>
            </div>
            <div class="col-lg-4 my-3 my-lg-0">
                <div class="d-flex flex-wrap h-100">
                    <div class="align-self-center text-center w-100">
                        <h4 class="h-subtitle pb-1">¿Por qué Pacífica?</h4>
                        <hr class="line-subtitle">
                        <p class="my-4 text-normal">Cuidamos cada detalle de nuestros hoteles para que en tus vacaciones únicamente te preocupes por descansar y disfrutar de la maravillosa experiencia de Pacífica.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>