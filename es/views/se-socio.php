<div class="w-100 banner_fluid">
        <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[20]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[20]; ?>" alt="Ixtapa">
        <div class="message-banner" >
            <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[21]; ?></h1>
            <hr class="line-text-message my-2" />
        </div>        
    </div>
    <div class="w-100">
        <div class="container">
            <p class="mt-5 text-normal text-center"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[22]; ?></p>
            <p class="mt-5 mb-0 text-line-bottom text-center font-weight-bolder">Contáctanos</p>
        </div>
    </div>
    <div class="w-100 pt-4 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-0 col-md-4"></div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <form id="form-contacto" class="form-contacto mt-4 mb-4" onsubmit="return sendEmailContact(this);">
                        <h6 class="primary-color text-normal mb-4"><i class="fas fa-asterisk fa-obl"></i> Los campos marcados son esenciales:</h6>
                        <input type="hidden" name="tipo_contacto" value="socio">
                        <input type="radio" name="es_socio" value="0" style="display: none;" checked>
                        <input type="hidden" name="fecha_evento" value="">
                        <input type="hidden" name="fecha_respaldo" value="">
                        <input type="hidden" name="cantidad_invitados" value="0">
                        <div class="form-group">
                            <label class="primary-color text-normal"><i class="fas fa-asterisk fa-obl"></i> Nombre completo</label>
                            <input type="text" class="form-control" name="nombre" required placeholder="">
                        </div>
                        <div class="form-group">
                            <label class="primary-color text-normal"><i class="fas fa-asterisk fa-obl"></i> Teléfono</label>
                            <input type="tel" class="form-control" name="telefono" pattern="[0-9]{10}" minlength="10" maxlength="10" required placeholder="10 dígitos">
                        </div>
                        <div class="form-group">
                            <label class="primary-color text-normal"><i class="fas fa-asterisk fa-obl"></i> Correo electrónico</label>
                            <input type="email" class="form-control" name="correo" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required placeholder="">
                        </div>
                        <div class="form-group">
                            <label class="primary-color text-normal">Mensaje</label>
                            <textarea class="form-control" name="mensaje" rows="6"></textarea>
                        </div>
                        <!-- <input type="hidden" name="recaptcha_response" id="recaptchaResponse" value="03AOLTBLR0uFFiRx_GSOIiRl6PcRPzTwg-XTfUJelp5EWaUx9I0fqZIRWlvZa4eR_lENywaQklVXOuSCUofmg1rXJh-tkc9PZibWXzmNm2dICLvlwdd_sZLELWEhmS9gze4NxG1himjdl5wBZ5FV3QZnuNOl7iDFjBULybFYU73yPJCEMgLyDLd6eQjgRQUVCWLlp_SMDPyIOCAStX87ddTDgrm9U-mmKFm_Ub4tE47SE_vXuPt3BlTfBNpJjoYJkL7g18b1l5rA57rxuAjsIUXwkAkNVuPp-1P0Oq4fj9qCK3iNsNw8YJcEj8TlsN82JKJnbBAvgCrkWQcqHjHP3U5k8UJwSLlJPG3g"> -->
                        <button type="submit" class="btn_black_dest text-left w-100 mt-2">Enviar mensaje a pacífica</button>
                    </form>
                </div>
                <div class="col-sm-0 col-md-4"></div>
            </div>
        </div>
    </div>