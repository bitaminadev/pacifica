<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[39]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[39]; ?>" alt="Ixtapa">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[40]; ?></h1>
        <hr class="line-text-message my-2" />
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row">
            <div class="col-10 mx-auto d-block">
                <h2 class="px-5 pt-5 pb-3 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[163]; ?></h2>
                <div class="row">
                    <figure class="col-md-12 col-lg-6 card-rel pr-0 m-0">
                        <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[165]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[165]; ?>" alt="Compromiso Sustentabilidad">
                    </figure>
                    <section class="col-md-12 col-lg-6 card-dest-gray">
                        <div class="map-down px-4 text-left">
                            <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[164]; ?></p>
                        </div>
                    </section>
                </div>
                <h2 class="px-5 pt-5 pb-3 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[41]; ?></h2>
                <div class="row">
                    <div class="col-md-3 col-lg-2 text-center">
                        <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/about/casa_ninios_logo.png" alt="casa niños">
                    </div>
                    <div class="col-md-12 col-lg-10">
                        <p class="my-4 text-normal"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[42]; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w-100 p-0 mt-5">
    <div class="slide-destinations">
        <?php 
            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[162];
            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 162);
            $cont = 0;
            while($elemento = mysqli_fetch_assoc($result_1)){
                $cont ++;
                $elemento = array_map("utf8_encode", $elemento);
                if($elemento["tipo_elemento"] == "imagen"){ ?>
                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                    <figure class="v-slide" ><div class="youtubeContainer">
                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                    </div></figure>
                <?php
                } 
            }
        ?>
    </div>
</div>
<div class="w-100">
    <div class="container"> 
        <div class="row">
            <article class="col-10 mx-auto d-block">
                <div class="row">
                    <!-- <figure class="col-md-6 col-lg-5 m-0 fig-card text-right" ><img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[45]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[45]; ?>" alt="00"></figure> -->
                    <div class="col-md-12 col-lg-7">
                        <div class="description-card p-3">
                            <h4><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[43]; ?></h4>
                            <p class="my-4 text-normal text-justify"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[44]; ?></p>
                        </div>
                    </div>
                </div>
            </article>

            <article class="col-10 mx-auto d-block">
                <div class="row">
                    <div class="col-md-12 col-lg-7 order-2 order-lg-1">
                        <div class="description-card dsc-card-2 p-3">
                            <h4 class="text-right"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[46]; ?></h4>
                            <p class="my-4 text-normal text-justify"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[47]; ?></p>
                        </div>
                    </div>
                    <!-- <figure class="col-md-6 col-lg-5 m-0 fig-card text-left order-1 order-lg-2" ><img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[48]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[48]; ?>" alt="00"></figure> -->
                </div>
            </article>
        </div>
    </div>
</div>
<div class="w-100 ">
    <div class="container">
        <div class="row">
            <div class="col-9 mx-auto d-block">
                <p class="my-5 text-line-bottom text-center text-uppercase">Aportaciones y donativos</p>
                <p class="mb-5 text-normal">Casa para Niños del Pacífico es una Institución de Asistencia Privada, por lo que todos los donativos pueden ser deducibles de impuestos.
                        Todas las aportaciones, tanto económicas como materiales, son bienvenidas, ya que con ellas podemos mejorar nuestra labor altruista día a día.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="w-100 section_gray">
    <div class="container">
        <div class="row">
            <div class="col-9 mx-auto d-block">
                <p class="my-4 text-normal">Puede enviar sus aportaciones económicas a nombre de “Casa para Niños del Pacífico I.A.P.”, en cualquiera de las siguientes cuentas:</p>
                <p class="my-3 text-normal"><strong>Contribuciones bancarias</strong><br>
                    BANORTE cuenta 0152697563<br>
                    <strong>Y sus contribuciones materiales a:</strong><br>
                    Calle Mar Caspio #135, Col. Centro, Zihuatanejo, Guerrero; México C.P. 40880 Teléfono: 755 554-5982<br><br>
                    Para mayor información puede contactar a: <strong>Marlene Teresa</strong> <a class="lnk-pac font-weight-bolder" target="_blank" href="mailto:casa.pacifica@pacifica.com.mx">casa.pacifica@pacifica.com.mx</a><br>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="w-100 ">
    <div class="container">
        <div class="row">
            <div class="col-9 mx-auto d-block">
                <p class="my-5 text-normal"><strong>Los Padrinos</strong><br>
                    En la Casa para Niños del Pacífico contamos con el apoyo de Padrinos Vitalicios, que son personas u organizaciones, que toman el compromiso de encargarse financieramente de uno o más de nuestros niños.
                </p>
            </div>
        </div>
    </div>
</div>