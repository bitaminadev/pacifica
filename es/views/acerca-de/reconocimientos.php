<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[23]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[23]; ?>" alt="Ixtapa">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[24]; ?></h1>
        <hr class="line-text-message my-2" />
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <h2 class="px-5 pt-5 pb-3 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[25]; ?></h2>
        <p class="my-4 text-normal text-center"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[26]; ?></p>
    </div>
</div>
<div class="w-100 py-5 mt-5">
    <div class="container">
        <div class="row section_gray py-4">
            <div class="col-md-12 col-lg-4">
                <div class="text-center text-lg-right w-100">
                    <h3 class="title-card-award font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[27]; ?></h3>
                </div>
            </div>
            <div class="col-md-12 col-lg-2 text-center">
                <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[28]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[28]; ?>" alt="trip advisor travelers choice">
            </div>
            <div class="col-md-12 col-lg-6">
                <p class="text-normal text-justify"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[29]; ?></p>
            </div>
        </div>

        <div class="row py-4">
            <div class="col-md-12 col-lg-4">
                <div class="text-center text-lg-right w-100">
                    <h3 class="title-card-award font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[30]; ?></h3>
                </div>
            </div>
            <div class="col-md-12 col-lg-2 text-center">
                <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[31]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[31]; ?>" alt="trip advisor travelers choice">
            </div>
            <div class="col-md-12 col-lg-6">
                <p class="text-normal text-justify"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[32]; ?><br><br><br><br></p>
            </div>
        </div>

        <div class="row section_gray py-4">
            <div class="col-md-12 col-lg-4">
                <div class="text-center text-lg-right w-100">
                    <h3 class="title-card-award font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[33]; ?></h3>
                </div>
            </div>
            <div class="col-md-12 col-lg-2 text-center">
                <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[34]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[34]; ?>" alt="trip advisor travelers choice">
            </div>
            <div class="col-md-12 col-lg-6">
                <p class="text-normal text-justify"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[35]; ?></p>
            </div>
        </div>

        <div class="row py-4">
            <div class="col-md-12 col-lg-4">
                <div class="text-center text-lg-right w-100">
                    <h3 class="title-card-award font-weight-lighter"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[36]; ?></h3>
                </div>
            </div>
            <div class="col-md-12 col-lg-2 text-center">
                <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[37]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[37]; ?>" alt="trip advisor travelers choice">
            </div>
            <div class="col-md-12 col-lg-6">
                <p class="text-normal text-justify"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[38]; ?></p>
            </div>
        </div>

    </div>
</div>