<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[16]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_es', 'id_seccion')[16]; ?>" alt="Ixtapa">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[17]; ?></h1>
        <hr class="line-text-message my-2" />
    </div>        
</div>
<div class="w-100">
    <div class="p-0">
        <div class="row my-0 mx-0 p-0">
            <div class="col-sm-12 p-0">
                <div class="w-75 mx-auto p-4">
                    <div class="my-5 text-normal text-justify"><p><?php echo array_column($secciones, 'contenido_es', 'id_seccion')[18]; ?></p></div>
                </div>
                <div class="w-100 p-0 pb-5 mb-5">
                    <div class="slide-destinations">
                        <?php 
                            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[19];
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 19);
                            $cont = 1;
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $cont ++;
                                $elemento = array_map("utf8_encode", $elemento);
                                if($elemento["tipo_elemento"] == "imagen"){ ?>
                                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                                    <figure class="v-slide" ><div class="youtubeContainer">
                                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                    </div></figure>
                                <?php
                                } 
                            }
                        ?>
                    </div>
                </div>
            </div>     
        </div>
    </div>
</div>