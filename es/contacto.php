<!DOCTYPE html>
<?php 
    $title = 'Pacífica | Contacto';
	$description = 'Estamos a tus órdenes: reserva, adquiere alguno de nuestros paquetes o solicita información especial en nuestros siguientes medios de contacto.';
	$keywords = 'contacto pacífica resort, teléfono pacífica resort, e-mail pacifica resort';
    $page = 'home';
    include('commons/_headOpen.php');
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");
    
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/contacto.js"></script>';
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesContact.css">';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/contacto.html');
    include('views/footer.html');
?>
</body>
</html>