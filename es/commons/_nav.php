<nav class="nav">
	<div itemscope itemtype="http://schema.org/Brand" class="navContent">
		<a itemprop="url" href="<?php echo $httpProtocol.$host.$url.$ln ?>" class="navLogoSmall"><img itemprop="logo" src="<?php echo $httpProtocol.$host.$url ?>images/brand/logo_pacifica.svg" class='navLogo' alt='Pacifica Resort'></a>
		<a href="<?php echo $httpProtocol.$host.$url.$ln ?>" class="navLogoBig"><img src="<?php echo $httpProtocol.$host.$url ?>images/menu/logo-pacifica-big.png" class="navLogo" alt="Pacifica Resort"></a>
		<i class="fas fa-bars mobile toggleMenu" onclick="toggleMobileMenu()"></i>
		<ul class="menu" itemscope itemtype="http://schema.org/BreadcrumbList">

			<li class="menuItem" onclick="linkTo('https:\/\/login.pacifica.com.mx/Login?leng=2', true)" data-layer='1'>
				Acceso a socios
			</li>

			<li class="menuItem destinations togglerContainer" data-layer='1'>
				<span class="submenuDisplayer" data-togglesection="displayNoSection">
					Destinos  <i class="fas fa-chevron-down"></i>
				</span>
				<div class="submenu">
					<ul class="menu">
						<li class="menuItem" data-layer='2'>
							<span class="submenuDisplayer ixtapa">
								Ixtapa <i class="fas fa-chevron-right"></i>
							</span>
							<div class="submenu">
								<ul class="menu">
									<li class="menuItem" data-layer='3' data-togglesection="displayNoSection" onclick="this.closest('.submenu').previousElementSibling.click();">
										<i class="fas fa-chevron-left"></i>&nbsp;&nbsp;Regresar
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-hoteles" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/"><span itemprop="name">Hoteles Resort</span></a>
			                            <meta itemprop="position" content="1"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-gastronomia" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/gastronomia/"><span itemprop="name">Gastronomía</span></a>
			                            <meta itemprop="position" content="2"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-actividades" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/que-hacer/"><span itemprop="name">Actividades</span></a>
			                            <meta itemprop="position" content="3"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-golf" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/golf<?php echo $ext; ?>"><span itemprop="name">Golf</span></a>
			                            <meta itemprop="position" content="4"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-entretenimiento" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/entretenimiento<?php echo $ext; ?>"><span itemprop="name">Entretenimiento</span></a>
			                            <meta itemprop="position" content="5"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-compras" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/que-hacer/compras<?php echo $ext; ?>"><span itemprop="name">Compras</span></a>
			                            <meta itemprop="position" content="6"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-bodas" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/bodas<?php echo $ext; ?>"><span itemprop="name">Bodas</span></a>
			                            <meta itemprop="position" content="7"/>
									</li>
								</ul>
							</div>
						</li>
						<li class="menuItem" data-layer='2'>
							<span class="submenuDisplayer zihuatanejo">
								Zihuatanejo <i class="fas fa-chevron-right"></i>
							</span>
							<div class="submenu">
								<ul class="menu">
									<li class="menuItem" data-layer='3' data-togglesection="displayNoSection" onclick="this.closest('.submenu').previousElementSibling.click();">
										<i class="fas fa-chevron-left"></i>&nbsp;&nbsp;Regresar
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="zihuatanejo-hoteles" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/"><span itemprop="name">Hoteles Resort</span></a>
			                            <meta itemprop="position" content="8"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="zihuatanejo-gastronomia" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/gastronomia/"><span itemprop="name">Gastronomía</span></a>
			                            <meta itemprop="position" content="9"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="zihuatanejo-actividades" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/que-hacer/"><span itemprop="name">Actividades</span></a>
			                            <meta itemprop="position" content="10"/>
									</li>
								</ul>
							</div>
						</li>
					</ul>
					<div class="navSectionContent notInMobile">
						<div class="menuBackSection notHidden" style="height: 100%;width: auto;">
							<span class="mapLocation ixtapa" data-toggle="tooltip" title="Ixtapa" onclick="document.querySelector('.nav .submenuDisplayer.ixtapa').click();"></span>
							<span class="mapLocation zihuatanejo" data-toggle="tooltip" title="Zihuatanejo" onclick="document.querySelector('.nav .submenuDisplayer.zihuatanejo').click();"></span>
							<img src="<?php echo $httpProtocol.$host.$url ?>images/menu/img_mapa_destinos.png" style="height: 100%; width: auto; float: right;" alt="Mapa Destinos">
						</div>
						<div class="menuBackSection hidden ixtapa-hoteles">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-gastronomia">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/gastronomia/" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-actividades">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/que-hacer/" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-golf">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/golf<?php echo $ext; ?>" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-entretenimiento">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/entretenimiento<?php echo $ext; ?>" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-compras">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/que-hacer/compras<?php echo $ext; ?>" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-bodas">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>En Ixtapa, Pacífica Resort ofrece todo el confort que necesitas para disfrutar tus vacaciones rodeado de la belleza del Pacífico.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/bodas<?php echo $ext; ?>" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden zihuatanejo-hoteles">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Disfruta de unas vacaciones relajantes a orillas del Pacífico en Pacífica Grand.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden zihuatanejo-gastronomia">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Disfruta de unas vacaciones relajantes a orillas del Pacífico en Pacífica Grand.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/gastronomia/" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden zihuatanejo-actividades">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Disfruta de unas vacaciones relajantes a orillas del Pacífico en Pacífica Grand.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/que-hacer/" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<!-- <div class="menuBackSection hidden zihuatanejo-golf">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Disfruta de unas vacaciones relajantes a orillas del Pacífico en Pacífica Grand.</p>
									<a href="<?php //echo $httpProtocol.$host.$url ?>destinos/zihuatanejo/golf<?php //echo $ext; ?>" title="Title" class="v-button"> Más información <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div> -->
					</div>
					<span class="closeSubmenu mobile" onclick='this.parentElement.previousElementSibling.click()'>
						X
					</span>
				</div>
			</li>

			<li class="menuItem hotels togglerContainer" data-layer='1'>
				<span class="submenuDisplayer" data-togglesection="hoteles">
					Hoteles Resort  <i class="fas fa-chevron-down"></i>
				</span>
				<div class="submenu">
					<ul class="menu">

						<li class="menuItem" data-layer='2' data-togglesection="hoteles-ixtapa">
							<a class="w-100 h-100 sbumenuDisplayer" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/ixtapa/">
								Ixtapa <i class="fas fa-chevron-right"></i>
							</a>
						</li>

						<li class="menuItem" data-layer='2' data-togglesection="hoteles-zihuatanejo">
							<a class="w-100 h-100 sbumenuDisplayer" href="<?php echo $httpProtocol.$host.$url.$ln ?>destinos/zihuatanejo/">
								Zihuatanejo <i class="fas fa-chevron-right"></i>
							</a>
						</li>

					</ul>
					<div class="navSectionContent notInMobile">
						<div class="menuBackSection hoteles">
							<div class="sectionContent">
								<h3>Hoteles Resort</h3>
								<p>Encuentra la experiencia ideal para tus vacaciones en nuestros hoteles resort, ya sea que busques disfrutar tus vacaciones en familia con múltiples actividades o relajarte en un ambiente sofisticado y tranquilo a la orilla del mar.</p>
							</div>
							<div class="sectionHotels">
								<div class="menuBackSection pacifica-resort"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destinos/ixtapa/">&nbsp;</a></div>
								<div class="menuBackSection pacifica-grand"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destinos/zihuatanejo/">&nbsp;</a></div>
							</div>
						</div>
						<div class="menuBackSection hidden hoteles-ixtapa"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destinos/ixtapa/">&nbsp;</a>
						</div>
						<div class="menuBackSection hidden hoteles-zihuatanejo"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destinos/zihuatanejo/">&nbsp;</a>
						</div>
					</div>
					<span class="closeSubmenu mobile" onclick='this.parentElement.previousElementSibling.click()'>
						X
					</span>
				</div>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>bodas<?php echo $ext; ?>"><span itemprop="name">Bodas</span></a>
				<meta itemprop="position" content="11"/>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>gastronomia<?php echo $ext; ?>"><span itemprop="name">Gastronomía</span></a>
				<meta itemprop="position" content="12"/>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>entretenimiento<?php echo $ext; ?>"><span itemprop="name">Entretenimiento</span></a>
				<meta itemprop="position" content="13"/>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>se-socio<?php echo $ext; ?>"><span itemprop="name">Sé socio</span></a>
				<meta itemprop="position" content="14"/>
			</li>

			<li class="menuItem about" data-layer='1'>
				<span class="submenuDisplayer">
					Acerca de  <i class="fas fa-chevron-down"></i>
				</span>
				<div class="submenu">
					<ul class="menu">
						<li class="menuItem" data-layer='2' data-background='<?php echo $httpProtocol.$host.$url?>images/menu/menu_back_reconocimientos.jpg' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemprop="item" href="<?php echo $httpProtocol.$host.$url.$ln ?>reconocimientos<?php echo $ext; ?>"><span itemprop="name">Reconocimientos</span></a>
							<meta itemprop="position" content="15"/>
						</li>
						<li class="menuItem" data-layer='2' data-background='<?php echo $httpProtocol.$host.$url?>images/menu/menu_back_compromiso.jpg' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemprop="item" href="<?php echo $httpProtocol.$host.$url.$ln ?>responsabilidad-social<?php echo $ext; ?>"><span itemprop="name">Nuestro compromiso</span></a>
							<meta itemprop="position" content="16"/>
						</li>
					</ul>
					<span class="closeSubmenu mobile" onclick='this.parentElement.previousElementSibling.click()'>
						X
					</span>
				</div>
			</li>
			
			<li class="menuItem onlyInMobile" data-layer='1'>
				<a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>contacto<?php echo $ext; ?>"><span>Contáctanos</span></a>
			</li>
		</ul>
		<div class="contacto">
			<a class="text-white telhead" href="tel:800-221-5830" >800-221-5830</a>
			<span class="d-block small notInMobile">
				<a class="text-white" href="<?php echo $httpProtocol.$host.$url.$ln ?>contacto<?php echo $ext; ?>" >Contáctanos</a>
			</span>
		</div>
		<div class="notInMobile idioma">
			<div class="dropdown show">
				<a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><img class="smallFlag" src="<?php echo $httpProtocol.$host.$url?>images/menu/en_flag_mexico.svg" alt="Idioma Español"></a>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuLink" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-127px, 20px, 0px);">
					<a class="dropdown-item" href="<?php echo $httpProtocol.$host.$url; ?>en/"><img class="smallFlag" src="<?php echo $httpProtocol.$host.$url?>images/menu/en_flag_usa.svg" alt="English language"></a>
				</div>
			</div>
		</div>
	</div>
</nav>