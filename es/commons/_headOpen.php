<?php
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];

    $url = '/web/';
    // $url = '/pacifica/';

    /* $httpProtocol = 'http://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/Bitamina/pacifica/'; */
    $ln = 'es/';
    $author = 'Bitamina Digital';

    $ext = '';
    // $ext = '.php';

    // Variables que almacenan el css y el js de la pagina
    $js = '
        <script src="'.$httpProtocol.$host.$url.'js/jquery-3.4.1.min.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/popper.min.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/bootstrap.min.js"></script>
        <script>
        $(function () {
          $(document).scroll(function () {
            var $nav = $("#navbar-transparent");
            var $nav2 = $("#banner-home");
            var $tog = $(".custom-toggler");
            $nav.toggleClass("scrolled", $(this).scrollTop() > $nav2.height());
            $tog.toggleClass("scrolled", $(this).scrollTop() > $nav2.height());
          });
        });
        </script>
        <script src="'.$httpProtocol.$host.$url.'js/toggler.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/main.js"></script>
    ';
    $css = '
        <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/webfonts/all.min.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/styles.css">
    ';
    $hreflang = '<link rel="alternate" hreflang="x-default" href="'.$httpProtocol.$host.$_SERVER["REQUEST_URI"].'">';
?>
<html prefix="og: http://ogp.me/ns#" lang="es">
<head>
    <title><?php echo $title?></title>
    <meta charset="UTF-8">
    <meta name="description" content="<?php echo $description ?>">
    <meta name="keywords" content="<?php echo $keywords ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    
    <link rel="icon" type="image/png" href="<?php echo $httpProtocol.$host.$url.'images/favicon/favicon.ico'?>">
    
    <?php if(isset($canonicalUrl) && $canonicalUrl != ""){ ?>
    	<link rel="canonical" href="<?php echo $canonicalUrl; ?>" />
    <?php }else{ ?>
    	<link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://$_SERVER[HTTP_HOST]".parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>" />
    <?php } ?>
    
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $httpProtocol.$host.$url.'images/favicon/apple-touch-icon.png'?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $httpProtocol.$host.$url.'images/favicon/favicon-32x32.png'?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $httpProtocol.$host.$url.'images/favicon/favicon-16x16.png'?>">
    <link rel="mask-icon" href="<?php echo $httpProtocol.$host.$url.'images/favicon/safari-pinned-tab.svg'?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">

    <!--Metas Geo-->
    <meta name="geo.region" content="MX-QUE" />
    <meta name="geo.placename" content="Quer&eacute;taro" />
    <meta name="geo.position" content="20.589103;-100.365155" />
    <meta name="ICBM" content="20.589103, -100.365155" />
    
    <!--Metas OG-->
    <meta property="og:locale" content="es_MX" />
    <meta property="og:title" content="<?php echo $title?>" />
    <meta property="og:description" content="<?php echo $description ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $httpProtocol.$host.$_SERVER['REQUEST_URI'] ?>" />
    <meta property="og:image" content="<?php echo $httpProtocol.$host.$url.'images/favicon/favicon.ico'?>" />
    <meta property="og:site_name" content="Pacífica" />

    <!--Metas DC-->
    <meta content="<?php echo $title?>" NAME='DC.Title'/>
    <meta content="<?php echo $description ?>" NAME='DC.Description'/>
    <meta content="<?php echo $author ?>" NAME='DC.Creator'/>
    <meta content='Pacífica' NAME='DC.Publisher'/>
    <meta content="<?php echo $httpProtocol.$host.$_SERVER['REQUEST_URI'] ?>" NAME='DC.Identifier'/>
    <meta content="<?php echo $keywords ?>" NAME='DC.keywords'/>