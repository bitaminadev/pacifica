<!DOCTYPE html>
<?php 
    $page = 'home';
    require("../php/controllers/connect_sql.php");
    require("../php/class/destinos.php");
    require("../php/class/navegacion.php");

    $obj_destinations = new Destinos();

    $id_pagina = 1;
    include("../php/class/paginas.php");
    include("../php/class/secciones.php");
    include("../php/class/elementos-galeria.php");
    $m_paginas = new Paginas();
    $m_secciones = new Secciones();
    $m_elementos = new ElementosGaleria();
    $result = $m_paginas->get_pagina($bd, $id_pagina);
    $pagina = mysqli_fetch_assoc($result);
    $pagina = array_map("utf8_encode", $pagina);

    $result = $m_secciones->get_secciones_por_pagina($bd, $id_pagina);
    $secciones = [];
    while($seccion = mysqli_fetch_assoc($result)){
        array_push($secciones, array_map("utf8_encode", $seccion));
    }

    $title = $pagina["meta_titulo_en"];
	$description = $pagina["meta_descripcion_en"];
	$keywords = $pagina["meta_keywords_en"];

    include('commons/_headOpen.php');
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/swiper.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/index.js"></script>
    ';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/swiper.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/index.php');
    include('views/footer.html');
?>
</body>
</html>