<!DOCTYPE html>
<?php 
    $title = 'Pacífica | Wedding contact';
	$description = "Let's plan your dream wedding together at Pacifica Resort. Contact us here.";
	$keywords = 'contact for weddings at pacifica, weddings at pacifica resort, phone weddings pacifica, email pacifica resort';
    $page = 'home';
    include('commons/_headOpen.php');
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");

    $js .= '<script src="'.$httpProtocol.$host.$url.'js/contacto.js"></script>';
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesContact.css">';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/bodas-contacto.html');
    include('views/footer.html');
?>
</body>
</html>