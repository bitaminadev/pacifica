<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/<?php echo utf8_encode($var["img_banner_habitaciones"]); ?>" alt="<?php echo utf8_encode($var["nombre_destino"]); ?>">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo utf8_encode($var["nombre_destino"]); ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">Shopping</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[64]; ?></h2>
                    <p class="mb-4 text-line-bottom text-center">The best shopping experience</p>
                    <div class="my-4 text-normal text-justify"><p><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[65]; ?></p></div>
                </div>
                <div class="w-100 p-0 mt-5">
                    <div class="slide-destinations">
                        <?php 
                            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[66];
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 66);
                            $cont = 1;
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $cont ++;
                                $elemento = array_map("utf8_encode", $elemento);
                                if($elemento["tipo_elemento"] == "imagen"){ ?>
                                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                                    <figure class="v-slide" ><div class="youtubeContainer">
                                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                    </div></figure>
                                <?php
                                } 
                            }
                        ?>
                    </div>
                </div>
                <div class="w-100 p-0 pb-5 section_gray">
                    <h3 class="text-center text-line-bottom  font-weight-normal pt-5 pb-3 rest-title-gray m-0">Business Hours</h3>
                    <p class="text-normal my-4 text-center text-mini">Monday to Friday 9:00 to 21:00</p>
                </div>
            </div>     
        </div>
    </div>
</div>