<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/<?php echo utf8_encode($var["img_banner_habitaciones"]); ?>" alt="<?php echo utf8_encode($var["nombre_destino"]); ?>">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo utf8_encode($var["nombre_destino"]); ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">Spa</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle-l text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[120]; ?></h2>
                    <p class="mb-4 text-line-bottom text-center">Have a relaxing experience in Ixtapa</p>
                    <div class="my-4 text-normal">
                        <p><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[121]; ?></p>
                    </div>
                </div>
                <div class="w-100 p-0 mt-5">
                    <div class="slide-destinations">
                        <?php 
                            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[122];
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 122);
                            $cont = 0;
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $cont ++;
                                $elemento = array_map("utf8_encode", $elemento);
                                if($elemento["tipo_elemento"] == "imagen"){ ?>
                                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                                    <figure class="v-slide" ><div class="youtubeContainer">
                                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                    </div></figure>
                                <?php
                                } 
                            }
                        ?>
                    </div>
                </div>
                <div class="w-100 mb-5">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 card-rel pr-0 m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[125]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_en', 'id_seccion')[125]; ?>" alt="01">
                        </figure>
                        <section class="w-50 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Services</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[123]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[124]; ?></p>
                            </div>
                        </section>
                    </div>
                    <div class="row">
                        <section class="col-md-12 col-lg-6 order-2 order-lg-1 card-white">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Services</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[126]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[127]; ?></p>
                            </div>
                        </section>
                        <figure class="col-md-12 col-lg-6 order-1 order-lg-2 pl-0 card-rel m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[128]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_en', 'id_seccion')[128]; ?>" alt="01">
                        </figure>
                    </div>
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 card-rel pr-0 m-0">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[131]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_en', 'id_seccion')[131]; ?>" alt="01">
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray">
                            <div class="map-down px-4 text-left">
                                <p class="m-0 p-0 text-uppercase text-mini">Services</p>
                                <h4 class="m-0 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[129]; ?></h4>
                                <p class="text-normal my-4 text-justify text-mini"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[130]; ?></p>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="w-100 mb-5">
                    <h3 class="text-center text-line-bottom  font-weight-normal pt-0 pb-3 rest-title-gray m-0">Schedule an appointment right now</h3>
                    <div class="my-4">
                        <div class="col-4 text-mini m-auto text-normal">
                            <p>Telephone:<br><br>
                                55 4160 9065 (MX)<br>
                                755 555 2500<br>
                                ext. 2913  (MX)
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>