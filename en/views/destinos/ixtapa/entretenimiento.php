<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/destinos/hoteles/<?php echo utf8_encode($var["img_banner_habitaciones"]); ?>" alt="<?php echo utf8_encode($var["nombre_destino"]); ?>">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo utf8_encode($var["nombre_destino"]); ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">Entertainment</p>
    </div>        
</div>
<div class="w-100">
    <div class="container">
        <div class="row my-0 mx-0 p-0">
            <div class="col-lg-3 px-0">
                <?php include('../../views/navaside.html'); ?>
            </div>
            <div class="col-lg-9 p-0">
                <div class="w-100 px-5">
                    <h2 class="px-5 py-4 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[144]; ?></h2>
                    <p class="mb-4 text-line-bottom text-center">Night Shows.</p>
                    <div class="my-4 text-normal text-justify"><p><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[145]; ?></p></div>
                </div>
                <div class="w-100 p-0 mt-5">
                    <div class="slide-destinations">
                        <?php 
                            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[146];
                            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 146);
                            $cont = 0;
                            while($elemento = mysqli_fetch_assoc($result_1)){
                                $cont ++;
                                $elemento = array_map("utf8_encode", $elemento);
                                if($elemento["tipo_elemento"] == "imagen"){ ?>
                                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                                    <figure class="v-slide" ><div class="youtubeContainer">
                                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                    </div></figure>
                                <?php
                                } 
                            } 
                        ?>
                    </div>
                </div>
                <div class="w-100 mb-5 mt-2">
                    <div class="row">
                        <figure class="col-md-12 col-lg-6 card-rel m-0 px-0 text-center">
                            <?php if(array_column($secciones, 'tipo_contenido', 'id_seccion')[149] == "imagen" || array_column($secciones, 'tipo_contenido', 'id_seccion')[149] == "iy_imagen"){ ?>
                                <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[149]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_en', 'id_seccion')[149]; ?>" alt="01">
                            <?php }else if(array_column($secciones, 'tipo_contenido', 'id_seccion')[149] == "iy_youtube"){ ?>
                                <div class="youtubeContainer">
                                    <iframe style="width:100%; height:100%;" src="<?php echo array_column($secciones, 'contenido_en', 'id_seccion')[149]; ?>?autoplay=0&showinfo=0&controls=0" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                </div>
                            <?php } ?>
                        </figure>
                        <section class="col-md-12 col-lg-6 card-dest-gray px-0">
                            <div class="map-down px-4 text-left">
                                <h4 class="mt-3 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[147]; ?></h4>
                                <p class="text-normal my-5 text-justify text-mini"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[148]; ?></p>
                            </div>
                        </section>
                    </div>
                    <div class="row">
                        <section class="col-md-12 col-lg-6 order-2 order-lg-1 card-white px-0">
                            <div class="map-down px-4 text-left">
                                <h4 class="mt-3 p-0 subtitle-card-bottom"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[150]; ?></h4>
                                <p class="text-normal my-5 text-justify text-mini"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[151]; ?></p>
                            </div>
                        </section>
                        <figure class="col-md-12 col-lg-6 order-1 order-lg-2 card-rel m-0 px-0 text-center">
                            <?php if(array_column($secciones, 'tipo_contenido', 'id_seccion')[152] == "imagen" || array_column($secciones, 'tipo_contenido', 'id_seccion')[152] == "iy_imagen"){ ?>
                                <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url; ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[152]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_en', 'id_seccion')[152]; ?>" alt="01">
                            <?php }else if(array_column($secciones, 'tipo_contenido', 'id_seccion')[152] == "iy_youtube"){ ?>
                                <div class="youtubeContainer">
                                    <iframe style="width:100%; height:100%;" src="<?php echo array_column($secciones, 'contenido_en', 'id_seccion')[152]; ?>?autoplay=0&showinfo=0&controls=0" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                                </div>
                            <?php } ?>
                        </figure>
                    </div>
                </div>
            </div>     
        </div>
    </div>
</div>