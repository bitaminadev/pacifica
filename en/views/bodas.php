<div class="w-100 banner_fluid">
    <img class="img-fluid h-100 w-100" src="<?php echo $httpProtocol.$host.$url ?>images/<?php $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[5]; echo $nombre_subcarpeta.array_column($secciones, 'contenido_en', 'id_seccion')[5]; ?>" alt="Ixtapa">
    <div class="message-banner" >
        <h1 class="text-uppercase text-white text-center w-100 m-0"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[6]; ?></h1>
        <hr class="line-text-message my-2" />
        <p class="w-100 text-uppercase text-white text-center font-weight-bolder message-slogan">IN PACÍFICA IXTAPA AND PACÍFICA GRAND</p>
    </div>        
</div>
<div class="w-100 mb-5">
    <div class="container">
        <h2 class="px-5 pt-5 pb-3 h-subtitle text-center font-weight-lighter"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[7]; ?></h2>
        <p class="my-4 text-normal text-center"><?php echo array_column($secciones, 'contenido_en', 'id_seccion')[8]; ?></p>
    </div>
</div>
<div class="w-100 p-0 mt-5">
    <div class="slide-destinations">
        <?php 
            $nombre_subcarpeta = array_column($secciones, 'nombre_subcarpeta', 'id_seccion')[9];
            $result_1 = $m_elementos->get_elementos_por_seccion($bd, 9);
            $cont = 0;
            while($elemento = mysqli_fetch_assoc($result_1)){
                $cont ++;
                $elemento = array_map("utf8_encode", $elemento);
                if($elemento["tipo_elemento"] == "imagen"){ ?>
                    <figure class="v-slide" ><img src="<?php echo $httpProtocol.$host.$url; ?>images/<?php echo $nombre_subcarpeta.$elemento["contenido"]; ?>" alt="<?php echo $cont; ?>"></figure>
                <?php }else if($elemento["tipo_elemento"] == "youtube"){ ?>
                    <figure class="v-slide" ><div class="youtubeContainer">
                        <iframe class="youtubeVideo" src="<?php echo $elemento["contenido"] ?>?autoplay=0&showinfo=0&controls=0&enablejsapi=1" frameborder="0" allow="encrypted-media; picture-in-picture"></iframe>
                    </div></figure>
                <?php
                } 
            }
        ?>
    </div>
</div>
<div class="w-100 p-0 my-5">
    <div class="container">
        <h3 class="text-center text-line-bottom font-weight-normal pt-0 pb-3 rest-title-gray m-0">Shall we start to plan your ideal wedding? Contact us!</h3>
        <div class="row my-5">
            <div class="col-md-12 col-lg-6">
                <div class="row py-3">
                    <div class="col-md-12 col-lg-6 px-5 px-lg-3 mb-3 mb-lg-0">
                        <div class="text-mini text-normal ml-4">
                            Telephone:<br>
                            <table class="w-100">
                                <tbody>
                                    <tr><td class="font-weight-bold pr-2">MX</td><td class="py-2">55 4160 9065<br>800 711 1934 ext. 9065<br></td></tr>
                                    <tr><td class="font-weight-bold pr-2">USA</td><td class="py-2">877 387 2370 ext. 9065</td></tr>
                                    <tr><td class="font-weight-bold pr-2">Canada</td><td class="py-2">866 924 8888 ext. 9065</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <p class="text-mini m-auto text-normal px-5">
                            Email:<br>
                            <a class="lnk-pac" href="mailto:tuboda@pacifica.com.mx" target="_blank">tuboda@pacifica.com.mx</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="h-100 w-100 border-left border-gray-bold px-3">
                    <div class="row py-3">
                        <div class="col-md-12 col-lg-5">
                            <img class="img-fluid" src="<?php echo $httpProtocol.$host.$url ?>images/bodas/ixtapa_bodas_destacado.jpg" alt="Weddings in Ixtapa">
                        </div>
                        <div class="col-md-12 col-lg-5">
                            <p class="text-mini m-auto text-normal pb-5">Let us know what you have in mind and we will send you a quote for the event.</p>
                            <a class="btn_black_dest text-uppercase text-left" href="<?php echo $httpProtocol.$host.$url.$ln ?>weddings-contact<?php echo $ext; ?>" tabindex="0">Get a quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>