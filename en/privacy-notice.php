<!DOCTYPE html>
<?php 
    $title = 'Pacífica | Privacy Policy';
	$description = 'We invite you to read our privacy policy.';
	$keywords = 'privacy policy, data privacy, consumer information';
    $page = 'home';
    include('commons/_headOpen.php');
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");
    
    $js .= '';
    $css .= '';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/aviso-de-privacidad.html');
    include('views/footer.html');
?>
</body>
</html>