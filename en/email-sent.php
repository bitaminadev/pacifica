<!DOCTYPE html>
<?php 
    $page = 'home';
    $title = "Pacífica | Correo enviado";
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");

    include('commons/_headOpen.php');
    $js .= '';
    // $css .= '
    // <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesEnvioCorreo.css">';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/correo-enviado.html');
    include('views/footer.html');
?>
</body>
</html>