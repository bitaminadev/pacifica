<nav class="nav">
	<div itemscope itemtype="http://schema.org/Brand" class="navContent">
		<a itemprop="url" href="<?php echo $httpProtocol.$host.$url.$ln ?>" class="navLogoSmall"><img itemprop="logo" src="<?php echo $httpProtocol.$host.$url ?>images/brand/logo_pacifica.svg" class='navLogo' alt='Pacifica Resort'></a>
		<a href="<?php echo $httpProtocol.$host.$url.$ln ?>" class="navLogoBig"><img src="<?php echo $httpProtocol.$host.$url ?>images/menu/logo-pacifica-big.png" class="navLogo" alt="Pacifica Resort"></a>
		<i class="fas fa-bars mobile toggleMenu" onclick="toggleMobileMenu()"></i>
		<ul class="menu" itemscope itemtype="http://schema.org/BreadcrumbList">

			<li class="menuItem" onclick="linkTo('https:\/\/login.pacifica.com.mx/Login?leng=1', true)" data-layer='1'>
				Owner's Access
			</li>

			<li class="menuItem destinations togglerContainer" data-layer='1'>
				<span class="submenuDisplayer" data-togglesection="displayNoSection">
					Destinations  <i class="fas fa-chevron-down"></i>
				</span>
				<div class="submenu">
					<ul class="menu">
						<li class="menuItem" data-layer='2'>
							<span class="submenuDisplayer ixtapa">
								Ixtapa <i class="fas fa-chevron-right"></i>
							</span>
							<div class="submenu">
								<ul class="menu">
									<li class="menuItem" data-layer='3' data-togglesection="displayNoSection" onclick="this.closest('.submenu').previousElementSibling.click();">
										<i class="fas fa-chevron-left"></i>&nbsp;&nbsp;Back to menu
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-hoteles" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/"><span itemprop="name">Resort Hotels</span></a>
			                            <meta itemprop="position" content="1"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-gastronomia" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/gastronomy/"><span itemprop="name">Gastronomy</span></a>
			                            <meta itemprop="position" content="2"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-actividades" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/what-to-do/"><span itemprop="name">Activities</span></a>
			                            <meta itemprop="position" content="3"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-golf" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/golf<?php echo $ext; ?>"><span itemprop="name">Golf</span></a>
			                            <meta itemprop="position" content="4"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-entretenimiento" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/entertainment<?php echo $ext; ?>"><span itemprop="name">Entertainment</span></a>
			                            <meta itemprop="position" content="5"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-compras" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/what-to-do/shopping<?php echo $ext; ?>"><span itemprop="name">Shopping</span></a>
			                            <meta itemprop="position" content="6"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="ixtapa-bodas" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/weddings<?php echo $ext; ?>"><span itemprop="name">Weddings</span></a>
			                            <meta itemprop="position" content="7"/>
									</li>
								</ul>
							</div>
						</li>
						<li class="menuItem" data-layer='2'>
							<span class="submenuDisplayer zihuatanejo">
								Zihuatanejo <i class="fas fa-chevron-right"></i>
							</span>
							<div class="submenu">
								<ul class="menu">
									<li class="menuItem" data-layer='3' data-togglesection="displayNoSection" onclick="this.closest('.submenu').previousElementSibling.click();">
										<i class="fas fa-chevron-left"></i>&nbsp;&nbsp;Back to menu
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="zihuatanejo-hoteles" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/"><span itemprop="name">Resort Hotels</span></a>
			                            <meta itemprop="position" content="8"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="zihuatanejo-gastronomia" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/gastronomy/"><span itemprop="name">Gastronomy</span></a>
			                            <meta itemprop="position" content="9"/>
									</li>
									<li class="menuItem" data-layer='3' data-togglesection="zihuatanejo-actividades" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/what-to-do/"><span itemprop="name">Activities</span></a>
			                            <meta itemprop="position" content="10"/>
									</li>
								</ul>
							</div>
						</li>
					</ul>
					<div class="navSectionContent notInMobile">
						<div class="menuBackSection notHidden" style="height: 100%;width: auto;">
							<span class="mapLocation ixtapa" data-toggle="tooltip" title="Ixtapa" onclick="document.querySelector('.nav .submenuDisplayer.ixtapa').click();"></span>
							<span class="mapLocation zihuatanejo" data-toggle="tooltip" title="Zihuatanejo" onclick="document.querySelector('.nav .submenuDisplayer.zihuatanejo').click();"></span>
							<img src="<?php echo $httpProtocol.$host.$url ?>images/menu/img_map_destinations.png" style="height: 100%; width: auto; float: right;" alt="Destinations Map">
						</div>
						<div class="menuBackSection hidden ixtapa-hoteles">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-gastronomia">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/gastronomy/" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-actividades">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/what-to-do/" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-golf">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/golf<?php echo $ext; ?>" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-entretenimiento">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/entertainment<?php echo $ext; ?>" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-compras">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/what-to-do/shopping<?php echo $ext; ?>" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden ixtapa-bodas">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">IXTAPA</h3>
									<p>In Ixtapa, Pacifica Resort offers all the comfort you need to enjoy your vacations surrounded by the beauty of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/weddings<?php echo $ext; ?>" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden zihuatanejo-hoteles">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Enjoy a relaxing time at the shore of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden zihuatanejo-gastronomia">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Enjoy a relaxing time at the shore of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/gastronomy/" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<div class="menuBackSection hidden zihuatanejo-actividades">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Enjoy a relaxing time at the shore of the Pacific Ocean.</p>
									<a href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/what-to-do/" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
						<!-- <div class="menuBackSection hidden zihuatanejo-golf">
							<div class="menuBackSectionContainer">
								<div class="sectionContent">
									<h3 class="opening__title">ZIHUATANEJO</h3>
									<p>Enjoy a relaxing time at the shore of the Pacific Ocean.</p>
									<a href="<?php //echo $httpProtocol.$host.$url ?>destination/zihuatanejo/golf<?php //echo $ext; ?>" title="Title" class="v-button"> More Information <i class="fas fa-chevron-right"></i></a>
								</div>
							</div>
						</div> -->
					</div>
					<span class="closeSubmenu mobile" onclick='this.parentElement.previousElementSibling.click()'>
						X
					</span>
				</div>
			</li>

			<li class="menuItem hotels togglerContainer" data-layer='1'>
				<span class="submenuDisplayer" data-togglesection="hoteles">
					Resort Hotels  <i class="fas fa-chevron-down"></i>
				</span>
				<div class="submenu">
					<ul class="menu">

						<li class="menuItem" data-layer='2' data-togglesection="hoteles-ixtapa">
							<a class="w-100 h-100 sbumenuDisplayer" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/ixtapa/">
								Ixtapa <i class="fas fa-chevron-right"></i>
							</a>
						</li>

						<li class="menuItem" data-layer='2' data-togglesection="hoteles-zihuatanejo">
							<a class="w-100 h-100 sbumenuDisplayer" href="<?php echo $httpProtocol.$host.$url.$ln ?>destination/zihuatanejo/">
								Zihuatanejo <i class="fas fa-chevron-right"></i>
							</a>
						</li>

					</ul>
					<div class="navSectionContent notInMobile">
						<div class="menuBackSection hoteles">
							<div class="sectionContent">
								<h3>Resort Hotels</h3>
								<p>Find the ideal experience in our resort hotels, from  a family-friendly activity packed vacation to relaxing times in a sophisticated and quiet environment by the seashore.</p>
							</div>
							<div class="sectionHotels">
								<div class="menuBackSection pacifica-resort"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destination/ixtapa/">&nbsp;</a></div>
								<div class="menuBackSection pacifica-grand"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destination/zihuatanejo/">&nbsp;</a></div>
							</div>
						</div>
						<div class="menuBackSection hidden hoteles-ixtapa"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destination/ixtapa/">&nbsp;</a>
						</div>
						<div class="menuBackSection hidden hoteles-zihuatanejo"><a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln; ?>destination/zihuatanejo/">&nbsp;</a>
						</div>
					</div>
					<span class="closeSubmenu mobile" onclick='this.parentElement.previousElementSibling.click()'>
						X
					</span>
				</div>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>weddings<?php echo $ext; ?>"><span itemprop="name">Weddings</span></a>
				<meta itemprop="position" content="11"/>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>gastronomy<?php echo $ext; ?>"><span itemprop="name">Gastronomy</span></a>
				<meta itemprop="position" content="12"/>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>entertainment<?php echo $ext; ?>"><span itemprop="name">Entertainment</span></a>
				<meta itemprop="position" content="13"/>
			</li>

			<li class="menuItem" data-layer='1' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a itemprop="item" class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>be-an-owner<?php echo $ext; ?>"><span itemprop="name">Become an owner</span></a>
				<meta itemprop="position" content="14"/>
			</li>

			<li class="menuItem about" data-layer='1'>
				<span class="submenuDisplayer">
					About us  <i class="fas fa-chevron-down"></i>
				</span>
				<div class="submenu">
					<ul class="menu">
						<li class="menuItem" data-layer='2' data-background='<?php echo $httpProtocol.$host.$url?>images/menu/menu_back_reconocimientos.jpg' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemprop="item" href="<?php echo $httpProtocol.$host.$url.$ln ?>awards<?php echo $ext; ?>"><span itemprop="name">Awards</span></a>
							<meta itemprop="position" content="15"/>
						</li>
						<li class="menuItem" data-layer='2' data-background='<?php echo $httpProtocol.$host.$url?>images/menu/menu_back_compromiso.jpg' itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemprop="item" href="<?php echo $httpProtocol.$host.$url.$ln ?>social-responsability<?php echo $ext; ?>"><span itemprop="name">Social responsibility</span></a>
							<meta itemprop="position" content="16"/>
						</li>
					</ul>
					<span class="closeSubmenu mobile" onclick='this.parentElement.previousElementSibling.click()'>
						X
					</span>
				</div>
			</li>

			<li class="menuItem onlyInMobile" data-layer='1'>
				<a class="w-100 h-100" href="<?php echo $httpProtocol.$host.$url.$ln ?>contact-us<?php echo $ext; ?>"><span>Contact us</span></a>
			</li>
		</ul>
		<div class="contacto">
			<a class="text-white telhead" href="tel:1-877-387-2370" >1-877-387-2370</a>
			<span class="d-block small notInMobile">
				<a class="text-white" href="<?php echo $httpProtocol.$host.$url.$ln ?>contact-us<?php echo $ext; ?>" >Contact us</a>
			</span>
		</div>
		<div class="notInMobile idioma">
			<div class="dropdown show">
				<a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><img class="smallFlag" src="<?php echo $httpProtocol.$host.$url?>images/menu/en_flag_usa.svg" alt="English language"></a>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuLink" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-127px, 20px, 0px);">
					<a class="dropdown-item" href="<?php echo $httpProtocol.$host.$url; ?>es/"><img class="smallFlag" src="<?php echo $httpProtocol.$host.$url?>images/menu/en_flag_mexico.svg" alt="Idioma Español"></a>
				</div>
			</div>
		</div>
	</div>
</nav>