<!DOCTYPE html>
<?php 
    $page = 'entretenimiento';
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/navegacion.php");
    $_nb9 = "active_nav_sect"; 
    $id_destino = 2;
    $url_destino = "zihuatanejo";

    $myConsulta = new Destinos();
    $consulta = $myConsulta->lista_destinos($bd,$id_destino);
    $var = mysqli_fetch_array($consulta);

    $id_pagina = 22;
    include("../../../php/class/paginas.php");
    include("../../../php/class/secciones.php");
    include("../../../php/class/elementos-galeria.php");
    $m_paginas = new Paginas();
    $m_secciones = new Secciones();
    $m_elementos = new ElementosGaleria();
    $result = $m_paginas->get_pagina($bd, $id_pagina);
    $pagina = mysqli_fetch_assoc($result);
    $pagina = array_map("utf8_encode", $pagina);
    $result = $m_secciones->get_secciones_por_pagina($bd, $id_pagina);
    $secciones = [];
    while($seccion = mysqli_fetch_assoc($result)){
        array_push($secciones, array_map("utf8_encode", $seccion));
    }
    $title = $pagina["meta_titulo_en"];
	$description = $pagina["meta_descripcion_en"];
	$keywords = $pagina["meta_keywords_en"];

    include('../../commons/_headOpen.php');
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/slick.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/destinos.js"></script>';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/slick.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    // include('../../views/destinos/'.$url_destino.'/entretenimiento.html');
    include('../../views/destinos/'.$url_destino.'/entretenimiento.php');
    include('../../views/footer.html');
?>
</body>
</html>
