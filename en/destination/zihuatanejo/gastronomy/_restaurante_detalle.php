<!DOCTYPE html>
<?php 
    // $title = 'Pacífica | Ixtapa Gastronomía';
	// $description = '';
	// $keywords = '';
    require("../../../../php/controllers/connect_sql.php");
    require("../../../../php/class/destinos.php");
    require("../../../../php/class/restaurantes.php");
    require("../../../../php/class/restaurantes_platillos.php");
    require("../../../../php/class/restaurantes_galeria.php");
    require("../../../../php/class/navegacion.php");
    $_nb4 = "active_nav_sect"; 
    $_ds2 = "show";
    $id_destino = 2;
    $url_destino = "zihuatanejo";

    //Datos generales del destino
    $destinos = new Destinos();
    $destino = $destinos->lista_destinos($bd, $id_destino);
    $destino = mysqli_fetch_assoc($destino);

    $restaurantes = new Restaurantes();
    $platillos = new RestaurantesPlatillos();
    $galeria = new RestaurantesGaleria();

    $consulta = $restaurantes->detalle_restaurante($bd,$id_restaurante);
    $var = mysqli_fetch_array($consulta);
    // $title = 'Pacífica | '.utf8_encode($var["nombre_restaurante"]);
    // $description = ''.utf8_encode($var["meta_descripcion"]);
    $title = utf8_encode($var["meta_titulo_en"]);
    $description = utf8_encode($var["meta_descripcion_en"]);
	$keywords = utf8_encode($var["meta_keywords_en"]);
    
    //Galería
    $con_galeria = $galeria ->galeria_restaurante($bd,$id_restaurante);

    //Platillo destacado
    $con_platillo = $platillos ->platillo_destacado_restaurante($bd,$id_restaurante);
    

    $page = 'home';
    include('../../../commons/_headOpen.php');

    if($var["status_restaurante"] == 0){
        header("Location: ".$httpProtocol.$host.$url.$ln);
        die();
    }

    $js .= '<script src="'.$httpProtocol.$host.$url.'js/slick.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/destinos.js"></script>';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/slick.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../../commons/_headClose.php');
    include('../../../commons/_nav.php');
    include('../../../views/gastronomia/restaurante.html');
    include('../../../views/footer.html');
?>
</body>
</html>