<!DOCTYPE html>
<?php 
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/destinos_galeria.php");
    require("../../../php/class/habitaciones_categoria.php");
    require("../../../php/class/navegacion.php");
    $_nb1 = "active_nav_sect"; 

    $myConsulta = new Destinos();
    $consulta_gallery = new DestinosGaleria();
    $consulta_categ = new HabitacionesCategoria();

    $consulta = $myConsulta->lista_destinos($bd,2);
    $var = mysqli_fetch_array($consulta);
    
    $title = utf8_encode($var["meta_titulo_en"]);
	$description = utf8_encode($var["meta_descripcion_en"]);
	$keywords = utf8_encode($var["meta_keywords_en"]);
    
    //Galería destinos
    $id_destino = 2;
    $url_destino = "zihuatanejo";
    $destinos_gal = $consulta_gallery->lista_galeria($bd,$id_destino);

    $destinos_categ = $consulta_categ->lista_categorias_destino($bd,$id_destino);

    $page = 'home';
    include('../../commons/_headOpen.php');
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/slick.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/destinos.js"></script>';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/slick.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/destinos/destino.html');
    include('../../views/footer.html');
?>
</body>
</html>
