<!DOCTYPE html>
<?php 
    $title = 'Routes and maps to Pacífica Grand Zihuatanejo';
	$description = 'Find the way to Pacifica Grand Zihuatanejo. Check out car routes from different cities of Mexico and estimated time of arrival.';
	$keywords = 'routes paficica, routes zihuatanejo, maps zihuatanejo mexico';
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/navegacion.php");
    $_nb3 = "active_nav_sect"; 
    $id_destino = 2;
    $url_destino = "zihuatanejo";

    $myConsulta = new Destinos();
    $consulta = $myConsulta->lista_destinos($bd,$id_destino);
    $var = mysqli_fetch_array($consulta);
    
    $a = 17.621641;
    $b = -101.547965;

    $page = 'home';
    include('../../commons/_headOpen.php');
    $js .= '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBq1TmYzh04WOLyFlZQdMWAkMQXYXNvA60"></script>
    <script src="'.$httpProtocol.$host.$url.'js/como-llegar.js"></script>
    <script>var a='.$a.',b='.$b.';</script>
    ';
    $css .= '
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/como-llegar.html');
    include('../../views/footer.html');
?>
</body>
</html>
