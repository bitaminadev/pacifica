<!DOCTYPE html>
<?php 
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/habitaciones.php");
    require("../../../php/class/habitaciones_galeria.php");
    require("../../../php/class/navegacion.php");
    $_nb2 = "active_nav_sect";
    $_ds1 = "show";
    $id_destino = 1;
    $url_destino = "ixtapa";

    //Datos generales del destino
    $destinos = new Destinos();
    $destino = $destinos->lista_destinos($bd, $id_destino);
    $destino = mysqli_fetch_assoc($destino);
    
    //Habitación información general
    $consulta_hab = new Habitaciones();
    $habitaciones = $consulta_hab->lista_habitaciones($bd,$id_habitacion,"");
    $var = mysqli_fetch_array($habitaciones);
    $id_categoria = $var["id_categoria"];
    $id_tipo = $var["id_tipo_habitacion"];

    $title = utf8_encode($var["meta_titulo_en"]);
	$description = utf8_encode($var["meta_descripcion_en"]);
	$keywords = utf8_encode($var["meta_keywords_en"]);

    //Galería
    $consulta_gal = new HabitacionesGaleria();
    $habitaciones_gal = $consulta_gal->lista_galeria($bd,$id_habitacion);

    $page = 'home';
    include('../../commons/_headOpen.php');

    if($var["status_habitacion"] == 0){
        header("Location: ".$httpProtocol.$host.$url.$ln);
        die();
    }

    $js .= '<script src="'.$httpProtocol.$host.$url.'js/slick.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/destinos.js"></script>';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/slick.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/destinos/detalle-producto.html');
    include('../../views/footer.html');
?>
</body>
</html>