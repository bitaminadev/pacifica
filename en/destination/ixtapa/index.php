<!DOCTYPE html>
<?php 
    require("../../../php/controllers/connect_sql.php");
    require("../../../php/class/destinos.php");
    require("../../../php/class/destinos_galeria.php");
    require("../../../php/class/habitaciones_categoria.php");
    require("../../../php/class/navegacion.php");
    $_nb1 = "active_nav_sect"; 

    $myConsulta = new Destinos();
    $consulta_gallery = new DestinosGaleria();
    $consulta_categ = new HabitacionesCategoria();

    $id_destino = 1;
    $url_destino = "ixtapa";
    $consulta = $myConsulta->lista_destinos($bd,$id_destino);
    $var = mysqli_fetch_array($consulta);
    $destinos_gal = $consulta_gallery->lista_galeria($bd,$id_destino);
    
    $destinos_categ = $consulta_categ->lista_categorias_destino($bd,$id_destino);

    $title = utf8_encode($var["meta_titulo_en"]);
	$description = utf8_encode($var["meta_descripcion_en"]);
	$keywords = utf8_encode($var["meta_keywords_en"]);

    $page = 'home';
    include('../../commons/_headOpen.php');
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/slick.min.js"></script>
    <script src="'.$httpProtocol.$host.$url.'js/destinos.js"></script>';
    $css .= '<link href="'.$httpProtocol.$host.$url.'css/slick.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesDestinations.css">';
    include('../../commons/_headClose.php');
    include('../../commons/_nav.php');
    include('../../views/destinos/destino.html');
    include('../../views/footer.html');
?>
</body>
</html>