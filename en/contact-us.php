<!DOCTYPE html>
<?php 
    $title = 'Address, Phone Numbers and Reservations in Pacífica Resort Ixtapa';
	$description = 'Contact us, make on-line reservations, call us and learn about Pacifica Resorts location in Ixtapa.';
	$keywords = 'pacifica resort phone numbers, pacifica resort address, paicfica resort location, hotels in ixtapa, on line reservations in ixtapa, pacifica resort, where is pacifica resort located';
    $page = 'home';
    include('commons/_headOpen.php');
    require("../php/controllers/connect_sql.php");
    require("../php/class/navegacion.php");
    
    $js .= '<script src="'.$httpProtocol.$host.$url.'js/contacto.js"></script>';
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesContact.css">';
    include('commons/_headClose.php');
    include('commons/_nav.php');
    include('views/contacto.html');
    include('views/footer.html');
?>
</body>
</html>